﻿
using System;
using System.Collections.Generic;
using System.Drawing;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.IO;
using System.Linq;
using System.Xml.Linq;
using System.Text;
using System.Text.RegularExpressions;
using Imperialware.Resources;

using Microsoft.VisualBasic.FileIO;
using Microsoft.Win32;
using System.Threading;
using System.Diagnostics; 

// Used to find out whether Imperialware has Admin rights
using System.Security;
using System.Security.Principal;



namespace Imperialware
{
    public partial class MainWindow
    {

        //=====================//

        public string Get_Desktop()
        { return Environment.GetFolderPath(Environment.SpecialFolder.Desktop); }
         
        //=====================//

        void Rename_File(string Path_and_File, string New_Path_and_File)
        {
            if (Debug_Mode == "true")
            {
                System.IO.File.Move(Path_and_File, New_Path_and_File);
            }

            else
            { 
                try { System.IO.File.Move(Path_and_File, New_Path_and_File); } catch {}
            }
        }


        //=====================//

        public void Renaming(string Path_and_File, string New_File_Name)
        {  
            if (Debug_Mode == "true")
            {
                FileAttributes The_Attribute = File.GetAttributes(Path_and_File);

                if ((The_Attribute & FileAttributes.Directory) == FileAttributes.Directory)
                { Directory.Move(Path_and_File, Path.GetDirectoryName(Path_and_File) + @"\" + New_File_Name); }
                else { File.Move(Path_and_File, Path.GetDirectoryName(Path_and_File) + @"\" + New_File_Name); }
            }

            else
            {   try
                {   // get the file attributes for file or directory
                    FileAttributes The_Attribute = File.GetAttributes(Path_and_File);

                    //detect whether its a directory or file
                    if ((The_Attribute & FileAttributes.Directory) == FileAttributes.Directory)
                    {
                        try
                        {   // Getting the Path only for the right parameter and appending the new name to it
                            Directory.Move(Path_and_File, Path.GetDirectoryName(Path_and_File) + @"\" + New_File_Name);
                        } catch {}
                    }
                    else
                    {  
                        try { File.Move(Path_and_File, Path.GetDirectoryName(Path_and_File) + @"\" + New_File_Name); } catch {}
                    }
                }  catch {}
            }      
        }
        //=====================//

        public void Moving(string Path_and_File, string New_Path)
        {   // This little function just saves me annoying syntax typing so I can just write destination path

            if (Debug_Mode == "false")
            { 
                try
                {   // get the file attributes for file or directory
                    FileAttributes The_Attribute = File.GetAttributes(Path_and_File);

                    if (!Directory.Exists(New_Path)) { Directory.CreateDirectory(New_Path); }

                    //detect whether its a directory or file
                    if ((The_Attribute & FileAttributes.Directory) == FileAttributes.Directory)
                    { 
                        try { System.IO.Directory.Move(Path_and_File, New_Path + @"\" + Path.GetFileName(Path_and_File)); }  catch { }
                    }
                    else
                    {
                        try { System.IO.File.Move(Path_and_File, New_Path + @"\" + Path.GetFileName(Path_and_File)); } catch { }
                    }
                } catch { }
            }

            else if (Debug_Mode == "true")
            {
                // get the file attributes for file or directory
                FileAttributes The_Attribute = File.GetAttributes(Path_and_File);

                if (!Directory.Exists(New_Path)) { Directory.CreateDirectory(New_Path); }

                //detect whether its a directory or file
                if ((The_Attribute & FileAttributes.Directory) == FileAttributes.Directory)
                { System.IO.Directory.Move(Path_and_File, New_Path + @"\" + Path.GetFileName(Path_and_File)); } 
               
                else { System.IO.File.Move(Path_and_File, New_Path + @"\" + Path.GetFileName(Path_and_File)); } 
            }                  
        }

        //========= Thanks to jaysponsored form Stackoverfl0w ==========//
        // I almost can't believe Microsoft doesent provide a proper Copy function for Directories !! 
        public void Copy_Now(string Source_Directory, string Destination_Directory) 
        {
            if (Debug_Mode == "false")
            {
                // substring is to remove Destination_Directory absolute path (E:\).
                try
                {   // Create subdirectory structure in destination    
                    foreach (string dir in Directory.GetDirectories(Source_Directory, "*", System.IO.SearchOption.AllDirectories))
                    {
                        Directory.CreateDirectory(Destination_Directory + dir.Substring(Source_Directory.Length));
                        // Example:
                        //     > C:\sources (and not C:\E:\sources)
                    }
                } catch { }

                try 
                {   foreach (string file_name in Directory.GetFiles(Source_Directory, "*.*", System.IO.SearchOption.AllDirectories))
                    {
                        File.Copy(file_name, Destination_Directory + file_name.Substring(Source_Directory.Length), true);
                    }
                } catch { }
            }

            else if (Debug_Mode == "true")
            {
                // Create subdirectory structure in destination    
                foreach (string dir in Directory.GetDirectories(Source_Directory, "*", System.IO.SearchOption.AllDirectories))
                {
                    Directory.CreateDirectory(Destination_Directory + dir.Substring(Source_Directory.Length));
                    // Example:
                    //     > C:\sources (and not C:\E:\sources)
                }

                foreach (string file_name in Directory.GetFiles(Source_Directory, "*.*", System.IO.SearchOption.AllDirectories))
                {
                    File.Copy(file_name, Destination_Directory + file_name.Substring(Source_Directory.Length), true);
                }
            }

        }
        //=====================//
        // If not exist add from source path
        public void Verify_Copy(string Source_Path_and_File, string New_Path_and_File)
        {
            if (!Directory.Exists(Path.GetDirectoryName(New_Path_and_File)))
            { Directory.CreateDirectory(Path.GetDirectoryName(New_Path_and_File)); }

            if (Debug_Mode == "false")
            {   try 
                {   if (!File.Exists(New_Path_and_File))
                    { File.Copy(Source_Path_and_File, New_Path_and_File, true); }
                } catch { }
            }           
            else if (Debug_Mode == "true")
            { 
                if (!File.Exists(New_Path_and_File))
                { File.Copy(Source_Path_and_File, New_Path_and_File, true); }
            }
        }


        
        // If exists in source path copy and OVERWRITE that to target
        public void Overwrite_Copy(string Source_Path_and_File, string New_Path_and_File)
        {   
            if (Debug_Mode == "false")
            {   try
                {   if (File.Exists(Source_Path_and_File))
                    { File.Copy(Source_Path_and_File, New_Path_and_File, true); }
                } catch { }
            }
            else if (Debug_Mode == "true")
            {
                if (File.Exists(Source_Path_and_File))
                { File.Copy(Source_Path_and_File, New_Path_and_File, true); }
            }
        }
        //=====================//
        public void Deleting(string Data)
        {   int Error_Count = 0;
           
            if (!Directory.Exists(Data)) { Error_Count = Error_Count + 10; } 
            else 
            {   try { Microsoft.VisualBasic.FileIO.FileSystem.DeleteDirectory(Data, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin); }
                catch { Error_Count++; }
            }

            if (!File.Exists(Data)) { Error_Count = Error_Count + 10; }
            else
            {   try { Microsoft.VisualBasic.FileIO.FileSystem.DeleteFile(Data, Microsoft.VisualBasic.FileIO.UIOption.OnlyErrorDialogs, RecycleOption.SendToRecycleBin); }
                catch { Error_Count++; }
            }

            // If both methods failed that probably means no Visual Basic is installed.
            if (Error_Count != 0 & Error_Count < 3) { Imperial_Console(600, 100, "Error: You seem to be missing Net Framework 4.0 and Visual Basic.FileIO on your System."); }
            else if (Debug_Mode == "true" & Error_Count > 19) { Imperial_Console(600, 100, "   Could not find the selected Object for deletion."); }

            Error_Count = 0;
        }

           
        //=====================//
        public string Wash_String(string The_String)
        { return Regex.Replace(The_String, "[\n\r\t ]", ""); }


        //========== DON'T use emptyspace signs for this function, except in "quote marks"! ===========//
        public string Load_Setting(string Text_File, string Variable)
        {
            string Result = "";

            // This enables the user to assign files to certain values
            if (Text_File == "0" | Text_File == "1" | Text_File == "2") { Text_File = Setting; }
            else if (Text_File == "4") { Text_File = Maximum_Values; }


            /* --> Wont work because if the iEnumerable Boot_Settings is innitiated as global variable it causes a open process to freeze the application!
            IEnumerable<string> The_File;
            // Performance related: When boot loading we use a preset file, otherwise we load it new each time the function is called.
            if (User_Input == false) { The_File = Boot_Settings; }
            else { The_File = File.ReadLines(Text_File);} 
            */


            try
            {   foreach (string Line in File.ReadLines(Text_File))
                {
                    // If contains our Variable and if the line doesent start with //, # or :: (Comments)
                    if (Line.Contains(Variable) & !Regex.IsMatch(Line, "^//.*?") & !Regex.IsMatch(Line, "^#.*?") & !Regex.IsMatch(Line, "^::.*?"))
                    // if (Regex.IsMatch(Line, ".*?" + Variable + ".*?") 
                    {   // Then we split the line into a string array, at positon of the = sign.
                        string[] Value = Line.Split('=');
                        // Value[0] is the Variable itself, Value[1] is its Value! We try it because sometimes the value is empty, to prevent it from loading a non existing table
                        try
                        {   // If the Variable is THE DESIRED ONE and not just with similar name
                            if (Regex.Replace(Value[0], " ", "").Equals(Variable)) 
                            {   // If the value has "" it is protected from this interpreter and the _ emptyspaces wont be removed
                                if (Value[1].Contains(@"""")) { 
                                    Result = Regex.Replace(Value[1], @"""", "");
                                    Result = Result.Substring(1, Result.Length - 1);
                                }
                                else {Result = Regex.Replace(Value[1], " ", "");}
                            }
                        } catch { }

                        // Adding the removed emptyspace back for program directory or it would break many pathsv
                        if (Result.Contains("ProgramFiles(x86)")) { Result = Regex.Replace(Result, "ProgramFiles(x86)", "Program Files (x86)"); }
                        else if (Result.Contains("ProgramFiles")) { Result = Regex.Replace(Result, "ProgramFiles", "Program Files"); }
                    }
                }
            }
            
            catch 
            {   if (Debug_Mode == "true")
                {
                    if (User_Input & !Variable.Contains("Max")) { Imperial_Console(600, 100, "Sorry, I was not able to load the value of the variable " + Variable); }
                   
                    // Collecting all variables that failed to load in that list to display it whether after the startup function or (if Debug Mode) right now.                  
                    else { Error_List.Add(Variable); }                 
                }             
            }

            // Getting Rid of the Emptyspace, exchange this with "Result" to deactivate
            return Result;
 
        }


        //=====================//

        public void Load_All_Settings(string Text_File, string[] Load_Settings)
        {
          foreach (string Entry in Load_Settings)
            {
                
                try 
                {   // Get the variable from the string array "Load_Settings" using FieldInfo.
                    System.Reflection.FieldInfo Window_Info = typeof(Imperialware.MainWindow).GetField(Entry);

                    // Set static field (variable) to this value.
                    Window_Info.SetValue(null, Load_Setting(Text_File, Entry));
                }
                
              
                catch {Error_List.Add(Entry); Temporal_A = "true";}               
            }

            if (Debug_Mode == "true" & Temporal_A == "true")
            {   Temporal_A = "false";
                string Error_Text = "";
                int Console_Height = 200;
                if (Error_List.Count > 5) { Console_Height = 600; } 
              
                foreach (string Error in Error_List) { Error_Text += "    " + Error + Add_Line; }
                Imperial_Console(600, Console_Height, Add_Line + "    Error: Could not load " + Add_Line + Error_Text);
            }
        }
        //=====================//

        public void Save_Setting(string Text_File, string Variable, string New_Value)
        {
            bool Quote = false;


            // "2" and "4" is used for int type values, then we won't need to try this
            if (Text_File != "2" & Text_File != "4" & Text_File != Maximum_Values
                & Text_File != Maximum_Values_Fighter & Text_File != Maximum_Values_Bomber & Text_File != Maximum_Values_Corvette & Text_File != Maximum_Values_Frigate & Text_File != Maximum_Values_Capital
                & Text_File != Maximum_Values_Infantry & Text_File != Maximum_Values_Vehicle & Text_File != Maximum_Values_Hero & Text_File != Maximum_Values_Structure)

            {   // This sets our variable in the memory to the new value
                try
                {   // Get our variable 
                    System.Reflection.FieldInfo Window_Info = typeof(Imperialware.MainWindow).GetField(Variable);

                    // Set variable to new value.
                    Window_Info.SetValue(Variable, New_Value);
                }
                catch
                {
                    try
                    {
                        System.Reflection.FieldInfo Window_Info = typeof(Imperialware.MainWindow).GetField(Variable);
                        Window_Info.SetValue(null, New_Value.ToString());
                    }
                    catch
                    {
                        if (Debug_Mode == "true")
                        {
                                Imperial_Console(700, 150, Add_Line + "    Error: Could not convert the new Value for " + @"""" + Variable + @"""" 
                                                     + Add_Line + "    to String in order to store it.");
                        }
                    }
                }
            }

            // This enables the user to assign files to certain values
            // 0 = In Quotemarks, 1 = Normal with variable setup, 2 = No variable setup ( use for int values)          
            if (Text_File == "0") { Text_File = Setting; Quote = true; }
            else if (Text_File == "1" | Text_File == "2") { Text_File = Setting; }
            else if (Text_File == "4") { Text_File = Maximum_Values; }
           

            // Reading File      
            string The_Text = File.ReadAllText(Text_File);

            // If the New Variable is not the old one
            if (Load_Setting(Text_File, Variable) != New_Value)
            {        
                string Old_Entry = "";

                foreach (string Line in File.ReadLines(Text_File))
                {   // If contains our Variable and if the line doesent start with //, # or :: (Comments)
                    if (Line.Contains(Variable) & !Regex.IsMatch(Line, "^//.*?") & !Regex.IsMatch(Line, "^#.*?") & !Regex.IsMatch(Line, "^::.*?"))
                    {
                        // First I split Variable = Result into a array
                        string[] Value = Line.Split('=');
                        // Then we use Regex to get rid of the _Emptyspaces and make sure we got the right Variable, if so we got our Old_Entry selected.
                        try { if (Regex.Replace(Value[0], " ", "").Equals(Variable)) { Old_Entry = Line; } }
                        catch { }
                    }
                }


                // If the parameter 0 was choosen that means the new value needs to be a quoted path!
                if (Quote)
                {   // Loading the specified Variable and Replacing the Old value it returns with a new one (if duplicates exist it replaces the last one)
                    try { The_Text = The_Text.Replace(Old_Entry, Variable + " = " + @"""" + New_Value + @""""); } catch { }             
                }
                else
                {   // Otherwise we store without quote marks
                    try { The_Text = The_Text.Replace(Old_Entry, Variable + " = " + New_Value); } catch { }
                }


                // Saving Changes
                File.WriteAllText(Text_File, The_Text);
            }
        }
 
        //=====================//
        // Returns only selected items
        public List<string> Select_List_View_Items(ListView List_View)
        {
            List<string> Selection_List  = new List<string>();
            if (List_View.Items.Count < 1) { return null; }

            for (int i = List_View.Items.Count - 1; i >= 0; --i)
            {   // Was "if (List_View.Items[i].Selected & List_View_Hard_Points.Items[i].Text != "")"
                if (List_View.Items[i].Selected & List_View.Items[i].Text != "")
                { Selection_List.Add(List_View.Items[i].Text); }
            } return Selection_List;     
        }

        //=====================//
        // This returns all, even the unselected ones
        public List<string> Get_All_List_View_Items(ListView List_View)
        {
            List<string> Selection_List = new List<string>();
            if (List_View.Items.Count < 1) { return null; }

            for (int i = List_View.Items.Count - 1; i >= 0; --i)
            {   if (List_View.Items[i].Text != "") { Selection_List.Add(List_View.Items[i].Text); }
            } return Selection_List;
        }

        //=====================//
        public string Select_List_View_First(ListView List_View, bool Fallback_to_Slot_0)
        {   // If none is selected we grab anything in Slot 0
            if (List_View.SelectedItems.Count < 1 & Fallback_to_Slot_0 && List_View.Items.Count > 0) 
            {   try
                {   if (List_View.Items[0].Text != "")
                    {   List_View.Items[0].Selected = true;
                        List_View.Select();
                    }
                } catch {}        
            }

            if (List_View.SelectedItems.Count > 0)
            { if (List_View.SelectedItems[0].Text != "") { return List_View.SelectedItems[0].Text; } }

            return null;
        }

        //=====================//
        public void Remove_List_View_Selection(ListView List_View, string Deletion_Text, bool Only_Selected) // Deletion_Text is Optional, can also be null instead
        {
            if (List_View.Items.Count < 1) { return; }

            if (Only_Selected)
            {
                if (Deletion_Text != null)
                {
                    for (int i = List_View.Items.Count - 1; i >= 0; --i)
                    { if (List_View.Items[i].Selected & List_View.Items[i].Text == Deletion_Text) { List_View.Items[i].Remove(); } }
                }
                else
                {
                    for (int i = List_View.Items.Count - 1; i >= 0; --i)
                    { if (List_View.Items[i].Selected) { List_View.Items[i].Remove(); } }
                }  
            }
            else if (!Only_Selected)
            {   if (Deletion_Text != null)
                {   for (int i = List_View.Items.Count - 1; i >= 0; --i)
                    { if (List_View.Items[i].Text == Deletion_Text) { List_View.Items[i].Remove(); } }
                }
                else
                {   // !Only_Selected and without Deletion_Text it will just wipe the whole ListView
                    List_View.Items.Clear();
                } 
            }
               
        }


        //=====================//
        public string List_View_To_String(ListView List_View) 
        {
            int Cycle_Count = 0;
            string The_Text = "";

            foreach (ListViewItem Item in List_View.Items)
            {
                Cycle_Count++;

                if (Cycle_Count == List_View.Items.Count)
                {   // This only triggers for the last item, which is not supposed to append the , sign
                    The_Text += Item.Text;
                }
                // Adding all Items to a single string
                else { The_Text += Item.Text +", "; }
            }
            return The_Text;
        }


        //=====================//
        public bool Table_Matches(string[] Table, string Item)
        {
            bool Is_Match = false;
            foreach (string Entry in Table)
            {
                if (Entry == Item) { Is_Match = true; }
            }

            return Is_Match;
        }

        
        public bool List_Matches(List<string> The_List, string Item)
        {  
            bool Is_Match = false;
            foreach (string Entry in The_List)
            {
                if (Entry == Item) { Is_Match = true; }
            }

            return Is_Match;
        }

        public bool List_View_Matches(ListView The_List, string Item)
        {
            bool Is_Match = false;
            foreach (ListViewItem Entry in The_List.Items)
            {
                if (Entry.Text == Item) { Is_Match = true; }
            }

            return Is_Match;
        }

        //=====================//

        public void Remove_From_List(List<string> The_List, string Entry)
        {
            foreach (string Item in The_List)
            {
                if (Item == Entry) { The_List.Remove(Item); return; }
            }
        }

        // Remove_From_List_View(Text_Box_Planet_Name, Entry);
        public void Remove_From_List_View(ListView The_List, string Entry)
        {       
            foreach (ListViewItem Item in The_List.Items)
            {
                if (Item.Text == Entry) { Item.Remove(); return;}
            }
        }

       //=====================//

       // Window Size expects 2 int parameters like 700, 200
       public void Imperial_Console(int Window_Size_X, int Window_Size_Y, string Text)
       {   // Innitiating new Form
           Caution_Window Display = new Caution_Window();
           Display.Opacity = Transparency;
           Display.Size = new Size(Window_Size_X, Window_Size_Y);

           // Using Theme colors for Text and Background
           Display.Text_Box_Caution_Window.BackColor = Color_05;
           Display.Text_Box_Caution_Window.ForeColor = Color_03;

           Display.Text_Box_Caution_Window.Text = Text; 

           Display.Show();

       }
       //=====================//

       public string Array_To_String(string[] Source_List)
       {
           Temporal_A = "";

           foreach (string Entry in Source_List)
           { if (Entry != null & Entry != "") { Temporal_A += Entry + ", "; } }

           // Getting rid of , at beginning and end
           if (Temporal_A.StartsWith(", ")) { Temporal_A = Temporal_A.Substring(2, Temporal_A.Length - 2); }
           if (Temporal_A.EndsWith(", ")) { Temporal_A = Temporal_A.Remove(Temporal_A.Length - 2); }

           return Temporal_A;
       }

       //=====================//
       public string List_To_String(List<string> Source_List)
       {
           Temporal_A = "";
    
           foreach (string Entry in Source_List)
           { if (Entry != null & Entry != "") { Temporal_A += Entry + ", "; } }

           // Getting rid of , at beginning and end
           if (Temporal_A.StartsWith(", ")) { Temporal_A = Temporal_A.Substring(2, Temporal_A.Length - 2); }
           if (Temporal_A.EndsWith(", ")) { Temporal_A = Temporal_A.Remove(Temporal_A.Length - 2); }

           return Temporal_A;
       }
       //=====================//

       public void Imperial_Dialogue(int Window_Size_X, int Window_Size_Y, string Button_A_Text, string Button_B_Text, string Button_C_Text, string Text) 
       {
         //========== Displaying Error Messages to User   
            // Innitiating new Form
            Caution_Window Display = new Caution_Window();
            Display.Opacity = Transparency;
            Display.Size = new Size(Window_Size_X, Window_Size_Y);

            // Using Theme colors for Text and Background
            Display.Text_Box_Caution_Window.BackColor = Color_05;
            Display.Text_Box_Caution_Window.ForeColor = Color_03;


            if (Button_A_Text != "false" & Button_C_Text == "false")
            {   Display.Button_Caution_Box_1.Visible = true;
                Display.Button_Caution_Box_1.Text = Button_A_Text;
                Display.Button_Caution_Box_1.Location = new Point(120, 86);
            }


            if (Button_B_Text != "false" & Button_C_Text == "false")
            {   Display.Button_Caution_Box_2.Visible = true;
                Display.Button_Caution_Box_2.Text = Button_B_Text;
                Display.Button_Caution_Box_2.Location = new Point(280, 86);
            }

            else if (Button_C_Text != "false")
            {
                // The first 2 buttons moves aside to free space for this one
                Display.Button_Caution_Box_1.Visible = true;
                Display.Button_Caution_Box_1.Text = Button_A_Text;
                Display.Button_Caution_Box_1.Location = new Point(60, 86);
             
                Display.Button_Caution_Box_2.Visible = true;
                Display.Button_Caution_Box_2.Text = Button_B_Text;
                Display.Button_Caution_Box_2.Location = new Point(380, 86);

                Display.Button_Caution_Box_3.Visible = true;
                Display.Button_Caution_Box_3.Text = Button_C_Text;
                Display.Button_Caution_Box_3.Location = new Point(220, 86);
            }

            Display.Text_Box_Caution_Window.Text = Text;


            Display.ShowDialog(this);

       }
 
       //=====================//

       public void Set_Maximal_Value_Directories()
       {
           Temporal_B = Mod_Name;

           // This line just makes sure we use the same info directory for different version of the Stargate TPC Mod
           if (Temporal_B == "StargateAdmin" | Temporal_B == "StargateBeta" | Temporal_B == "StargateOpenBeta") { Temporal_B = "Stargate"; }

           // Distinguishing between EAW and FOC version of Stargate
           if (Game_Path == Game_Path_EAW & Temporal_B == "Stargate") { Temporal_B = "Stargate_Empire_at_War"; }
         

          
           Maximum_Values_Fighter = Program_Directory + Mods_Directory + Temporal_B + @"\Maximal_Values\Fighter.txt";
           Maximum_Values_Bomber = Program_Directory + Mods_Directory + Temporal_B + @"\Maximal_Values\Bomber.txt";
           Maximum_Values_Corvette = Program_Directory + Mods_Directory + Temporal_B + @"\Maximal_Values\Corvette.txt";
           Maximum_Values_Frigate = Program_Directory + Mods_Directory + Temporal_B + @"\Maximal_Values\Frigate.txt";
           Maximum_Values_Capital = Program_Directory + Mods_Directory + Temporal_B + @"\Maximal_Values\Capital.txt";

           Maximum_Values_Infantry = Program_Directory + Mods_Directory + Temporal_B + @"\Maximal_Values\Infantry.txt";
           Maximum_Values_Vehicle = Program_Directory + Mods_Directory + Temporal_B + @"\Maximal_Values\Vehicle.txt";
           Maximum_Values_Air = Program_Directory + Mods_Directory + Temporal_B + @"\Maximal_Values\Air.txt";
           Maximum_Values_Hero = Program_Directory + Mods_Directory + Temporal_B + @"\Maximal_Values\Hero.txt";
           Maximum_Values_Structure = Program_Directory + Mods_Directory + Temporal_B + @"\Maximal_Values\Structure.txt";
       }


       public void Load_Maximal_Values(string Text_File)
       {
           // Loading Int type Variables from settings file 
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Model_Scale"), out Maximum_Model_Scale);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Model_Height"), out Maximum_Model_Height);

           Int32.TryParse(Load_Setting(Text_File, "Maximum_Select_Box_Scale"), out Maximum_Select_Box_Scale);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Health_Bar_Height"), out Maximum_Health_Bar_Height);

           // Power Values
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Credits"), out Maximum_Credits);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Hull"), out Maximum_Hull);

           Int32.TryParse(Load_Setting(Text_File, "Maximum_Shield"), out Maximum_Shield);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Shield_Rate"), out Maximum_Shield_Rate);

           Int32.TryParse(Load_Setting(Text_File, "Maximum_Energy"), out Maximum_Energy);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Energy_Rate"), out Maximum_Energy_Rate);

           Int32.TryParse(Load_Setting(Text_File, "Maximum_Speed"), out Maximum_Speed);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_AI_Combat"), out Maximum_AI_Combat);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Projectile"), out Maximum_Projectile);
    

           // Requirement Values
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Build_Cost"), out Maximum_Build_Cost);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Skirmish_Cost"), out Maximum_Skirmish_Cost);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Build_Time"), out Maximum_Build_Time);

           Int32.TryParse(Load_Setting(Text_File, "Maximum_Tech_Level"), out Maximum_Tech_Level);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Star_Base_LV"), out Maximum_Star_Base_LV);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Ground_Base_LV"), out Maximum_Ground_Base_LV);

           Int32.TryParse(Load_Setting(Text_File, "Maximum_Timeline"), out Maximum_Timeline);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Slice_Cost"), out Maximum_Slice_Cost);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Build_Limit"), out Maximum_Build_Limit);
           Int32.TryParse(Load_Setting(Text_File, "Maximum_Lifetime_Limit"), out Maximum_Lifetime_Limit);


           Int32.TryParse(Load_Setting(Text_File, "Costum_Tag_4_Max_Value"), out Costum_Tag_4_Max_Value);
           Int32.TryParse(Load_Setting(Text_File, "Costum_Tag_5_Max_Value"), out Costum_Tag_5_Max_Value);
           Int32.TryParse(Load_Setting(Text_File, "Costum_Tag_6_Max_Value"), out Costum_Tag_6_Max_Value);


           // Setting Maximal Values to their Textboxes
           Text_Box_Max_Model_Scale.Text = Maximum_Model_Scale.ToString();
           Text_Box_Max_Model_Height.Text = Maximum_Model_Height.ToString();
           Text_Box_Max_Select_Box_Scale.Text = Maximum_Select_Box_Scale.ToString();
           Text_Box_Max_Health_Bar_Height.Text = Maximum_Health_Bar_Height.ToString();
          
           Text_Box_Max_Credits.Text = Maximum_Credits.ToString();
           Text_Box_Max_Hull.Text = Maximum_Hull.ToString();
           Text_Box_Max_Shield.Text = Maximum_Shield.ToString();
           Text_Box_Max_Shield_Rate.Text = Maximum_Shield_Rate.ToString();

           Text_Box_Max_Energy.Text = Maximum_Energy.ToString();
           Text_Box_Max_Energy_Rate.Text = Maximum_Energy_Rate.ToString();
           Text_Box_Max_Speed.Text = Maximum_Speed.ToString();
           Text_Box_Max_AI_Combat.Text = Maximum_AI_Combat.ToString();
           Text_Box_Max_Projectile.Text = Maximum_Projectile.ToString();   

           Text_Box_Max_Build_Cost.Text = Maximum_Build_Cost.ToString();
           Text_Box_Max_Skirmish_Cost.Text = Maximum_Skirmish_Cost.ToString();
           Text_Box_Max_Build_Time.Text = Maximum_Build_Time.ToString();
           Text_Box_Max_Tech_Level.Text = Maximum_Tech_Level.ToString();
           Text_Box_Max_Star_Base_LV.Text = Maximum_Star_Base_LV.ToString();
           Text_Box_Max_Ground_Base_LV.Text = Maximum_Ground_Base_LV.ToString();
           Text_Box_Max_Timeline.Text = Maximum_Timeline.ToString();
           Text_Box_Max_Slice_Cost.Text = Maximum_Slice_Cost.ToString();
           Text_Box_Max_Build_Limit.Text = Maximum_Build_Limit.ToString();
           Text_Box_Max_Lifetime_Limit.Text = Maximum_Lifetime_Limit.ToString();

           Text_Box_Max_Costum_4.Text = Costum_Tag_4_Max_Value.ToString();
           Text_Box_Max_Costum_5.Text = Costum_Tag_5_Max_Value.ToString();
           Text_Box_Max_Costum_6.Text = Costum_Tag_6_Max_Value.ToString();


           // Report function for all missloaded Variables
           if (Loading_Completed & Debug_Mode == "true" & Error_List.Count > 0)
           {
               Temporal_A = "";
               Temporal_C = 200;
               if (Error_List.Count > 5) { Temporal_C = 600; }

               foreach (string Error in Error_List) { Temporal_A += "    " + Error + Add_Line; }
               Imperial_Console(600, Temporal_C, Add_Line + "    Error: Could not load " + Add_Line + Temporal_A);

               Error_List.Clear();
           }

       }


       // Usage: Set_Maximal_Values((TextBox)sender, "Maximum_Hull", Maximum_Hull);
       public void Set_Maximal_Values(TextBox Text_Box, string Variable_Name, int Variable_Value)
       {   // Making sure this doesent bug at startup to permanently retry and loop open the error Message !!
           if (User_Input == true)
           {
               // Need to make sure the Mod dir exists, otherwise stackoverflow!
               if (Mod_Name != "" & Mod_Name != "Inactive" & Mod_Name != "false" & Directory.Exists(Mod_Path + Mod_Name))
               {   // We forward the Name of our Element as String the function, at the same time we use that function to check whether a valid entry was typed.                  
                   if (Edit_Max_Value_Text(Text_Box.Name.ToString()) == true) 
                   { 
                   // Converting the text string to int
                   Int32.TryParse(Text_Box.Text, out Variable_Value);

                   if (Maximal_Value_Class == "1") { Save_Setting(Maximum_Values_Fighter, Variable_Name, Variable_Value.ToString()); }
                   else if (Maximal_Value_Class == "2") { Save_Setting(Maximum_Values_Bomber, Variable_Name, Variable_Value.ToString()); }
                   else if (Maximal_Value_Class == "3") { Save_Setting(Maximum_Values_Corvette, Variable_Name, Variable_Value.ToString()); }
                   else if (Maximal_Value_Class == "4") { Save_Setting(Maximum_Values_Frigate, Variable_Name, Variable_Value.ToString()); }
                   else if (Maximal_Value_Class == "5") { Save_Setting(Maximum_Values_Capital, Variable_Name, Variable_Value.ToString()); }

                   else if (Maximal_Value_Class == "6") { Save_Setting(Maximum_Values_Infantry, Variable_Name, Variable_Value.ToString()); }
                   else if (Maximal_Value_Class == "7") { Save_Setting(Maximum_Values_Vehicle, Variable_Name, Variable_Value.ToString()); }
                   else if (Maximal_Value_Class == "8") { Save_Setting(Maximum_Values_Hero, Variable_Name, Variable_Value.ToString()); }
                   else if (Maximal_Value_Class == "9") { Save_Setting(Maximum_Values_Structure, Variable_Name, Variable_Value.ToString()); }
                   }
               }
               // Otherwise we restore the original Value in the Text Box -- This else line could cause a Stack Overflow Exception!
               else { Text_Box.Text = Variable_Value.ToString(); }
           }
       }

       //=====================//
       public void Set_Language()
       {   // Setting combobox field to the language
            Combo_Box_Language.Text = Game_Language;
       
            // Setting flag image, the images are from: http://www.iconarchive.com/show/flag-icons-by-gosquared.8.html
            Image Language_Flag = new Bitmap(Program_Directory + @"Images\Languages\Flag_" + Game_Language + ".jpg");
            Picture_Box_Languages.BackgroundImage = Language_Flag;
            

            if (Game_Language == "English")
            {
                
            }
            else if (Game_Language == "German")
            {
                
            }
        }

       //============ Set Checkbox ===========//
       // This wont work if exectuted from a Checkbox because then it causes a Stack Overflow.
       public void Toggle_Checkbox(CheckBox Check_Box, string Text_File, string Variable_Name)
       {   // User_Input prevents the loading function from unintentionally toggeling the value
           if (User_Input == true)
           {
               // If the variable is already true, we are going to toggle it to false
               if (Check_Box.Checked)
               {   // Switching Name Checkbox Color in order to highlight the selected Checkbox, and unchecking it
                   // Check_Box.ForeColor = Color.FromArgb(200, 40, 130, 240);
                   Check_Box.ForeColor = Color_02;
                   Check_Box.Checked = !Check_Box.Checked;

                   Save_Setting(Text_File, Variable_Name, "false");

               } // Otherwise it toggles to be true

               else
               {
                   Check_Box.ForeColor = Color_03;
                   Check_Box.Checked = !Check_Box.Checked;

                   Save_Setting(Text_File, Variable_Name, "true");
               }
           }
       }


        //========= Auto Set Checkbox (without causeing Stackoverfl0w) ========//
        public void Auto_Toggle_Checkbox(CheckBox Check_Box, string Text_File, string Variable_Name)
        {   // User_Input prevents the loading function from unintentionally toggeling the value
            if (User_Input == true)
            {
                // If the variable is already true, we are going to toggle it to false
                if (!Check_Box.Checked)
                {   // Switching Name Checkbox Color in order to highlight the selected Checkbox, and unchecking it
                    Check_Box.ForeColor = Color_02;
                    Save_Setting(Text_File, Variable_Name, "false");

                } // Otherwise it toggles to be true

                else
                {   Check_Box.ForeColor = Color_03;     
                    Save_Setting(Text_File, Variable_Name, "true");
                }
            }
        }

        // You can also use "(CheckBox)sender" as general variable to call this function:
        // Auto_Toggle_Checkbox((CheckBox)sender, Setting, "Evade_Language");


       //============ Set Radiobutton ===========// temp
       public void Toggle_Radio_Button(RadioButton Radio_Button, string Text_File, string Variable_Name, string Variable_Value)
       {   // User_Input prevents the loading function from unintentionally toggeling the value
           if (User_Input == true)
           {

               // If the variable is already true, we are going to toggle it to false
               if (Variable_Value == "true")
               {   // Switching Name Checkbox Color in order to highlight the selected Checkbox, and unchecking it
                   Radio_Button.ForeColor = Color_02;
                   Radio_Button.Select();

                   Variable_Value = "false";
                   Save_Setting(Text_File, Variable_Name, "false");

               } // Otherwise it toggles to be true
               else if (Variable_Value == "false")
               {
                   Radio_Button.ForeColor = Color_03;
                   Radio_Button.Select();

                   Variable_Value = "true";
                   Save_Setting(Text_File, Variable_Name, Variable_Value);
               }
           }
       }



        //============ Moving to Texture Directory (Push) ===========//

        public void Switch_Loading_Screens()
        {

            // These are needed to cycle the ingame screen images
            Image_Cycle = Load_Setting(Setting, "Image_Cycle");
            Moved_Images = Load_Setting(Setting, "Moved_Images");


            // If a Cycle Images directory was found in .\Art\Textures of this mod
            if (Directory.Exists(Art_Directory + @"Textures\Cycle_Images") & Moved_Images != "false" & Moved_Images != "")
            {
                // Extracting all stored image data from string format
                string[] All_Moved_Images = Moved_Images.Split(',');

                try
                {
                    foreach (string Image in All_Moved_Images)
                    {   // Moving the Images from the last usage back into their original directory we stored
                        File.Move(Art_Directory + @"Textures\" + Image, Art_Directory + @"Textures\Cycle_Images\" + Image_Cycle + @"\" + Image);
                    }
                } catch { }
            }


            //=========== Drawing from Cycle Directory (Pull) =======//
       

            // Innitiating int list and adding just 0 to get rid of the 0 slot, so we can later start at 1
            List<string> Found_Numbers = new List<string>();
            Found_Numbers.Add("0");


            // This for loop iterates up to 20 until all directories were found
            for (int i = 1; i <= 20; ++i)
            {

                // We need to concider the 0 from the first 9 digits, we use this state to test the value.
                if (i < 10)
                {
                    // If any numbered directory was found in there, we add it into our list variable
                    if (Directory.Exists(Art_Directory + @"Textures\Cycle_Images\0" + i)) { Found_Numbers.Add("0" + i); }
                }
                else
                {
                    if (Directory.Exists(Art_Directory + @"Textures\Cycle_Images\" + i)) { Found_Numbers.Add(i.ToString()); }
                }
            }


            // Generating random Variable
            Random Randomizer = new Random();
            // Generating random Number from this range
            int Random = Randomizer.Next(1, Found_Numbers.Count());

            if (Image_Cycle != "false" & Image_Cycle != "" & Random.ToString() == Image_Cycle)
            // If the same value as before was chosen we repeat again and hope to get a new one
            { Random = Randomizer.Next(1, Found_Numbers.Count()); }


     
            //Fetching all files inside of that folder
            string[] Images = System.IO.Directory.GetFiles(Art_Directory + @"Textures\Cycle_Images\" + Found_Numbers[Random]);
            

            // Resetting Variable for next usage
            Moved_Images = "";

            try
            {
                foreach (string Image in Images)
                {   // We save all images as longer string seperated by the , sign
                    Moved_Images += Path.GetFileName(Image) + ",";
                    System.IO.File.Move(Image, Art_Directory + @"Textures\" + Path.GetFileName(Image)); 
                }
            } catch { }


            // Storing the number of cycle directory so we can move all files back during next usage!
            Save_Setting(Setting, "Image_Cycle", Found_Numbers[Random]);
            Save_Setting(Setting, "Moved_Images", Moved_Images);

            Found_Numbers.Clear();

        }                    
        //=====================//

        public string Get_Game_Path(string Game)
        {
            RegistryKey The_Key;

            if (Game == "EAW")
            {
                try
                {
                    The_Key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\LucasArts\Star Wars Empire at War\1.0");
                    if (The_Key == null) { The_Key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\LucasArts\Star Wars Empire at War\1.0"); }

                    Temporal_A = The_Key.GetValue("ExePath").ToString();
                    string The_Value = Temporal_A.Remove(Temporal_A.Length - 9);

                    return The_Value; 

                } catch { Imperial_Console(700, 100, Add_Line + "    Error: could not extract Star Wars - EAW game path from Registry."
                                                  + Add_Line + @"    You have to search .\LucasArts\Star Wars Empire at War\GameData\ "
                                                  + Add_Line + @"    and to insert it manually in Settings\Game Path: EAW.");
                }
            }
           
            else if (Game == "FOC")
            {
                try
                {
                    The_Key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\LucasArts\Star Wars Empire at War Forces of Corruption\1.0");
                    if (The_Key == null) { The_Key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Wow6432Node\LucasArts\Star Wars Empire at War Forces of Corruption\1.0"); }


                    Temporal_B = The_Key.GetValue("InstallPath").ToString() + @"\"; // Need to append corruption for Steam version!
                    if (Regex.IsMatch(Temporal_B, "(?i).*?" + "Steam.*?")) { Temporal_B += @"corruption\"; } 

                    return Temporal_B; 
                }
                catch
                {
                    Imperial_Console(750, 100, Add_Line + "    Error: could not extract Star Wars - FOC game path from Registry."
                                            + Add_Line + @"    You have to search .\LucasArts\Star Wars Empire at War Forces of Corruption\ "
                                            + Add_Line + @"    and to insert it manually in Settings\Game Path: FOC.");
                }
            }

            return null;
        }

        //=====================//
        public Color Load_Color(string Setting_File, string Variable)
        {
            List<int> Brush = new List<int>();
            string[] Value = Load_Setting(Setting_File, Variable).Split(',');


            foreach (string Entry in Value)
            {   int Number;
                Int32.TryParse(Entry, out Number);
                Brush.Add(Number);
            }

            // Brush 3 in front because I switched position of the A value 
            Color The_Color = Color.FromArgb(Brush[3], Brush[0], Brush[1], Brush[2]);

            return The_Color;
        }
        //=====================//    
        // To use RGB Values just write Set_Background_Color(Color.Transparent, 0, 0, 0, 190);
        // To save a color do Save_Setting("2", "Color_01", "200, 200, 200, 200");

        // Red = 240, 50, 20
        // Light Blue = 90, 180, 230
        // Dark Blue = 40, 130, 240
        // Green = 10, 245, 155  and 2, 253, 24
        // Nice Orange/Gold = 7, 0, 50
        // int[] Background_Color = { 40, 130, 240, 220 };



        public void Set_Color(Color Chosen_Color, int Color_Index)
        {   
            // if (Background_Color == Color.Transparent) { Background_Color = Color.FromArgb(Brightness, Red, Blue, Green); }


            // Now depending on which color the User chose we set certain controls connected to them
            if (Color_Index == 01)
            { 
                // Defining arrays of dynamic variables for the white/black/grey Lable objects
                // Color_01: Maincolor
                dynamic[] The_Controls = 
                {
                    Label_Mod_Info,
                    Label_Faction, Label_Planet_1, Label_Planet_2, 
                    Label_Credits, Label_Unit_Name, Label_Unit_Type,
                    Label_Is_Variant, Check_Box_Is_Variant,
                       
                    // Art Settings
                    Label_Model_Name, Label_Icon_Name, Label_Radar_Icon_Size, Label_Radar_Icon, 
                    Label_Text_Id, Label_Encyclopedia_Text, Label_Unit_Class, 
                   
                    Label_Scale_Factor, Label_Model_Height, Label_Select_Box_Scale,
                    Label_Select_Box_Height, Label_Health_Bar_Height, Check_Box_Health_Bar_Size,

                    // Land Settings
                    Label_Movement_Animation, Label_Walk_Animation, Label_Crouch_Animation,
                    Label_Rotation_Animation, Label_Deploy_Animation, Label_Damage_Stages,
                    Label_Collison_Box, Label_Remove_Upon_Death, Label_Capture_Point, Label_Hover,
                    Label_Wind_Disturbances, Label_Hover_Height, Label_Enemy_Fire_Height,
                    Label_Sensor_Range,
                
                    // Power Values                  
                    Label_Class, Label_Hull, Label_Shield,
                    Label_Energy, Label_Speed, Label_Population,
                    Label_AI_Comabat, Label_Projectile, Label_Current_Balancing,

                    // Unit Abilities
                    Label_Ability_Type, Label_Auto_Fire, Label_Activated_GUI_Ability, 
                    Label_Expiration_Seconds, Label_Recharge_Seconds, 
                    Label_Alternate_Name, Label_Alternate_Description, Label_Alternate_Icon,
                    Label_SFX_Ability_Activated, Label_SFX_Ability_Deactivated, Lable_Lua_Script,
                    Label_Mod_Multiplier, Label_Additional_Abilities, 

                    // Unit Properties
                    Label_Is_Team, Label_Use_Container, Label_Team_Name, Label_Team_Type, 
                    Label_Team_Is_Variant, Label_Container_Name, Label_Shuttle_Type, 
                    Label_Team_Members, Check_Box_Team_Is_Variant,

                    Label_Is_Hero, Label_Show_Head, Label_God_Mode, Label_Use_Particle,
                    Label_Starting_Spawned_Units, Label_Reserve_Spawned_Units, Label_Death_Clone, Label_Death_Clone_Model,
                    Label_Inactive_Behavoir, Label_Active_Behavoir, 
                    Check_Box_Has_Hyperspace,
 
                    // Build Requirements
                    Label_Inactive_Affiliations, Label_Active_Affiliations, Check_Box_Add_To_Base,  
                    Label_Enable_All, Label_Build_Tab_Space, Label_Build_Cost, Label_Skirmish_Cost, 
                    Label_Build_Time, Label_Tech_Level, Label_Star_Base_LV, Label_Ground_Base_LV, 
                    Label_Innitially_Locked, Label_Required_Timeline, 
                   


                    // Game Tab
                    Lable_Language, Label_Is_Steam_Version, Label_EAW_Path,              
                    Label_FOC_Path, Label_Savegame_Path, Label_Vanilla_Command_Bar,
                    Label_Dashboard_Mod, Label_Dashboard_Source_Faction,
                    Label_Dashboard_Target_Faction, Label_Mod_Declaration,

                    // Conquest Tab
                    Label_Planet_Name, Label_Is_Planet_Variant, Check_Box_Planet_Is_Variant,   
                    Check_Box_Vanilla_Planets, Label_Planet_Text_ID, Label_Planet_Encyclopedia, 
                    Label_Planet_Model, Label_Destroyed_Planet_Model, Label_Planet_Model_Scale,     
                    Label_Surface_Accessible, Label_Planet_Is_Pre_Lit, Label_Planet_Animation, 
                    Label_Galactic_Position, Label_Facing_Adjust,      
                    Label_Space_Tactical_Map, Label_Land_Tactical_Map, Label_Destroyed_Tactical_Map,       
                    Label_Max_Space_Base, Label_Space_Structures, Label_Land_Structures, 
                    Label_Credit_Income, Label_Capture_Bonus, Label_Additonal_Population, 
                    Label_Planet_Terrain, 

                    Label_Inactive_Factions, Label_Active_Factions,
                    Label_Faction_Name, Label_Faction_Is_Variant, Label_Faction_Text_Id, 
                    Label_Basic_AI, Label_Faction_Color, Label_Faction_Icon_Name, 
                    Label_Star_Base_Icon, Label_Faction_Leader, Label_Faction_Leader_Company, 
                    Label_Tactical_Unit_Cap, Label_Is_Playable, Label_Is_In_Multiplayer,

                    // Settings Tab
                    Lable_Transperency, Label_Version, Label_Special_Button_Color, Label_Imperialware_Themes,
                    Label_Folder_Path, 
                    
                    Label_Maximum_Model_Scale, Label_Maximum_Model_Height, 
                    Label_Maximum_Select_Box_Scale, Label_Maximum_Health_Bar_Height, 
                    Label_Maximum_Credits, Label_Maximum_Hull, 
                    Label_Maximum_Shield, Label_Maximum_Shield_Rate, Label_Maximum_Energy, 
                    Label_Maximum_Energy_Rate, Label_Maximum_Speed, Label_Maximum_AI_Combat, Label_Maximum_Projectile,

                    Label_Maximum_Build_Cost, Label_Maximum_Skirmish_Cost, Label_Maximum_Slice_Cost,
                    Label_Maximum_Build_Time, Label_Maximum_Tech_Level, Label_Maximum_Timeline,
                    Label_Maximum_Star_Base_LV, Label_Maximum_Ground_Base_LV, 
                    Label_Maximum_Build_Limit, Label_Maximum_Lifetime_Limit, 

                    Label_Maximum_Costum_4, Label_Maximum_Costum_5, Label_Maximum_Costum_6,
                };

                // Until the iteration cycle reached the maximum number of Items in this list
                for (int cycle = 0; cycle < The_Controls.Length; cycle++)
                {
                    // We take the array item with the current cycle numer, and change its fore color to the Layout color!
                    The_Controls[cycle].ForeColor = Chosen_Color;
                }
            }

            /*
            else if (Color_Index == 02)
            {   // Color_03: Selected              
                dynamic[] The_Controls = { };
            }
            */

            else if (Color_Index == 03)
            {   // Color_03: Selected              
                dynamic[] The_Controls = 
                {                
                    // Launcher Tab
                    Label_Addon_Name, Label_Mod_Name, Label_Game_Name,

                    // Manage Tab
                    Label_Cheating, Label_Start_Planet, Label_Target_Planet, Label_Units,
                    Label_Planet_1_Name, Label_Planet_2_Name, Text_Box_Search_Bar, 
                    Label_Galaxy_File, Label_Credit_Sign, Label_Xml_Name, 
                    Label_Model_Alo, Label_Icon_Tga, Label_Radar_Icon_Tga, Label_Dat_Editor,
                    Label_String_Editor, Label_Art_Settings, Label_Land_Settings, Label_Power_Values, Label_Balancing_Info, Label_Damage_Per_Minute_Info,
                    Label_Abilities, Label_Expiration_Sec, Label_Recharge_Sec, Label_Ability_Icon, Check_Box_Use_In_Team,
                    Label_Unit_Properties, Label_Move_as_Team, Label_Start_Units_Amount, Label_Reserve_Units_Amount, Label_Costum_Tags,
                    Label_Build_Requirements, Check_Box_Add_To_Skirmish, Check_Box_Add_To_Campaign, Label_Slice_Credits,
                    Label_Build_Credits, Label_Skirmish_Credits, Label_Seconds,
               
                    // Conquest Tab
                    Label_Conquest_File, Label_Planet_File, Label_Planet_Model_Alo, Label_Destroyed_Planet_Alo,
                    Label_Space_Map_Ted, Label_Land_Map_Ted, Label_Destroyed_Map_Ted, Check_Box_Planet_Game_Objects,
                                       
                    Label_Faction_File, Label_Faction_Icon_Tga, Label_Star_Base_Icon_Tga,

                    // Settings Tab
                    Label_Mod_Name_2, Label_Galactic_Alo, Label_Tactical_Alo, Label_Log_File_Location,
                    Label_EAW_Savegame, Label_FOC_Savegame, Label_Imperialware_Version, 
                    Label_Xml_Editor_Settings, Label_UI_Settings, Label_Maximal_Values,
                };

                // Until the iteration cycle reached the maximum number of Items in this list
                for (int cycle = 0; cycle < The_Controls.Length; cycle++)
                {
                    // We take the array item with the current cycle numer, and change its fore color to the Layout color!
                    The_Controls[cycle].ForeColor = Chosen_Color;
                }
            }


            else if (Color_Index == 04)
            {  // Color_04: Buttons
                dynamic[] The_Controls = 
                {
                    Button_Debug_Mode,

                    // Mods Tab
                    Button_Open_Map_Editor, Button_Set_Mod, Button_Start_Mod, 

                    // Apps Tab
                    Text_Box_Object_Searchbar, Button_Download_App, Button_Add_App, Button_Remove_App, 
                    Button_Select_Mods, Button_Select_Addons, Button_Start_App, 
                    Button_Load_Addon, Button_Unload_Addon, 

                    // Manage Tab              
                    Button_Expand_A, Button_Expand_B, Button_Expand_C,
                    Button_Expand_D, Button_Expand_E, Button_Expand_F, 
                    Button_Expand_G, Button_Expand_H, Button_Expand_I,
                    Button_Expand_J, Button_Expand_K, 
                    Button_Expand_N, Button_Expand_O,

                    Button_Collaps_A, Button_Collaps_B, Button_Collaps_C,
                    Button_Collaps_D, Button_Collaps_F, Button_Collaps_G,
                    Button_Collaps_I, Button_Collaps_J,
                    Button_Collaps_N, Button_Collaps_O,

                    Combo_Box_Faction, Button_Destroy_Planet, Button_Select_Planet,                 
                    Button_Teleport, Button_Spawn, Button_Unit_Selector, Combo_Box_Filter_Type,
                  

                    Button_Edit, Button_Edit_Xml, Button_Give_Credits,
                    Text_Box_Name, Combo_Box_Type, Text_Box_Name,
                    Text_Box_Is_Variant, Text_Box_Credits, 

                    Button_Affiliation_to_Active, Button_Affiliation_Exchange, Button_Affiliation_to_Inactive, 
                    List_View_Inactive_Affiliations, List_View_Active_Affiliations, List_View_Requirements,  
                    Text_Box_Build_Cost, Text_Box_Skirmish_Cost, Text_Box_Build_Time,
                    Text_Box_Tech_Level, Text_Box_Star_Base_LV, Text_Box_Ground_Base_LV,
                    Text_Box_Required_Timeline, Text_Box_Slice_Cost, Text_Box_Current_Limit,
                    Text_Box_Lifetime_Limit, 
                    
                  
                    // Xml Editing Values
                    Button_Open_Xml, Button_Search_Model,
                    Button_Default_Radar_Icon, Text_Box_Model_Name, Text_Box_Icon_Name,
                    Text_Box_Radar_Icon, Text_Box_Radar_Size, Text_Box_Text_Id,
                    Text_Box_Unit_Class, Text_Box_Encyclopedia_Text,                     
                    Text_Box_Scale_Factor, Text_Box_Model_Height, 
                    Text_Box_Health_Bar_Height, Text_Box_Select_Box_Scale, Text_Box_Select_Box_Height,
                    
                    Combo_Box_Health_Bar_Size, Combo_Box_Class, 
                    Text_Box_Hull, Text_Box_Shield, Text_Box_Shield_Rate, 
                    Text_Box_Energy, Text_Box_Energy_Rate,                    
                    Text_Box_Speed, Text_Box_Rate_Of_Turn, Text_Box_Bank_Turn_Angle,
                    Text_Box_AI_Combat, Text_Box_Population,                    
                    Text_Box_Projectile, 

                    // Land Values
                    Text_Box_Movement_Animation, Text_Box_Walk_Animation, Text_Box_Crouch_Animation, 
                    Text_Box_Rotation_Animation, Text_Box_Deploy_Animation, Text_Box_Damage_Stages, 
                    Text_Box_Collison_Box_X, Text_Box_Collison_Box_Y, Text_Box_Wind_Disturbances, Text_Box_Wind_Radius, 
                    Text_Box_Hover_Height, Text_Box_Fire_Height, Text_Box_Sensor_Range,

                    // Unit Abilities
                    Combo_Box_Ability_Type, Combo_Box_Activated_GUI_Ability, Combo_Box_Mod_Multiplier, Combo_Box_Additional_Abilities,
                    Text_Box_Expiration_Seconds, Text_Box_Recharge_Seconds, 
                    Text_Box_Alternate_Name, Text_Box_Alternate_Description, Text_Box_Ability_Icon, 
                    Text_Box_SFX_Ability_Activated, Text_Box_SFX_Ability_Deactivated, Text_Box_Lua_Script,
                    Text_Box_Mod_Multiplier, Text_Box_Additional_Abilities, 
                    
                    // Unit Properties
                    Text_Box_Name, Text_Box_Team_Is_Variant, Text_Box_Container_Name, Text_Box_Shuttle_Type,
                    Text_Box_Team_Amount, Text_Box_Team_Offsets, Text_Box_Team_Members,
                    Combo_Box_Team_Type, 

                    Text_Box_Hyperspace_Speed, Text_Box_Starting_Unit_Name, Text_Box_Spawned_Unit,
                    Text_Box_Reserve_Unit_Name, Text_Box_Reserve_Unit, Text_Box_Death_Clone, Text_Box_Death_Clone_Model,
                    List_Box_Inactive_Behavoir, List_Box_Active_Behavoir,

                    // Build Requirements
                    Button_Move_to_Active, Button_Behavoir_Exchange, Button_Move_to_Inactive, 

                    Button_Save_Names, Button_Reset_Tag_Names, 
                    Text_Box_Costum_Tag_1, Text_Box_Costum_Tag_2, Text_Box_Costum_Tag_3,
                    Text_Box_Costum_Tag_4, Text_Box_Costum_Tag_5, Text_Box_Costum_Tag_6,
                    Button_Save, Button_Test_Ingame, Button_Add_Into, Button_Save_As,

                    // Edit Xml Tab
                    Text_Box_Xml_Search_Bar, Button_Load_Xml, Button_Add_Xml_Tag,
                    Button_Remove_Xml_Tag, Button_Remove_Xml_Tag, Button_Save_Xml_As,
                    Button_Undo_Change, Button_Redo_Change, Button_Save_Xml, 

                    // Game Tab
                    Combo_Box_Language, 
                    Text_Box_Debug_Path, Text_Box_EAW_Path, Button_Open_EAW_Path, Button_Reset_EAW_Path, 
                    Text_Box_FOC_Path, Button_Open_FOC_Path, Button_Reset_FOC_Path,
                    Text_Box_Savegame_Path, Button_Open_Savegame_Path, Button_Reset_Savegame_Path,
                    Button_Commandbar_Color_Blue, Button_Reset_Vanilla_Commandbar, 
                    Combo_Box_Dashboard_Mod, Combo_Box_Target_Faction, Button_Restore_Dashboards,
                    Button_Apply_Dashboard,

                    // Conquest Tab
                    List_View_Galaxy,
                    Text_Box_Planet_Name, Text_Box_Planet_Is_Variant, Text_Box_Planet_Text_ID,     
                    Text_Box_Planet_Encyclopedia, Combo_Box_Planet_Model, Text_Box_Destroyed_Planet_Model, 
                    Text_Box_Planet_Scale, Text_Box_Galactic_Position, Text_Box_Facing_Adjust, 
                    Text_Box_Space_Map, Text_Box_Land_Map, Text_Box_Destroyed_Map,    
                    Text_Box_Max_Star_Base, Text_Box_Space_Structures, Text_Box_Land_Structures,
                    Text_Box_Credit_Income, Text_Box_Destroyed_Income, Text_Box_Bar_Capture_Bonus, 
                    Text_Box_Planetary_Population, 

                    List_Box_Inactive_Factions, List_Box_Active_Factions,   
                    Text_Box_Faction_Name, Text_Box_Faction_Is_Variant, Text_Box_Faction_Text, 
                    Combo_Box_Basic_AI, Text_Box_Faction_Color, Text_Box_Faction_Icon,
                    Text_Box_Starbase_Icon, Text_Box_Faction_Leader, Text_Box_Faction_Leader_Company,
                    Text_Box_Unit_Capacity, List_View_Faction_List,
                    Button_Faction_to_Inactive, Button_Faction_Exchange, Button_Faction_to_Active, 
                    Button_AI_Defibrilator, Button_Edit_Faction,

                    // Settings Tab
                    Button_Add_Scepter_Sway, Text_Box_Color_Selection, 
                    Button_Color_1, Button_Color_2, Button_Color_3,
                    Button_Color_4, Button_Color_5, Button_Color_6,
                    Button_Choose_Theme, Button_Set_Background, Button_Cycle_Background,
                    Text_Box_Folder_Path, Button_Set_Folder_Path, 
                    Button_Open_Folder_Path, Button_Reset_Folder_Path,

                    // Maximal Values
                    Text_Box_Max_Model_Scale, Text_Box_Max_Model_Height, 
                    Text_Box_Max_Select_Box_Scale, Text_Box_Max_Health_Bar_Height,

                    Button_Restore_Default_Values, Text_Box_Max_Credits, Text_Box_Max_Hull,
                    Text_Box_Max_Shield, Text_Box_Max_Shield_Rate, Text_Box_Max_Energy,
                    Text_Box_Max_Energy_Rate, Text_Box_Max_Speed, Text_Box_Max_AI_Combat, Text_Box_Max_Projectile,

                    Text_Box_Max_Build_Cost, Text_Box_Max_Skirmish_Cost, Text_Box_Max_Slice_Cost,
                    Text_Box_Max_Build_Time, Text_Box_Max_Tech_Level, Text_Box_Max_Timeline, 
                    Text_Box_Max_Star_Base_LV, Text_Box_Max_Ground_Base_LV, 
                    Text_Box_Max_Build_Limit, Text_Box_Max_Lifetime_Limit,

                    Text_Box_Max_Costum_4, Text_Box_Max_Costum_5, Text_Box_Max_Costum_6,
                    Button_Reset_All_Settings, Button_Withdraw_Files, Button_Uninstall_Imperialware  
                };


                // Until the iteration cycle reached the maximum number of Items in this list
                for (int cycle = 0; cycle < The_Controls.Length; cycle++)
                {
                    // We take the array item with the current cycle numer, and change its fore color to the Layout color!
                    The_Controls[cycle].ForeColor = Chosen_Color;
                }
            }

            else if (Color_Index == 05)
            {
                // Changing Color of the Background Window
                // Graphics The_Graphic = this.CreateGraphics();
                // The_Graphic.FillRectangle(new SolidBrush(Chosen_Color), this.ClientRectangle);
                this.BackColor = Chosen_Color;


                Brush Background_Brush = new System.Drawing.SolidBrush(Chosen_Color);
                // Setting Background Color of selected Tabs
                // Tab_Control_01.SelectedTab.BackColor = Chosen_Color;
                // Tab_Control_01.TabPages[1].BackColor = Chosen_Color;
                // Tab_Control_01.TabPages[2].BackColor = Chosen_Color;

                
                // Color_05: User Interface Background of all tab Pages
                dynamic[] The_Controls = 
                {
                    Tab_Control_01.TabPages[1], Tab_Control_01.TabPages[2], Tab_Control_01.TabPages[3], 
                    Tab_Control_01.TabPages[4], Tab_Control_01.TabPages[5], Tab_Control_01.TabPages[6], 
                    Tab_Control_01.TabPages[7], Track_Bar_Mod_Image_Size, Track_Bar_Xml_Values,
                };

                // Until the iteration cycle reached the maximum number of Items in this list
                for (int cycle = 0; cycle < The_Controls.Length; cycle++)
                {
                    // We take the array item with the current cycle numer, and change its fore color to the Layout color!
                    The_Controls[cycle].BackColor = Chosen_Color;
                }
            }

            else if (Color_Index == 06)
            {   // Color_06: Costum Xml Tags
                dynamic[] The_Controls = 
                {
                    Text_Box_Tag_1_Name, Text_Box_Tag_2_Name, Text_Box_Tag_3_Name,
                    Text_Box_Tag_4_Name, Text_Box_Tag_5_Name, Text_Box_Tag_6_Name,
                };
            

                // Adjusting the Backcolor according to the Parent object of these Textboxes
                Text_Box_Tag_1_Name.BackColor = Group_Box_Costum_Tags.BackColor;
                Text_Box_Tag_2_Name.BackColor = Group_Box_Costum_Tags.BackColor;
                Text_Box_Tag_3_Name.BackColor = Group_Box_Costum_Tags.BackColor;
                Text_Box_Tag_4_Name.BackColor = Group_Box_Costum_Tags.BackColor;
                Text_Box_Tag_5_Name.BackColor = Group_Box_Costum_Tags.BackColor;
                Text_Box_Tag_6_Name.BackColor = Group_Box_Costum_Tags.BackColor;

                // Until the iteration cycle reached the maximum number of Items in this list
                for (int cycle = 0; cycle < The_Controls.Length; cycle++)
                {
                    // We take the array item with the current cycle numer, and change its fore color to the Layout color!
                    The_Controls[cycle].ForeColor = Chosen_Color;
                }
            }
                       
        }



        // This covers all UNchecked Radio Buttons and Checkboxes !
        void Set_Checkbox_Color(Color Background_Color, bool Is_Checked)
        {
            // Defining a array of dynamic variables for the white/black/grey Lable objects
            dynamic[] Colored_Lables = 
            {
                // Mods Tab
                Button_Select_EAW, Button_Select_FOC, Check_Box_No_Mod,            

                // Manage Tab
                Radio_Button_Planet_1, Radio_Button_Planet_2, Radio_Button_Planet_3, Radio_Button_Planet_4,               
                Check_Box_Is_Variant, Check_Box_Game_Object_Files, Check_Box_Hard_Point_Data,
                Check_Box_Slice_Cost, Check_Box_Current_Limit, Check_Box_Lifetime_Limit,
                Radio_Button_HP_Template, Radio_Button_First_HP, Radio_Button_Custom_HP,

                // Game Tab
                Check_Box_Use_Language, Check_Box_Evade_Language, Check_Box_Windowed_Mode, Check_Box_Debug_Mode_Game, Check_Box_Disable_Workshop, Check_Box_Mod_Savegame_Directory,

                // Conquest Tab
                Check_Box_Is_Faction_Variant,

                // Settings Tab
                Check_Box_Copy_Editor_Art, Check_Box_Xml_Checkmate, Check_Box_Set_Theme,
                Check_Box_Close_After_Launch, Check_Box_Debug_Mode, Check_Box_Copy_Editor_Art,
                Check_Box_Xml_Checkmate, Check_Box_Load_Rescent, Check_Box_Load_Issues, Check_Box_Load_Tag_Issues, 
                Check_Box_Add_Core_Code, Check_Box_Auto_Save_Planets, Check_Box_Set_Theme, Check_Box_Set_Color, Check_Box_Cycle_Mod_Image, 
                Check_Box_Show_Mod_Button, Check_Box_Show_Addon_Button,
            };

            // Until the iteration cycle reached the maximum number of Items in this list
            for (int cycle = 0; cycle < Colored_Lables.Length; cycle++)
            {
                // We take the array item with the current cycle numer, and change its fore color to the Layout color!

                if (Is_Checked) 
                {   
                    if (Colored_Lables[cycle].Checked) { Colored_Lables[cycle].ForeColor = Background_Color; }
                }              
                else 
                {   
                    if (!Colored_Lables[cycle].Checked) { Colored_Lables[cycle].ForeColor = Background_Color; }
                }

            }
        }

   

        void Make_Backgrounds_Transparent()
        {
            dynamic[] Colored_Lables = 
            {                
                // Color 01
                Label_Faction, Label_Planet_1, Label_Planet_2, Label_Units,
                Label_Credits, Label_Unit_Name, Label_Unit_Type,
                Label_Is_Variant, 
                       
                // Art Settings
                Label_Model_Name, Label_Icon_Name, Label_Radar_Icon_Size, Label_Radar_Icon, 
                Label_Text_Id, Label_Encyclopedia_Text, Label_Unit_Class, Mini_Button_Text_Info_A,

                Label_Scale_Factor, Label_Model_Height, Label_Select_Box_Scale,
                Label_Select_Box_Height, Label_Health_Bar_Height, Check_Box_Health_Bar_Size,                
                Button_Operator_Model_Height, Button_Operator_Select_Box_Height,
                
                // Land Settings
                Label_Movement_Animation, Label_Walk_Animation, Label_Crouch_Animation,
                Label_Rotation_Animation, Label_Deploy_Animation, Label_Damage_Stages,
                Label_Collison_Box, Label_Remove_Upon_Death, Label_Capture_Point, Label_Hover,
                Label_Wind_Disturbances, Label_Hover_Height, Label_Enemy_Fire_Height,
                Label_Sensor_Range, Switch_Remove_Upon_Death, Switch_Capture_Point, Switch_Hover,

                // Power Values
                Label_Class, Label_Hull, Label_Shield, Mini_Button_Hull_Ballance,
                Label_Energy, Label_Speed, Button_Operator_Population, 
                Label_Population, Label_AI_Comabat, Label_Projectile,
                Label_Current_Balancing,

                // Unit Abilities
                Label_Ability_Type, Label_Auto_Fire, Label_Activated_GUI_Ability, 
                Label_Expiration_Seconds, Label_Recharge_Seconds, 
                Label_Alternate_Name, Label_Alternate_Description, Label_Alternate_Icon,
                Label_SFX_Ability_Activated, Label_SFX_Ability_Deactivated, Lable_Lua_Script,
                Label_Mod_Multiplier, Label_Additional_Abilities,
                Switch_Button_Auto_Fire, Button_Add_Ability,
                 
                // Unit Properties
                Label_Is_Team, Label_Use_Container, Label_Team_Name, Label_Team_Type, 
                Label_Team_Is_Variant, Label_Container_Name, Label_Shuttle_Type, 
                Label_Team_Members, Check_Box_Team_Is_Variant,

                Label_Is_Hero, Label_Show_Head, Label_God_Mode, Label_Use_Particle,
                Label_Starting_Spawned_Units, Label_Reserve_Spawned_Units, Label_Death_Clone, Label_Death_Clone_Model,
                Label_Inactive_Behavoir, Label_Active_Behavoir, 
                Check_Box_Has_Hyperspace,

                Switch_Button_Is_Team, Switch_Button_Use_Container, Switch_Button_Is_Hero, Switch_Button_Show_Head, Switch_Button_God_Mode, Switch_Button_Use_Particle,

                // Build Requirements
                Label_Inactive_Affiliations, Label_Active_Affiliations, Check_Box_Add_To_Base,  
                Label_Enable_All, Label_Build_Tab_Space, Label_Build_Cost, Label_Skirmish_Cost, 
                Label_Build_Time, Label_Tech_Level, Label_Star_Base_LV, Label_Ground_Base_LV, 

                Switch_Button_Enable_All, Switch_Button_Build_Tab, Switch_Button_Innitially_Locked, 
                Button_Required_Planets, Button_Required_Structures, 
                Button_Operator_Planet_Plus, Button_Operator_Planet_Minus,
                Label_Innitially_Locked, Label_Required_Timeline, Check_Box_Slice_Cost, 
                Check_Box_Current_Limit, Check_Box_Lifetime_Limit,

                // Hardpoints
                Switch_Is_Targatable, Switch_Is_Destroyable, Switch_Is_Repairable, Switch_Is_Turret,
                Mini_Button_DPM_Info_Search, Mini_Button_DPM_Info, Mini_Button_DPM_Info_HP,
                Picture_Box_Cone_Width, Picture_Box_Cone_Height, Picture_Box_Turret_Rotation, 
                Picture_Box_Turret_Elevation, Picture_Box_Turret_Location,


                // Game Tab
                Lable_Language, Label_Is_Steam_Version, Label_EAW_Path,              
                Label_FOC_Path, Label_Savegame_Path, Label_Vanilla_Command_Bar,
                Label_Dashboard_Mod, Label_Dashboard_Source_Faction,
                Label_Dashboard_Target_Faction, Label_Mod_Declaration,

                // Conquest Tab
                Button_Move_Mode, Button_Creation_Mode, Button_Trade_Routes_Mode,
                Segment_Button_Galaxy, Segment_Button_Planets, Segment_Button_Planet_Pool, Segment_Button_Trade_Routes,    
                Button_Operator_Galaxy_Minus, Track_Bar_Galactic_Background, Button_Refresh_Galaxy_Area,

                Mini_Button_Copy_Planet, Operator_Remove_Planet, Operator_Add_Planet, Mini_Button_Clear_Galaxy,    
                Button_Open_Planet_Variant, Button_Browse_Planet, 
                Switch_Button_Surface_Accessible, Switch_Button_Is_Pre_Lit, 
                Switch_Button_Planet_Animation, Picture_Box_Terrain,
                
                // Conquest Tab
                Label_Planet_Name, Label_Is_Planet_Variant, Check_Box_Planet_Is_Variant,   
                Check_Box_Vanilla_Planets, Label_Planet_Text_ID, Label_Planet_Encyclopedia, 
                Label_Planet_Model, Label_Destroyed_Planet_Model, Label_Planet_Model_Scale,     
                Label_Surface_Accessible, Label_Planet_Is_Pre_Lit, Label_Planet_Animation, 
                Label_Galactic_Position, Label_Facing_Adjust,      
                Label_Space_Tactical_Map, Label_Land_Tactical_Map, Label_Destroyed_Tactical_Map,       
                Label_Max_Space_Base, Label_Space_Structures, Label_Land_Structures, 
                Label_Credit_Income, Label_Capture_Bonus, Label_Additonal_Population, 
                Label_Planet_Terrain,

                Label_Inactive_Factions, Label_Active_Factions, Check_Box_Is_Faction_Variant,
                Label_Faction_Name, Label_Faction_Is_Variant, Label_Faction_Text_Id, 
                Label_Basic_AI, Label_Faction_Color, Label_Faction_Icon_Name, 
                Label_Star_Base_Icon, Label_Faction_Leader, Label_Faction_Leader_Company, 
                Label_Tactical_Unit_Cap, Label_Is_Playable, Label_Is_In_Multiplayer,
                Switch_Button_Is_Playable, Switch_Button_Multiplayer,

                // Settings Tab
                Lable_Transperency, Label_Version, Label_Special_Button_Color, Label_Imperialware_Themes,
                Label_Folder_Path, 

                Label_Maximum_Model_Scale, Label_Maximum_Model_Height, 
                Label_Maximum_Select_Box_Scale, Label_Maximum_Health_Bar_Height, 
                Label_Maximum_Credits, Label_Maximum_Hull, 
                Label_Maximum_Shield, Label_Maximum_Shield_Rate, Label_Maximum_Energy, 
                Label_Maximum_Energy_Rate, Label_Maximum_Speed, Label_Maximum_AI_Combat, Label_Maximum_Projectile,
                            
                Label_Maximum_Build_Cost, Label_Maximum_Skirmish_Cost, Label_Maximum_Slice_Cost,
                Label_Maximum_Build_Time, Label_Maximum_Tech_Level, Label_Maximum_Timeline,
                Label_Maximum_Star_Base_LV, Label_Maximum_Ground_Base_LV, 
                Label_Maximum_Build_Limit, Label_Maximum_Lifetime_Limit, 
                Label_Maximum_Costum_4, Label_Maximum_Costum_5, Label_Maximum_Costum_6,  

                // Color 03, Launcher Tab
                Label_Addon_Name, Label_Mod_Name, Label_Game_Name,

                // Manage Tab
                Mini_Button_Save, Button_Unit_Mod, Button_Unit_Selection, Mini_Button_Save_As, Mini_Button_Copy_Instance, Mini_Button_Copy_Unit, Mini_Button_Clear_Unit,
                Mini_Button_Copy_Team, Label_Cheating, Label_Start_Planet, Label_Target_Planet, 
                Label_Galaxy_File, Label_Credit_Sign, Label_Xml_Name, Button_Open_Variant, Button_Open_Team_Variant,
                Label_Model_Alo, Label_Icon_Tga, Label_Radar_Icon_Tga, Label_Dat_Editor,
                Label_String_Editor, Label_Art_Settings, Label_Land_Settings, Label_Power_Values, Label_Balancing_Info, Label_Damage_Per_Minute_Info, 
                Label_Abilities, Label_Expiration_Sec, Label_Recharge_Sec, Label_Ability_Icon, Check_Box_Use_In_Team,
                Label_Unit_Properties, Label_Move_as_Team, Label_Start_Units_Amount, Label_Reserve_Units_Amount, Label_Costum_Tags,
                Label_Build_Requirements, Check_Box_Add_To_Skirmish, Check_Box_Add_To_Campaign, Label_Slice_Credits,
                Label_Build_Credits, Label_Skirmish_Credits, Label_Seconds,

                
                // Projectiles
                Mini_Button_Refresh_Proj_Area, Mini_Button_Clear_All_Instances, Mini_Button_Clear_Proj_Area, Mini_Button_Save_Projectile, Mini_Button_Save_Projectile_As,
                Button_Projectiles_Mod, Button_Projectiles_Selection,

                Mini_Button_Copy_Proj_Name, Mini_Button_Clear_Projectile, Mini_Button_Open_Proj_Variant, 
                Switch_Shield_Damage, Switch_Energy_Damage, Switch_Hitpoint_Damage,
                Button_Ground, Button_Use_Both, Button_Space, Button_Projectile_Small, Button_Projectile_Medium, Button_Projectile_Large,
                Button_Stick, Button_Ignore, Button_Explode, Switch_Target_On_Terrain, Switch_Stun_On_Detonation, Switch_Projectile_Disable_Engines, 
                Picture_Box_Projectile_Color_Picker,

                // Hardpoint Layout
                Mini_Button_Refresh_HP_Area, Mini_Button_Clear_HP_Area, Mini_Button_Save_Hardpoint, Mini_Button_Save_Hardpoint_As, Mini_Button_Copy_HP_Name, Mini_Button_Clear_HP, Mini_Button_Open_HP_Variant,
                Mini_Button_Restrictions, Mini_Button_Fire_Projectile,
                Button_Hardpoints_Mod, Button_Hardpoints_Selection,
                Button_Fire_Deployed, Button_Fire_Undeployed,
                
                // Conquest Tab
                Label_Conquest_File, Label_Planet_File, Label_Planet_Model_Alo, Label_Destroyed_Planet_Alo,
                Label_Space_Map_Ted, Label_Land_Map_Ted, Label_Destroyed_Map_Ted, Check_Box_Planet_Game_Objects,                                
                Label_Faction_File, Label_Faction_Icon_Tga, Label_Star_Base_Icon_Tga,

                Button_Faction_List_1, Button_Faction_List_2, Button_Faction_List_3,
                Button_Faction_List_4, Button_Faction_List_5, Picture_Box_Faction_Color_Picker,
                Picture_Box_Faction_Color_Indicator, Mini_Button_Copy_Faction, Operator_Add_Faction, Operator_Remove_Faction, 
                Button_Open_Faction_Variant, Operator_Add_Faction_List, Operator_Remove_Faction_List, 

                // Settings Tab
                Label_Mod_Name_2, Label_Galactic_Alo, Label_Tactical_Alo, Label_Log_File_Location,
                Label_EAW_Savegame, Label_FOC_Savegame, Label_Imperialware_Version, 
                Label_Xml_Editor_Settings, Label_UI_Settings, Label_Maximal_Values,



                // Color 04
                Label_Planet_1_Name, Label_Planet_2_Name,      


                // Segment Buttons
                Button_Refresh_Galaxy, Button_Refresh_Planets, 
                Button_Teleport_Planets, Button_Teleport_Both, Button_Teleport_Units,
                Button_Use_Game_Language, Button_Use_Mod_Language, 
                Button_EAW_Command_Bar, Button_FOC_Command_Bar, 
                Button_Primary_Ability, Button_Secondary_Ability, 
                Button_Color_Switch_1, Button_Color_Switch_7,
                Button_Class_1, Button_Class_5, Button_Class_6, Button_Class_10,

                 
                // Other things
                Button_Launch_Mod, Button_Mod_Only, Button_Launch_Addon, Switch_Button_Is_Steam_Version,
                Mod_Picture, Picture_Box_Dashboard_Preview,  Picture_Box_Color_Picker,                        
            };

            // Applying translucent Background color to all Text Boxes, this is important because Background color changes with themes
            foreach (var Item in Colored_Lables) { try { Item.BackColor = Color.Transparent; } catch { } }

        }


        void Adjust_Text_Fonts(int Font_Scale_Value)
        {          
            dynamic[] Text_Items = 
            {
                // Color 01
                Label_Faction, Label_Planet_1, Label_Planet_2, Label_Units,
                Label_Credits, Label_Unit_Name, Label_Unit_Type,
                Label_Is_Variant, 
                       
                // Art Settings
                Label_Model_Name, Label_Icon_Name, Label_Radar_Icon_Size, Label_Radar_Icon, 
                Label_Text_Id, Label_Encyclopedia_Text, Label_Unit_Class, 

                Label_Scale_Factor, Label_Model_Height, Label_Select_Box_Scale,
                Label_Select_Box_Height, Label_Health_Bar_Height, Check_Box_Health_Bar_Size,

                // Land Settings
                Label_Movement_Animation, Label_Walk_Animation, Label_Crouch_Animation,
                Label_Rotation_Animation, Label_Deploy_Animation, Label_Damage_Stages,
                Label_Collison_Box, Label_Remove_Upon_Death, Label_Capture_Point, Label_Hover,
                Label_Wind_Disturbances, Label_Hover_Height, Label_Enemy_Fire_Height,
                Label_Sensor_Range,
                
                // Power Values
                Label_Class, Label_Hull, Label_Shield,
                Label_Energy, Label_Speed, Label_Population,
                Label_AI_Comabat, Label_Projectile, Label_Current_Balancing,

                // Unit Abilities
                Label_Ability_Type, Label_Auto_Fire, Label_Activated_GUI_Ability, 
                Label_Expiration_Seconds, Label_Recharge_Seconds, 
                Label_Alternate_Name, Label_Alternate_Description, Label_Alternate_Icon,
                Label_SFX_Ability_Activated, Label_SFX_Ability_Deactivated, Lable_Lua_Script,
                Label_Mod_Multiplier, Label_Additional_Abilities,

                // Unit Properties
                Label_Is_Team, Label_Use_Container, Label_Team_Name, Label_Team_Type, 
                Label_Team_Is_Variant, Label_Container_Name, Label_Shuttle_Type, 
                Label_Team_Members, Check_Box_Team_Is_Variant,

                Label_Is_Hero, Label_Show_Head, Label_God_Mode, Label_Use_Particle,
                Label_Starting_Spawned_Units, Label_Reserve_Spawned_Units, Label_Death_Clone, Label_Death_Clone_Model,
                Label_Inactive_Behavoir, Label_Active_Behavoir, 
                Check_Box_Has_Hyperspace,

                // Build Requirements
                Label_Inactive_Affiliations, Label_Active_Affiliations, Check_Box_Add_To_Base,  
                Label_Enable_All, Label_Build_Tab_Space, Label_Build_Cost, Label_Skirmish_Cost, 
                Label_Build_Time, Label_Tech_Level, Label_Star_Base_LV, Label_Ground_Base_LV,

                Label_Innitially_Locked, Label_Required_Timeline, Check_Box_Slice_Cost, 
                Check_Box_Current_Limit, Check_Box_Lifetime_Limit,


                // Game Tab
                Lable_Language, Label_Is_Steam_Version, Label_EAW_Path,              
                Label_FOC_Path, Label_Savegame_Path, Label_Vanilla_Command_Bar,
                Label_Dashboard_Mod, Label_Dashboard_Source_Faction,
                Label_Dashboard_Target_Faction, Label_Mod_Declaration,

                // Conquest Tab
                Label_Planet_Name, Label_Is_Planet_Variant, Check_Box_Planet_Is_Variant,   
                Check_Box_Vanilla_Planets, Label_Planet_Text_ID, Label_Planet_Encyclopedia, 
                Label_Planet_Model, Label_Destroyed_Planet_Model, Label_Planet_Model_Scale,     
                Label_Surface_Accessible, Label_Planet_Is_Pre_Lit, Label_Planet_Animation, 
                Label_Galactic_Position, Label_Facing_Adjust,      
                Label_Space_Tactical_Map, Label_Land_Tactical_Map, Label_Destroyed_Tactical_Map,       
                Label_Max_Space_Base, Label_Space_Structures, Label_Land_Structures, 
                Label_Credit_Income, Label_Capture_Bonus, Label_Additonal_Population, 
                Label_Planet_Terrain,
 
                Label_Inactive_Factions, Label_Active_Factions, Check_Box_Is_Faction_Variant,
                Label_Faction_Name, Label_Faction_Is_Variant, Label_Faction_Text_Id, 
                Label_Basic_AI, Label_Faction_Color, Label_Faction_Icon_Name, 
                Label_Star_Base_Icon, Label_Faction_Leader, Label_Faction_Leader_Company, 
                Label_Tactical_Unit_Cap, Label_Is_Playable, Label_Is_In_Multiplayer,

                // Settings Tab
                Lable_Transperency, Label_Version, Label_Special_Button_Color, Label_Imperialware_Themes,
                Label_Folder_Path, 
                
                Label_Maximum_Model_Scale, Label_Maximum_Model_Height, 
                Label_Maximum_Select_Box_Scale, Label_Maximum_Health_Bar_Height, 
                Label_Maximum_Credits, Label_Maximum_Hull, 
                Label_Maximum_Shield, Label_Maximum_Shield_Rate, Label_Maximum_Energy, 
                Label_Maximum_Energy_Rate, Label_Maximum_Speed, Label_Maximum_AI_Combat, Label_Maximum_Projectile,

                Label_Maximum_Build_Cost, Label_Maximum_Skirmish_Cost, Label_Maximum_Slice_Cost,
                Label_Maximum_Build_Time, Label_Maximum_Tech_Level, Label_Maximum_Timeline,
                Label_Maximum_Star_Base_LV, Label_Maximum_Ground_Base_LV, 
                Label_Maximum_Build_Limit, Label_Maximum_Lifetime_Limit, 

                Label_Maximum_Costum_4, Label_Maximum_Costum_5, Label_Maximum_Costum_6,


                // Color 03, Launcher Tab
                Label_Addon_Name, Label_Mod_Name, Label_Game_Name,

                // Manage Tab
                Label_Cheating, Label_Start_Planet, Label_Target_Planet, 
                Label_Galaxy_File, Label_Credit_Sign, Label_Xml_Name, 
                Label_Model_Alo, Label_Icon_Tga, Label_Radar_Icon_Tga, Label_Dat_Editor,
                Label_String_Editor, Label_Art_Settings, Label_Land_Settings, Label_Power_Values, Label_Balancing_Info, Label_Damage_Per_Minute_Info, 
               
                Label_Abilities, Label_Expiration_Sec, Label_Recharge_Sec, Label_Ability_Icon, Check_Box_Use_In_Team,
                Label_Unit_Properties, Label_Move_as_Team, Label_Start_Units_Amount, Label_Reserve_Units_Amount, Label_Costum_Tags,
                Label_Build_Requirements, Check_Box_Add_To_Skirmish, Check_Box_Add_To_Campaign, Label_Slice_Credits,
                Label_Build_Credits, Label_Skirmish_Credits, Label_Seconds,
                
                // Conquest Tab
                Label_Conquest_File, Label_Planet_File, Label_Planet_Model_Alo, Label_Destroyed_Planet_Alo,
                Label_Space_Map_Ted, Label_Land_Map_Ted, Label_Destroyed_Map_Ted, Check_Box_Planet_Game_Objects,
                                       
                Label_Faction_File, Label_Faction_Icon_Tga, Label_Star_Base_Icon_Tga,

                // Settings Tab
                Label_Mod_Name_2, Label_Galactic_Alo, Label_Tactical_Alo, Label_Log_File_Location,
                Label_EAW_Savegame, Label_FOC_Savegame, Label_Imperialware_Version, 
                Label_Xml_Editor_Settings, Label_UI_Settings, Label_Maximal_Values,



                // Color 04
                Label_Planet_1_Name, Label_Planet_2_Name,      


                // Segment Buttons
                Button_Refresh_Galaxy, Button_Refresh_Planets, 
                Button_Teleport_Planets, Button_Teleport_Both, Button_Teleport_Units,
                Button_Use_Game_Language, Button_Use_Mod_Language, 
                Button_EAW_Command_Bar, Button_FOC_Command_Bar,
                
                Button_Primary_Ability, Button_Secondary_Ability, 

                Button_Color_Switch_1, Button_Color_Switch_2, Button_Color_Switch_3, 
                Button_Color_Switch_4, Button_Color_Switch_5, Button_Color_Switch_6, Button_Color_Switch_7, 

                Button_Class_1,Button_Class_2, Button_Class_3, Button_Class_4, Button_Class_5, 
                Button_Class_6, Button_Class_7, Button_Class_8, Button_Class_9, Button_Class_10,
            

                // Other things
                Mod_Picture, Picture_Box_Dashboard_Preview, Picture_Box_Color_Picker, 
                Button_Launch_Mod, Button_Mod_Only, Button_Launch_Addon,

               
                //======= CHECK BOXES =======
                // Mods Tab
                Button_Select_EAW, Button_Select_FOC, Check_Box_No_Mod,            

                // Manage Tab
                Radio_Button_Planet_1, Radio_Button_Planet_2, Radio_Button_Planet_3, Radio_Button_Planet_4, 
                Check_Box_Is_Variant, Check_Box_Game_Object_Files, Check_Box_Hard_Point_Data,
                Radio_Button_HP_Template, Radio_Button_First_HP, Radio_Button_Custom_HP,

                // Game Tab
                Check_Box_Use_Language, Check_Box_Evade_Language, Check_Box_Windowed_Mode, Check_Box_Debug_Mode_Game, Check_Box_Disable_Workshop, Check_Box_Mod_Savegame_Directory,

                // Settings Tab
                Check_Box_Copy_Editor_Art, Check_Box_Xml_Checkmate, Check_Box_Set_Theme,
                Check_Box_Close_After_Launch, Check_Box_Debug_Mode, Check_Box_Copy_Editor_Art,
                Check_Box_Xml_Checkmate, Check_Box_Load_Rescent, Check_Box_Load_Issues,
                Check_Box_Load_Tag_Issues, Check_Box_Add_Core_Code, Check_Box_Auto_Save_Planets, 
                Check_Box_Set_Theme, Check_Box_Set_Color, Check_Box_Cycle_Mod_Image, 
                Check_Box_Show_Mod_Button, Check_Box_Show_Addon_Button,   
          

                //======= BUTTON TEXTS =======

                Button_Debug_Mode,

                // Mods Tab
                Button_Open_Map_Editor, Button_Set_Mod, Button_Start_Mod, 

                // Apps Tab
                Text_Box_Object_Searchbar, Button_Download_App, Button_Add_App, Button_Remove_App, 
                Button_Select_Mods, Button_Select_Addons, Button_Start_App, 
                Button_Load_Addon, Button_Unload_Addon, 

                // Manage Tab              
                Button_Expand_A, Button_Expand_B, Button_Expand_C,
                Button_Expand_D, Button_Expand_E, Button_Expand_F,
                Button_Expand_G, Button_Expand_H, Button_Expand_I,
                Button_Expand_J, Button_Expand_K,
                Button_Expand_N, Button_Expand_O,
               
                Button_Collaps_A, Button_Collaps_B, Button_Collaps_C,
                Button_Collaps_D, Button_Collaps_F, Button_Collaps_G,
                Button_Collaps_I, Button_Collaps_J,
                Button_Collaps_N, Button_Collaps_O,
           

                Combo_Box_Faction, Button_Destroy_Planet, Button_Select_Planet,                 
                Button_Teleport, Text_Box_Search_Bar, Combo_Box_Filter_Type,
                
                Button_Spawn, Button_Unit_Selector, Button_Edit, Button_Edit_Xml, Button_Give_Credits,
                Text_Box_Name, Combo_Box_Type, Text_Box_Name,
                Text_Box_Is_Variant, Text_Box_Credits,  

                Button_Affiliation_to_Active, Button_Affiliation_Exchange, Button_Affiliation_to_Inactive, 
                List_View_Inactive_Affiliations, List_View_Active_Affiliations, List_View_Requirements,  
                Text_Box_Build_Cost, Text_Box_Skirmish_Cost, Text_Box_Build_Time,
                Text_Box_Tech_Level, Text_Box_Star_Base_LV, Text_Box_Ground_Base_LV,
                Text_Box_Required_Timeline, Text_Box_Slice_Cost, Text_Box_Current_Limit,
                Text_Box_Lifetime_Limit, 

                // Xml Editing Values
                Button_Open_Xml, Button_Search_Model,
                Button_Default_Radar_Icon, Text_Box_Model_Name, Text_Box_Icon_Name,
                Text_Box_Radar_Icon, Text_Box_Radar_Size, Text_Box_Text_Id,
                Text_Box_Unit_Class, Text_Box_Encyclopedia_Text,                
                Text_Box_Scale_Factor, Text_Box_Model_Height, 
                Text_Box_Health_Bar_Height, Text_Box_Select_Box_Scale, Text_Box_Select_Box_Height,
                    
                Combo_Box_Health_Bar_Size, Combo_Box_Class, 
                Text_Box_Hull, Text_Box_Shield, Text_Box_Shield_Rate, 
                Text_Box_Energy, Text_Box_Energy_Rate, 
                Text_Box_Speed, Text_Box_Rate_Of_Turn, Text_Box_Bank_Turn_Angle,
                Text_Box_AI_Combat, Text_Box_Population,    
                Text_Box_Projectile, 

                // Land Values
                Text_Box_Movement_Animation, Text_Box_Walk_Animation, Text_Box_Crouch_Animation, 
                Text_Box_Rotation_Animation, Text_Box_Deploy_Animation, Text_Box_Damage_Stages, 
                Text_Box_Collison_Box_X, Text_Box_Collison_Box_Y, Text_Box_Wind_Disturbances, Text_Box_Wind_Radius, 
                Text_Box_Hover_Height, Text_Box_Fire_Height, Text_Box_Sensor_Range,

                // Unit Abilities
                Combo_Box_Ability_Type, Combo_Box_Activated_GUI_Ability, Combo_Box_Mod_Multiplier, Combo_Box_Additional_Abilities,
                Text_Box_Expiration_Seconds, Text_Box_Recharge_Seconds, 
                Text_Box_Alternate_Name, Text_Box_Alternate_Description, Text_Box_Ability_Icon, 
                Text_Box_SFX_Ability_Activated, Text_Box_SFX_Ability_Deactivated, Text_Box_Lua_Script,
                Text_Box_Mod_Multiplier, Text_Box_Additional_Abilities, 
                            
                // Unit Properties
                Text_Box_Name, Text_Box_Team_Is_Variant, Text_Box_Container_Name, Text_Box_Shuttle_Type,
                Text_Box_Team_Amount, Text_Box_Team_Offsets, Text_Box_Team_Members,
                Combo_Box_Team_Type, 

                Text_Box_Hyperspace_Speed, Text_Box_Starting_Unit_Name, Text_Box_Spawned_Unit,
                Text_Box_Reserve_Unit_Name, Text_Box_Reserve_Unit, Text_Box_Death_Clone, Text_Box_Death_Clone_Model,
                List_Box_Inactive_Behavoir, List_Box_Active_Behavoir,
                Button_Move_to_Active, Button_Behavoir_Exchange, Button_Move_to_Inactive, 

                // Build Requirements
                Button_Save_Names, Button_Reset_Tag_Names, 
                Text_Box_Costum_Tag_1, Text_Box_Costum_Tag_2, Text_Box_Costum_Tag_3,
                Text_Box_Costum_Tag_4, Text_Box_Costum_Tag_5, Text_Box_Costum_Tag_6,
                Button_Save, Button_Test_Ingame, Button_Add_Into, Button_Save_As,

                // Edit Xml Tab
                Text_Box_Xml_Search_Bar, Button_Load_Xml, Button_Add_Xml_Tag,
                Button_Remove_Xml_Tag, Button_Remove_Xml_Tag, Button_Save_Xml_As,
                Button_Undo_Change, Button_Redo_Change, Button_Save_Xml,

                // Game Tab
                Combo_Box_Language, 
                Text_Box_Debug_Path, Text_Box_EAW_Path, Button_Open_EAW_Path, Button_Reset_EAW_Path, 
                Text_Box_FOC_Path, Button_Open_FOC_Path, Button_Reset_FOC_Path,
                Text_Box_Savegame_Path, Button_Open_Savegame_Path, Button_Reset_Savegame_Path,
                Button_Commandbar_Color_Blue, Button_Reset_Vanilla_Commandbar, 
                Combo_Box_Dashboard_Mod, Combo_Box_Target_Faction, Button_Restore_Dashboards,
                Button_Apply_Dashboard, Button_Faction_to_Inactive, Button_Faction_Exchange, Button_Faction_to_Active, 
                Button_AI_Defibrilator, Button_Edit_Faction,

                // Conquest Tab
                Button_Move_Mode, Button_Creation_Mode, Button_Trade_Routes_Mode,
                Segment_Button_Galaxy, Segment_Button_Planets, Segment_Button_Planet_Pool,
                Segment_Button_Trade_Routes, List_View_Galaxy, 

                Text_Box_Planet_Name, Text_Box_Planet_Is_Variant, Text_Box_Planet_Text_ID,     
                Text_Box_Planet_Encyclopedia, Combo_Box_Planet_Model, Text_Box_Destroyed_Planet_Model, 
                Text_Box_Planet_Scale, Text_Box_Galactic_Position, Text_Box_Facing_Adjust, 
                Text_Box_Space_Map, Text_Box_Land_Map, Text_Box_Destroyed_Map,    
                Text_Box_Max_Star_Base, Text_Box_Space_Structures, Text_Box_Land_Structures,
                Text_Box_Credit_Income, Text_Box_Destroyed_Income, Text_Box_Bar_Capture_Bonus, 
                Text_Box_Planetary_Population, 


                List_Box_Inactive_Factions, List_Box_Active_Factions,   
                Text_Box_Faction_Name, Text_Box_Faction_Is_Variant, Text_Box_Faction_Text, 
                Combo_Box_Basic_AI, Text_Box_Faction_Color, Text_Box_Faction_Icon,
                Text_Box_Starbase_Icon, Text_Box_Faction_Leader, Text_Box_Faction_Leader_Company,
                Text_Box_Unit_Capacity, List_View_Faction_List,

                // Settings Tab
                Button_Add_Scepter_Sway, Text_Box_Color_Selection, 
                Button_Color_1, Button_Color_2, Button_Color_3,
                Button_Color_4, Button_Color_5, Button_Color_6,
                Button_Choose_Theme, Button_Set_Background, Button_Cycle_Background,
                Text_Box_Folder_Path, Button_Set_Folder_Path, 
                Button_Open_Folder_Path, Button_Reset_Folder_Path,

                // Maximal Values
                Text_Box_Max_Model_Scale, Text_Box_Max_Model_Height, 
                Text_Box_Max_Select_Box_Scale, Text_Box_Max_Health_Bar_Height,
                Button_Restore_Default_Values, Text_Box_Max_Credits, Text_Box_Max_Hull,             
                Text_Box_Max_Shield, Text_Box_Max_Shield_Rate, Text_Box_Max_Energy,
                Text_Box_Max_Energy_Rate, Text_Box_Max_Speed, Text_Box_Max_AI_Combat, Text_Box_Max_Projectile,            

                Text_Box_Max_Build_Cost, Text_Box_Max_Skirmish_Cost, Text_Box_Max_Slice_Cost,
                Text_Box_Max_Build_Time, Text_Box_Max_Tech_Level, Text_Box_Max_Timeline, 
                Text_Box_Max_Star_Base_LV, Text_Box_Max_Ground_Base_LV, 
                Text_Box_Max_Build_Limit, Text_Box_Max_Lifetime_Limit, 

                Text_Box_Max_Costum_4, Text_Box_Max_Costum_5, Text_Box_Max_Costum_6,
                Button_Reset_All_Settings, Button_Withdraw_Files, Button_Uninstall_Imperialware,


                //======= RTF COSTUM XML TAGS =======
                Text_Box_Tag_1_Name, Text_Box_Tag_2_Name, Text_Box_Tag_3_Name,
                Text_Box_Tag_4_Name, Text_Box_Tag_5_Name, Text_Box_Tag_6_Name,
            };
          
            foreach (var Item in Text_Items) 
            {
                // We devide the old font size by Font_Scale_Value (which is 120) and multiply with 100 to get back at 100% as usual
                Item.Font = new Font("Georgia", (Item.Font.Size / Font_Scale_Value) * 100, FontStyle.Regular);
            }  

        }

        //=====================//

        void Change_Button_Color()
        {
            dynamic[] All_Buttons = 
            {
                Button_Debug_Mode,

                // Mods Tab
                Button_Open_Map_Editor, Button_Set_Mod, Button_Start_Mod, 

                // Apps Tab
                Button_Download_App, Button_Add_App, Button_Remove_App, 
                Button_Select_Mods, Button_Select_Addons, Button_Start_App, 
                Button_Load_Addon, Button_Unload_Addon, 

                // Manage Tab              
                Button_Expand_A, Button_Expand_B, Button_Expand_C,
                Button_Expand_D, Button_Expand_E, Button_Expand_F,
                Button_Expand_G, Button_Expand_H, Button_Expand_I,
                Button_Expand_J, Button_Expand_K,
                Button_Expand_N, Button_Expand_O,

                Button_Collaps_A, Button_Collaps_B, Button_Collaps_C,
                Button_Collaps_D, Button_Collaps_F, Button_Collaps_G,
                Button_Collaps_I, Button_Collaps_J, 
                Button_Collaps_N, Button_Collaps_O, 

                Button_Destroy_Planet, Button_Select_Planet,                 
                Button_Teleport, Button_Unit_Selector, Button_Spawn,

                Button_Edit, Button_Edit_Xml, Button_Give_Credits,
                Button_Affiliation_to_Active, Button_Affiliation_Exchange, Button_Affiliation_to_Inactive, 
                                             

                // Xml Editing Values
                Button_Open_Xml, Button_Search_Model,
                Button_Default_Radar_Icon,                  
                Button_Move_to_Active, Button_Behavoir_Exchange, Button_Move_to_Inactive, 

                Button_Save_Names, Button_Reset_Tag_Names,                 
                Button_Save, Button_Test_Ingame, Button_Add_Into, Button_Save_As,

                // Edit Xml Tab
                Button_Load_Xml, Button_Add_Xml_Tag, Button_Toggle_Value,
                Button_Remove_Xml_Tag, Button_Remove_Xml_Tag, Button_Save_Xml_As,
                Button_Save_Xml, 

                // Game Tab
                Button_Open_EAW_Path, Button_Reset_EAW_Path, 
                Button_Open_FOC_Path, Button_Reset_FOC_Path,
                Button_Open_Savegame_Path, Button_Reset_Savegame_Path,
                Button_Commandbar_Color_Blue, Button_Reset_Vanilla_Commandbar, 
                Button_Restore_Dashboards, Button_Apply_Dashboard,

                // Conquest Tab
                Button_Faction_to_Inactive, Button_Faction_Exchange, Button_Faction_to_Active,   
                Button_AI_Defibrilator, Button_Edit_Faction,

                // Settings Tab
                Button_Add_Scepter_Sway,
                Button_Color_1, Button_Color_2, Button_Color_3,
                Button_Color_4, Button_Color_5, Button_Color_6,
                Button_Choose_Theme, Button_Set_Background, Button_Cycle_Background,
                Button_Set_Folder_Path, Button_Open_Folder_Path, Button_Reset_Folder_Path,
                              
                // Maximal Values
                Button_Restore_Default_Values, Button_Reset_All_Settings, Button_Withdraw_Files, Button_Uninstall_Imperialware
            };


            foreach (var Button in All_Buttons)
            {   // Setting a translucent (gray and gradinent) button image over the Background Color                                                      

                if (File.Exists(Selected_Theme + @"Buttons\Button.png")) { Button.Image = Resize_Image(Selected_Theme + @"Buttons\", "Button.png", Button.Size.Width, Button.Size.Height); }
                else { Button.Image = Resize_Image(Program_Directory + @"Themes\Default\Buttons\", "Button.png", Button.Size.Width, Button.Size.Height); }

                if (Button != Button_Reset_All_Settings & Button != Button_Withdraw_Files & Button != Button_Uninstall_Imperialware) { Button.BackColor = Color_02; }
                Button.ForeColor = Color_04;

                // Registering a hover and leave event for each button, they exchange button Images in order to highlight/unhilight them! 
                Button.MouseHover += new EventHandler(Highlight_Button);
                Button.MouseLeave += new EventHandler(Unhighlight_Button);
            }



            //========  Dealing with special Buttons
            dynamic[] Search_Buttons = 
            {   // Search Buttons need a own button
                Button_Search_Object, Button_Search_Unit, Button_Search_Ability, Button_Search_Projectile, Button_Search_Projectile_Type,
                Button_Search_Projectile_Damage, Button_Search_Hard_Point, Button_Search_HP_Type, Button_Xml_Search
            };

            foreach (var Button in Search_Buttons)
            {
                if (File.Exists(Selected_Theme + @"Buttons\Button_Search.png")) { Button.Image = Resize_Image(Selected_Theme + @"Buttons\", "Button_Search.png", Button.Size.Width - 3, Button.Size.Height - 3); }
                else { Button.Image = Resize_Image(Program_Directory + @"Themes\Default\Buttons\", "Button_Search.png", Button.Size.Width - 3, Button.Size.Width - 3); }

                Button.BackColor = Color_02;
                Button.ForeColor = Color_04;
                Button.MouseHover += new EventHandler(Highlight_Button);
                Button.MouseLeave += new EventHandler(Unhighlight_Button);
            }


            if (File.Exists(Selected_Theme + @"Buttons\Button_Undo.png")) { Button_Undo_Change.Image = Resize_Image(Selected_Theme + @"Buttons\", "Button_Undo.png", Button_Undo_Change.Size.Width - 4, Button_Undo_Change.Size.Height - 4); }
            else { Button_Undo_Change.Image = Resize_Image(Program_Directory + @"Themes\Default\Buttons\", "Button_Undo.png", Button_Undo_Change.Size.Width - 4, Button_Undo_Change.Size.Height - 4); }

            Button_Undo_Change.BackColor = Color_02;
            Button_Undo_Change.ForeColor = Color_04;
            Button_Undo_Change.MouseHover += new EventHandler(Highlight_Button);
            Button_Undo_Change.MouseLeave += new EventHandler(Unhighlight_Button);


            if (File.Exists(Selected_Theme + @"Buttons\Button_Redo.png")) { Button_Redo_Change.Image = Resize_Image(Selected_Theme + @"Buttons\", "Button_Redo.png", Button_Redo_Change.Size.Width - 4, Button_Undo_Change.Size.Height - 4); }
            else { Button_Redo_Change.Image = Resize_Image(Program_Directory + @"Themes\Default\Buttons\", "Button_Redo.png", Button_Redo_Change.Size.Width - 4, Button_Redo_Change.Size.Height - 4); }

            Button_Redo_Change.BackColor = Color_02;
            Button_Redo_Change.ForeColor = Color_04;
            Button_Redo_Change.MouseHover += new EventHandler(Highlight_Button);
            Button_Redo_Change.MouseLeave += new EventHandler(Unhighlight_Button);


            if (File.Exists(Selected_Theme + @"Buttons\Button_Arrow.png")) { Button_Open_Variant.Image = Resize_Image(Selected_Theme + @"Buttons\", "Button_Arrow.png", Button_Open_Variant.Size.Width, Button_Open_Variant.Size.Height); }
            else { Button_Open_Variant.Image = Resize_Image(Program_Directory + @"Themes\Default\Buttons\", "Button_Arrow.png", Button_Open_Variant.Size.Width, Button_Open_Variant.Size.Height); }


            Mini_Button_Clear_Unit_MouseLeave(null, null);
            Mini_Button_Text_Info_A_MouseLeave(null, null);

            // Manage Tab
            Mini_Button_Copy_Instance_MouseLeave(null, null);
            Mini_Button_Copy_Unit_MouseLeave(null, null);
            Mini_Button_Copy_Team_MouseLeave(null, null);
            Mini_Button_Hull_Ballance_Click(null, null);

            Mini_Button_Save_MouseLeave(null, null);
            Mini_Button_Save_As_MouseLeave(null, null);

            Mini_Button_Open_HP_Variant_MouseLeave(null, null);
            Mini_Button_Fire_Projectile_MouseLeave(null, null);

            Mini_Button_DPM_Info_Search_MouseLeave(null, null);
            Mini_Button_DPM_Info_MouseLeave(null, null);
            Mini_Button_DPM_Info_HP_MouseLeave(null, null);

            // For these we can directly set them to false as there is never a true scenario.

            Set_Minibutton(Mini_Button_Clear_All_Instances, "Delete", false);
            Set_Minibutton(Mini_Button_Clear_Proj_Area, "Delete", false);
            Set_Minibutton(Mini_Button_Refresh_Proj_Area, "Refresh", false);
            Set_Minibutton(Mini_Button_Save_Projectile, "Save", false);
            Set_Minibutton(Mini_Button_Save_Projectile_As, "Save_As", false);

            Set_Minibutton(Mini_Button_Copy_Proj_Name, "Copy", false);
            Set_Minibutton(Mini_Button_Clear_Projectile, "Delete", false);
            Set_Minibutton(Mini_Button_Open_Proj_Variant, "Arrow", false);

                       
   
            Set_Minibutton(Mini_Button_Clear_HP_Area, "Delete", false);
            Set_Minibutton(Mini_Button_Refresh_HP_Area, "Refresh", false);
            Set_Minibutton(Mini_Button_Copy_HP_Name, "Copy", false);

            Set_Minibutton(Mini_Button_Clear_HP, "Delete", false);           
            Set_Minibutton(Mini_Button_Save_Hardpoint, "Save", false);
            Set_Minibutton(Mini_Button_Save_Hardpoint_As, "Save_As", false);
            Set_Button(Picture_Box_Cone_Width, "Radar_180");
            Set_Button(Picture_Box_Cone_Height, "Radar_90");
            Set_Button(Picture_Box_Turret_Rotation, "Radar_360");
            Set_Button(Picture_Box_Turret_Elevation, "Radar_45"); 
            Set_Button(Picture_Box_Turret_Location, "Radar_0"); 
    
         
            // Planet Subtab
            Mini_Button_Save_Conquest_MouseLeave(null, null);
            Mini_Button_Save_Conquest_As_MouseLeave(null, null);
            Mini_Button_Copy_Faction_MouseLeave(null, null);
            Operator_Add_Faction_MouseLeave(null, null);
            Operator_Remove_Faction_MouseLeave(null, null);
            Button_Open_Faction_Variant_MouseLeave(null, null);

            Mini_Button_Copy_Planet_MouseLeave(null, null);
            Operator_Remove_Planet_MouseLeave(null, null);
            Operator_Add_Planet_MouseLeave(null, null);
            Button_Open_Planet_Variant_MouseLeave(null, null);
            Mini_Button_Clear_Galaxy_MouseLeave(null, null);


            // Faction Subtab
            Mini_Button_Save_Faction_MouseLeave(null, null);
            Mini_Button_Save_Faction_As_MouseLeave(null, null);
            Operator_Remove_Faction_List_MouseLeave(null, null);
            Operator_Add_Faction_List_MouseLeave(null, null);
        }
    
        //=====================//

        // Requires using System.Windows.Forms;
        /* <summary>
        (In/De)creases a height of the «control» and the window «form», and moves accordingly
        down or up an elements in the «move_list». To decrease size you have to give a negative
        argument in the «the_sz».
        Usually used to collapse (or expand) an elements of a form, and to move the controls of
        the «move_list» down to fill an appeared gap.
        </summary>
        <param name="control">control to collapse/expand</param>
        <param name="form">form which would be resized accordingly after size of a control
        changed (give «null» if you don't want to)</param>
        <param name="move_list">A list of a controls that should also be moved up or down to
        «the_sz» size (e.g. to fill a gap after the «control» collapsed)</param>
        <param name="the_sz">size to change the control, form, and the «move_list»</param>
        */


        // Expander Box Function
        public static void ToggleControlY(Control Control, Form Expander_Object, List<Control> Move_List, int The_Size)
        {
            //→ Change Size of control
            if (Control != null) { Control.Height += The_Size; }

            //→ Change Size of Expander_Object
            if (Expander_Object != null) { Expander_Object.Height += The_Size; }
            //*** We leaved a gap(or intersected with another controls) now!
            //→ So, move up/down a list of a controls                  

            try
            {   foreach (Control Item in Move_List)
                {   try
                    {  Point Location = Item.Location;

                        Location.Y += The_Size;
                        Item.Location = Location;
                    } catch {}
                }            
           } catch {}

        }


        //=====================//
        // This function auto detects whether or not the Child Group Box is collapsed and adjusts size to it.
        // Keep The_Setting to null on startup so it doesen't mess with the saved setting, 
        // Then also set its parameter to "true" so it flipps into "false" state
         
        public void Expand_Box(GroupBox Expander_Parent, GroupBox Expander, GroupBox Expander_Child, Control The_Button, string The_Setting, string Setting_Value, List<Control> Move_List, int Subtract_Difference)
        {   bool Child_Is_Collapsed = false;
            int Delta = 0;
            int Minimized_Parent_Height = 0;
            int Minimized_Height = 0;
            int Difference_Height = 0;
            int Minimized_Child_Height = 0;


            if (Expander_Parent != null)
            { Minimized_Parent_Height = Expander_Parent.MaximumSize.Height - Expander_Parent.MinimumSize.Height; }

            if (Expander != null)
            {   Minimized_Height = Expander.MaximumSize.Height - Expander.MinimumSize.Height;
                Difference_Height = Expander.MaximumSize.Height - Subtract_Difference;
            }
      

            if (Expander_Child != null) 
            {   Minimized_Child_Height = Expander_Child.MaximumSize.Height - Expander_Child.MinimumSize.Height;
               
                if (Expander_Child.Height + Expander_Child.TabIndex <= Minimized_Child_Height) // = Collapsed State; This triggers wrong when Child has a own Child..
                {   Child_Is_Collapsed = true;
                    if (Expander != null) { Delta = Minimized_Height - Minimized_Child_Height; }
                    // Imperial_Console(500, 200, "= " + Expander_Child.Height + " < " + Minimized_Child_Height);
                }
                else if (Expander != null) { Delta = Minimized_Height; } // Cause Minimized_Child_Height is already inside here                         
            }


            if (Setting_Value == "true") // Collapsed
            {   if (The_Button != null) { The_Button.Text = "+"; }
                if (The_Setting != null) { Save_Setting(Setting, The_Setting, "false"); }
                if (Expander != null) { Expander.Height = Expander.MinimumSize.Height; }
                if (Expander_Parent != null) { Expander_Parent.Height = Expander_Parent.MaximumSize.Height - Minimized_Height; }                                                          
            }
            else if (Setting_Value == "false") // Expanded
            {   if (The_Button != null) { The_Button.Text = "-"; }
                if (The_Setting != null) { Save_Setting(Setting, The_Setting, "true"); }
             
                if (Child_Is_Collapsed)
                {   // Was Expander.MaximumSize.Height - Minimized_Child_Height before
                    if (Expander != null) { Expander.Height = Difference_Height - Minimized_Child_Height; }
                    if (Expander_Parent != null) { Expander_Parent.Height = Expander_Parent.MaximumSize.Height - Minimized_Child_Height; }
                }
                else
                {   if (Expander != null) { Expander.Height = Difference_Height; } // Was Expander.MaximumSize.Height before
                    // if (Expander_Parent != null) { Expander_Parent.Height = Minimized_Parent_Height + Delta; }
                    if (Expander_Parent != null) { Expander_Parent.Height = Expander_Parent.MaximumSize.Height + Minimized_Height; }
                }          
            }


            // Saving the Size of Child into TabIndex so it can be retrived later on above when this "Expander" is used as "Expander_Child"
            if (Expander != null & Expander_Child != null) { Expander.TabIndex = Expander_Child.Height; }



            try
            {   foreach (Control Item in Move_List)
                {   try
                    {   Point Location = Item.Location;

                        if (Setting_Value == "true") { Location.Y -= Delta; }
                        else { Location.Y += Delta; }
                        Item.Location = Location;
                    } catch {}
                }
            } catch { }

        }



        //=====================//


        public static Image Resize_Image(string Image_Path, string Image_Name, int new_width, int new_height)
        {   try 
            {   Bitmap image = new Bitmap(Image_Path + Image_Name);

                Bitmap new_image = new Bitmap(new_width, new_height);
                Graphics Picture = Graphics.FromImage((Image)new_image);
                Picture.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                Picture.DrawImage(image, 0, 0, new_width, new_height);
                return new_image;
            } catch { }
            return null;
        }

        
        public static Image Text_Image(string Image_Path, string Image_Name, int new_width, int new_height, int X_Position, int Y_Position, int Font_Size, Color Used_Color, string The_Text)
        {   try 
            {   Bitmap image = new Bitmap(Image_Path + Image_Name);

                
                Bitmap new_image = new Bitmap(new_width, new_height);
                Graphics Picture = Graphics.FromImage((Image)new_image);
                Picture.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.High;
                Picture.DrawImage(image, 0, 0, new_width, new_height);

                Brush myBrush = new System.Drawing.SolidBrush(Used_Color);
               
                // This part draws a text over the picture
                using (Font myFont = new Font("Georgia", Font_Size))
                {
                    Picture.DrawString(The_Text, myFont, myBrush, new Point(X_Position, Y_Position));
                }

                return new_image;
            } catch {}
            return null;
        }


        // This only processes a new aspect ratio 
        public static Size Get_Aspect_Ratio(Size Source, int Max_Width, int Max_Height)
        {
            decimal rnd = Math.Min(Max_Width / (decimal)Source.Width, Max_Height / (decimal)Source.Height);
            return new Size((int)Math.Round(Source.Width * rnd), (int)Math.Round(Source.Height * rnd));
        }



        //=====================// 

        // Console.WriteLine(Command(Git_Exe_Path, @"status")); // Without Local_Repo_Path it defaults to null 
        // Command(Git_Exe_Path, @"status", Local_Repo_Path); // = Silent Mode 
        public string Command(string Program_Path, string Arguments, string Target_Directory = null, bool Silent_Mode = false)
        {
            try
            {
                ProcessStartInfo procStartInfo = new ProcessStartInfo("cmd", "/c " + Program_Path + " " + Arguments);

                procStartInfo.RedirectStandardError = procStartInfo.RedirectStandardInput = procStartInfo.RedirectStandardOutput = true;
                procStartInfo.UseShellExecute = false;
                procStartInfo.CreateNoWindow = true;

                if (Target_Directory != null) { procStartInfo.WorkingDirectory = Target_Directory; }

                Process The_Process = new Process();
                The_Process.StartInfo = procStartInfo;
                The_Process.Start();

                StringBuilder Output = new StringBuilder();
                The_Process.OutputDataReceived += delegate(object sender, DataReceivedEventArgs e) { Output.AppendLine(e.Data); };
                The_Process.ErrorDataReceived += delegate(object sender, DataReceivedEventArgs e) { Output.AppendLine(e.Data); };

                The_Process.BeginOutputReadLine();
                The_Process.BeginErrorReadLine();
                The_Process.WaitForExit();
                // if (!Silent_Mode) { Console_Log += Output.ToString(); } 
                return Output.ToString();

            }
            catch { }

            return null;
        }


        //=====================// 
       
        void Adjust_Wallpaper (string Image_Path)
        {         
            // Turning the selected path into a Bitmap, then getting its sizes
            Bitmap Selected_Image = new Bitmap(Image_Path);
            Size Source = Selected_Image.Size;
            // -20 and -70 to compensate for program borders
            int Max_Width = this.Size.Width - 20;
            int Max_Height = this.Size.Height - 70;

            // Processing a new size according to the window size, in oder to adjust the background image to it 
            decimal rnd = Math.Max(Max_Width / (decimal)Source.Width, Max_Height / (decimal)Source.Height);
            Size New_Ratio = new Size((int)Math.Round(Source.Width * rnd), (int)Math.Round(Source.Height * rnd));

            // Setting Image size
            Background_Wallpaper.Size = New_Ratio;
            // Resizing the image to the new size
            Background_Wallpaper.Image = Resize_Image(Path.GetDirectoryName(Image_Path) + @"\", Path.GetFileName(Image_Path), New_Ratio.Width, New_Ratio.Height);               
        }


        //===========================//
        void Set_Checkmate_Color(ListView The_Box)
        {
            // Sorting here instead of the default property because of timing issues with the loop below
            The_Box.Sort();

            foreach (ListViewItem Item in The_Box.Items)
            {   // Every second Tag should have this value in order to create a checkmate pattern with good contrast
                if (Checkmate_Color == "true" & Item.Index % 2 == 0)
                {   Item.ForeColor = Color_03;
                    Item.BackColor = Color_07;
                }
                else
                {   Item.ForeColor = Color.Black;
                    Item.BackColor = Color.White;
                }
            }           
        }

        //===========================//
        void Refresh_Theme_List()
        {
            List_View_Theme_Selection.Items.Clear();

            // If the Mods directory was found 
            if (Directory.Exists(Program_Directory + "Themes"))
            {
                // We put all found directories inside of our target folder into a string table
                string[] File_Paths = Directory.GetDirectories(Program_Directory + "Themes");
                int FileCount = File_Paths.Count();

                // Cycling up from 0 to the total count of folders found above in this directory;
                for (int Cycle_Count = 0; Cycle_Count < FileCount; Cycle_Count = Cycle_Count + 1)
                {
                    // Getting the Name only from all folder paths, Cycle_Count increases by 1 in each cycle 
                    string List_Value = Path.GetFileName(File_Paths[Cycle_Count]);


                    // Inserting that folder name into the List View box                    
                    var Item = List_View_Theme_Selection.Items.Add(List_Value); 

                    Item.ForeColor = Color.Black;

                    // Every second item should have this value in order to create a checkmate pattern with good contrast
                    if (Checkmate_Color == "true" & Item.Index % 2 == 0)
                    {
                        Item.ForeColor = Color_03;
                        Item.BackColor = Color_07;
                    }

                }
            }

            else // Probably the path is wrong
            {
                List_View_Theme_Selection.Items.Add(Program_Directory + "Themes");
                List_View_Theme_Selection.Items.Add("Seems to not exist.");
            }
        }


        void Change_Theme(string Selected_Theme, bool Save_Background)
        {
            try
            {   // We are going to set that file as our background Image
                Save_Setting("2", "Background_Image_Path", @"""" + Selected_Theme + @"""");

                if (Save_Background) { Save_Setting("2", "Selected_Theme", @"""" + Selected_Theme + @""""); }
             
                Save_Setting(Setting, "Background_Image_A", "Background.jpg");

                Save_Setting(Setting, "Background_Image_B", "Background_Small.jpg");
                                              
                // Background_Wallpaper.Image = Image.FromFile(Selected_Theme + @"\Background.jpg");
                Adjust_Wallpaper(Selected_Theme + @"Background.jpg");

                Background_Image_Path = Selected_Theme;

                if (File.Exists(Selected_Theme + @"Buttons\Color_Picker.png")) { Picture_Box_Color_Picker.Image = Resize_Image(Selected_Theme + @"Buttons\", "Color_Picker.png", 320, 320); }
                else { Picture_Box_Color_Picker.Image = Resize_Image(Program_Directory + @"Themes\Default\Buttons\", "Color_Picker.png", 320, 320); }



                string Setting_File = Selected_Theme + "Colors.txt"; 

                if (File.Exists(Setting_File) & Auto_Color == "true")
                {   // Loading Colors
                    try { Color_01 = Load_Color(Setting_File, "Color_01"); } catch { Color_01 = Color.FromArgb(135, 206, 250, 255); } // Main Color
                    try { Color_02 = Load_Color(Setting_File, "Color_02"); } catch { Color_02 = Color.FromArgb(40, 130, 240, 220); } // Unselect

                    try { Color_03 = Load_Color(Setting_File, "Color_03"); } catch { Color_03 = Color.FromArgb(200, 200, 200, 200); } // Select
                    try { Color_04 = Load_Color(Setting_File, "Color_04"); } catch { Color_04 = Color.FromArgb(0, 0, 0, 255); } // Button Color

                    try { Color_05 = Load_Color(Setting_File, "Color_05"); } catch { Color_05 = Color.FromArgb(64, 64, 64, 240); } // UI Color
                    try { Color_06 = Load_Color(Setting_File, "Color_06"); } catch { Color_06 = Color.FromArgb(147, 112, 219, 255); } // Costum Tag Color
                    try { Color_07 = Load_Color(Setting_File, "Color_07"); } catch { Color_07 = Color.FromArgb(120, 120, 120, 255); } // Checker Color 


                    // Setting Colors: 
                    try { Set_Color(Color_01, 01); } catch { }
                    try { Set_Color(Color_02, 02); } catch { }

                    // The Checkboxes have their own function because of the checked/unchecked state
                    try { Set_Checkbox_Color(Color_02, false); } catch { }                  
                    try { Set_Checkbox_Color(Color_03, true); } catch { }

                    try { Set_Color(Color_03, 03); } catch { }
                    try { Set_Color(Color_04, 04); } catch { }

                    try { Set_Color(Color_05, 05); } catch { }
                    try { Set_Color(Color_06, 06); } catch { }


                    // Saving Colors: 
                    string Current_Color = Color_01.R.ToString() + ", " + Color_01.G.ToString() + ", " + Color_01.B.ToString() + ", " + Color_01.A.ToString();
                    Save_Setting("2", "Color_01", Current_Color);

                    Current_Color = Color_02.R.ToString() + ", " + Color_02.G.ToString() + ", " + Color_02.B.ToString() + ", " + Color_02.A.ToString();
                    Save_Setting("2", "Color_02", Current_Color);

                    Current_Color = Color_03.R.ToString() + ", " + Color_03.G.ToString() + ", " + Color_03.B.ToString() + ", " + Color_03.A.ToString();
                    Save_Setting("2", "Color_03", Current_Color);

                    Current_Color = Color_04.R.ToString() + ", " + Color_04.G.ToString() + ", " + Color_04.B.ToString() + ", " + Color_04.A.ToString();
                    Save_Setting("2", "Color_04", Current_Color);

                    Current_Color = Color_05.R.ToString() + ", " + Color_05.G.ToString() + ", " + Color_05.B.ToString() + ", " + Color_05.A.ToString();
                    Save_Setting("2", "Color_05", Current_Color);

                    Current_Color = Color_06.R.ToString() + ", " + Color_06.G.ToString() + ", " + Color_06.B.ToString() + ", " + Color_06.A.ToString();
                    Save_Setting("2", "Color_06", Current_Color);

                    Current_Color = Color_07.R.ToString() + ", " + Color_07.G.ToString() + ", " + Color_07.B.ToString() + ", " + Color_07.A.ToString();
                    Save_Setting("2", "Color_07", Current_Color);


                  

                    // Saving "Button_Color" after loading it from "Setting_File" inside of the selected Theme
                    try { Save_Setting(Setting, "Button_Color", Load_Setting(Setting_File, "Button_Color")); } catch {}
                    
                    // If not specified we try to get the Buttons inside of the current Theme
                    if (Button_Color == "") { Save_Setting(Setting, "Button_Color", List_View_Theme_Selection.SelectedItems[0].Text); }
                    Set_All_Switch_Buttons();

                    // Adjusting the new colors for the segment buttons
                    Set_Color_Segment_Buttons();
                    Set_All_Segment_Buttons();

                }

                Change_Button_Color();

                // Adjusting the new colors for launch buttons
                Set_Launch_Button(Button_Launch_Mod, "Button_Launch.png", "Button_Launch.png", 34, "Launch", 158);
                Button_Launch_Mod.Parent = Background_Wallpaper;

                Set_Launch_Button(Button_Mod_Only, "Button_Mod.png", "Button_Launch.png", 20, "Mod Only", 100);
                Button_Mod_Only.Parent = Background_Wallpaper;

                Set_Launch_Button(Button_Launch_Addon, "Button_Addon.png", "Button_Launch.png", 8, "Addon + Mod", 100);
                Button_Launch_Addon.Parent = Background_Wallpaper;


                Set_Minibutton(Button_Indicator_Player_A, "Indicator", false);
                Set_Minibutton(Button_Indicator_Player_B, "Indicator", false);
                Set_Minibutton(Button_Indicator_Player_C, "Indicator", false);

                Set_Minibutton(Button_Indicator_Home_A, "Indicator", false);
                Set_Minibutton(Button_Indicator_Home_B, "Indicator", false);
                Set_Minibutton(Button_Indicator_Home_C, "Indicator", false);

                Set_Minibutton(Button_Indicator_Story_A, "Indicator", false);
                Set_Minibutton(Button_Indicator_Story_B, "Indicator", false);
                Set_Minibutton(Button_Indicator_Story_C, "Indicator", false);

                Set_Minibutton(Button_Indicator_Forces_A, "Indicator", false);
                Set_Minibutton(Button_Indicator_Forces_B, "Indicator", false);
                Set_Minibutton(Button_Indicator_Forces_C, "Indicator", false);
               
            }
            catch
            {
                if (Debug_Mode == "true")
                {Imperial_Console(600, 100, "     Could not find the selected Theme in" 
                               + Add_Line + "     " + Program_Directory + @"Themes\" 
                               + Add_Line + "     please try it again.");}
            }
        
            // Showing the right tab, so the user gets the connection
            Tab_Control_01.SelectedIndex = 0;

        }

        //===========================//

        void Set_Launch_Button(PictureBox The_Button, string Selected_Button, string Alternative_Button, int Text_Distance, string Button_Name, int Button_Size)
        {             
            // This button has different size so it needs other treatment
            if (The_Button == Button_Launch_Mod)
            {              
                if (File.Exists(Selected_Theme + @"Buttons\" + Selected_Button))
                {
                    The_Button.Image = Text_Image(Selected_Theme + @"Buttons\", Selected_Button, Button_Size, Button_Size, Text_Distance, 60, 18, Color_03, Button_Name);
                }  // Otherwise we esacpe to Imperialware's default one
                else { The_Button.Image = Text_Image(Program_Directory + @"Themes\Default\Buttons\", Selected_Button, Button_Size, Button_Size, Text_Distance, 60, 18, Color_03, Button_Name); }
                return;
            }


            // This enables us to use a single source image if the other 2 are missing (because optioinal)
            if (File.Exists(Selected_Theme + @"Buttons\" + Selected_Button)) { The_Button.Image = Text_Image(Selected_Theme + @"Buttons\", Selected_Button, Button_Size, Button_Size, Text_Distance, 40, 10, Color_03, Button_Name); }
            else
            {
                if (File.Exists(Selected_Theme + @"Buttons\" + Alternative_Button))
                {// Otherwise we can alternatively use the Launch button for all 3 buttons
                    The_Button.Image = Text_Image(Selected_Theme + @"Buttons\", Alternative_Button, 100, 100, Text_Distance, 40, 10, Color_03, Button_Name);
                }
                else
                {// If the selected theme has no button at all we will use the standard ones from Imperialware's black theme            
                    The_Button.Image = Text_Image(Program_Directory + @"Themes\Default\Buttons\", Selected_Button, Button_Size, Button_Size, Text_Distance, 40, 10, Color_03, Button_Name);
                }               
            }                 
        }


        //===========================//
        // Toggle_Button(Switch_Button_Enable_All, "Button_On", "Button_Off", 0, true);

        void Toggle_Button(PictureBox The_Button, string Active_Button, string Inactive_Button, int Scale_Treshold, bool Is_On)
        {
            string Source_Directory = @"C:\Program Files\Imperialware\Themes\" + Button_Color + @"\";

            if (Is_On)
            {
                if (File.Exists(Source_Directory + @"Buttons\" + Active_Button + ".png"))
                { The_Button.BackgroundImage = Resize_Image(Source_Directory + @"Buttons\", Active_Button + ".png", The_Button.Size.Width + Scale_Treshold, The_Button.Size.Height + Scale_Treshold); }
                else try { The_Button.BackgroundImage = Resize_Image(Program_Directory + @"Themes\Default\Buttons\", Active_Button + ".png", The_Button.Size.Width + Scale_Treshold, The_Button.Size.Height + Scale_Treshold); } catch { }
            }
            else
            {
                if (File.Exists(Source_Directory + @"Buttons\" + Inactive_Button + ".png"))
                { The_Button.BackgroundImage = Resize_Image(Source_Directory + @"Buttons\", Inactive_Button + ".png", The_Button.Size.Width + Scale_Treshold, The_Button.Size.Height + Scale_Treshold); }
                else try { The_Button.BackgroundImage = Resize_Image(Program_Directory + @"Themes\Default\Buttons\", Inactive_Button + ".png", The_Button.Size.Width + Scale_Treshold, The_Button.Size.Height + Scale_Treshold); } catch { }
            }
        }


        //===========================//
        void Set_Galaxy(ListView The_Box)
        {
            // Removing all listed items from the List Box in order to refresh
            The_Box.Items.Clear();
            if (The_Box == List_View_Galaxy) { The_Box.Items.Add("All"); The_Box.Items.Add("Assamble Randomely"); }
     
            foreach (var Xml in Get_Xmls())
            {
                try // To prevent the program from crashing because of xml errors:  
                {
                    
                    // Loading the Xml File PATHS:
                    XElement Xml_File = XElement.Load(Xml);


                    // Defining a XElement List
                    IEnumerable<XElement> Content =

                    // Selecting all Tags with the string Value Campaign:
                    from All_Tags in Xml_File.Descendants("Campaign")
                    // Selecting all non empty tags (null because we need all selected)
                    where (string)All_Tags.Attribute("Name") != null
                    select All_Tags;

                    foreach (XElement Tags in Content)
                    {
                        // Putting these Element of the Tags into a string Variable  
                        string Selected_Tag = (string)Tags.FirstAttribute.Value;

                        // Setting last Galaxy
                        if (The_Box == List_Box_Galaxy) { Selected_Galaxy += Selected_Tag.ToString() + ","; }
                        else if (The_Box == List_View_Galaxy) { Selected_Editor_Galaxy += Selected_Tag.ToString() + ","; }
                      
                        The_Box.Items.Add(Selected_Tag);                                     
                    }                                                                                             
                } catch { }
            }


            Load_List_View_Formatting(The_Box); 

        }

        //===========================//
        void Set_Planets(ListView The_Box)
        {
            // Getting the currently selected Galaxy
            string Galaxy = "";
            string Galaxy_Xml = "";
            Found_Editor_Planets = "";

            // IMPORTRANT: This check prevents a timing exception!
            if (The_Box.SelectedItems.Count > 0)
            {
                if (The_Box.SelectedItems[0].Text != "") { Galaxy = The_Box.SelectedItems[0].Text; }

                
                // Making sure the button doesent show in Galactic Mode           
                if (Galaxy != "")
                {
                    foreach (var Xml in Get_Xmls())
                    {   // To prevent the program from crashing because of xml errors:          
                        try
                        {
                            // Loading the Xml File PATHS:
                            XElement Xml_File = XElement.Load(Xml);


                            // Defining a XElement List
                            IEnumerable<XElement> Content =

                            // Selecting all Tags with the current string Value:
                            from All_Tags in Xml_File.Descendants("Campaign")
                            // Selecting the tag with our desired Unit Attribute
                            where (string)All_Tags.Attribute("Name") == Galaxy
                            select All_Tags;


                            // Here we loop for each Tag in List_02 (which only contains the Attribute name of "Unit"
                            foreach (XElement Tags in Content)
                            {
                                // If this tag was found (and you need to be aware we are still in the Get_Xmls() loop and check this for each .xml) 
                                // Then we found the file that contains our unit and we return its Pathname
                                if (Tags != null)
                                {   // And we select it for editing
                                    Galaxy_Xml = Xml.ToString();
                                    if (The_Box == List_Box_Galaxy) { Label_Galaxy_File.Text = Path.GetFileName(Galaxy_Xml); }
                                    else { Label_Conquest_File.Text = Path.GetFileName(Galaxy_Xml); }
                                }
                            }
                        }
                        catch { }
                    }


                    if (The_Box == List_View_Galaxy) 
                    {   // Loading Galaxy to UI
                        //try { 
                            Load_Galaxy_To_UI(Xml_Directory + Label_Conquest_File.Text, Galaxy); Label_Galactic_Conquest.Text = Galaxy;
                       // } catch { Imperial_Console(640, 100, Add_Line + "    Sorry, I failed to load Galaxy to UI."); }
                    }


                    // If the Variable did not change by now that means, no files were matched and the User probably has files from a other mod in the cache variable 
                    if (Galaxy_Xml == "") { Imperial_Console(640, 100, Add_Line + "    These seem to be Galaxies of a other mod, please press \"Galaxy\" to refresh."); }

                    else if (The_Box == List_Box_Galaxy)
                    {   // Turning Selection button to pressable state
                        Picture_Box_Select_Planet.Visible = false;
                        Button_Select_Planet.Visible = true;
                    }
                }
                else { Imperial_Console(400, 100, Add_Line + "    Please select a Galactic Conquest."); }
            }
            else { Imperial_Console(400, 100, Add_Line + "    Please press Galaxy first."); }


            // ================================= Part II =================================
            // Now that we know which file contains our xml instance (The Attribute is its root tag), we load it
            try
            {
                // Removing all listed items from the List Box in order to replace the Galaxies for Planets, then the User can choose one
                The_Box.Items.Clear();


                foreach (string Entry in Xml_Utility(Galaxy_Xml, Galaxy, "Locations", null).Split(','))
                {
                    if (Entry != null & Entry != "")
                    {   // Using Regex to filter the Galaxy Model prop and Emptyspaces out                    
                        Temporal_A = Regex.Replace(Entry, ".*_Core_Art_Model$", "");
                        Temporal_B = Wash_String(Temporal_A);

                        if (Temporal_B != "") { The_Box.Items.Add(Temporal_B); }

                        if (The_Box == List_Box_Galaxy) { Found_Planets += Temporal_B + ","; }
                        else if (The_Box == List_View_Galaxy) { Found_Editor_Planets += Temporal_B + ","; }
                    }
                }

                Load_List_View_Formatting(The_Box); 
                        
            } catch { }
        }

        //=====================//
        void Load_Last_Galaxy(ListView The_Box)
        {
            try
            {
                Temporal_A = "";

                if (The_Box == List_Box_Galaxy)
                {   // Turning Selection button to unpressable state
                    Picture_Box_Select_Planet.Visible = true;
                    Button_Select_Planet.Visible = false;
                    Temporal_A = Selected_Galaxy;
                }
                else if (The_Box == List_View_Galaxy) { Temporal_A = Selected_Editor_Galaxy; }
          

                // Splitting all entries from this long string
                if (Temporal_A != null & Temporal_A != "") 
                {
                    // Removing all listed items from the List Box in order to refresh
                    The_Box.Items.Clear();

                    // This option enables us to show all Planets
                    if (The_Box == List_View_Galaxy) { List_View_Galaxy.Items.Add("All"); List_View_Galaxy.Items.Add("Assamble Randomely"); }


                    Temporal_E = Temporal_A.Split(',');

                    foreach (string Entry in Temporal_E)
                    {   // And listing it in the Galaxy List Box
                        The_Box.Items.Add(Entry);
                    }

                    Load_List_View_Formatting(The_Box);
                }               

            } catch { Imperial_Console(600, 100, Add_Line + "    There is no default mod set, please choose a Mod in the Mods tab."); }
        }
     
        
        //=====================//
        void Load_Last_Entries(ListView The_Box)
        {
            try
            {   Temporal_A = "";

                if (The_Box == List_Box_Galaxy)
                {   // Turning Selection button back to pressable state
                    Picture_Box_Select_Planet.Visible = false;
                    Button_Select_Planet.Visible = true;
                    Temporal_A = Found_Planets;
                }
                // else if (The_Box == List_View_Hard_Points) { Temporal_A = Found_Hard_Points; } TODO              
                else { Temporal_A = Found_Editor_Planets; }


                // Splitting all entries from this long string
                if (Temporal_A != null & Temporal_A != "") 
                {
                    // Removing all listed items from the List Box in order to refresh
                    The_Box.Items.Clear();

                    if (Temporal_A.StartsWith(",")) { Temporal_A.Substring(1, Temporal_A.Length - 1); }

                    // Splitting all entries from this long string (loaded from .txt file at the top of "Main_Window.cs"
                    foreach (string Object in Temporal_A.Split(','))
                    {   // And listing it in the Galaxy List Box
                        The_Box.Items.Add(Object);
                    }
                 
                    Load_List_View_Formatting(The_Box);              
                }
            }
            catch { Imperial_Console(600, 100, Add_Line + "    There is no default mod set, please choose a Mod in the Mods tab."); }
        }

        //=====================//
        void Load_List_View_Formatting(ListView The_Box)
        {   
            if (Radio_Button_Planet_4.Checked | The_Box == List_View_Galaxy)
            {
                The_Box.Sort();

                foreach (ListViewItem Item in The_Box.Items)
                {
                    Item.ForeColor = Color.Black;
                    Item.BackColor = Color.White;
                }
            }
            else if (The_Box == List_Box_Galaxy) { Set_Checkmate_Color(The_Box); }
        }

        //===========================//

        void Toggle_Planet_List(ListView List_View_Box)
        {
            if (List_View_Box.SelectedItems.Count > 0)
            {
                if (List_View_Box.SelectedItems[0].Text != "")
                {
                    ListViewItem Selected_Item = List_View_Box.SelectedItems[0];


                    if (Selected_Item.BackColor != Color_07)
                    {
                        Selected_Item.ForeColor = Color_03;
                        Selected_Item.BackColor = Color_07;
                        Current_Planet_List.Add(Selected_Item.Text);
                    }
                    else
                    {
                        Selected_Item.ForeColor = Color.Black;
                        Selected_Item.BackColor = Color.White;
                        Current_Planet_List.Remove(Selected_Item.Text);
                    }
                }


                Temporal_A = "";

                if (List_View_Box == List_Box_Galaxy) 
                {
                    // Saving the List                  
                    foreach (string Entry in Current_Planet_List)
                    { if (Entry != "") { Temporal_A += Entry + ", "; } }

                    if (Temporal_A != "")
                    { Save_Setting(Setting, "Planet_List", Temporal_A); }
                }

                else if (List_View_Box == List_View_Galaxy) 
                {  
                    foreach (string Entry in Conquest_Planet_List)
                    { if (Entry != "") { Temporal_A += Entry + ", "; } }

                    if (Temporal_A != "")  { Save_Setting(Setting, "Conquest_Planet_List", Temporal_A); }
                }
               
            }
        }

        //===========================//
        // Toggle_Conquest_List(List_View_Box, Conquest_Planet_List, "Conquest_Planet_List");

        void Toggle_Conquest_List(ListView List_View_Box, List<string> Selection_List, string Selection_List_Variable)
        {
            ListViewItem Selected_Item = List_View_Box.SelectedItems[0];

            if (Selected_Item.BackColor != Color_07)
            {
                Selected_Item.ForeColor = Color_03;
                Selected_Item.BackColor = Color_07;
                Selection_List.Add(Selected_Item.Text);
            }
            else
            {
                Selected_Item.ForeColor = Color.Black;
                Selected_Item.BackColor = Color.White;
                Selection_List.Remove(Selected_Item.Text);
            }
      

            if (Selection_List.Count() > 0) { Save_Setting(Setting, Selection_List_Variable, List_To_String(Selection_List)); }


            if (Selection_List_Variable == "Trade_Route_List")
            {   // Added to Selection list, and removing from Auto_Trade_Routes to conclude the swap
                for (int i = Auto_Trade_Routes.Count() - 1; i >= 0; i--)
                {
                    if (Auto_Trade_Routes[i] == Selected_Item.Text) { Auto_Trade_Routes.Remove(Auto_Trade_Routes[i]); }
                }

                if (Auto_Trade_Routes.Count() > 0) { Save_Setting(Setting, "Auto_Trade_Routes", List_To_String(Auto_Trade_Routes)); }
            }
         
        }
        //===========================//

        // CAUTION: This returns only the XMl names in a Table (to prevent for larger entries), so don't forget to add "Xml_Direcotry + " in front of it.
        string [] Refresh_Planet_Xmls()
        {
            Temporal_B = "";

            foreach (string The_File in Get_Xmls())
            {   // If the XML contains any Instance of the "Planet" Type
                if (Get_Instance_Table(The_File, "Planet").Count() > 1) { Temporal_B += Path.GetFileName(The_File) + ","; } 
            }

            Save_Setting("2", "Planet_Data_Files", Temporal_B);
            
            return Temporal_E = Temporal_B.Split(',');        
        }
   

        //===========================//
        string[] Get_Listed_Xmls(string Xml_File_Name) // "TraderouteFiles.xml"  "HardPointDataFiles.xml"
        {
            Temporal_B = "";

            if (File.Exists(Xml_Directory + Xml_File_Name))
            {   // Getting all <File> Content from TraderouteFiles.xml 
                Temporal_E = Xml_Utility(Xml_Directory + Xml_File_Name, "Root", "File", "Return_All").Split(',');
            }
            else // If nothing was found we assume the Mod uses just the Vanilla Game Files.
            {   if (Xml_File_Name == "TraderouteFiles.xml") { Temporal_E = new string[] { "TradeRoutes.xml" }; }
                else if (Xml_File_Name == "HardPointDataFiles.xml") { Temporal_E = new string[] { "HardPoints.xml", "CIN_HardPoints.xml", "HardPoints_Underworld.xml"}; }
            }

            try { Temporal_D = Temporal_E.Count() - 1; } catch {}
            Temporal_C = 0;

            foreach (string Entry in Temporal_E)
            {
                Temporal_C++;

                if (Entry != "") // + 1 to revert the -1 above, I think we need that -1 above because of the List View
                {   if (Temporal_D + 1 == Temporal_C) { Temporal_B += Entry; }
                    else { Temporal_B += Entry + ","; }
                }
            }

            if (Xml_File_Name == "TraderouteFiles.xml") { Save_Setting("2", "Traderoute_Data_Files", Temporal_B); }
            else if (Xml_File_Name == "HardPointDataFiles.xml") { Save_Setting("2", "Hard_Point_Data_Files", Temporal_B); }
                        
            return Temporal_E;
        }
        //===========================//

        void Get_Planet_Images()
        {
            Temporal_A = Mod_Name;
            Temporal_B = "";
            Planet_Images.Clear();
            Combo_Box_Planet_Model.Items.Clear();

            // This line just makes sure we use the same info directory for different version of the Stargate TPC Mod
            if (Mod_Name == "StargateAdmin" | Mod_Name == "StargateBeta" | Mod_Name == "StargateOpenBeta") { Temporal_A = "Stargate"; }

          

            try // Loading Map Planets
            {
                foreach (string Image in Directory.GetFiles(Program_Directory + @"Mods\" + Temporal_A + @"\Planets"))
                {
                    if (Image.EndsWith(".PNG") | Image.EndsWith(".png"))
                    {
                        Temporal_B = Path.GetFileNameWithoutExtension(Image);
                        Planet_Images.Add(Temporal_B);
                        if (!Combo_Box_Planet_Model.Items.Contains(Temporal_B)) { Combo_Box_Planet_Model.Items.Add(Temporal_B); }
                    }
                }
            } catch {}


            // If no Mod planet was found or if the User wants to load them
            if (Planet_Images.Count == 0 | Check_Box_Vanilla_Planets.Checked == true)
            {
                try // Loading Vanilla Game Planets
                {
                    foreach (string Image in Directory.GetFiles(Program_Directory + @"Mods\Vanilla_Game\Planets"))
                    {
                        if (Image.EndsWith(".PNG") | Image.EndsWith(".png"))
                        {
                            Temporal_B = Path.GetFileNameWithoutExtension(Image);

                            if (Temporal_B != "None") //Excluding the Lid image
                            {   Planet_Images.Add(Temporal_B);
                                Combo_Box_Planet_Model.Items.Add(Temporal_B);
                            }                          
                        }
                    }
                } catch {}
            }
        }
    
        //===========================//

        void Set_All_Switch_Buttons()
        {
            // Land Settings
            if (Toggle_Remove_Upon_Death) { Toggle_Button(Switch_Remove_Upon_Death, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Remove_Upon_Death, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Capture_Point) { Toggle_Button(Switch_Capture_Point, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Capture_Point, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Hover) { Toggle_Button(Switch_Hover, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Hover, "Button_On", "Button_Off", 0, false); }


            // Manage Tab
            if (Toggle_Enable_All) { Toggle_Button(Switch_Button_Enable_All, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Enable_All, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Build_Tab) { Toggle_Button(Switch_Button_Build_Tab, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Build_Tab, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Innitially_Locked) { Toggle_Button(Switch_Button_Innitially_Locked, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Innitially_Locked, "Button_On", "Button_Off", 0, false); }

            if (Is_Team) { Toggle_Button(Switch_Button_Is_Team, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Is_Team, "Button_On", "Button_Off", 0, false); }

            if (Use_Container) { Toggle_Button(Switch_Button_Use_Container, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Use_Container, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Is_Hero) { Toggle_Button(Switch_Button_Is_Hero, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Is_Hero, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Show_Head) { Toggle_Button(Switch_Button_Show_Head, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Show_Head, "Button_On", "Button_Off", 0, false); }

            if (Toggle_God_Mode) { Toggle_Button(Switch_Button_God_Mode, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_God_Mode, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Use_Particle) { Toggle_Button(Switch_Button_Use_Particle, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Use_Particle, "Button_On", "Button_Off", 0, false); }

            // Projectiles
            if (Toggle_Target_On_Terrain) { Toggle_Button(Switch_Target_On_Terrain, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Target_On_Terrain, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Stun_On_Detonation) { Toggle_Button(Switch_Stun_On_Detonation, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Stun_On_Detonation, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Disable_Engines) { Toggle_Button(Switch_Projectile_Disable_Engines, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Projectile_Disable_Engines, "Button_On", "Button_Off", 0, false); }




            // Hardpoints
            if (Toggle_Auto_Fire) { Toggle_Button(Switch_Button_Auto_Fire, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Auto_Fire, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Is_Targatable) { Toggle_Button(Switch_Is_Targatable, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Is_Targatable, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Is_Destroyable) { Toggle_Button(Switch_Is_Destroyable, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Is_Destroyable, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Is_Repairable) { Toggle_Button(Switch_Is_Repairable, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Is_Repairable, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Is_Turret) { Toggle_Button(Switch_Is_Turret, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Is_Turret, "Button_On", "Button_Off", 0, false); }





            // Conquest Tab
            if (Toggle_Surface_Accessible) { Toggle_Button(Switch_Button_Surface_Accessible, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Surface_Accessible, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Is_Pre_Lit) { Toggle_Button(Switch_Button_Is_Pre_Lit, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Is_Pre_Lit, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Planet_Animation) { Toggle_Button(Switch_Button_Planet_Animation, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Planet_Animation, "Button_On", "Button_Off", 0, false); }


            if (Toggle_Is_Playable) { Toggle_Button(Switch_Button_Is_Playable, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Is_Playable, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Is_Multiplayer) { Toggle_Button(Switch_Button_Multiplayer, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Button_Multiplayer, "Button_On", "Button_Off", 0, false); }

            if (Is_Steam_Version == "true")
            {
                Toggle_Button(Switch_Button_Is_Steam_Version, "Button_On", "Button_Off", 0, true);
                Check_Box_Disable_Workshop.Visible = true;
                Check_Box_Debug_Mode_Game.Visible = true;
                Label_Log_File_Location.Visible = true;
                Text_Box_Debug_Path.Visible = true;
            }
            else
            {
                Toggle_Button(Switch_Button_Is_Steam_Version, "Button_On", "Button_Off", 0, false);
                Check_Box_Disable_Workshop.Visible = false;
                Check_Box_Debug_Mode_Game.Visible = false;
                Text_Box_Debug_Path.Visible = false;
                Label_Log_File_Location.Visible = false;
            }


            // Projectile Tab
            if (Toggle_Shield_Damage) { Toggle_Button(Switch_Shield_Damage, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Shield_Damage, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Energy_Damage) { Toggle_Button(Switch_Energy_Damage, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Energy_Damage, "Button_On", "Button_Off", 0, false); }

            if (Toggle_Hitpoint_Damage) { Toggle_Button(Switch_Hitpoint_Damage, "Button_On", "Button_Off", 0, true); }
            else { Toggle_Button(Switch_Hitpoint_Damage, "Button_On", "Button_Off", 0, false); }


        }

        //===========================//
        void Set_All_Mini_Buttons()
        {           
            Set_Minibutton(Mini_Button_Refresh_Mods, "Refresh", false);
            Set_Minibutton(Mini_Button_Save_Mods, "Save", false);
            Set_Minibutton(Mini_Button_Save_Addon_Info, "Save", false);

            Set_Minibutton(Button_Refresh_Galaxy_Area, "Refresh", false);
            Set_Minibutton(Operator_Decimate_Planets, "Minus", false);
            Set_Minibutton(Operator_Add_Conquest, "Plus", false);
            Set_Minibutton(Mini_Button_Clear_Galaxy, "Delete", false);

            Set_Minibutton(Operator_Starting_Forces_Minus, "Minus", false);
            Set_Minibutton(Operator_Starting_Forces_Plus, "Plus", false);

            Set_Minibutton(Mini_Button_Restrictions, "Delete", false); 
        }  

        //===========================//

        
        void Render_Segment_Button(PictureBox The_Button, string Button_Type, bool Is_Highlighted, int New_Width, int New_Height, int X_Position, int Y_Position, int Font_Size, Color Selected_Color, string The_Text)
        {
            string Button_Mode = "";
            string Source_Directory = @"C:\Program Files\Imperialware\Themes\" + Button_Color + @"\";

            try 
            {   // Need the Button Mode to decide whether to take Highlighted or normal version
                if (Is_Highlighted) { Button_Mode = "_Highlighted.png"; } else { Button_Mode = ".png"; }

                if (File.Exists(Source_Directory + @"Buttons\Segment_Button_" + Button_Type + Button_Mode))
                { The_Button.Image = Text_Image(Source_Directory + @"Buttons\", "Segment_Button_" + Button_Type + Button_Mode, New_Width, New_Height, X_Position, Y_Position, Font_Size, Selected_Color, The_Text); }
                // Otherwise we set the one from the default Theme
                else { The_Button.Image = Text_Image(Program_Directory + @"Themes\Default\Buttons\", "Segment_Button_" + Button_Type + Button_Mode, New_Width, New_Height, X_Position, Y_Position, Font_Size, Selected_Color, The_Text); }               
            }
            catch
            {
                if (Is_Highlighted) { Button_Mode = "_Highlighted.jpg"; } else { Button_Mode = ".jpg"; }

                if (File.Exists(Source_Directory + @"Buttons\Segment_Button_" + Button_Type + Button_Mode))
                { The_Button.Image = Text_Image(Source_Directory + @"Buttons\", "Segment_Button_" + Button_Type + Button_Mode, New_Width, New_Height, X_Position, Y_Position, Font_Size, Selected_Color, The_Text); }
                // Otherwise we set the one from the default Theme
                else { The_Button.Image = Text_Image(Program_Directory + @"Themes\Default\Buttons\", "Segment_Button_" + Button_Type + Button_Mode, New_Width, New_Height, X_Position, Y_Position, Font_Size, Selected_Color, The_Text); }                        
            }
        }

        //===========================//
        void Set_All_Segment_Buttons()
        {
            // NOTE: The Function Make_Backgrounds_Translucent() setts all background button Colors to Color.Transparent

            // Setting "Planet_Switch" Segment Button
            if (Planet_Switch == "Galaxy")
            {
                Render_Segment_Button(Button_Refresh_Galaxy, "Left", true, 82, 34, 17, 8, 10, Color_03, "Galaxy");
                Render_Segment_Button(Button_Refresh_Planets, "Right", false, 82, 34, 17, 8, 10, Color_04, "Planets");
            }
            else if (Planet_Switch != "Galaxy")
            {
                Render_Segment_Button(Button_Refresh_Galaxy, "Left", false, 82, 34, 17, 8, 10, Color_04, "Galaxy");
                Render_Segment_Button(Button_Refresh_Planets, "Right", true, 82, 34, 17, 8, 10, Color_03, "Planets");
            }


            // Setting "Teleportation_Mode" Segment Button  
            if (Teleportation_Mode == "Planets")
            {   // Choosing Teleport Method
                Teleport_from_Planet = "Teleport_from_Planet = true";

                Render_Segment_Button(Button_Teleport_Planets, "Left", true, 82, 34, 12, 6, 13, Color_03, "Planets");
                Render_Segment_Button(Button_Teleport_Both, "Middle", false, 82, 34, 20, 6, 13, Color_04, "Both");
                Render_Segment_Button(Button_Teleport_Units, "Right", false, 82, 34, 15, 6, 13, Color_04, "Units");
            }
            else if (Teleportation_Mode == "Planets_and_Units")
            {
                Teleport_Units_on_Planet = "Teleport_Both = true";

                Render_Segment_Button(Button_Teleport_Planets, "Left", false, 82, 34, 12, 6, 13, Color_04, "Planets");
                Render_Segment_Button(Button_Teleport_Both, "Middle", true, 82, 34, 20, 6, 13, Color_03, "Both");
                Render_Segment_Button(Button_Teleport_Units, "Right", false, 82, 34, 15, 6, 13, Color_04, "Units");
            }
            else if (Teleportation_Mode == "Units")
            {
                Teleport_Units = "Teleport_Units = true";

                Render_Segment_Button(Button_Teleport_Planets, "Left", false, 82, 34, 12, 6, 13, Color_04, "Planets");
                Render_Segment_Button(Button_Teleport_Both, "Middle", false, 82, 34, 20, 6, 13, Color_04, "Both");
                Render_Segment_Button(Button_Teleport_Units, "Right", true, 82, 34, 15, 6, 13, Color_03, "Units");
            }


            // Units (These are 3 the large Tabs)
            if (Unit_Switch == "Mod" | Unit_Switch == "Chosen_Xml") { Button_Unit_Mod_Click(null, null); }
            else if (Unit_Switch == "Selection") { Button_Unit_Mod_Click(null, null); }

            // Projectiles
            if (Projectile_Switch == "Mod" | Projectile_Switch == "Chosen_Xml") { Button_Projectiles_Mod_Click(null, null); }
            else if (Projectile_Switch == "Selection") { Button_Projectiles_Selection_Click(null, null); }

            // Hard Point Switch, calling the full functions in order to load the menu
            if (Hard_Point_Switch == "Mod" | Hard_Point_Switch == "Chosen_Xml") { Button_Hardpoints_Mod_Click(null, null); }
            else if (Hard_Point_Switch == "Selection") { Button_Hardpoints_Selection_Click(null, null); }


            

            if (Fire_Mode_Switch == "Deployed") { Button_Fire_Deployed_Click(null, null); }
            else if (Fire_Mode_Switch == "Both") { Button_Fire_Both_Click(null, null); }
            else if (Fire_Mode_Switch == "Undeployed") { Button_Fire_Undeployed_Click(null, null); }




            // Setting "Mod Language" Segment Button
            if (Language_Mode == "Game")
            {
                Render_Segment_Button(Button_Use_Game_Language, "Left", true, 85, 36, 6, 8, 10, Color_03, "From Game");
                Render_Segment_Button(Button_Use_Mod_Language, "Right", false, 85, 36, 6, 8, 10, Color_04, "From Mod");
            }
            else if (Language_Mode == "Mod")
            {
                Render_Segment_Button(Button_Use_Game_Language, "Left", false, 85, 36, 6, 8, 10, Color_04, "From Game");
                Render_Segment_Button(Button_Use_Mod_Language, "Right", true, 85, 36, 6, 8, 10, Color_03, "From Mod");
            }
            else if (Language_Mode == "false")
            {
                Render_Segment_Button(Button_Use_Game_Language, "Left", false, 85, 36, 6, 8, 10, Color_04, "From Game");
                Render_Segment_Button(Button_Use_Mod_Language, "Right", false, 85, 36, 6, 8, 10, Color_04, "From Mod");
            }


            // Setting "Vanilla Command Bar" Segment Button: This one always starts as EAW so we dont care to store it in settings
            Render_Segment_Button(Button_EAW_Command_Bar, "Left", true, 85, 36, 22, 8, 12, Color_03, "EAW");
            Render_Segment_Button(Button_FOC_Command_Bar, "Right", false, 85, 36, 22, 8, 12, Color_04, "FOC");


            // Calling from function because this code is used multiple times
            Set_Class_Button();

            Set_Navigation_Segment_Button();
            Set_Galactic_Segment_Buttons();
            Set_Faction_List_Button();


            // As Selected_Ability and Switch_Required_Object innitiate with the value 1 on startup, so the first one will fire        
            if (Selected_Ability == 1) { Render_Segment_Button(Button_Primary_Ability, "Left", true, 85, 36, 11, 8, 12, Color_03, "Primary"); }
            else { Render_Segment_Button(Button_Primary_Ability, "Left", false, 85, 36, 18, 9, 10, Color_04, "Primary"); }

            if (Selected_Ability == 2) { Render_Segment_Button(Button_Secondary_Ability, "Right", true, 85, 36, 2, 8, 12, Color_03, "Secondary"); }
            else { Render_Segment_Button(Button_Secondary_Ability, "Right", false, 85, 36, 6, 9, 10, Color_04, "Secondary"); }


            if (Switch_Required_Object == 1) { Render_Segment_Button(Button_Required_Planets, "Left", true, 85, 36, 13, 8, 12, Color_03, "Planets"); }
            else { Render_Segment_Button(Button_Required_Planets, "Left", false, 85, 36, 20, 9, 10, Color_04, "Planets"); }

            if (Switch_Required_Object == 2) { Render_Segment_Button(Button_Required_Structures, "Right", true, 85, 36, 1, 8, 12, Color_03, "Structures"); }
            else { Render_Segment_Button(Button_Required_Structures, "Right", false, 85, 36, 9, 8, 10, Color_04, "Structures"); }


            if (Projectile_Mode_Switch == "Ground") { Button_Ground_Click(null, null); }
            else if (Projectile_Mode_Switch == "Both") { Button_Use_Both_Click(null, null); }
            else if (Projectile_Mode_Switch == "Space") { Button_Space_Click(null, null); }

            if (Projectile_Size_Switch == "Small") { Button_Projectile_Small_Click(null, null); }
            else if (Projectile_Size_Switch == "Medium") { Button_Projectile_Medium_Click(null, null); }
            else if (Projectile_Size_Switch == "Large") { Button_Projectile_Large_Click(null, null); }

            if (Projectile_Target_Switch == "Stick") { Button_Stick_Click(null, null); }
            else if (Projectile_Target_Switch == "Ignore") { Button_Ignore_Click(null, null); }
            else if (Projectile_Target_Switch == "Explode") { Button_Explode_Click(null, null); }
            
            
        }


        //===========================//
        void Set_Class_Button()
        {
            if (Maximal_Value_Class == "1") { Render_Segment_Button(Button_Class_1, "Left", true, 82, 34, 11, 6, 13, Color_03, "Fighter"); }
            else { Render_Segment_Button(Button_Class_1, "Left", false, 82, 34, 11, 6, 13, Color_04, "Fighter"); }

            if (Maximal_Value_Class == "2") { Render_Segment_Button(Button_Class_2, "Middle", true, 82, 34, 9, 6, 13, Color_03, "Bomber"); }
            else { Render_Segment_Button(Button_Class_2, "Middle", false, 82, 34, 9, 6, 13, Color_04, "Bomber"); }

            if (Maximal_Value_Class == "3") { Render_Segment_Button(Button_Class_3, "Middle", true, 82, 34, 8, 6, 13, Color_03, "Corvette"); }
            else { Render_Segment_Button(Button_Class_3, "Middle", false, 82, 34, 8, 6, 13, Color_04, "Corvette"); }

            if (Maximal_Value_Class == "4") { Render_Segment_Button(Button_Class_4, "Middle", true, 82, 34, 12, 6, 13, Color_03, "Frigate"); }
            else { Render_Segment_Button(Button_Class_4, "Middle", false, 82, 34, 12, 6, 13, Color_04, "Frigate"); }

            if (Maximal_Value_Class == "5") { Render_Segment_Button(Button_Class_5, "Right", true, 82, 34, 10, 6, 13, Color_03, "Capital"); }
            else { Render_Segment_Button(Button_Class_5, "Right", false, 82, 34, 10, 6, 13, Color_04, "Capital"); }

            if (Maximal_Value_Class == "6") { Render_Segment_Button(Button_Class_6, "Left", true, 82, 34, 8, 6, 13, Color_03, "Infantry"); }
            else { Render_Segment_Button(Button_Class_6, "Left", false, 82, 34, 8, 6, 13, Color_04, "Infantry"); }

            if (Maximal_Value_Class == "7") { Render_Segment_Button(Button_Class_7, "Middle", true, 82, 34, 11, 6, 13, Color_03, "Vehicle"); }
            else { Render_Segment_Button(Button_Class_7, "Middle", false, 82, 34, 11, 6, 13, Color_04, "Vehicle"); }

            if (Maximal_Value_Class == "8") { Render_Segment_Button(Button_Class_8, "Middle", true, 82, 34, 27, 6, 13, Color_03, "Air"); }
            else { Render_Segment_Button(Button_Class_8, "Middle", false, 82, 34, 27, 6, 13, Color_04, "Air"); }

            if (Maximal_Value_Class == "9") { Render_Segment_Button(Button_Class_9, "Middle", true, 82, 34, 20, 6, 13, Color_03, "Hero"); }
            else { Render_Segment_Button(Button_Class_9, "Middle", false, 82, 34, 20, 6, 13, Color_04, "Hero"); }

            if (Maximal_Value_Class == "10") { Render_Segment_Button(Button_Class_10, "Right", true, 82, 34, 1, 6, 13, Color_03, "Structure"); }
            else { Render_Segment_Button(Button_Class_10, "Right", false, 82, 34, 1, 6, 13, Color_04, "Structure"); }

        }
    
        //===========================//

        void Set_Navigation_Segment_Button()
        {
            if (Switch_Navigation_Button == "1") { Render_Segment_Button(Button_Move_Mode, "Left", true, 82, 34, 17, 8, 10, Color_03, " Move"); }
            else { Render_Segment_Button(Button_Move_Mode, "Left", false, 82, 34, 17, 8, 10, Color_04, " Move"); }
       
            if (Switch_Navigation_Button == "2") { Render_Segment_Button(Button_Creation_Mode, "Middle", true, 82, 34, 17, 8, 10, Color_03, "Creation"); }
            else { Render_Segment_Button(Button_Creation_Mode, "Middle", false, 82, 34, 17, 8, 10, Color_04, "Creation"); }

            if (Switch_Navigation_Button == "3") { Render_Segment_Button(Button_Trade_Routes_Mode, "Right", true, 82, 34, 17, 8, 10, Color_03, "Routes"); }
            else { Render_Segment_Button(Button_Trade_Routes_Mode, "Right", false, 82, 34, 17, 8, 10, Color_04, "Routes"); }
        }

        //===========================//

        void Set_Galactic_Segment_Buttons()
        {   
            if (Switch_Conquest_Button == "1") { Render_Segment_Button(Segment_Button_Galaxy, "Left", true, 82, 34, 17, 8, 10, Color_03, "Galaxy"); }
            else { Render_Segment_Button(Segment_Button_Galaxy, "Left", false, 82, 34, 17, 8, 10, Color_04, "Galaxy"); }

            if (Switch_Conquest_Button == "2") { Render_Segment_Button(Segment_Button_Planets, "Middle", true, 82, 34, 17, 8, 10, Color_03, "Planets"); }
            else { Render_Segment_Button(Segment_Button_Planets, "Middle", false, 82, 34, 17, 8, 10, Color_04, "Planets"); }

            if (Switch_Conquest_Button == "3") { Render_Segment_Button(Segment_Button_Planet_Pool, "Middle", true, 82, 34, 17, 8, 10, Color_03, "  Pool"); }
            else { Render_Segment_Button(Segment_Button_Planet_Pool, "Middle", false, 82, 34, 17, 8, 10, Color_04, "  Pool"); }

            if (Switch_Conquest_Button == "4") { Render_Segment_Button(Segment_Button_Trade_Routes, "Right", true, 82, 34, 17, 8, 10, Color_03, "Routes"); }
            else { Render_Segment_Button(Segment_Button_Trade_Routes, "Right", false, 82, 34, 17, 8, 10, Color_04, "Routes"); }
        }

        //===========================//

        void Set_Faction_List_Button()
        {
            if (Switch_Faction_List == "1") { Render_Segment_Button(Button_Faction_List_1, "Left", true, 82, 34, 11, 7, 12, Color_03, "Enemies"); }
            else { Render_Segment_Button(Button_Faction_List_1, "Left", false, 82, 34, 11, 7, 11, Color_04, "Enemies"); }

            if (Switch_Faction_List == "2") { Render_Segment_Button(Button_Faction_List_2, "Middle", true, 82, 34, 11, 7, 12, Color_03, "  Allies"); }
            else { Render_Segment_Button(Button_Faction_List_2, "Middle", false, 82, 34, 11, 7, 11, Color_04, "  Allies"); }

            if (Switch_Faction_List == "3") { Render_Segment_Button(Button_Faction_List_3, "Middle", true, 82, 34, 9, 7, 12, Color_03, "SK Space"); }
            else { Render_Segment_Button(Button_Faction_List_3, "Middle", false, 82, 34, 9, 7, 11, Color_04, "SK Space"); }

            if (Switch_Faction_List == "4") { Render_Segment_Button(Button_Faction_List_4, "Middle", true, 82, 34, 11, 7, 12, Color_03, "SK Land"); }
            else { Render_Segment_Button(Button_Faction_List_4, "Middle", false, 82, 34, 11, 7, 11, Color_04, "SK Land"); }

            if (Switch_Faction_List == "5") { Render_Segment_Button(Button_Faction_List_5, "Right", true, 82, 34, 6, 7, 12, Color_03, "MP Hero"); }
            else { Render_Segment_Button(Button_Faction_List_5, "Right", false, 82, 34, 6, 7, 11, Color_04, "MP Hero"); }           
        }

        //===========================//
        void Set_Color_Segment_Buttons()
        {           
            if (Button_Color == "Red") 
            { Render_Segment_Button(Button_Color_Switch_1, "Left", true, 60, 34, 14, 6, 13, Color.White, "Red"); }
            else { Render_Segment_Button(Button_Color_Switch_1, "Left", false, 60, 34, 14, 6, 13, Color.Black, "Red"); }
              
            if (Button_Color == "Ice")
            { Render_Segment_Button(Button_Color_Switch_2, "Middle", true, 60, 34, 11, 6, 13, Color.White, "Blue"); }
            else { Render_Segment_Button(Button_Color_Switch_2, "Middle", false, 60, 34, 11, 6, 13, Color.Black, "Blue"); }
                
            if (Button_Color == "Green_Matrix")
            { Render_Segment_Button(Button_Color_Switch_3, "Middle", true, 60, 34, 6, 6, 13, Color.White, "Green"); }
            else { Render_Segment_Button(Button_Color_Switch_3, "Middle", false, 60, 34, 6, 6, 13, Color.Black, "Green"); }
                
            if (Button_Color == "Gold")
            { Render_Segment_Button(Button_Color_Switch_4, "Middle", true, 60, 34, 3, 6, 13, Color.White, "Yellow"); }
            else { Render_Segment_Button(Button_Color_Switch_4, "Middle", false, 60, 34, 3, 6, 13, Color.Black, "Yellow"); }
                
            if (Button_Color == "Pink")
            { Render_Segment_Button(Button_Color_Switch_5, "Middle", true, 60, 34, 10, 6, 13, Color.White, "Pink"); }
            else { Render_Segment_Button(Button_Color_Switch_5, "Middle", false, 60, 34, 10, 6, 13, Color.Black, "Pink"); }
                
            if (Button_Color == "Stargate")
            { Render_Segment_Button(Button_Color_Switch_6, "Middle", true, 60, 34, 9, 6, 13, Color.White, "Cyan"); }
            else { Render_Segment_Button(Button_Color_Switch_6, "Middle", false, 60, 34, 9, 6, 13, Color.Black, "Cyan"); }
                
            if (Button_Color == "Metal")
            { Render_Segment_Button(Button_Color_Switch_7, "Right", true, 60, 34, 6, 6, 13, Color.White, "Silver"); }
            else { Render_Segment_Button(Button_Color_Switch_7, "Right", false, 60, 34, 6, 6, 13, Color.Black, "Silver"); }
        }

        //===========================//
        List<string> Get_Mod_Info_Directories()
        {   // Innitiating string list 
            List<string> Found_Objects = new List<string>();


            // Refreshing Values
            Selected_Object = Load_Setting(Setting, "Selected_Object");
            Info_Directory_Path = Program_Directory + Selected_Object + @"\";



            // If a data directory of this Mod/Addon is found in Imperialwares Directory
            if (Directory.Exists(Info_Directory_Path))
            {
                // We put all found directories inside of our target folder into a string table
                string[] File_Paths = Directory.GetDirectories(Info_Directory_Path);
                int Folder_Count = File_Paths.Count();


                // Cycling up from 0 to the total count of folders found above in this directory;
                for (int Cycle_Count = 0; Cycle_Count < Folder_Count; Cycle_Count = Cycle_Count + 1)
                {
                    // Getting the Name only from all folder paths, Cycle_Count increases by 1 in each cycle 
                    string File_Name = Path.GetFileName(File_Paths[Cycle_Count]);
                    // And inserting that folder name into the List
                    Found_Objects.Add(File_Name);
                }
            }

            return Found_Objects;
        }


        //=====================//
   
        void Refresh_Apps()
        {
            // Getting rid of the whole site with old row style
            Table_Layout_Panel_Appstore.Controls.Clear();

            string Object_Path = "";
            string Image_Name = "01.png";
            string Image_Text = "";
            int Cycle_Count = 0;


            foreach (string Item in Get_Mod_Info_Directories())
            {
                // Ignoring the Template Directory
                if (Item != "Mod_Template") 
                {

                    // Exception for some mods with own images, there it will search their Addon directory
                    string Exception_List = "Wrackage_and_Bodies_Stay_Mod";
                    if (Exception_List.Contains(Item) & Selected_Object == "Addons") { Object_Path = Info_Directory_Path + Item + @"\Images\"; }

                   

                    else if (Selected_Object == "Addons")
                    {
                        // Gathering Mod and Game Information of the selected Addon    
                        string Info_File = Program_Directory + @"Addons\" + Item + @"\Info.txt";                       
                        string Host_Mod = Load_Setting(Info_File, "Mod");

                        // There are currently no Addons for TPC, so we choose Stargate - EAW
                        if (Host_Mod == "Stargate") { Host_Mod = "Stargate_Empire_at_War"; }

                        // Setting image path according to the mod inside of the Info.txt file
                        Object_Path = Program_Directory + @"Mods\" + Host_Mod + @"\Images\";
                    }
                    else { Object_Path = Info_Directory_Path + Item + @"\Images\"; }
                             


                    Image_Name = "01.png";
                    // If no .jpg was found we switch to .png
                    if (!File.Exists(Object_Path + Image_Name)) { Image_Name = "01.jpg"; }

                    // Otherwise we chose a black background image with directory name in white text
                    if (!File.Exists(Object_Path + Image_Name))
                    {   Object_Path = Program_Directory + @"Images\";
                        Image_Name = "Plain_Mod_Image.jpg";
                        Image_Text = Item;
                    }

                    // Showing name of the Stargate Addons
                    //if (Item.Contains("Stargate")) { Image_Text = Item; }



                    // Add a new Control button with the following specifications
                    var New_Button = new Button()
                    {
                        Name = Item,
                        Text = Image_Text,
                        ForeColor = Color.White,
                        // BackColor = Color_02,
                        Width = 120,
                        Height = 120,
                        Image = Resize_Image(Object_Path, Image_Name, 120, 120),
                    };


                    New_Button.Click += new EventHandler(App_Image_Click);

                    // The last 2 values specify button position in column and row, we need to place new buttons at the very end after the first row was filled with 4 buttons.
                    if (Cycle_Count < 5) { Table_Layout_Panel_Appstore.Controls.Add(New_Button, 0, 0); }
                    else { Table_Layout_Panel_Appstore.Controls.Add(New_Button, Table_Layout_Panel_Appstore.ColumnCount, 0); }


                    Image_Text = "";
                    Cycle_Count++;
                }
            }

        }

        

        //=====================//
        void Remove_Apps()
        {  
           Table_Layout_Panel_Appstore.Controls.Clear();
           Table_Layout_Panel_Appstore.ColumnCount = 4;
        }

      
        //=====================//
        void Load_Recent_Units()
        {   try
            {   // Removing all listed items from the List Box in order to refresh
                List_Box_All_Instances.Items.Clear();

                foreach (string Unit in Found_Units)
                {   if (Unit != "")
                    {   // And listing it in the Galaxy List Box
                        List_Box_All_Instances.Items.Add(Unit);
                    }
                }

                Set_Checkmate_Color(List_Box_All_Instances);
            } catch {}
        }

        //=====================//


        public List<string> Get_Xmls()
        {
            // Removing all listed items from the listbox in order to refresh
            List_Box_Xmls.Items.Clear();

            List<string> All_Xmls = new List<string>();
          

            // Trying it because if the player still has no mod setted it would cause an exception:
            try 
            {
                // If the Xml directory was found 
                if (Directory.Exists(Xml_Directory))
                {             
                    // We are going to store the found xmls including their path in this list, so the program can access it for other purposes
                    foreach (string The_File in Directory.GetFiles(Xml_Directory, "*.xml", System.IO.SearchOption.AllDirectories))
                    {All_Xmls.Add(The_File);}
                    // if (The_File.EndsWith(".XML") | The_File.EndsWith(".xml")) { All_Xmls.Add(The_File); }


                    int File_Count = All_Xmls.Count();

                     // Cycling up from 0 to the total count of files found above in this directory;
                    for (int Cycle_Count = 0; Cycle_Count < File_Count; Cycle_Count = Cycle_Count + 1)
                    {
                        // Getting the Name only of all files, Cycle_Count increases by 1 in each cycle 
                        string List_Value = Path.GetFileName(All_Xmls[Cycle_Count]);
                        // And inserting that file name into the Listbox
                        List_Box_Xmls.Items.Add(List_Value);
                    }
                }
                else
                { Imperial_Console(620, 100, Add_Line + "    No Mod directory found, please set a Mod in the Mods tab."
                                           + Add_Line + "    Or the wrong Game checkbox is selected, then you could"
                                           + Add_Line + "    try selecting the other Game."); return null;
                }
            } catch { }
                                  

            return All_Xmls;
        }
        //=====================//

        void Check_Cheat_Dummy()
        {
            if (!Directory.Exists(Xml_Directory + @"Core")) { Directory.CreateDirectory(Xml_Directory + @"Core"); }


            string Cheating_Dummy = Xml_Directory + @"Core\Cheating_Dummy.xml";

            // If no Cheating Dummy file exists, we copy it and make sure it is registered in the GameObjectFiles.xml
            if (!File.Exists(Cheating_Dummy))
            {
                // File.Copy("Cheating_Dummy.xml", Cheating_Dummy, true);
               
                byte[] Dummy_Resource = Imperialware.Properties.Resources.Cheating_Dummy;
                File.WriteAllBytes(Cheating_Dummy, Dummy_Resource);


                // Checking if the tag "File" exists in the specified GOF Xml File above with the Value "Core\Cheating_Dummy"
                Save_Tag_Into_Xml(Xml_Directory + "GameObjectFiles.xml", "Root", "File", @"Core\Cheating_Dummy.xml", false);




                // ======= Making sure all factions can build the Cheating dummy, using the load factions code =======
                string Playable_Factions = "";         


                // Getting all Factions from the Combo box and adding them to the File
                foreach (string Item in All_Playable_Factions)
                { if (Item != "Load Factions" & Item != "") { Playable_Factions += Item + ", "; } }

                
                try
                {   // Reading File      
                    string The_Text = File.ReadAllText(Cheating_Dummy);

                    if (Playable_Factions == "") 
                    { The_Text = Regex.Replace(The_Text, @"<Affiliation>.*?</Affiliation>", "<Affiliation>" + "Empire, Rebel, Underworld" + "</Affiliation>"); }
                    // We replace anything in all Affiliation tags with the current Playable_Factions

                    else { The_Text = Regex.Replace(The_Text, @"<Affiliation>.*?</Affiliation>", "<Affiliation>" + Playable_Factions + "</Affiliation>"); }
                       

                    // Saving Changes
                    File.WriteAllText(Cheating_Dummy, The_Text);
              
                } catch { }


                if (Allowed_Patching == "true")
                {  
                    Make_Buildable_In_Base("All", "StarBase", "Cheating_Dummy_Space", "Cheating_Dummy_Space", false);
                    Make_Buildable_In_Base("All", "SpecialStructure", "Cheating_Dummy_Land_GC", "Cheating_Dummy_Land_SK", false);
                }
            }
                      
        }
        

        // ============== PATCHING all Ground Headquarter Buildings and Starbases with the Dummy ===============
        // Make_Buildable_In_Base("All", "StarBase", "Unit_Name_Campaign", "Unit_Name_Skirmish", false);  If "All" factions are selected it will add the unit to Starbases of all Factions

        // Make_Buildable_In_Base("All", "StarBase", null, "Tactical_Buildable_Objects_Multiplayer", false);  3rd and 4th parameters can be deactivated, then it won't be added to Singleplayer or Skirmish Objects.
        // Make_Buildable_In_Base("All", "StarBase", "Tactical_Buildable_Objects_Campaign", null, false); 

        void Make_Buildable_In_Base(string Required_Faction, string Unit_Type, string Patch_Unit_GC, string Patch_Unit_SK, bool Show_Loading_Screen)
        {          
            Splash_Screen splashForm = null;

            if (Show_Loading_Screen) 
            {                
                Thread splashThread = new Thread(new ThreadStart(
                   delegate
                   {
                       splashForm = new Splash_Screen();
                       Application.Run(splashForm);
                   }
                 ));

                splashThread.SetApartmentState(ApartmentState.STA);
                splashThread.Start();
            }
               
  

            foreach (var Xml in Get_Xmls())
            {   // To prevent the program from crashing because of xml errors:
                 try {  
                    // ===================== Opening Xml File =====================
                    XDocument Xml_File = XDocument.Load(Xml, LoadOptions.PreserveWhitespace);


                    // Defining a XElement List
                    IEnumerable<XElement> Xml_Content =

                    // Selecting all Tags with this string Value:
                    from All_Tags in Xml_File.Descendants(Unit_Type)
                    // Selecting all non empty tags (null because we need all selected)
                    where (string)All_Tags.Attribute("Name") != null
                    select All_Tags;



                    string Current_Value = "";

                    // =================== Editing Xml Instance ===================
                    foreach (XElement Instance in Xml_Content)
                    {   // Resetting it because this Massage should appear for each Starbase.
                        Error_Buildables_Exceeded = false;

                        // Setting any variable, just to find out if that tag is not inside the current Instance. If not inside the catch breaks the loop and continues with the next instance
                        if (Unit_Type == "SpecialStructure") try { Current_Value = Instance.Descendants("HQ_Win_Condition_Relevant").First().Value; } catch { continue; }              
                       
                        
                        // string Instance_Name = (string)Instance.FirstAttribute.Value;


                        foreach (string Faction in All_Playable_Factions)
                        {
                            // Escaping Instance patching if this Faction is not listed in the List_View_Active_Affiliations.
                            if (Required_Faction == "false" & !List_View_Matches(List_View_Active_Affiliations, Faction)) { continue; }
                                // !List_Box_Active_Affiliations.Items.Contains(Faction))

                            // Escaping Instance patching if any Required_Faction was specified, but this one is none of the specified ones.
                            else if (Required_Faction != "false" & Required_Faction != "All" & !Required_Faction.Contains(Faction)) { continue; }

                            // Escaping if any Required_Faction was specified and this Instance doesen't have it. Inactive because for Skirmish Starbase variants it would not work. 
                            // if (!Instance.Descendants("Affiliation").Any()) { continue; }


                            if (Patch_Unit_GC != null & Patch_Unit_GC != "false")
                            {   try
                                {   if (Instance.Descendants("Tactical_Buildable_Objects_Campaign").Any())
                                    {   Current_Value = Instance.Descendants("Tactical_Buildable_Objects_Campaign").First().Value;

                                        if (Current_Value != "")
                                        {   Temporal_E = Current_Value.Split(',');
                                            
                                            // 106 is the Safe zone, if that tag contains more units the starbase might crash the game!
                                            if (Temporal_E.Count() > 105)
                                            {   
                                                if (!Error_Buildables_Exceeded)
                                                {   Error_Buildables_Exceeded = true;

                                                    Imperial_Console(720, 190, Add_Line + "    Caution: <Tactical_Buildable_Objects_Campaign> in "
                                                                                                         + Add_Line + "    " + Path.GetFileName(Xml) + @"\" + (string)Instance.Attribute("Name")
                                                                                                         + Add_Line + "    has more then 105 entries. If you add few more entries it will crash"
                                                                                                         + Add_Line + "    the game on startup. You would need to manually replace existing entries"
                                                                                                         + Add_Line + "    by the name of this unit without exceeding the maximal 106 entries.");
                                                }                                            
                                            }

                                            else if (Current_Value.Contains(Faction) & !Regex.IsMatch(Current_Value, Faction + ".*?" + Patch_Unit_GC) & !Regex.IsMatch(Current_Value, Faction + "," + ".        " + Patch_Unit_GC, RegexOptions.Singleline))
                                            { Instance.Descendants("Tactical_Buildable_Objects_Campaign").First().Value = Regex.Replace(Current_Value, Faction + ",", Faction + "," + Add_Line + "        " + Patch_Unit_GC + ","); }                                   
                                        }
                                    }
                                    else
                                    {   if (Instance.Descendants("Affiliation").First().Value.Contains(Faction) & Unit_Type != "StarBase")
                                        {   Instance.Add("    ");
                                            Instance.Add(new XElement("Tactical_Buildable_Objects_Campaign", Add_Line + "      " + Faction + "," + Add_Line + "        " + Patch_Unit_GC + "," + Add_Line + "    "));
                                            Instance.Add(Add_Line);
                                        }
                                    }
                                } catch { }
                            }


                            if (Patch_Unit_SK != null & Patch_Unit_SK != "false")
                            {   try
                                {   if (Instance.Descendants("Tactical_Buildable_Objects_Multiplayer").Any())
                                    {   Current_Value = Instance.Descendants("Tactical_Buildable_Objects_Multiplayer").First().Value;

                                        if (Current_Value != "")
                                        {   Temporal_E = Current_Value.Split(',');

                                            if (Temporal_E.Count() > 105)
                                            {   
                                                if (!Error_Buildables_Exceeded)
                                                {   Error_Buildables_Exceeded = true;

                                                    Imperial_Console(720, 190, Add_Line + "    Caution: <Tactical_Buildable_Objects_Multiplayer> in "
                                                                                                         + Add_Line + "    " + Path.GetFileName(Xml) + @"\" + (string)Instance.Attribute("Name")
                                                                                                         + Add_Line + "    has more then 105 entries. If you add few more entries it will crash"
                                                                                                         + Add_Line + "    the game on startup. You would need to manually replace existing entries"
                                                                                                         + Add_Line + "    by the name of this unit without exceeding the maximal 106 entries.");                                            
                                                }
                                            }

                                            else if (Current_Value.Contains(Faction) & !Regex.IsMatch(Current_Value, Faction + ".*?" + Patch_Unit_SK) & !Regex.IsMatch(Current_Value, Faction + "," + ".        " + Patch_Unit_SK, RegexOptions.Singleline))
                                            { Instance.Descendants("Tactical_Buildable_Objects_Multiplayer").First().Value = Regex.Replace(Current_Value, Faction + ",", Faction + "," + Add_Line + "        " + Patch_Unit_SK + ","); }
                                        }                                                                                                                                                                          
                                    }
                                    else
                                    {   if (Instance.Descendants("Affiliation").First().Value.Contains(Faction) & Unit_Type != "StarBase")
                                        {   Instance.Add("    ");
                                            Instance.Add(new XElement("Tactical_Buildable_Objects_Multiplayer", Add_Line + "      " + Faction + "," + Add_Line + "        " + Patch_Unit_SK + "," + Add_Line + "    "));
                                            Instance.Add(Add_Line);
                                        }
                                    }
                                } catch { }
                            }                                                            
                        }                     
                    }

                    Xml_File.Save(Xml);
                    // ===================== Closing Xml File =====================

                } catch { /*Imperial_Console(600, 100, "Failed to load " + Path.GetFileName(Xml) + Add_Line + "Or a Unit inside of it.");*/ }
            }

            if (Show_Loading_Screen) 
            {   // Closing the loading screen again once we're done
                splashForm.Invoke(new Action(splashForm.Close));
                splashForm.Dispose();
                splashForm = null;
            }
        }


        //=========== UI to Xml Tag Functions ==========//
        void Remove_God_Mode()
        {   
            try
            {
                Temporal_A = Load_Setting(Setting, "GMU");
                if (!Regex.IsMatch(Temporal_A, "[^A-Za-z]")) { return; } // Prevents crashes
          
                string[] God_Mode_Unit = Temporal_A.Split(',');
                Temporal_B = Regex.Replace(God_Mode_Unit[1], " ", "");

                if (Xml_Utility(God_Mode_Unit[0], Temporal_B, "Shield_Refresh_Rate", null) == "100000")
                {
                    string Unit_Class = Xml_Utility(God_Mode_Unit[0], Temporal_B, "CategoryMask", null);
                    string Text_File = "";

                    switch (Unit_Class)
                    {
                        case "Fighter":
                            Text_File = Maximum_Values_Fighter;
                            break;
                        case "Bomber":
                            Text_File = Maximum_Values_Bomber;
                            break;

                        case "Corvette":
                            Text_File = Maximum_Values_Corvette;
                            break;
                        case "Frigate":
                            Text_File = Maximum_Values_Frigate;
                            break;
                        case "Capital":
                            Text_File = Maximum_Values_Capital;
                            break;

                        case "Infantry":
                            Text_File = Maximum_Values_Infantry;
                            break;
                        case "Vehicle":
                            Text_File = Maximum_Values_Vehicle;
                            break;
                        case "Air":
                            Text_File = Maximum_Values_Air;
                            break;
                        case "Hero":
                            Text_File = Maximum_Values_Hero;
                            break;
                        case "Structure":
                            Text_File = Maximum_Values_Structure;
                            break;
                    }

                    Int32.TryParse(Load_Setting(Text_File, "Maximum_Shield_Rate"), out Temporal_C);
                    // This variable strengh gives 75%, depening on class of selected unit
                    Temporal_A = ((Temporal_C / 4) * 3).ToString();           

                    // Writing the new value we just processed into the Tag
                    Verify_Edit_Xml_Tag(God_Mode_Unit[0], Temporal_B, "Shield_Refresh_Rate", Temporal_A);             
                }
            }  catch { }
        }
                      
        //=========== UI to Xml Tag Functions ==========//

        // Scroll_Xml_Value(Track_Bar, Progress_Bar, Text_Box, Maximal_Value);
        void Scroll_Xml_Value(TrackBar Track_Bar, ProgressBar Progress_Bar, TextBox Text_Box, int Maximal_Value, int Number)
        {
            Scrolling = true;

            // Fighters and Ground Units have a smaller maximum value, so we adjust the divisor number for such cases
            if (Maximal_Value < 999 & Number == 100) { Number = 10; }


            if (Progress_Bar != null)
            {
                Progress_Bar.Maximum = Maximal_Value / Number;
                Track_Bar.Maximum = Progress_Bar.Maximum;
                Progress_Bar.Value = Track_Bar.Value;
            }
            
            // Setting Value of the Text Box, according to the Maximal Value 
            Text_Box.Text = (Track_Bar.Value * Number).ToString();

            Scrolling = false;


            if (User_Input & Tab_Control_01.SelectedIndex == 3 & Edited_Selected_Unit == false) { Edited_Selected_Unit = true; }
        }


        //=========== For Hardpoints ==========//
        void Scroll_HP_Value(TrackBar Track_Bar, ProgressBar Progress_Bar, TextBox Text_Box, int Maximal_Value, int Number)
        {
            Scrolling = true;

            // Fighters and Ground Units have a smaller maximum value, so we adjust the divisor number for such cases
            if (Maximal_Value < 999 & Number == 100) { Number = 10; }


            if (Progress_Bar != null)
            {
                Progress_Bar.Maximum = Maximal_Value / Number;
                Track_Bar.Maximum = Progress_Bar.Maximum;
                Progress_Bar.Value = Track_Bar.Value;
            }

            // Setting Value of the Text Box, according to the Maximal Value 
            Text_Box.Text = (Track_Bar.Value * Number).ToString();

            Scrolling = false;


            if (User_Input & Tab_Control_01.SelectedIndex == 3) { Edited_Hard_Point = true; }
        }


        void Scroll_Xml_Decimal(TrackBar Track_Bar, ProgressBar Progress_Bar, TextBox Text_Box, int Maximal_Value)
        {   // Processing Decimal Value 
            // System.Globalization.CultureInfo.GetCultureInfo("de-DE");

            Scrolling = true; 

            // Need to make sure the . is not turned into , for the european (german) languages: System.Globalization.CultureInfo.InvariantCulture.NumberFormat
            Text_Box.Text = ((decimal)Track_Bar.Value / 10).ToString(System.Globalization.CultureInfo.InvariantCulture.NumberFormat);
            if (!Text_Box.Text.Contains(".") & Text_Box.Text != "0" & Text_Box.Text != "") { Text_Box.Text = Text_Box.Text + ".0"; }

            Track_Bar.Maximum = Maximal_Value * 10;

            if (Progress_Bar != null)
            {   Progress_Bar.Maximum = Maximal_Value * 10;
                Progress_Bar.Value = (Track_Bar.Value);
            }
           
            Scrolling = false; 
        }


        // Text_Box_Text_Changed(Track_Bar, Progress_Bar, Maximal_Value, Typed_Value, Number)
        void Text_Box_Text_Changed(TrackBar Track_Bar, ProgressBar Progress_Bar, int Maximal_Value, int Typed_Value, int Number)
        {
            if (Scrolling) { return; }

            // Fighters and Ground Units have a smaller maximum value, so we adjust the divisor number for such cases
            if (Maximal_Value < 999 & Number == 100) { Number = 10; }

            // If the value is higher then Maximum we make sure the Bars are full
            if (Typed_Value > Maximal_Value)
            {
                if (Progress_Bar != null) { Progress_Bar.Value = Progress_Bar.Maximum; }              
                Track_Bar.Value = Track_Bar.Maximum;
            }
            else
            {   try
                {   // Setting amount in the Trackbar according to the Maximal Value 
                    Track_Bar.Maximum = Maximal_Value / Number;
                    // Then we need to set the Track Bar according to the Text box / 100      
                    Track_Bar.Value = Typed_Value / Number;


                    // Adjusting the Maximum Value of the Progress Bar and multiplicating x 100 to get the percentage
                    if (Progress_Bar != null) 
                    {   Progress_Bar.Maximum = Maximal_Value;
                        Progress_Bar.Value = Typed_Value;
                    }
                } catch {}
            }
           
        }

        //=========== Text to Xml Tag Functions for saving ==========//
        //if (!Is_Team) { Verify_Tag_with_Extension(Unit_Type, "Icon_Name", Text_Box_Icon_Name, ".tga")}


        void Verify_Tag_with_Extension(IEnumerable<XElement> The_Unit, string Tag_Name, Control Text_Box, string Suffix)
        {
            Temporal_A = Text_Box.Text;
            Error_Tag = Tag_Name + " = " + Temporal_A;
            
            try // Save_Xml_File.Element(Root_Tag).Element(Unit_Type)
            {   
                if (The_Unit == null) { The_Unit = Instance; }

                if (The_Unit.Descendants(Tag_Name).Any())
                {   // If tag is not existing we create it below
                    if (Text_Box.Text != "" & Text_Box.Text != The_Unit.Descendants(Tag_Name).First().Value) 
                    {                       
                        if (Regex.IsMatch(Temporal_A, "(?i).*?" + Suffix + Suffix))
                        {    // We remove all .tga suffixes if there are more then one
                            Temporal_B = Regex.Replace(Temporal_A, "(?i)" + Suffix, "");

                            // And add a fresh suffix                  
                            The_Unit.Descendants(Tag_Name).First().Value = Temporal_B + Suffix;
                        }

                        else if (Regex.IsMatch(Temporal_A, "(?i).*?" + Suffix)) // If ends with suffix 
                        {   // Thats fine to save
                            The_Unit.Descendants(Tag_Name).First().Value = Temporal_A;
                        }
                        else // Otherwise we asume there is no suffix, so we add .tga
                        {
                            Temporal_B = Regex.Replace(Temporal_A, Temporal_A, Temporal_A + Suffix);
                            The_Unit.Descendants(Tag_Name).First().Value = Temporal_B;
                        }
                    }
                }
                else if (Text_Box.Text != "")
                {                    
                    if (Regex.IsMatch(Temporal_A, "(?i).*?" + Suffix + Suffix))
                    {   Temporal_B = Regex.Replace(Temporal_A, "(?i)" + Suffix, "");
                        The_Unit.First().Add("\t\t", new XElement(Tag_Name, Temporal_B + Suffix), Add_Line);
                    }
                    else if (Regex.IsMatch(Temporal_A, "(?i).*?" + Suffix)) // If ends with suffix 
                    {   // Thats fine to save                        
                        The_Unit.First().Add("\t\t", new XElement(Tag_Name, Temporal_A), Add_Line);
                    }
                    else 
                    {   Temporal_B = Regex.Replace(Temporal_A, Temporal_A, Temporal_A + Suffix);
                        The_Unit.First().Add("\t\t", new XElement(Tag_Name, Temporal_B), Add_Line);
                    }                              
                }             
            } catch { }
        }      


        //=====================//

        string Verify_Ability_Icon(string Ability_Icon)
        {
            if (Ability_Icon != null)
            {   // Verifying Ability icon suffix
                if (!Ability_Icon.Contains(".tga")) { Ability_Icon = Ability_Icon + ".tga"; }
                
                else if (Regex.IsMatch(Ability_Icon, "(?i).*?.tga.tga"))
                {    // We remove all .tga suffixes if there are more then one
                    Temporal_A = Regex.Replace(Ability_Icon, "(?i).tga", "");

                    // And add a fresh suffix                  
                    Ability_Icon = Temporal_A + ".tga";
                }
            }        

            return Ability_Icon;
        } 

        //=====================//

        void Add_Extension_To_Instance(XElement Instance, string Tag_Name, Control Text_Box, string Suffix, string Line_Parameters)
        {
            Temporal_A = Text_Box.Text;

            if (Temporal_A != "")
            {              
                if (Regex.IsMatch(Temporal_A, "(?i).*?" + Suffix + Suffix))
                {    // We remove all suffixes if there are more then one
                    Temporal_B = Regex.Replace(Temporal_A, "(?i)" + Suffix, "");
                    Temporal_B = Temporal_B + Suffix;
                }
                else if (!Regex.IsMatch(Temporal_A, "(?i).*?" + Suffix)) // If not ends with .suffix 
                {
                    Temporal_B = Regex.Replace(Temporal_A, Temporal_A, Temporal_A + Suffix);
                }

                Instance.Add(new XElement(Tag_Name, Temporal_B), Line_Parameters);
            }       
        }


        //if (!Is_Team) { Add_Tag_with_Extension(Unit_Type, "Icon_Name", Text_Box_Icon_Name, ".tga")}
        void Add_Tag_with_Extension(string Unit_Type, string Tag_Name, Control Text_Box, string Suffix)
        {   
            if (Text_Box.Text != "")
                {
                    Temporal_A = Text_Box.Text;

                    if (Regex.IsMatch(Temporal_A, "(?i).*?" + Suffix + Suffix))
                    {    // We remove all suffixes if there are more then one
                        Temporal_B = Regex.Replace(Temporal_A, "(?i)" + Suffix, "");

                        // And add a fresh suffix               
                        Save_Xml_File.Root.Element(Unit_Type).Add(new XElement(Tag_Name, Temporal_B + Suffix));
                    }

                    else if (Regex.IsMatch(Temporal_A, "(?i).*?" + Suffix)) // If ends with .suffix 
                    {   // Thats fine to save
                        Save_Xml_File.Root.Element(Unit_Type).Add(new XElement(Tag_Name, Temporal_A));
                    }
                    else // Otherwise we asume there is no suffix, so we add .suffix
                    {
                        Temporal_B = Regex.Replace(Temporal_A, Temporal_A, Temporal_A + Suffix);
                        Save_Xml_File.Root.Element(Unit_Type).Add(new XElement(Tag_Name, Temporal_B));
                    }
                }                   
          //  } catch {}

        }

        /*
        void Add_Tag(XDocument Xml_File, string Tag_Name, Control Text_Box)
        {
            if (Text_Box.Text != "")
            { Xml_File.Root.Element(Unit_Type).Add(new XElement(Tag_Name, Text_Box.Text)); }     
        }
         */


        void Add_Instance_Tag(XElement Instance, string Tag_Name, Control Text_Box)
        {
            if (Text_Box.Text != "")
            { Instance.Descendants().First().Add(new XElement(Tag_Name, Text_Box.Text), "\n\t\t"); }
        }

        void Add_Tag(XDocument Xml_File, string Instance_Name, string Tag_Name, Control Text_Box)
        {   if (Text_Box.Text != "")
            { Xml_File.Root.Descendants().Where(x => (string)x.Attribute("Name") == Instance_Name).First().Add(new XElement(Tag_Name, Text_Box.Text)); }     
        }

        void Add_Tag_Text(XDocument Xml_File, string Instance_Name, string Tag_Name, string Tag_Value, string Line_Parameters)
        {   if (Tag_Value != "")
            { Xml_File.Root.Descendants().Where(x => (string)x.Attribute("Name") == Instance_Name).First().Add(new XElement(Tag_Name, Tag_Value), Line_Parameters); }
        }
                       

        void Add_Tag_To_Team(string Tag_Name, Control Text_Box)
        { 
            if (Text_Box.Text != "")
            { Save_Xml_File.Element(Root_Tag).Element(Team_Type).Add(new XElement(Tag_Name, Text_Box.Text)); }
        }

        void Add_Text_To_Team(string Tag_Name, string Tag_Value)
        {   if (Tag_Value != "")
            { Save_Xml_File.Element(Root_Tag).Element(Team_Type).Add(new XElement(Tag_Name, Tag_Value)); }
        }

        // Validate_Save_Tag(Text_Box, Xml_File, Tag_Name);
        void Validate_Save_Tag(Control Text_Box, string Tag_Name)
        {
            Error_Tag = Tag_Name + " = " + Text_Box.Text;

            // if (Text_Box.Text != "") {// If that tag exists
               try // We try to update that tag 
                {
                    if (Instance.Descendants(Tag_Name).Any())
                    {   if (Text_Box.Text != Instance.Descendants(Tag_Name).First().Value)
                        { Instance.Descendants(Tag_Name).First().Value = Text_Box.Text; }                   
                    }
                    else if (Text_Box.Text != "")
                    {   Instance.First().Add("\t\t");
                        Instance.First().Add(new XElement(Tag_Name, Text_Box.Text), Add_Line);
                    }
                } catch { }
            // }      
        }

        void Validate_Save_Unit_Tag(IEnumerable<XElement> The_Unit, string Tag_Name, Control Text_Box)
        {
            Error_Tag = Tag_Name + " = " + Text_Box.Text;

            if (Text_Box.Text != "")
            {   try
                {   if (The_Unit.Descendants(Tag_Name).Any())
                    {
                        if (Text_Box.Text != The_Unit.Descendants(Tag_Name).First().Value)
                        { The_Unit.Descendants(Tag_Name).First().Value = Text_Box.Text; }
                    }
                    else
                    {   The_Unit.First().Add("\t\t");
                        The_Unit.First().Add(new XElement(Tag_Name, Text_Box.Text), Add_Line);
                    }
                } catch {}
            }
        }  

        void Validate_Save_Unit_Text(IEnumerable<XElement> The_Unit, string Tag_Name, string Tag_Value)
        {
            Error_Tag = Tag_Name + " = " + Tag_Value;

            if (Tag_Value != "")
            {   try 
                {   if (The_Unit.Descendants(Tag_Name).Any())
                    {   if (Tag_Value != The_Unit.Descendants(Tag_Name).First().Value)
                        { The_Unit.Descendants(Tag_Name).First().Value = Tag_Value; }
                    }
                    else
                    {   The_Unit.First().Add("\t\t");
                        The_Unit.First().Add(new XElement(Tag_Name, Tag_Value), Add_Line);
                    }
                } catch { }
            }
        }  


        void Validate_Save_Text(string Tag_Name, string Tag_Value)
        {
            Error_Tag = Tag_Name + " = " + Tag_Value;

            try // We try to update that tag 
            {   if (Instance.Descendants(Tag_Name).Any())
                {   if (Tag_Value != Instance.Descendants(Tag_Name).First().Value) 
                    { Instance.Descendants(Tag_Name).First().Value = Tag_Value; } 
                } 
                else if (Tag_Value != "") { Instance.First().Add("\t\t", new XElement(Tag_Name, Tag_Value), Add_Line); }
            } catch {}                  
        }

        //=====================// 
        // Validate_Save_Ability_Code(Ability_Type, Tag_Name, Tag_Value);

        // You can also use this to clear all tags from the Text_Box_Mod_Multiplier.Text which runns through Ability_1_Mod_Multipliers and Ability_2_Mod_Multipliers:
        // Validate_Save_Ability_Code(Ability_1_Type, null, "Clear");

        // Or clear the whole Ability
        // Validate_Save_Ability_Code(Ability_1_Type, null, "Clear_All");

        void Validate_Save_Ability_Code(string Selected_Ability, string Tag_Name, string Tag_Value)
        {
            Error_Tag = Tag_Name + " = " + Tag_Value + " in " + Selected_Ability;

            // Makig sure Abilities Data exists
            if (!Instance.Descendants("Unit_Abilities_Data").Any())
            {   Instance.First().Add("\t\t", new XComment(" ============ Abilities ============ "), 
                Add_Line, "\t\t", new XElement("Unit_Abilities_Data", new XAttribute("SubObjectList", "Yes")), Add_Line + "\t");
            }

            // If no such Ability exists and while we have less then 2 abilities, we create one 
            if (Instance.Descendants("Unit_Abilities_Data").Descendants("Unit_Ability").Count() < 2 & Selected_Ability != "" & Selected_Ability != " NONE" &
                !Instance.Descendants("Unit_Abilities_Data").Descendants("Unit_Ability").Descendants("Type").Any(x => x.Value == Selected_Ability))
            {
                // if (Selected_Ability == Ability_2_Type) // Adding this Comment only to second ability
                // { Instance.Descendants("Unit_Abilities_Data").First().Add(Add_Line, "\t\t\t", new XComment(" ========= Secondary Ability ========= ")); } 

                Instance.Descendants("Unit_Abilities_Data").First().Add(                               
                Add_Line, "\t\t\t", new XElement("Unit_Ability",
                Add_Line, "\t\t\t\t", new XElement("Type", Selected_Ability), Add_Line), Add_Line + "\t\t");
                Temporal_B = "true";
            }

           

            Temporal_C = 0;
     
            if (Tag_Value != "")
            {   // We search all "Unit_Ability" subtags
                foreach (var Tag in Instance.Descendants("Unit_Abilities_Data").Descendants("Unit_Ability"))               
                {   try 
                    {   Temporal_C++; // Increasing count for each Ability
                        
                                          
                        if (Regex.IsMatch(Tag.Descendants("Type").First().Value, "(?i)" + Selected_Ability))
                        {

                            if (Tag_Value == "Clear") // This function clears all added tags, except for the "Keep_Tags"
                            {   string Keep_Tags = "Type, Supports_Autofire, GUI_Activated_Ability_Name, Expiration_Seconds, Recharge_Seconds, Alternate_Name_Text, Alternate_Description_Text, Alternate_Icon_Name, SFXEvent_GUI_Unit_Ability_Activated, SFXEvent_GUI_Unit_Ability_Deactivated";
                              
                                // Needs to be a for loop, foreach and i++ would not work
                                for (int i = Tag.Descendants().Count() - 1; i > 0; i--)
                                {
                                    var Subtag = Tag.Descendants().ElementAt(i);
                         
                                    // Clearing all non standard tags
                                    if (!Keep_Tags.Contains(Subtag.Name.ToString())) { Subtag.Remove(); }                        
                                }
                                return;
                            }
                                                         
                                                                                                                                                                                                           
                            // Appending these special tags directly to the Selected_Ability after Clearing all tags above
                            if (Tag_Name == "Unit_Ability" & Tag_Value != "") 
                            { Tag.Add("\t\t\t\t", new XComment("here" + Tag_Value + "here"), Add_Line); return; }


                            if (Tag_Value != null & Tag.Descendants(Tag_Name).Any()) // If exists we will adjust the value
                            {
                                if (Tag.Descendants(Tag_Name).First().Value != Tag_Value)
                                { Tag.Descendants(Tag_Name).First().Value = Tag_Value; }
                            }
                            else if (Tag_Value != null & Tag_Value != "") // Otherwise we create this tag 
                            { Tag.Add("\t\t\t\t", new XElement(Tag_Name, Tag_Value), Add_Line); }
                        }

                        // Changing Ability Name
                        else if (Tag_Name == "Type" & Tag.Descendants(Tag_Name).Any()) 
                        {
                            if (Temporal_C == 1 & Selected_Ability == Ability_1_Type) // Then we know its Ability at Slot 1
                            {   // This triggers only in the first loop
                                if (Tag.Descendants(Tag_Name).First().Value != Tag_Value)
                                { Tag.Descendants(Tag_Name).First().Value = Tag_Value; }
                            }

                            else if (Temporal_C == 2 & Selected_Ability == Ability_2_Type) // Then we know its Ability at Slot 2                              
                            {   // This triggers only in the second loop while Temporal_C == 2 & Selected_Ability == Ability_2_Type
                                if (Tag.Descendants(Tag_Name).First().Value != Tag_Value)
                                { Tag.Descendants(Tag_Name).First().Value = Tag_Value; }
                            }
                        }
                       

                    } catch { }
                }      
            }        
        }

        //=====================// 

        void Remove_Abilities()           
        {
            Temporal_B = "Squadron, GroundCompany, HeroCompany";

            if (Ability_1_Remove != "")
            {
                Instance.Descendants("Unit_Abilities_Data").Descendants("Unit_Ability").Where(x => x.Descendants("Type").First().Value == Ability_1_Remove).Remove();
                // For Team units wait with deletion of the ability for Ability_1_Remove until we passed the normal unit and reached the Squadron/Team 
                if (Is_Team & Instance.Any()) { if (Temporal_B.Contains(Instance.First().Name.ToString())) Ability_1_Remove = ""; }
                else { Ability_1_Remove = ""; } 
            }

            if (Ability_2_Remove != "")
            {
                Instance.Descendants("Unit_Abilities_Data").Descendants("Unit_Ability").Where(x => x.Descendants("Type").First().Value == Ability_2_Remove).Remove();
                if (Is_Team & Instance.Any()) { if (Temporal_B.Contains(Instance.First().Name.ToString())) Ability_2_Remove = ""; }
                else { Ability_2_Remove = ""; } 
            }

            Temporal_B = "";
        }

        //=====================// 

        void Remove_Instance(string Xml_File_Path, string Instance_Name)           
        {
            XDocument Xml_File = XDocument.Load(Xml_File_Path, LoadOptions.PreserveWhitespace);

            var Deletion_Instance =
               from All_Tags in Xml_File.Root.Descendants()
               // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
               where (string)All_Tags.Attribute("Name") == Instance_Name
               select All_Tags;

            Deletion_Instance.Remove();

            Xml_File.Save(Xml_File_Path);          
        }


        //  Remove_Tag(string Xml_File_Path, string Instance_Name, string Tag_Name, null)   = Delets this tag anyways if With_This_Value is null
        void Remove_Tag(string Xml_File_Path, string Instance_Name, string Tag_Name, string With_This_Value)
        {
            XDocument Xml_File = XDocument.Load(Xml_File_Path, LoadOptions.PreserveWhitespace);

            var Deletion_Instance =
               from All_Tags in Xml_File.Root.Descendants()
               // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
               where (string)All_Tags.Attribute("Name") == Instance_Name
               select All_Tags;

            // Deleting this Tag
            if (With_This_Value == null) { Deletion_Instance.Descendants(Tag_Name).Remove(); } 
            // Deleting it only if it has the specified value
            else { Deletion_Instance.Descendants(Tag_Name).Where(x => x.Value == With_This_Value).Remove(); }          
            
            Xml_File.Save(Xml_File_Path);
        }

        //=====================// 

        void Overwrite_Save_Tag(Control Text_Box, string Tag_Name)
        {
            Error_Tag = Tag_Name + " = " + Text_Box.Text;
            
            try 
            {   if (Instance.Descendants(Tag_Name).Any())
                {   if (Text_Box.Text != Instance.Descendants(Tag_Name).First().Value)
                    { Instance.Descendants(Tag_Name).First().Value = Text_Box.Text; }
                }
                else if (Text_Box.Text != "")
                {   Instance.First().Add("\t\t"); 
                    Instance.First().Add(new XElement(Tag_Name, Text_Box.Text), Add_Line); 
                }
            } catch { }                 
        }

        void Overwrite_Save_Text(string Tag_Name, string Tag_Value)
        {
            Error_Tag = Tag_Name + " = " + Tag_Value;

            try
            {   if (Instance.Descendants(Tag_Name).Any())
                {
                    if (Tag_Value != Instance.Descendants(Tag_Name).First().Value)
                    { Instance.Descendants(Tag_Name).First().Value = Tag_Value; }
                }
                else if (Tag_Value != "")
                {
                    Instance.First().Add("\t\t");
                    Instance.First().Add(new XElement(Tag_Name, Tag_Value), Add_Line);
                }
            } catch { }
        }
         
        // Validate_Save_Costum_Tag(Text_Box_Name, Text_Box);
        void Validate_Save_Costum_Tag(RichTextBox Text_Box_Name, TextBox Text_Box)
        {
            Error_Tag = Text_Box_Name.Text + " = " + Text_Box.Text;
            
            try
            {   // Removing <> signs from name, if the User keeps the brackets
                string Costum_Tag_Name_Value = Regex.Replace(Text_Box_Name.Text, "[</>]", "");

                if (Instance.Descendants(Costum_Tag_Name_Value).Any())
                {
                    if (Text_Box.Text != "" & Text_Box_Name.Text != "<Tag_1_Name>" & Text_Box.Text != Instance.Descendants(Costum_Tag_Name_Value).First().Value)
                    { Instance.Descendants(Costum_Tag_Name_Value).First().Value = Text_Box.Text; }
                }
                else if (Text_Box.Text != "" & Text_Box_Name.Text != "<Tag_1_Name>")
                { Instance.First().Add("\t\t"); Instance.First().Add(new XElement(Costum_Tag_Name_Value, Text_Box.Text), Add_Line); }
             
            } catch { }
        }

        void Save_Switch(string Tag_Name, string Bool_Type, bool Switch)
        {
            string Is_True = "Yes";
            string Is_False = "No";

            if (Bool_Type == "1")
            {
                Is_True = "True";
                Is_False = "False";
            }

            Error_Tag = Tag_Name + " = " + Switch;

            try
            {   if (Instance.Descendants(Tag_Name).Any())
                {   // Aligning Tag value to Toggle
                    if (Switch == true & Is_True != Instance.Descendants(Tag_Name).First().Value) { Instance.Descendants(Tag_Name).First().Value = Is_True; }
                    else if (Switch == false & Is_False != Instance.Descendants(Tag_Name).First().Value) { Instance.Descendants(Tag_Name).First().Value = Is_False; }
                }
                else
                {   if (Switch == true) { Instance.First().Add("\t\t"); Instance.First().Add(new XElement(Tag_Name, Is_True), Add_Line); }
                    else if (Switch == false) { Instance.First().Add("\t\t"); Instance.First().Add(new XElement(Tag_Name, Is_False), Add_Line); }
                }
            } catch { }
        }



        //=====================//
        // Position of these 2 parameters (left or right side) decides in which direction they move!
        // Switch_Value_Activity(List_Box_Inactive, List_Box_Active);
        void Switch_Value_Activity(ListBox List_Box_Inactive, ListBox List_Box_Active)
        {
            string Current_Selection = List_Box_Inactive.GetItemText(List_Box_Inactive.SelectedItem).ToString();
            if (User_Input & Edited_Selected_Unit == false) { Edited_Selected_Unit = true; }

            for (int i = List_Box_Inactive.Items.Count - 1; i >= 0; --i)
            {
                string Item = List_Box_Inactive.Items[i].ToString();

                if (Item == Current_Selection)
                {   // Transfer; removing selection from one Listbox and adding to the other
                    List_Box_Active.Items.Add(Item);
                    List_Box_Inactive.Items.Remove(Item);

                    // Selecting the newest Entry in the other Listbox, which is the one we just added
                    List_Box_Active.SetSelected(List_Box_Active.Items.Count - 1, true);
                }
            }

            // Then selecting the highest one in this Listbox
            try { List_Box_Inactive.SelectedItem = List_Box_Inactive.Items[0]; } catch { }
        }


        //=====================// 
        void Exchange_Table_Values(ListBox List_Box_A, ListBox List_Box_B)
        {
            Temporal_A = "";

            // Getting all Items of first list into the string
            for (int i = List_Box_A.Items.Count - 1; i >= 0; --i)
            {
                var Item = List_Box_A.Items[i].ToString();

                if (Item != "")
                {
                    Temporal_A += Item + ",";
                }
            }

            List_Box_A.Items.Clear();


            // Then transfering all items to the other list          
            for (int i = List_Box_B.Items.Count - 1; i >= 0; --i)
            { List_Box_A.Items.Add(List_Box_B.Items[i].ToString()); }

            List_Box_B.Items.Clear();

            foreach (string Entry in Temporal_A.Split(','))
            { List_Box_B.Items.Add(Entry); }
        }

        //=====================// 
        void Switch_List_View_Value(ListView List_View_1, ListView List_View_2)
        {
            if (User_Input & Edited_Hard_Point == false) { Edited_Hard_Point = true; }
      

            string Selection = Select_List_View_First(List_View_1, true);
            Temporal_List = Get_All_List_View_Items(List_View_2);


            if (Temporal_List == null)
            {
                var New_Entry = List_View_2.Items.Add(Selection);
                // Selecting the newest Entry in the other Listbox, which is the one we just added
                New_Entry.Selected = true;
                Remove_From_List_View(List_View_1, Selection);
            }
            else if (List_View_2.Items.Count > 0)
            {
                for (int i = 0; i < List_View_2.Items.Count; ++i) // Cycle up
                {   if (!List_View_Matches(List_View_2, Selection))
                    {   if (List_View_2.Items[i].Text == "")
                        {   List_View_2.Items[i].Text = Selection; 
                            Remove_From_List_View(List_View_1, Selection);
                            List_View_2.Items[i].Selected = true;
                            List_View_2.Select();  
                        }
                        else 
                        {   List_View_2.Items.Add(Selection); 
                            Remove_From_List_View(List_View_1, Selection);
                            List_View_2.Items[i].Selected = true;
                            List_View_2.Select();
                        }
                    }                    
                } // Imperial_Console(600, 300, Selection);
            }


            Set_Checkmate_Color(List_View_1);
            Set_Checkmate_Color(List_View_2);

            try // Then selecting the highest one in the other Listbox
            {   if (List_View_1.Items.Count < 1)
                {   List_View_1.Items[0].Selected = true;
                    List_View_1.Select();
                }
            } catch {}
        }



        //=====================// 
        void Exchange_List_View_Values(ListView List_View_1, ListView List_View_2)
        {
            if (User_Input & Edited_Hard_Point == false) { Edited_Hard_Point = true; }
            // Shifting ListView_2 to Temporal List
            Temporal_List = Get_All_List_View_Items(List_View_2);
            List_View_2.Items.Clear();

            
            if (List_View_1.Items.Count > 0)
            {   for (int i = 0; i < List_View_1.Items.Count; ++i) // Cycle up
                {   if (List_View_1.Items[i].Text != "")
                    { List_View_2.Items.Add(List_View_1.Items[i].Text); }
                }

                List_View_1.Items.Clear();
            }

            Temporal_A = "";
            // Migrating all entries back
            if (Temporal_List != null)
            {   foreach (string Entry in Temporal_List)
                {   if (Entry != "") 
                    {   List_View_1.Items.Add(Entry); 
                        // Temporal_A += Entry + ", "; 
                    }
                }              
            }
            // Imperial_Console(600, 300, Temporal_A);           


            Set_Checkmate_Color(List_View_1);
            Set_Checkmate_Color(List_View_2);

            try // Then selecting the highest one in the Listbox
            {
                if (List_View_1.Items.Count > List_View_2.Items.Count)
                {   List_View_1.Items[0].Selected = true;
                    List_View_1.Select();
                }
                else
                {   List_View_2.Items[0].Selected = true;
                    List_View_2.Select();
                }
            } catch {}
        }


        //=====================// 
        void Get_Alo_Path(Control Text_Box)
        {   // Setting Innitial Filename and Data for the Open Menu
            Open_File_Dialog_1.FileName = "";
            Open_File_Dialog_1.InitialDirectory = Data_Directory + @"Art\Models";
            Open_File_Dialog_1.Filter = "alo files (*.alo)|*.alo|All files (*.*)|*.*";
            Open_File_Dialog_1.FilterIndex = 1;
            Open_File_Dialog_1.RestoreDirectory = true;
            Open_File_Dialog_1.CheckFileExists = true;
            Open_File_Dialog_1.CheckPathExists = true;


            try
            {   // If the Open Dialog found a File
                if (Open_File_Dialog_1.ShowDialog() == DialogResult.OK)
                {
                    Text_Box.Text = Path.GetFileNameWithoutExtension(Open_File_Dialog_1.FileName);

                    // Getting Rid of everything infront of the Model Dir, as we only need subdirectories
                    Temporal_A = Regex.Replace(Path.GetDirectoryName(Open_File_Dialog_1.FileName), ".*Models", "");
                    Temporal_B = Temporal_A.Substring(1, Temporal_A.Length - 1);

                    if (Temporal_B == "") { Text_Box.Text = Path.GetFileNameWithoutExtension(Open_File_Dialog_1.FileName); }
                    else { Text_Box.Text = Temporal_B + @"\" + Path.GetFileNameWithoutExtension(Open_File_Dialog_1.FileName); }
                }
            } catch {}
            //catch (Exception The_Exception)
            //{ MessageBox.Show(The_Exception.Message, "MainWindow", MessageBoxButtons.OK, MessageBoxIcon.Error); }
        }

        //=====================// 
        void Open_Alo_File(Control Text_Box)
        {   try
            {   string The_File = Art_Directory + @"Models\" + Text_Box.Text + ".alo";
                System.Diagnostics.Process.Start(The_File);
            } 
            catch
            {
                if (Text_Box.Text == "") { Imperial_Console(540, 100, Add_Line + "Please add any model name into the model textbox."); }
                else
                {
                    Imperial_Console(540, 100, Add_Line + "Error; Maybe you need to assign .alo files"
                                             + Add_Line + "to the Alo Viewer tool in"
                                             + Add_Line + @".\Imperialware_Directory\Misc\Modding_Tools");
                }
            }
        }
   
        //=====================// 

        // This function must NOT be used inside any process of XML editing, or the Xml_File loads as XDocument then it saves the change, 
        // But the (older) XDocument you loaded before is still inside of the memory and overwrites the (edited) Validated XDocument save again! 
        // Use the function below to save from inside a editing/saving process.

        // If Allow_Duplicates is true that will add a new Tag with the name "Tag_Name" each time the code is executed, if false the "Tag_Content" 
        // in the existing tag will be updated.

        void Save_Tag_Into_Xml(string File_Name, string Required_Instance, string Tag_Name, string Tag_Content, bool Allow_Duplicates)
        {
            try // We need to try in order to prevent xml errors
            {               
                XDocument Xml_File = XDocument.Load(File_Name, LoadOptions.PreserveWhitespace);

               
                if (Required_Instance == "Root") // This saves automatically to root tag
                {
                    foreach (XElement Tag in Xml_File.Root.Descendants())
                    { if (!Allow_Duplicates & Tag.Value == Tag_Content) { return; } }
                    
                    // If it passed the duplicate check for ALL Descendant tags of the "Tag_Name" Variable, we continue
                    Xml_File.Root.Add("\t");
                    Xml_File.Root.Add(new XElement(Tag_Name, Tag_Content), Add_Line);
                    Xml_File.Save(File_Name); return;
                }


                var Current_Instance =
                   from All_Tags in Xml_File.Root.Descendants()
                   // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
                   where (string)All_Tags.Attribute("Name") == Required_Instance
                   select All_Tags;

               
                // =================== Checking Xml Instance ===================
                foreach (XElement Instance in Current_Instance)
                {
                    // If any duplicate tag was found but is not allowed
                    if (!Allow_Duplicates & Instance.Descendants(Tag_Name).Any())
                    {    try // We try to update that tag to the new Tag_Content
                         {  if (Tag_Content != "" & Tag_Content != Instance.Descendants(Tag_Name).First().Value)
                            { Instance.Descendants(Tag_Name).First().Value = Tag_Content; }
                         } catch { }                  
                    }
                    else
                    {   // This special tag is supposed to have the position at the very top
                        if (Tag_Name == "Variant_Of_Existing_Type")
                        { Instance.AddFirst(Add_Line, "\t\t", new XElement(Tag_Name, Tag_Content)); }
                       
                        else  // Adding new Element to the Root tag of this Xml File
                        { Instance.AddFirst("\t\t"); Instance.Add(new XElement(Tag_Name, Tag_Content), Add_Line); }
                    }
                }
                
                // Saving XML File
                Xml_File.Save(File_Name);
                

            } catch { if (Debug_Mode == "true") { Imperial_Console(600, 100, Add_Line + "Failed to save the <" + Tag_Name + "> tag."); } }
        }
        //=====================//

        // Use this function to save from inside any editing/saving process.

        void Validate_Tag_In_Xml(XDocument Xml_File, string Required_Instance, string Tag_Name, string Tag_Content, bool Allow_Duplicates)
        {
            Error_Tag = Tag_Name + " = " + Tag_Content;

            try // We need to try in order to prevent xml errors
            {              
                if (Required_Instance == "Root") // This saves automatically to root tag
                {
                    foreach (XElement Tag in Xml_File.Root.Descendants())
                    { if (!Allow_Duplicates & Tag.Value == Tag_Content) { return; } }

                    // If it passed the duplicate check for ALL Descendant tags of the "Tag_Name" Variable, we continue
                    Xml_File.Root.Add("\t");
                    Xml_File.Root.Add(new XElement(Tag_Name, Tag_Content), Add_Line);
                    return;
                }


                var Current_Instance =
                   from All_Tags in Xml_File.Root.Descendants()
                   // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
                   where (string)All_Tags.Attribute("Name") == Required_Instance
                   select All_Tags;


                // =================== Checking Xml Instance ===================
                foreach (XElement Instance in Current_Instance)
                {
                    // If any duplicate tag was found but is not allowed
                    if (!Allow_Duplicates & Instance.Descendants(Tag_Name).Any())
                    {
                        try // We try to update that tag to the new Tag_Content
                        {   if (Tag_Content != "" & Tag_Content != Instance.Descendants(Tag_Name).First().Value)
                            { Instance.Descendants(Tag_Name).First().Value = Tag_Content; }
                        } catch { }                          
                    }
                    else
                    {   // This special tag is supposed to have the position at the very top
                        if (Tag_Name == "Variant_Of_Existing_Type")
                        { Instance.AddFirst(Add_Line, "\t\t", new XElement(Tag_Name, Tag_Content)); }

                        else  // Adding new Element to the Root tag of this Xml File
                        { Instance.Add("\t\t"); Instance.Add(new XElement(Tag_Name, Tag_Content), Add_Line); }
                    }
                }     
            } catch { if (Debug_Mode == "true") { Imperial_Console(600, 100, Add_Line + "Failed to save the <" + Tag_Name + "> tag."); } }
        }

        //=====================//
     
        void Validate_Xml_Template(string Xml_File)
        {   try
            {
                if (!File.Exists(Xml_Directory + @"Core\" + Xml_File))
                {   // If our template files don't exist we are going to copy it into the Xml dir of the chosen Mod
                    File.Copy(Program_Directory + @"Xml_Core\" + Xml_File, Xml_Directory + @"Core\" + Xml_File, true);

                    // And add the tag "File" to GOF and HDF .xml with the Name of that Template.xml.
                    Save_Tag_Into_Xml(Xml_Directory + "GameObjectFiles.xml", "Root", "File", @"Core\" + Xml_File, false);
                    if (Xml_File != "Template_Planet.xml") { Save_Tag_Into_Xml(Xml_Directory + "HardpointDataFiles.xml", "Root", "File", @"Core\" + Xml_File, false); }
                }
            }
            catch { }
        }
        //=====================//


        void Apply_Cheats() 
        {

            // If the Directory path for Lua Scripts don't exist, we create one
            if (!Directory.Exists(Data_Directory + @"Scripts\GameObject"))
            { Directory.CreateDirectory(Data_Directory + @"Scripts\GameObject"); }


            string Lua_Script = Data_Directory + @"Scripts\GameObject\Story_Cheating.lua";

            // Overwriting Lua Script with its template in the .exe, in order to Edit it
            // File.Copy("Story_Cheating.lua", Lua_Script, true);

            byte[] Script_Resource = Imperialware.Properties.Resources.Story_Cheating;
            File.WriteAllBytes(Lua_Script, Script_Resource);
            
                         
            
            // We are going to replace the Unit Variable in our Cheating Lua Script.
            string Spawnlist_Text = "";           
            foreach (string Unit in Selected_Units)
            { Spawnlist_Text += "\"" + Unit + "\"" + ", "; }
         


            // Reading file                 
            string The_Text = File.ReadAllText(Lua_Script);

            // string Selected_Faction_Without_Whitespace = System.Text.RegularExpressions.Regex.Replace(Selected_Faction, "[\n\r\t]", " ");
               
            // This replaces these 3 keywords and overwrites the whole file:
            The_Text = The_Text.Replace("\"Faction_Name\"", "\"" + Selected_Faction + "\"");

            // If no Planet Variable was specified, we spawn on the planet where the Cheating dummy was built
            if (Selected_Planet == "Build_Planet")
            {   The_Text = The_Text.Replace("\"Planet_Name_01\"", "\"" + "" + "\"");
                The_Text = The_Text.Replace("Build_Planet = false", "Build_Planet = true"); }           
            else // Otherwise we use the assigned Planet Variable for Target Planet
            {The_Text = The_Text.Replace("\"Planet_Name_01\"", "\"" + Target_Planet + "\"");}
           
            The_Text = The_Text.Replace("\"Planet_Name_02\"", "\"" + Start_Planet + "\"");
            The_Text = The_Text.Replace("\"Selected_Units\"", Spawnlist_Text); // DON'T replace Selected_Units by Found_Units!

            // If Teleport, Destroy or Spawn were activated we will insert the Value "true" to enable them in the script
            The_Text = The_Text.Replace("Spawn = false", Activate_Spawn);
            The_Text = The_Text.Replace("Teleport_Units = false", Teleport_Units);
            The_Text = The_Text.Replace("Teleport_from_Planet = false", Teleport_from_Planet);
            The_Text = The_Text.Replace("Teleport_Both = false", Teleport_Units_on_Planet);
            The_Text = The_Text.Replace("Destroy_Target_Planet = false", Destroy_Target_Planet);

                           
            // Giving Player Money if he setted any value
            if (Player_Credits != 0) { The_Text = The_Text.Replace("\"Player_Credits\"", Player_Credits.ToString()); }
            else The_Text = The_Text.Replace("\"Player_Credits\"", "0" );


            File.WriteAllText(Lua_Script, The_Text);
           

            // Resetting for next usage
            Activate_Spawn = "Spawn = false";
            Teleport_Units = "Teleport_Units = false";
            Teleport_from_Planet = "Teleport_from_Planet = false";
            Teleport_Units_on_Planet = "Teleport_Both = false";
            Destroy_Target_Planet = "Destroy_Target_Planet = false";           
           
            Player_Credits = 0;
            Track_Bar_Credits.Value = 0;
            Progress_Bar_Credits.Value = 0;
            Text_Box_Credits.Text = "";
                               
        }

        //=====================//

        // Xml_Utility(Selected_Xml, null, null, null);     = Returns a list of all found Instances in this Xml
        // Xml_Utility(Selected_Xml, null, "Tag_Name", null);  = Returns a list of all found Instances of the SPECIFIED type

        // Xml_Utility(Selected_Xml, "Unit_Name", null, null);     = Returns the full Unit 
        // Xml_Utility(Selected_Xml, "Unit_Name", "Tag_Name", null);     = Returns value of that Tag
        // Xml_Utility(Selected_Xml, "Unit_Name", "Tag_Name", Tag_Value);     = Returns a boolean answer to find out whether that Tag value exists
        // Xml_Utility(Selected_Xml, "Unit_Name", "Tag_Name", "Return_All");     = Returns a string (seperated by ,) with all found values for this tag

        
        // Xml_Utility(null, "Unit_Name", null, null);     = Searches all Xml files and returns the full Unit if found (costs more performance)
        // Xml_Utility(null, "Unit_Name", "Tag_Name", null);      = Searches all Xml files and returns the Value of Tag_Name


        string Xml_Utility(string Xml_Name, string Required_Instance, string Required_Tag_Name, string Required_Tag_Value)
        {
            if (Xml_Name == null)
            {
                foreach (var Xml in Get_Xmls())
                {
                    try // Preventing the program from crashing because of xml errors
                    {
                        // ===================== Opening Xml File =====================
                        XDocument Xml_File = XDocument.Load(Xml, LoadOptions.PreserveWhitespace);
                   
                        
                        var Instances =
                            from All_Tags in Xml_File.Root.Descendants()
                            // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
                            where (string)All_Tags.Attribute("Name") == Required_Instance
                            select All_Tags;


                        // =================== Checking Xml Instance ===================
                        foreach (XElement Instance in Instances)
                        {
                            if (Required_Instance != null & Required_Tag_Name != null & Required_Tag_Name != "")
                            { if (Instance.Descendants(Required_Tag_Name).Any()) try { return Instance.Descendants(Required_Tag_Name).First().Value; } catch { } }                          
                        
                            else if (Required_Instance == null) { return null; } 

                            // If neither Tag Value nor Unit type was specified (nothing but the Instance Name) we simply return the whole instance
                            else if (Required_Tag_Name == null) { return Instance.ToString(); }  
                        }
                    }
                    catch { /* Imperial_Console(600, 100, "Failed to load " + Path.GetFileName(Xml) + Add_Line + "Or a Unit inside of it.");*/ }
                }
            }

            else if (Xml_Name != null)
            {   try
                {   

                    if (Required_Instance == "Root") // Opening one level over the Instances, the first tag
                    {
                        XDocument Xml_File = XDocument.Load(Xml_Name, LoadOptions.PreserveWhitespace);

                        var Tags =
                           from All_Tags in Xml_File.Descendants()
                           // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
                           where All_Tags.Name == Required_Tag_Name
                           select All_Tags;

                        
                        Temporal_A = "";

                        foreach (XElement Tag in Tags)
                        {   if (Required_Tag_Value == null) { return null; }
                            else if (Required_Tag_Value == "Return_All") { Temporal_A += Tag.Value + ","; }
                            // If any value was specified the Utility checks whether the Xml contais it and returns a boolean value if true
                            else if (Required_Tag_Value == Tag.Value) { Temporal_A = "true"; break; }
                            else if (Required_Tag_Value != Tag.Value) { Temporal_A = "false"; }   
                        }
                        return Temporal_A;
                    }
                    
                    else if (Required_Instance != null)
                    {   // ===================== Opening Xml File =====================
                        XDocument Xml_File = XDocument.Load(Xml_Name, LoadOptions.PreserveWhitespace);

                        var Instances =
                           from All_Tags in Xml_File.Root.Descendants()
                           // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
                           where (string)All_Tags.Attribute("Name") == Required_Instance
                           select All_Tags;


                        // =================== Checking Xml Instance ===================
                        foreach (XElement Instance in Instances)
                        {
                           if (Required_Instance != null & Required_Tag_Name != null & Required_Tag_Name != "")
                           { try { return Instance.Descendants(Required_Tag_Name).First().Value; } catch { } }

                           else if (Required_Instance == null) { return null; }

                           // If neither Tag Value nor Unit type was specified (nothing but the Instance Name) we simply return the whole instance
                           else if (Required_Tag_Name == null) { return Instance.ToString(); }  
                        }
                    }
                    else // Return Instance Table(s)
                    {
                        // ===================== Opening Xml File =====================
                        XDocument Xml_File = XDocument.Load(Xml_Name, LoadOptions.PreserveWhitespace);


                      IEnumerable<XElement> Xml_Content = null;

                        if (Required_Tag_Name == null & Required_Tag_Name == "") // No Tag Type Specified, return all Types it finds
                        {  Xml_Content =
                             from All_Tags in Xml_File.Root.Descendants()
                             // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
                             where (string)All_Tags.Attribute("Name") != null
                             select All_Tags;                      
                        }     
                        else // Return ONLY entries of the specified Tag_Name
                        {
                          Xml_Content =

                             from All_Tags in Xml_File.Root.Descendants(Required_Tag_Name)
                             // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
                             where (string)All_Tags.Attribute("Name") != null
                             select All_Tags;
                        }   
                        
                    
                        string All_Instances = "";
                        Temporal_C = 0;

                        // =================== Checking Xml Instance ===================
                        foreach (XElement Instance in Xml_Content)
                        {
                            Temporal_C++;

                            // Adding Attribute Name, the last one gets no ", " attached
                            if (Temporal_C == Xml_Content.Count()) { All_Instances += (string)Instance.FirstAttribute.Value; } 
                            else { All_Instances += (string)Instance.FirstAttribute.Value + ","; }                                                   
                        }



                        return All_Instances;                      
                    }
               
                }
                catch { /* Imperial_Console(600, 100, "Failed to load " + Path.GetFileName(Xml) + Add_Line + "Or a Unit inside of it.");*/ }
            }
            return null;
        }

        //===========================//

        // Get_Instance_Table(The_File, null);    Returns all found instances in this file
        // Get_Instance_Table(The_File, Unit_Type);    Returns all Instances of the Unit_Type
        
        string[] Get_Instance_Table(string The_File, string Type)
        {
            try
            {   // Opening Xml            
                XDocument Xml_File = XDocument.Load(The_File, LoadOptions.PreserveWhitespace);

                Temporal_A = "";
                IEnumerable<XElement> Instances;


                if (Type != null) { Instances = from All_Instances in Xml_File.Root.Descendants(Type) select All_Instances; }
                else { Instances = from All_Instances in Xml_File.Root.Descendants() select All_Instances; } // Otherwise returning a list of all other units


                foreach (XElement Instance in Instances) { Temporal_A += (string)Instance.Attribute("Name") + ","; }

                // Returning first found Instances 
                return Temporal_E = Temporal_A.Split(',');
            }
            catch {}

            return Temporal_E = new string[] {};
        }


        //===========================//

        // Get_Instance(The_File, Unit_Type, Unit_Name)    Returns all found Instances of that Type AND Name
        // Get_Instance(The_File, Unit_Type, null)    Returns all Instances of that Type in the Xml File
        // Get_Instance(The_File, null, Unit_Name)    Returns all Instances of that Name in the Xml File

        IEnumerable<XElement> Get_All_Instances(string The_File, string Unit_Type, string Unit_Name)
        {
            try
            {
                // Opening Xml            
                XDocument Xml_File = XDocument.Load(The_File, LoadOptions.PreserveWhitespace);


                if (Unit_Type != null & Unit_Name != null)
                {
                    var Instances =
                       from All_Instances in Xml_File.Root.Descendants(Unit_Type)
                       where (string)All_Instances.Attribute("Name") == Unit_Name
                       select All_Instances;

                    // Returning all found Instances of that type and name
                    return Instances;
                }
                else if (Unit_Type != null & Unit_Name == null)
                {   // Returning all Instances of that type in the Xml File
                    return from All_Instances in Xml_File.Root.Descendants(Unit_Type) select All_Instances;
                }
                else if (Unit_Type == null & Unit_Name != null)
                {
                    var Instances =
                       from All_Instances in Xml_File.Root.Descendants()
                       where (string)All_Instances.Attribute("Name") == Unit_Name
                       select All_Instances;

                    // Returning all Instances of that name in the Xml File
                    return Instances;
                }

            }
            catch { }

            return null;
        }

        //===========================//

        // Get_Instance(The_File, "Planet", Unit_Name) 
        XElement Get_Instance(string The_File, string Unit_Type, string Unit_Name)
        {
            try {   
                // Opening Xml            
                XDocument Xml_File = XDocument.Load(The_File, LoadOptions.PreserveWhitespace);
              

                if (Unit_Type != null & Unit_Name != null)
                {
                    var Instances =
                       from All_Instances in Xml_File.Root.Descendants(Unit_Type)
                       where (string)All_Instances.Attribute("Name") == Unit_Name
                       select All_Instances;

                    // Returning first found Instance 
                    foreach (XElement Instance in Instances) { return Instance; }
                }
                else if (Unit_Type == null & Unit_Name != null)
                {
                    var Instances =
                       from All_Instances in Xml_File.Root.Descendants()
                       where (string)All_Instances.Attribute("Name") == Unit_Name
                       select All_Instances;

                    // Returning first found Instance 
                    foreach (XElement Instance in Instances) { return Instance; }
                }

            } catch { }

            return null;
        }


        //=====================//
             
        // Returns Required_Tag_Value
        // Ability_Utility(Xml_Name, Required_Instance, Required_Ability, Required_Tag_Value):
        // Ability_Utility(null, Required_Instance, Required_Ability, Required_Tag_Value):

        // Returns all values of Mod_Multiplayers inside of a string seperated by , signs. We split it later
        // Ability_Utility(Xml_Name, Required_Instance, Required_Ability, "Mod_Multiplier"):

        // Returns the full Required_Ability code
        // Ability_Utility(Xml_Name, Required_Instance, Required_Ability, null):
        // Ability_Utility(null, Required_Instance, Required_Ability, null):

        // Returns all found "Ability" tags in this unit and seperates them by , which you can split then:
        // Ability_Utility(Xml_Name, Required_Instance, null, null):
        // Ability_Utility(null, Required_Instance, null, null):

        // Returns Name of all Instances with the required ability in this Xml or in the whole mod if Xml_Name == null
        // Ability_Utility(Xml_Name, null, "Stealth", null));
        // Ability_Utility(null, null, "Stealth", null));


        string Ability_Utility(string Xml_Name, string Required_Instance, string Required_Ability, string Required_Tag_Value)
        {   string Result = "";

            if (Xml_Name != null)
            { Result = Get_Ability_Code(Xml_Name, Required_Instance, Required_Ability, Required_Tag_Value); }
           
            else if (Xml_Name == null)
            {   foreach (var Xml in Get_Xmls())
                { Result += Get_Ability_Code(Xml, Required_Instance, Required_Ability, Required_Tag_Value); }
            }

            return Result;
        }


        string Get_Ability_Code(string Xml_Name, string Required_Instance, string Required_Ability, string Required_Tag_Value)
        {                    
            Temporal_A = "";
            Temporal_B = "";
            string Mod_Multipliers = "";

            try 
            {   // ===================== Opening Xml File =====================
                XDocument Xml_File = XDocument.Load(Xml_Name, LoadOptions.PreserveWhitespace);

                var Instances =
                    from All_Tags in Xml_File.Root.Descendants()
                    // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
                    where  (string)All_Tags.Attribute("Name") != null
                    & All_Tags.Descendants("Unit_Abilities_Data").Descendants("Unit_Ability").Descendants("Type").Any()
                    select All_Tags;


                // =================== Checking Xml Instance ===================
                foreach (XElement Instance in Instances)
                {
                    Temporal_A = (string)Instance.FirstAttribute.Value;
                    string Ability_Tag_Pool = "Type, Supports_Autofire, GUI_Activated_Ability_Name, GUI_Activated_Ability_Name, Expiration_Seconds, Recharge_Seconds, Alternate_Name_Text, Alternate_Description_Text, Alternate_Icon_Name, SFXEvent_GUI_Unit_Ability_Activated, SFXEvent_GUI_Unit_Ability_Deactivated";
 


                    if (Required_Tag_Value == "Abilities" & Instance.Descendants("Abilities").Any())
                    { return Instance.Descendants("Abilities").First().ToString(); }

                    if (Required_Instance == null)
                    {
                        if (Regex.IsMatch(Instance.Descendants("Unit_Abilities_Data").Descendants("Unit_Ability").Descendants("Type").First().Value, "(?i)" + Required_Ability))
                        {                         
                            // Adding Attribute Name to list of units that own this ability
                            Temporal_B += Temporal_A + ",";
                        }                     
                    }

                    else if (Required_Instance == Temporal_A)
                    {   // We search all "Unit_Ability" subtags
                        foreach (var Tag in Instance.Descendants("Unit_Abilities_Data").Descendants("Unit_Ability"))
                        {
                            // if (Tag.Descendants("Type").First().Value == Required_Ability) Using Regex instead to be not case sensitive
                            if (Regex.IsMatch(Tag.Descendants("Type").First().Value, "(?i)" + Required_Ability))
                            {   // If neither Instance nor Unit type was specified (nothing but the Instance Name) we simply return all abilities
                                if (Required_Ability != null & Required_Tag_Value == null)
                                { try { return Tag.ToString(); } catch {} }

                                else if (Required_Tag_Value != null & Required_Tag_Value != "Mod_Multiplier") 
                                { try { return Tag.Descendants(Required_Tag_Value).First().Value; } catch { } }


                                if (Required_Tag_Value == "Mod_Multiplier")
                                {   foreach (var Entry in Tag.Descendants())
                                    {   if (!Ability_Tag_Pool.Contains(Entry.Name.ToString()))
                                        { Mod_Multipliers += Regex.Replace(Entry.ToString(), @"\t", " ") + ";"; }                                  
                                    }                                                                  
                                }  
                              
                            }
                           
                            if (Required_Ability == null) { Temporal_B += Tag.Descendants("Type").First().Value + ","; }
                        }

                        if (Required_Tag_Value == "Mod_Multiplier") { return Mod_Multipliers; }
                        
                        // Otherwise we return the name of all found abilities 
                        if (Required_Ability == null) { return Temporal_B; }
                    }
                                                                                                                                                                                                                                                          
                 }
            } catch {} 
            return Temporal_B;
        }

        //=====================//

        // This function is called for both, the Unit itself and its Team/Squadron, 
        // Where "Instance" means the current unit which changes from Unit to target at Squadron/Team
        void Save_Abilities() 
        {

            Ability_1_Icon = Verify_Ability_Icon(Ability_1_Icon);
            Ability_2_Icon = Verify_Ability_Icon(Ability_2_Icon);

            // string Instance_Text = Instance.First().ToString();

            if (Ability_1_Mod_Multipliers != null)
            { Temporal_A = Regex.Replace(Ability_1_Mod_Multipliers, ";", "\n"); }
            else { Temporal_A = Ability_1_Mod_Multipliers; }



            // Clearing abilities 
            Remove_Abilities(); 

        
            // Ability_1_Type is set by Combo_Box_Ability_Type when the user selects anything
            if (Ability_1_Type != null & Ability_1_Type != "" & Ability_1_Type != " NONE")
            {
                if (Ability_1_Remove == null | Ability_1_Remove == "")
                {
                    // Clearing all tags that are not the ones below
                    Validate_Save_Ability_Code(Ability_1_Type, null, "Clear");

                    Validate_Save_Ability_Code(Ability_1_Type, "Type", Ability_1_Type);
                    Validate_Save_Ability_Code(Ability_1_Type, "Supports_Autofire", Ability_1_Toggle_Auto_Fire.ToString());
                    Validate_Save_Ability_Code(Ability_1_Type, "GUI_Activated_Ability_Name", Ability_1_Activated_GUI);
                    Validate_Save_Ability_Code(Ability_1_Type, "Expiration_Seconds", Ability_1_Expiration_Time);
                    Validate_Save_Ability_Code(Ability_1_Type, "Recharge_Seconds", Ability_1_Recharge_Time);

                    Validate_Save_Ability_Code(Ability_1_Type, "Alternate_Name_Text", Ability_1_Name);
                    Validate_Save_Ability_Code(Ability_1_Type, "Alternate_Description_Text", Ability_1_Description);
                    Validate_Save_Ability_Code(Ability_1_Type, "Alternate_Icon_Name", Ability_1_Icon);

                    Validate_Save_Ability_Code(Ability_1_Type, "SFXEvent_GUI_Unit_Ability_Activated", Ability_1_Activated_SFX);
                    Validate_Save_Ability_Code(Ability_1_Type, "SFXEvent_GUI_Unit_Ability_Deactivated", Ability_1_Deactivated_SFX);


                    if (Temporal_A != null)
                    {                        
                        // We use a XComment that is marked with "here" that we remove later in order to preserve < and > signs, also we add "\t\t\t\t" to each line
                        Validate_Save_Ability_Code(Ability_1_Type, "Unit_Ability", Regex.Replace(Temporal_A, "\n", "\n\t\t\t\t"));
                    }

                }               
            }        


            if (Ability_2_Mod_Multipliers != null)
            { Temporal_A = Regex.Replace(Ability_2_Mod_Multipliers, ";", "\n"); }
            else { Temporal_A = Ability_2_Mod_Multipliers; }


            if (Ability_2_Type != null & Ability_2_Type != "" & Ability_2_Type != " NONE")
            {
                if (Ability_2_Remove == null | Ability_2_Remove == "")
                {
                    Validate_Save_Ability_Code(Ability_2_Type, null, "Clear");

                    Validate_Save_Ability_Code(Ability_2_Type, "Type", Ability_2_Type);
                    Validate_Save_Ability_Code(Ability_2_Type, "Supports_Autofire", Ability_2_Toggle_Auto_Fire.ToString());
                    Validate_Save_Ability_Code(Ability_2_Type, "GUI_Activated_Ability_Name", Ability_2_Activated_GUI);
                    Validate_Save_Ability_Code(Ability_2_Type, "Expiration_Seconds", Ability_2_Expiration_Time);
                    Validate_Save_Ability_Code(Ability_2_Type, "Recharge_Seconds", Ability_2_Recharge_Time);

                    Validate_Save_Ability_Code(Ability_2_Type, "Alternate_Name_Text", Ability_2_Name);
                    Validate_Save_Ability_Code(Ability_2_Type, "Alternate_Description_Text", Ability_2_Description);
                    Validate_Save_Ability_Code(Ability_2_Type, "Alternate_Icon_Name", Ability_2_Icon);

                    Validate_Save_Ability_Code(Ability_2_Type, "SFXEvent_GUI_Unit_Ability_Activated", Ability_2_Activated_SFX);
                    Validate_Save_Ability_Code(Ability_2_Type, "SFXEvent_GUI_Unit_Ability_Deactivated", Ability_2_Deactivated_SFX);

                    if (Temporal_A != null) { Validate_Save_Ability_Code(Ability_2_Type, "Unit_Ability", Regex.Replace(Temporal_A, "\n", "\n\t\t\t\t")); }              
                }
            }



            // Making sure it ends with only 1 "</Abilities>"  
            if (!Is_Team & !Text_Box_Additional_Abilities.Text.EndsWith("</Abilities>") & Text_Box_Additional_Abilities.Text != "")
            {
                Text_Box_Additional_Abilities.Focus();

                Imperial_Dialogue(500, 160, "Save", "Cancel", "false", Add_Line + "    Warning: The text inside of the Passive Abilities"
                                                             + Add_Line + @"    Box does not end with ""</Abilities>"".");

                if (Caution_Window.Passed_Value_A.Text_Data == "false") { return; }
            }
            else if (!Is_Team & Text_Box_Additional_Abilities.Text.EndsWith("</Abilities></Abilities>"))
            { Text_Box_Additional_Abilities.Text = Text_Box_Additional_Abilities.Text.Replace("</Abilities></Abilities>", "</Abilities>"); }



            // Removing Tag Variable (was needed before in order to append Tags to it)
            if (Text_Box_Additional_Abilities.Text != "")
            {
                Temporal_B = Regex.Replace(Text_Box_Additional_Abilities.Text, @"<Abilities SubObjectList=""Yes"">", "");
                Temporal_A = Regex.Replace(Temporal_B, @"</Abilities>", "");
            }
            else { Temporal_A = ""; }


            // This wont run if the Checkbox for teams is unchecked
            Temporal_B = "Squadron, GroundCompany, HeroCompany";

            try
            {  if (Temporal_B.Contains(Instance.First().Name.ToString()) // Also if Text_Box_Additional_Abilities.Text contains nothing we can skip the returning
                & Text_Box_Additional_Abilities.Text != "" & Check_Box_Use_In_Team.Checked == false) { return; }
            } catch {}

            if (Instance.Descendants("Abilities").Any()) // If exists we will adjust the value
            {                
                // Need to delete and rebuild the whole Abilities tag each time because Text_Box_Additional_Abilities mitght has changed
                Instance.Descendants("Abilities").First().Value = "";
                Instance.Descendants("Abilities").First().Add(new XComment("here" + Temporal_A + "here"));
            }

            else if (Text_Box_Additional_Abilities.Text != null & Text_Box_Additional_Abilities.Text != "")
            {   Instance.First().Element("Unit_Abilities_Data").AddAfterSelf(Add_Line,
                    "\t\t", new XComment(" ======== Additional Abilities ========= "), Add_Line,
                    "\t\t", new XComment("here" + Text_Box_Additional_Abilities.Text + "here"), Add_Line,
                    "\t\t", new XComment(" ======================================= "), Add_Line
                ); // End of Abilities                                     
            }


            // Resetting Icon values
            try { Ability_1_Icon = Regex.Replace(Ability_1_Icon, "(?i).tga", ""); } catch {}
            try { Ability_2_Icon = Regex.Replace(Ability_2_Icon, "(?i).tga", ""); } catch { }                    
        }

        //=====================//
        void Verify_Edit_Xml_Tag(string Xml_Name, string Selected_Unit, string Tag_Name, string New_Tag_Value)
        {         
            try
            {   //===== Loading .Xml File =====
                XDocument Xml_File = XDocument.Load(Xml_Name, LoadOptions.PreserveWhitespace);
          
                           
                IEnumerable<XElement> Selected_Instance =
                // Selecting all child Tags from tags that have the Name "SpecialStructure"   
                from All_Tags in Xml_File.Root.Descendants()
                // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
                where (string)All_Tags.Attribute("Name") == Selected_Unit
                select All_Tags;
 
                if (Selected_Instance.Descendants(Tag_Name).First().Value != New_Tag_Value)
                { Selected_Instance.Descendants(Tag_Name).First().Value = New_Tag_Value; } 
                

                //====== Saving Changes ======

                Xml_File.Save(Xml_Name);
            } catch { }
        }
       
        //=====================//

        List<string> Load_Xml_Unit(string Xml_Name, string Current_Unit)
        {
            try 
            {   //======== Loading .Xml File ==========     
                XElement Xml_File = XElement.Load(Xml_Name);


                // Getting the current Unit and selecting all its Tags
                var Tags = Xml_File.Elements(Unit_Type)
                    .Where(Item => Item.Attribute("Name").Value == Current_Unit)
                    .SelectMany(Entry => Entry.Descendants()).ToList();



                // Removing all listed items from the List Box 
                List_View_Selected_Tag.Items.Clear();

                foreach (var Tag in Tags)
                {
                    // Inserting the the Names in tags and the Value between them
                    List_View_Selected_Tag.Items.Add("<" + Tag.Name + ">" + Tag.Value + "</" + Tag.Name + ">");

                    // Adding it to a List
                    Selected_Instance_Tags.Add(Tag.ToString());
                }
            } 
            catch { Imperial_Console(700, 150, "Could not find " + Add_Line + Xml_Name); }

            return Selected_Instance_Tags;   
        }


        //=====================//

        List<string> Load_Xml_File2(string Xml_Name)
        {

            int Xml_Entries = 0;

            try
            {
                //======== Loading .Xml File ==========     
                XElement Xml_File = XElement.Load(Xml_Name);

                Xml_Entries = 1;

                // Getting the current Unit and selecting all its Tags
                var Tags = Xml_File.Elements()
                .SelectMany(Entry => Entry.Descendants()).ToList();


                // Removing all listed items from the List Box 
                List_View_Selected_Tag.Items.Clear();

              

                foreach (var Tag in Tags)
                {
                    
                    // Inserting the the Names in tags and the Value between them               
                    var Item = List_View_Selected_Tag.Items.Add("<" + Tag.Name + ">" + Tag.Value + "</" + Tag.Name + ">");
                                      
                   
                    Item.ForeColor = Color.Black;

                    // Every second item should have this value in order to create a checkmate pattern with good contrast
                    if (Checkmate_Color == "true" & Item.Index % 2 == 0)
                    {   Item.ForeColor = Color_03;
                        Item.BackColor = Color_07;
                    }

                    if (Tag.Value == "Yes" | Tag.Value == "No" | Tag.Value == "yes" | Tag.Value == "no" | Tag.Value == "True" | Tag.Value == "False" | Tag.Value == "true" | Tag.Value == "false")
                    { Item.BackColor = Color_02; }


                    // Adding it to a List
                    Selected_Instance_Tags.Add(Tag.ToString());
                }
            } 
            
            catch 
            {   Size Window_Size = new Size(516, 200);

                // Using the Costum Massage Box
                Show_Message_Box_One_Button(Window_Size, Xml_Name, null);                      
            }

            return Selected_Instance_Tags;
        }



        //=====================//

        List<string> Load_Xml_File(string Xml_Name)
        {   // Removing all listed items from the List View 
            List_View_Selected_Tag.Items.Clear();

            
            try
            {
                foreach (string Line in File.ReadLines(Xml_Name))
                {
                    
                    string New_Line = Line;

                    // We remove all (t)abulator keys and emptyspace, except for comments with <!-- --> and Attributes
                    if (!Line.Contains(@"<!--") & !Line.Contains(@"-->") & !Line.Contains(@"Name="""))
                    {   
                        // Removing all Tab key values
                        string Untabbed = Regex.Replace(Line, @"\t", "");
                        // Removing all Emptyspace values
                        New_Line = Regex.Replace(Untabbed, @" ", "");
                      
                    }                   
                    else if (Line.Contains(@"Name="""))
                    {
                        string Untabbed = Regex.Replace(Line, @"\t", "");
                        New_Line = Regex.Replace(Untabbed, @".*?<", "<");
                    }
                  


                    // Drawing all of them in our List View                        
                    var Item = List_View_Selected_Tag.Items.Add(New_Line);


                    Item.ForeColor = Color.Black;

                    // Every second item should have this value in order to create a checkmate pattern with good contrast
                    if (Checkmate_Color == "true" & Item.Index % 2 == 0)
                    {
                        Item.ForeColor = Color_03;
                        Item.BackColor = Color_07;
                    }
                              

                    // After drawing it, we seperate the Xml tags from their values:
                    // Removing all <XML_Tags> to get the value only
                    string The_Value = Regex.Replace(New_Line, @"<.*?>", "");
                    
                    // Highlighting boolean type values, disabled because triggers to highlight wrong tags
                    if (The_Value == "Yes" | The_Value == "No" | The_Value == "yes" | The_Value == "no") { Item.ForeColor = Color_02; }
                    else if (The_Value == "True" | The_Value == "False" | The_Value == "true" | The_Value == "false") { Item.ForeColor = Color_02; }
                    
                    // Marking commented lines in green
                    else if (Line.Contains("<!--") | Line.Contains("-->")) { Item.ForeColor = Color.Green; }

        
                    /*
                    // If the current line doesen't contain any tags I originally wanted to assign the value in a single line to the tag above, didnt worked
                    if (!Regex.IsMatch(Line, "<.*?>") & !Line.Contains("\t") & Line !=  "")
                    {   // We get the count of the newest entry
                        
                        // var Last_Item = Selected_Instance_Tags.ElementAt(Selected_Instance_Tags.Count());
                        // And add our line to the entry above, in orer to align all values in the same line by eachother.
                        // Last_Item += Line; 
                    } 
                    */

                    // Adding tag + value of this line to a List
                    Selected_Instance_Tags.Add(New_Line);
                }
            }

            catch
            {             
                Size Window_Size = new Size(516, 200);

                // Using the Costum Massage Box
                Show_Message_Box_One_Button(Window_Size, Xml_Name, null);
            }


            return Selected_Instance_Tags;

        }
        //=====================//

        string Load_Instance_Tag(string Xml_Name, string Current_Unit, string Desired_Tag)
        {   
            // Preparing Return Value
            string Current_Selection = "";
            
            int Xml_Entries = 0;


            try
            {
                //======== Loading .Xml File ==========     
                XElement Selected_Xml_File = XElement.Load(Xml_Name);

                // If we made it so far this means the xml was found, otherwise the 
                Xml_Entries = 1;

                // Defining the Variable we are going to use below
                IEnumerable<XElement> Entries;
             
           
                /* // temp
                if (Desired_Tag == "Unit_Name")
                {   // No idea why, need to get the name again or Linq wont allow me to modify the damn tag -.- otherwise it says "Sequence contains more than one matching element"
                    Entries = from All_Tags in Selected_Xml_File.Descendants(Unit_Type)               
                              where (string)All_Tags.Attribute("Name").Value == Current_Unit
                              select All_Tags;

                    foreach (XElement Tags in Entries)
                    {
                        // Putting these Element of the Tags into a string Variable
                        Current_Selection = (string)Tags.FirstAttribute.Value;
                    }      
                }*/

                // Projectile is a special one because it is outside of the Xml Unit
                if (Desired_Tag == "Projectile_Damage") 
                { 
                     Entries = Selected_Xml_File.Elements("Projectile")
                        .Select(Tag => Tag.Descendants(Desired_Tag).First()).ToList();
                 
                } else {
                // Selecting the right Unit and choosing the desired tag from it
                     Entries = Selected_Xml_File.Elements(Unit_Type)
                    .Where(item => item.Attribute("Name").Value == Current_Unit)
                    .Select(Tag => Tag.Descendants(Desired_Tag).First()).ToList();                  
                }


                foreach (var Entry in Entries)
                {
                    if (Entry.Name == Desired_Tag)
                    {
                        Current_Selection = Entry.Value;
                    }
                }
              
            }
            catch
            {
                if (Xml_Entries == 0)
                {   // Disabled because this spamms 20 error boxes -.-
                    // Xml_Exception_Files.Add(Xml_Name);
                    // Imperial_Console(600, 100, Add_Line + "    Could not find the Xml File, please press Refresh and try again.");                   
                }

                // This returns that no Variant Tag was found for this Unit
                else if (Desired_Tag == "Variant_Of_Existing_Type") { Current_Selection = "Is_No_Variant"; }
                // Otherwise we could not find the Tag and adding that tag into this List to forward it to the User
                else { Misloaded_Tags.Add(Desired_Tag); }
            }

            // Returning our Result Values
            return Current_Selection;  
        }

        //=====================//
        string Load_Operator_Tag(string Tag_Content, EventHandler Operator_Button_Event, bool Operator_Switch)
        {
            string Result = "";

            if (Tag_Content.Contains("-")) 
            {
                Result = Tag_Content.Replace("-", ""); // If the + - button is at + (true) we turn it to -
                if (Operator_Switch) { Operator_Button_Event(null, null); }
            }
            else // Otherwise it is a + value
            {   // And we need to make sure it toggles to +
                if (Operator_Switch == false) { Operator_Button_Event(null, null); }
                Result = Tag_Content;
            } 
      
            return Result;
        }

        //=====================//
        void Set_Variant_Box(CheckBox Check_Box, Control Text_Box, Control Text_Cover, PictureBox Mini_Button)
        {
            if (Check_Box.Checked)
            {
                Check_Box.ForeColor = Color_03;
                if (Text_Box != null) { Text_Box.Visible = true; }               
                Text_Cover.Visible = false;
                if (Mini_Button != null) { Mini_Button.Visible = true; }
            }
            else
            {
                Check_Box.ForeColor = Color_02;
                if (Text_Box != null) { Text_Box.Visible = false; }               
                Text_Cover.Visible = true;
                if (Mini_Button != null) { Mini_Button.Visible = false; }
            }
        }
        
        //=====================//
        // Refresh, Save, Save_As, Copy, Indicator, Arrow, Plus, Minus, Delete (fehlt noch mülltone symbol TODO)
        void Set_Minibutton(PictureBox The_Button, string Button_Name, bool Is_Highlighted)
        {   if (Is_Highlighted)
            {   if (File.Exists(Selected_Theme + @"Buttons\Button_" + Button_Name + "_Highlighted.png")) { The_Button.Image = Resize_Image(Selected_Theme + @"Buttons\", "Button_" + Button_Name + "_Highlighted.png", The_Button.Size.Width, The_Button.Size.Height); }
                else { The_Button.Image = Resize_Image(Program_Directory + @"Themes\Default\Buttons\", "Button_" + Button_Name + "_Highlighted.png", The_Button.Size.Width, The_Button.Size.Height); }            
            }
            else
            {
                if (File.Exists(Selected_Theme + @"Buttons\Button_" + Button_Name + ".png")) { The_Button.Image = Resize_Image(Selected_Theme + @"Buttons\", "Button_" + Button_Name + ".png", The_Button.Size.Width, The_Button.Size.Height); }
                else { The_Button.Image = Resize_Image(Program_Directory + @"Themes\Default\Buttons\", "Button_" + Button_Name + ".png", The_Button.Size.Width, The_Button.Size.Height); }        
            }
       }

        //=====================//
        // Refresh, Save, Save_As, Copy, Indicator, Arrow, Plus, Minus, Delete 
        void Set_Button(PictureBox The_Button, string Button_Name)
        {
            if (File.Exists(Selected_Theme + @"Buttons\Button_" + Button_Name + ".png")) { The_Button.Image = Resize_Image(Selected_Theme + @"Buttons\", "Button_" + Button_Name + ".png", The_Button.Size.Width, The_Button.Size.Height); }
            else { The_Button.Image = Resize_Image(Program_Directory + @"Themes\Default\Buttons\", "Button_" + Button_Name + ".png", The_Button.Size.Width, The_Button.Size.Height); }                               
        }
        //=====================//
        void Set_Image(Control Picture_Box, string Image_Name)
        {   try
            {   if (File.Exists(Selected_Theme + Image_Name + ".png"))
                { Picture_Box.BackgroundImage = Resize_Image(Selected_Theme, Image_Name + ".png", Picture_Box.Size.Width, Picture_Box.Size.Height); }
                else if (File.Exists(Selected_Theme + Image_Name + ".jpg"))
                { Picture_Box.BackgroundImage = Resize_Image(Selected_Theme, Image_Name + ".jpg", Picture_Box.Size.Width, Picture_Box.Size.Height); }

                else if (File.Exists(Program_Directory + @"Themes\Default\" + Image_Name + ".png"))
                { Picture_Box.BackgroundImage = Resize_Image(Program_Directory + @"Themes\Default\", Image_Name + ".png", Picture_Box.Size.Width, Picture_Box.Size.Height); }

                else if (File.Exists(Program_Directory + @"Themes\Default\" + Image_Name + ".jpg"))
                { Picture_Box.BackgroundImage = Resize_Image(Program_Directory + @"Themes\Default\", Image_Name + ".jpg", Picture_Box.Size.Width, Picture_Box.Size.Height); }                       
       
            } catch {}         
        }

        //=====================//

        List<string> Load_Unit_Abilities(string Xml_Name)
        {

            try
            {
                //======== Loading .Xml File ==========     
                XElement Xml_File = XElement.Load(Xml_Name);


                // Getting the current Unit and selecting all its Tags
                var Tags = Xml_File.Elements()
                    .SelectMany(Entry => Entry.Elements().Descendants()).ToList();


                // Removing all listed items from the List Box 
                List_View_Selected_Tag.Items.Clear();

                foreach (var Tag in Tags)
                {

                    // Inserting the the Names in tags and the Value between them
                    List_View_Selected_Tag.Items.Add("<" + Tag.Name + ">" + Tag.Value + "</" + Tag.Name + ">");

                    // Adding it to a List
                    Selected_Instance_Tags.Add(Tag.ToString());
                }
            }
            catch { MessageBox.Show("The searched Tag was not found."); }

            return Selected_Instance_Tags;
        }
        //=====================//


        bool Edit_Max_Value_Text(string Text_Box) 
        {
            // Innitiating UI Textbox Element
            Control Text_Box_Element = this.Controls.Find(Text_Box, true).Single();

            
            // Making sure the User doesen't type any Letters // Here often StackOverfl0w happens because of missing Maximal_Values.txt files!!
            if (Regex.IsMatch(Text_Box_Element.Text, "[^0-9.,]"))
            {
                Imperial_Console(500, 100, Add_Line + "Please enter only numbers.");
                // Removing the wrong Text
                Text_Box_Element.Text = "";
            }

            int Typed_Value;
            Int32.TryParse(Text_Box_Element.Text, out Typed_Value);


            // Making sure the User doesen't type too low or too high values
            if (Typed_Value > 100000)
            {
                Imperial_Console(600, 100, Add_Line + "    Please enter a smaller value. Maximum = 100.000");
                Text_Box_Element.Text = "";
            }
            else if (Typed_Value < 0.9)
            {
                Text_Box_Element.Text = "";
            }

           
            //Depending on whether the user typed anything value we return true/false
            if (Text_Box_Element.Text != "") {return true;}
            else {return false;}
            
        }
        //=====================//


        int Check_Numeral_Text_Box(Control Text_Box, int Maximum_Value, bool Is_Decimal, string Changed_Unit) 
        {
            // Innitiating UI Textbox Elements
            // Control Text_Box_Element = this.Controls.Find(Text_Box, true).Single();
           
 
            // Making sure the User doesen't type any Letters
            if (System.Text.RegularExpressions.Regex.IsMatch(Text_Box.Text, "[^0-9.,]"))
            {
                if (User_Input)
                {   Imperial_Console(500, 100, Add_Line + "Please enter only numbers.");
                    // Removing the wrong Text
                    Text_Box.Text = "";              
                }                          

                // Exclusively for the Add Money Box 
                if (Maximum_Value == Maximum_Credits) Player_Credits = 0;
            }

           
            int Typed_Value;
            Int32.TryParse(Remove_Zero(Text_Box.Text), out Typed_Value);


            // Making sure the User doesen't type too low or too high values
            if (Typed_Value > Maximum_Value)
            {
                if (User_Input)
                {
                    Text_Box.Text = "";
                    Imperial_Console(600, 100, Add_Line + "    Please enter a smaller value or change the Max Settings."
                                                           + Add_Line + "    Maximum = " + Maximum_Value); 
                }
                // If triggered by the Loading Function, we add it to a list that notifies the User
                // else { Error_List.Add(Text_Box.Name.ToString()); }                
            }
            else if (User_Input & !Is_Decimal & Typed_Value < 0.9)
            {
                Text_Box.Text = "";

                //Hiding the Money sign
                if (Maximum_Value == Maximum_Credits) Label_Credit_Sign.Text = "";
            }

            if (User_Input)
            {   if (Changed_Unit == "Unit") { Edited_Selected_Unit = true; }
                else if (Changed_Unit == "Planet") { Edited_Planet = true; }
                else if (Changed_Unit == "Conquest") { Edited_Conquest = true; }
                else if (Changed_Unit == "HardPoint") { Edited_Hard_Point = true; }
            }

            return Typed_Value;
        }


        
        //=====================//
        decimal Set_From_Float_Text_Box(Control Text_Box, TrackBar Track_Bar, ProgressBar Progress_Bar, int Maximum_Value, int Number, bool Check_Maximum) 
        {
            decimal Typed_Value = 0;
            int Selected_Value = 0;
              
            // Making sure the User doesen't type any Letters
            if (Regex.IsMatch(Text_Box.Text, "[^0-9.,]") & User_Input)
            {
                Imperial_Console(500, 100, Add_Line + "Please enter only numbers.");
                // Removing the wrong Text
                Text_Box.Text = "";
                return Typed_Value;
            }

         
            decimal.TryParse(Text_Box.Text, out Typed_Value);          
            Selected_Value = (int)(Typed_Value * 10);

            /*
            // If contains a decimal sign but not on the last place 
            if (Text_Box.Text.Contains(".") & !Regex.IsMatch(Text_Box.Text, ".*?." + "?")) { Selected_Value = (int)(Typed_Value * Number); }
            else { Selected_Value = (int) Typed_Value; }
             */


            if (Typed_Value > (Maximum_Value * 10) & Maximum_Value != 0) 
            {   // Setting both bars to maximum
                Progress_Bar.Value = Progress_Bar.Maximum;
                Track_Bar.Value = Track_Bar.Maximum;

                if (User_Input & Check_Maximum) 
                {   Text_Box.Text = "";
                    Imperial_Console(700, 100, Add_Line + @"    Please enter a smaller ""decimal"" value or change the Max Settings."  
                                                           + Add_Line + "    Maximum = " + Maximum_Value);
                }
            }
            else
            {  try
                {
                    Progress_Bar.Maximum = Maximum_Value * Number;
                    Track_Bar.Maximum = Maximum_Value * Number;

                    Track_Bar.Value = Selected_Value;                
                    Progress_Bar.Value = Selected_Value;
                } catch { }
            } 

            if (User_Input & Edited_Selected_Unit == false) { Edited_Selected_Unit = true; }
            return Typed_Value;
        }

        //=====================//
        void Set_From_Decimal_Text_Box(Control Text_Box, TrackBar Track_Bar, ProgressBar Progress_Bar, int Maximum_Value, int Number, bool Check_Maximum)
        {
            decimal Typed_Value = 0;

            // Making sure the User doesen't type any Letters
            if (System.Text.RegularExpressions.Regex.IsMatch(Text_Box.Text, "[^0-9.,]") & User_Input)
            {
                Imperial_Console(500, 100, Add_Line + "Please enter only numbers.");
                // Removing the wrong Text
                Text_Box.Text = "";
                return;
            }


            decimal.TryParse(Text_Box.Text, out Typed_Value);

            int Selected_Value = (int)Typed_Value;


            if (Typed_Value > Maximum_Value & Maximum_Value != 0 & User_Input)
            {   // Setting both bars to maximum
                Progress_Bar.Value = Progress_Bar.Maximum;
                Track_Bar.Value = Track_Bar.Maximum;

                if (User_Input & Check_Maximum)
                {
                    Text_Box.Text = "";
                    Imperial_Console(700, 100, Add_Line + @"    Please enter a smaller ""decimal"" value or change the Max Settings."
                                                        + Add_Line + "    Maximum = " + Maximum_Value);
                }
            }
            else
            {   try
                {   Track_Bar.Maximum = Maximum_Value / Number;                   
                    Track_Bar.Value = Selected_Value / Number;

                    Progress_Bar.Maximum = Maximum_Value;          
                    Progress_Bar.Value = Selected_Value;                 
                } catch { }
            }

            if (User_Input & Edited_Selected_Unit == false) { Edited_Selected_Unit = true; }          
        }
                   
        //=====================//

        // This auto sets a value for a similar tag to the first one selected by the UI
        void Process_Tag_Value(ProgressBar Progress_Bar_Percent, TextBox Target_Text_Box, int Maximal_Value)
        {
            double Bar_Percentage = (double)(Progress_Bar_Percent.Value / (double)Progress_Bar_Percent.Maximum) * (double)100;
            Target_Text_Box.Text = ((int)((double)(Maximal_Value / (double)100) * Bar_Percentage)).ToString();         
        }


        //=====================//


        // Searches all .xmls for spawnable units, this function needs quite much performance.
        void Refresh_Units()
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();

            // Depending on the selection of the Unit Type Filter Combo Box, the Function will search all xmls for these kind of units:
            // string Selected_Type = Combo_Box_Filter_Type.Text;


            if (User_Input & Combo_Box_Filter_Type.Text == "")
            {
                Size Window_Size = new Size(516, 200);

                string Message_Text = "The Combo Box for Unit Type Filter is empty, \n   please select anything.";

                // Using the Costum Massage Box and exiting the function
                Show_Message_Box_One_Button(Window_Size, null, Message_Text);

                Application.UseWaitCursor = false;
                Application.DoEvents();

                return;
            }

            

            // Storing Settings for next Usage --> Disabled due to interferences with Save Button
            // if (Combo_Box_Filter_Type.Text != "") { Save_Setting(Setting, "Unit_Type", Selected_Type); }
 

            // Clearing Variable from last usge
            Found_Units.Clear();


            //===============  Adding Items to the lower list:

            // Removing all listed items from the listbox in order to refresh
            List_Box_All_Instances.Items.Clear();
            string Unit_Data_File = "";
            string Current_Line = "";
            bool Matched = false;
            
            foreach (var Xml in Get_Xmls())
            {
                Current_Line = "";
                Matched = false;

                // To prevent the program from crashing because of xml errors:          
                try
                {
                    // Loading the Xml File PATHS:
                    XElement Xml_File = XElement.Load(Xml);


                    // Defining a XElement List
                  IEnumerable<XElement> Matched_Entities =

                    // Selecting all Tags with this string Value:
                    from All_Tags in Xml_File.Descendants(Combo_Box_Filter_Type.Text)
                    // Selecting all non empty tags (null because we need all selected)
                    where (string)All_Tags.Attribute("Name") != null
                    select All_Tags;


                    // Left side, the key is the Filename itself
                    string Removed_Prefix = Xml.Substring(Xml_Directory.Length);
                    Current_Line += Add_Line + Removed_Prefix.Remove(Removed_Prefix.Length - 4) + "=";


                    foreach (XElement Tags in Matched_Entities)
                    {
                        // Putting these Element of the Tags into a string Variable
                        string Selected_Tag = (string)Tags.FirstAttribute.Value;

                        if (!Selected_Tag.Contains("Death_Clone") & Selected_Tag != "") // We filter all Death clone instances out
                        {   Matched = true;
                            // And listing it in its Listbox
                            List_Box_All_Instances.Items.Add(Selected_Tag);

                            // Setting Unit Cache
                            Found_Units.Add(Selected_Tag);
                            Current_Line += Selected_Tag + ",";
                        }                     
                    }

                    if (Matched) { Unit_Data_File += Current_Line; } // Otherwise we forget that line.                
                }
                catch
                {   // Adding Filename to List, later we show it to the User               
                    Xml_Exception_Files.Add(Path.GetFileName(Xml));
                }
            }


            Set_Checkmate_Color(List_Box_All_Instances);
            File.WriteAllText(Program_Directory + "Unit_Data_Files__" + Environment.UserName + ".txt", Unit_Data_File);

       


            if (Show_Load_Issues == "true" & User_Input)
            {
                Size Window_Size_01 = new Size(700, 400);
                // Using the Costum Massage Box
                Show_Message_Box_Xml_Fail(Window_Size_01); 
            }

            Xml_Exception_Files.Clear();

            Application.UseWaitCursor = false;
            Application.DoEvents();
         }
        
        //=====================//

        void Refresh_Selected_Xml()
        {         
            // Clearing Variable from last usge
            Found_Units.Clear();
            Deleting(Program_Directory + "Unit_Data_Files__" + Environment.UserName + ".txt");


            // To prevent the program from crashing because of xml errors:          
            try
            {
                // Loading the Xml File PATHS:
                XElement Xml_File = XElement.Load(Selected_Xml);


                // Defining a XElement List
                IEnumerable<XElement> Content =

                // Selecting all Tags with this string Value:
                from All_Tags in Xml_File.Descendants(Combo_Box_Filter_Type.Text)
                // Selecting all non empty tags (null because we need all selected)
                where (string)All_Tags.Attribute("Name") != null
                select All_Tags;

                foreach (XElement Tags in Content)
                {
                    // Putting these Element of the Tags into a string Variable
                    string Unit_Name = (string)Tags.FirstAttribute.Value;

                    // We filter all Death clone instances out        // TODO Not sure whether !List_Box_All_Instances.Items.ToString().Contains(Unit_Name) works
                    if (!Unit_Name.Contains("Death_Clone") & Unit_Name != "" & !List_Box_All_Instances.Items.ToString().Contains(Unit_Name)) 
                    {   // And listing it in its Listbox
                        List_Box_All_Instances.Items.Add(Unit_Name);                       
                    }
                }

                Set_Checkmate_Color(List_Box_All_Instances);
            }

            catch
            {   if (Show_Load_Issues == "true" & User_Input)
                {
                    Imperial_Console(600, 100, Add_Line + "Failed to load " + Path.GetFileName(Selected_Xml));
                }       
            }

        }
        //=====================//

        void Refresh_Projectile_Data_Files()
        {
            Application.UseWaitCursor = true;
            Application.DoEvents();


            Temporal_A = "";
            
            foreach (var Xml in Get_Xmls())
            {   try // To prevent the program from crashing because of xml errors:    
                {   // Loading the Xml File PATHS:
                    XElement Xml_File = XElement.Load(Xml);

                 IEnumerable<XElement> Found_Projectiles =

                    from All_Tags in Xml_File.Descendants("Projectile")
                    where (string)All_Tags.Attribute("Name") != null
                    select All_Tags;

                    if (Found_Projectiles.Any()) 
                    {   // Removing prefix from the path which leads to XML Directory. This leaves subdirectories allone!
                        if (Xml.StartsWith(Xml_Directory))  { Temporal_A += Xml.Substring(Xml_Directory.Length, Xml.Length - Xml_Directory.Length) + ","; }
                        else { Temporal_A += Path.GetFileName(Xml) + ","; }
                    }                 
                }
                catch { Xml_Exception_Files.Add(Path.GetFileName(Xml)); } // Adding Filename to List, later we show it to the User  
            }

            if (Show_Load_Issues == "true" & User_Input)
            {   Size Window_Size_01 = new Size(600, 400);
                // Using the Costum Massage Box
                Show_Message_Box_Xml_Fail(Window_Size_01); 
            }


            if (Temporal_A == "") { Projectile_Data_Files = new string[] { "Projectiles.xml" }; } // Fall back to Vanilla-Default
            else // Failsafe
            {   if (!Temporal_A.Contains("Projectiles.xml")) { Temporal_A += "Projectiles.xml"; } // No ; because its appended as last entry
                Projectile_Data_Files = Temporal_A.Split(',');           
            } 

            

            Save_Setting(Setting, "Projectile_Data_Files", Array_To_String(Projectile_Data_Files));
            

            Xml_Exception_Files.Clear();
            Application.UseWaitCursor = false;
            Application.DoEvents();           
        }
       

        //=====================//
        // Replace_Items(List_Box_Inactive_Behavoir, List_Box_Active_Behavoir, Old_Item, New_Item);

        public void Replace_Items(ListBox Source_List_Box, ListBox Target_List_Box, string Old_Item, string New_Item)
        {   // Replacing Locomotor Behavoir inside of the pool
            if (Source_List_Box.Items.Contains(Old_Item))
            { Source_List_Box.Items.Remove(Old_Item); }

            if (Target_List_Box.Items.Contains(Old_Item))
            { Target_List_Box.Items.Remove(Old_Item); }

            if (!Target_List_Box.Items.Contains(New_Item) & !Source_List_Box.Items.Contains(New_Item))
            { Target_List_Box.Items.Add(New_Item); }             
        }
          
        //=====================//
      
        public string[] Load_Behavoir_Pool()
        {
            string[] Behavoir_Pool = new string[] {};

            // Depending on the selection of the Unit Type Filter Combo Box, the Function will search all xmls for these kind of units:
            if (Combo_Box_Type.Text == "SpaceUnit" | Combo_Box_Type.Text == "UniqueUnit" | Combo_Box_Type.Text == "Squadron" | Combo_Box_Type.Text == "HeroCompany" | Combo_Box_Type.Text == "TransportUnit")
            {   // Defining Behavoir Pool, according to unit type
                Behavoir_Pool = new string[] { "ABILITY_COUNTDOWN", "IDLE", "SPAWN_SQUADRON", "SIMPLE_SPACE_LOCOMOTOR", "POWERED", "SHIELDED", "TARGETING", "HIDE_WHEN_FOGGED", "REVEAL", "UNIT_AI", "ASTEROID_FIELD_DAMAGE", "DAMAGE_TRACKING", "ION_STUN_EFFECT", "NEBULA", "SELF_DESTRUCT", "VEHICLE_THIEF" };
                Behavoir_Tag = "SpaceBehavior";
            }

            // For the Land Units
            else if (Combo_Box_Type.Text == "HeroUnit" | Combo_Box_Type.Text == "GroundInfantry" | Combo_Box_Type.Text == "GroundVehicle" | Combo_Box_Type.Text == "GroundCompany")
            {   // Disabled because they cause movement issues:  "LAND_TEAM_INFANTRY_LOCOMOTOR", "LAND_TEAM_CONTAINER_LOCOMOTOR", 
                Behavoir_Pool = new string[] { "WALK_LOCOMOTOR", "FLYING_LOCOMOTOR", "IDLE", "TARGETING", "TEAM_TARGETING", "WEAPON", "POWERED", "SHIELDED", "ABILITY_COUNTDOWN", "AFFECTED_BY_SHIELD", 
                        "TREAD_SCROLL", "SPAWN_SQUADRON", "REVEAL", "HIDE_WHEN_FOGGED", "SQUASH", "SURFACE_FX", "UNIT_AI", "TELEKINESIS_TARGET", "STUNNABLE", "WIND_DISTURBANCE", "DAMAGE_TRACKING", "SELF_DESTRUCT", "TURRET", "GARRISON_UNIT", "GARRISON_HOVER", "DEPLOY_TROOPERS", "VEHICLE_THIEF" };
                Behavoir_Tag = "LandBehavior";
            }

            // For Space Buildings
            if (Combo_Box_Type.Text == "StarBase" | Combo_Box_Type.Text == "SpaceBuildable" | Combo_Box_Type.Text == "SpecialStructure" | Combo_Box_Type.Text == "Squadron")
            {
                Behavoir_Pool = new string[] { "SPACE_OBSTACLE", "ABILITY_COUNTDOWN", "SELECTABLE", "POWERED", "SHIELDED", "IDLE", "TURRET", "TARGETING", "WEAPON", "DAMAGE_TRACKING", "SPAWN_SQUADRON", "HIDE_WHEN_FOGGED", 
                                               "REVEAL", "UNIT_AI", "ION_STUN_EFFECT", "STUNNABLE", "SPECIAL_WEAPON", "SELF_DESTRUCT"};
                Behavoir_Tag = "SpaceBehavior";
            }

            // For Ground Buildings
            if (Combo_Box_Type.Text == "TechBuilding" | Combo_Box_Type.Text == "GroundBase" | Combo_Box_Type.Text == "GroundStructure" | Combo_Box_Type.Text == "GroundBuildable")
            {
                Behavoir_Pool = new string[] { "LAND_OBSTACLE", "DUMMY_LAND_BASE_LEVEL_COMPONENT", "IDLE", "EARTHQUAKE_TARGET", "TELEKINESIS_TARGET", "SURFACE_FX", "WIND_DISTURBANCE", "TERRAIN_TEXTURE_MODIFICATION", "TURRET", 
                                               "TARGETING", "WEAPON", "LOBBING_SUPERWEAPON", "AFFECTED_BY_SHIELD", "TREAD_SCROLL", "SELF_DESTRUCT"};
                Behavoir_Tag = "LandBehavior";
            }
          
            else if (Combo_Box_Type.Text == "Projectile")
            {
                Behavoir_Pool = new string[] { "PROJECTILE", "HIDE_WHEN_FOGGED" };
                Behavoir_Tag = "Behavior";
            }
        
            return Behavoir_Pool;
        }
        //=====================//

        // Insert_Code_Into_File (Target_Xml, Unit, "top");  Position can be top, Unit Name or bottom
        // Insert_Code_Into_File (Target_Xml, Unit, "Unit_Name");  If you use name of any unit it will be appended under it
        // Insert_Code_Into_File (Target_Xml, Unit, "bottom"); 

        // Insert_Code_Into_File (null, Unit, "top");  If first parameter is null it will open the Selected Xml  


        void Insert_Code_Into_File(string Target_Xml, XElement Unit, string Position)
        {
            if (Target_Xml == null) { Target_Xml = Selected_Xml; }

            else if (Target_Xml != null)
            {
                try { // Preventing the program from crashing because of xml errors
                
                    // ===================== Opening Xml File =====================
                    XDocument Xml_File = XDocument.Load(Target_Xml, LoadOptions.PreserveWhitespace);

                    var Content = from All_Tags in Xml_File.Descendants() select All_Tags;


                    // =================== Checking Xml Instance ==================
                    foreach (XElement Entity in Content)
                    {
                        if (Position == "top")
                        { Entity.AddFirst(Unit); } // Entity.AddBeforeSelf(Unit); }

                        else if (Position == (string)Entity.Attribute("Name"))
                        { Entity.AddAfterSelf(Unit); }

                        else if (Position == "bottom")
                        { Entity.Add(Unit); }
                    }

                    // ===================== Saving Xml File ======================
                    Xml_File.Save(Target_Xml);
                } catch {}                         
            }
        }


        //===========================//
        void Generate_Random_Orbit(int Maximal_Planet_Distance)
        {          
            int Value_X = Random_Integer(-Maximal_Planet_Distance, Maximal_Planet_Distance);
            int Value_Y = Random_Integer(-Maximal_Planet_Distance, Maximal_Planet_Distance);
         

            // Checking all Planet Orbit slots, if anything is nearby that location is not good to place a planet
            foreach (PictureBox Planet in Picture_Galactic_Background.Controls)
            {
                             
                if (Orbit_Is_Clear(Planet, Value_X, Value_Y) == Value_X)
                {
                    Temporal_C = Random_Integer(1, 2); // Which means its whether + or -
                    if (Temporal_C == 1) { Temporal_D = -1; }
                    else if (Temporal_C == 2) { Temporal_D = 1; }

                    Value_X = Value_X + (Temporal_D * Random_Integer(34, 60));
                    Error_List.Add("i");                    
                    
                }
                if (Orbit_Is_Clear(Planet, Value_X, Value_Y) == Value_Y)
                {
                    Temporal_C = Random_Integer(1, 2); 
                    if (Temporal_C == 1) { Temporal_D = -1; }
                    else if (Temporal_C == 2) { Temporal_D = 1; }

                    Value_Y = Value_Y + (Temporal_D * Random_Integer(34, 60));
                    Error_List.Add("i");
                }

                // Second Attempt
                if (Orbit_Is_Clear(Planet, Value_X, Value_Y) == Value_X)
                {
                    Temporal_C = Random_Integer(1, 2); 
                    if (Temporal_C == 1) { Temporal_D = -1; }
                    else if (Temporal_C == 2) { Temporal_D = 1; }

                    Value_X = Value_X + (Temporal_D * Random_Integer(34, 60));
                    Error_List.Add("i");

                }
                if (Orbit_Is_Clear(Planet, Value_X, Value_Y) == Value_Y)
                {
                    Temporal_C = Random_Integer(1, 2); 
                    if (Temporal_C == 1) { Temporal_D = -1; }
                    else if (Temporal_C == 2) { Temporal_D = 1; }

                    Value_Y = Value_Y + (Temporal_D * Random_Integer(34, 60));
                    Error_List.Add("i");
                }

                // Third Attempt
                if (Orbit_Is_Clear(Planet, Value_X, Value_Y) == Value_X)
                {
                    Temporal_C = Random_Integer(1, 2); // Which means its whether + or -
                    if (Temporal_C == 1) { Temporal_D = -1; }
                    else if (Temporal_C == 2) { Temporal_D = 1; }

                    Value_X = Value_X + (Temporal_D * Random_Integer(34, 60));
                    Error_List.Add("i");

                }
                if (Orbit_Is_Clear(Planet, Value_X, Value_Y) == Value_Y)
                {
                    Temporal_C = Random_Integer(1, 2); // Which means its whether + or -
                    if (Temporal_C == 1) { Temporal_D = -1; }
                    else if (Temporal_C == 2) { Temporal_D = 1; }

                    Value_Y = Value_Y + (Temporal_D * Random_Integer(34, 60));
                    Error_List.Add("i");
                }


                // Turning the - values into +, to verify that the border is ok
                if (Value_X.ToString().Contains("-")) { Temporal_C = Value_X - Value_X - Value_X; } else { Temporal_C = Value_X; }
                if (Value_Y.ToString().Contains("-")) { Temporal_D = Value_Y - Value_Y - Value_Y; } else { Temporal_D = Value_Y; }
                if (Temporal_C > Maximal_Planet_Distance | Temporal_D > Maximal_Planet_Distance) { Error_List.Add("i"); return; }

                // Verification Check, if orbit is still not free we escape               
                if (Orbit_Is_Clear(Planet, Value_X, Value_Y) != 0) { Error_List.Add("i"); return; }                        
            }

            Create_Planet("Planet_" + Planet_Count, Value_X, Value_Y);
            Conquest_Planet_List.Add("Planet_" + Planet_Count);
            Planet_Count++;          
        }


        //===========================//              
        // Parameter 1 is a Planet (control) to check abgainst X and Y Value of the second Planet

        int Orbit_Is_Clear(Control Planet_Name, int Value_X, int Value_Y)
        {
            int Planet_X = 0;
            int Planet_Y = 0;
            int Planet_Distance = Track_Bar_Planet_Distance.Value;

            try
            {   // Getting Planet Info, stored in its text
                Temporal_E = Planet_Name.Text.Split(',');           
                Int32.TryParse(Temporal_E[2], out Planet_X);             
                Int32.TryParse(Temporal_E[3], out Planet_Y);                   
            } catch { }

           
            // If Distance between X or Y value of Planet A and Planet B is smaller then the Max Distance value it returns false, means no space left to place a planet here.
            if (Get_Distance(Planet_X, Value_X) < Planet_Distance) { return Value_X; }
            if (Get_Distance(Planet_Y, Value_Y) < Planet_Distance) { return Value_Y; }

            return 0; // Otherwise it is clear
        }

        //===========================//

        int Compare_Planets(Control Planet_Name_A, Control Planet_Name_B)
        {
            int Planet_Distance = Track_Bar_Planet_Distance.Value;


            int A_Planet_X = 0;
            int A_Planet_Y = 0;
                    
            try
            {   // Getting Planet Info, stored in its text
                Temporal_E = Planet_Name_A.Text.Split(',');
                Int32.TryParse(Temporal_E[2], out A_Planet_X);
                Int32.TryParse(Temporal_E[3], out A_Planet_Y);
            } catch {}


            int B_Planet_X = 0;
            int B_Planet_Y = 0;

            try
            {   // Getting Planet Info, stored in its text
                Temporal_E = Planet_Name_B.Text.Split(',');
                Int32.TryParse(Temporal_E[2], out B_Planet_X);
                Int32.TryParse(Temporal_E[3], out B_Planet_Y);
            } catch {}


            // If Distance between X or Y value of Planet A and Planet B is smaller then the Max Distance value it returns false, means no space left to place a planet here.
            if (Get_Distance(A_Planet_X, B_Planet_X) < Planet_Distance) { return B_Planet_X; }
            if (Get_Distance(A_Planet_Y, B_Planet_Y) < Planet_Distance) { return B_Planet_Y; }

            return 0; // Otherwise it is clear
        }
     
        //===========================//
        int Get_Distance(int Value_1, int Value_2)
        {
            int Distance = 0;  

            if (!Value_1.ToString().Contains("-") & !Value_2.ToString().Contains("-"))
            {   if (Value_1 > Value_2) { Distance = Value_1 - Value_2; }
                else if (Value_1 < Value_2) { Distance = Value_2 - Value_1; }
            }
            else if (Value_1.ToString().Contains("-") & Value_2.ToString().Contains("-"))
            {
                if (Value_1.ToString().Contains("-")) { Value_1 = Value_1 - Value_1 - Value_1; }
                if (Value_2.ToString().Contains("-")) { Value_2 = Value_2 - Value_2 - Value_2; }

                if (Value_1 > Value_2) { Distance = Value_1 - Value_2; }
                else if (Value_1 < Value_2) { Distance = Value_2 - Value_1; }
            }
            else 
            {
               // Turning the - values into +
               if (Value_1.ToString().Contains("-")) { Value_1 = Value_1 - Value_1 - Value_1; }
               if (Value_2.ToString().Contains("-")) { Value_2 = Value_2 - Value_2 - Value_2; }

               Distance = Value_1 + Value_2;
            }
        
            return Distance; //Stays 0 if Value_1 == Value_2
        }
      
        //===========================//
        void Save_Current_Planet()
        {
            // Needed for the .xml Editor Functions  
            string Planet_Name = Text_Box_Planet_Name.Text;
            string Planet_Xml = "";                           
            User_Input = false;

            
            // Saving the planet File
            if (Label_Planet_File.Text != "Creating Planet") // Then we have a Planet.xml assigned to it
            {
                Planet_Xml = Xml_Directory + Label_Planet_File.Text;
                           
                try
                {   //======================= Loading .Xml File =======================
                    XDocument Planet_Xml_File = XDocument.Load(Planet_Xml, LoadOptions.PreserveWhitespace);


                    IEnumerable<XElement> The_Planet =
                        // Selecting all child Tags of Root   
                       from All_Tags in Planet_Xml_File.Root.Descendants()
                       // Selecting our Unit
                       where (string)All_Tags.Attribute("Name") == Planet_Name
                       select All_Tags;

                   
             
                    if (Check_Box_Planet_Is_Variant.Checked & Text_Box_Planet_Is_Variant.Text != "")
                    { Validate_Save_Unit_Tag(The_Planet, "Variant_Of_Existing_Type", Text_Box_Planet_Is_Variant); }
                    else if (!Check_Box_Planet_Is_Variant.Checked)
                    { Validate_Save_Unit_Text(The_Planet, "Variant_Of_Existing_Type", ""); }


                    Validate_Save_Unit_Tag(The_Planet, "Text_ID", Text_Box_Planet_Text_ID);
                    Validate_Save_Unit_Tag(The_Planet, "Encyclopedia_Text", Text_Box_Planet_Encyclopedia);
                                              
                    Verify_Tag_with_Extension(The_Planet, "Galactic_Model_Name", Combo_Box_Planet_Model, ".alo");
                    Verify_Tag_with_Extension(The_Planet, "Destroyed_Galactic_Model_Name", Text_Box_Destroyed_Planet_Model, ".alo");
                    Validate_Save_Unit_Tag(The_Planet, "Scale_Factor", Text_Box_Planet_Scale);
            
                    
                    if (Toggle_Surface_Accessible) { Validate_Save_Unit_Text(The_Planet, "Planet_Surface_Accessible", "True"); }
                    else { Validate_Save_Unit_Text(The_Planet, "Planet_Surface_Accessible", "False"); }
                  
                    if (Toggle_Is_Pre_Lit) { Validate_Save_Unit_Text(The_Planet, "Pre_Lit", "Yes"); }
                    else { Validate_Save_Unit_Text(The_Planet, "Pre_Lit", "No"); }
                    
                    if (Toggle_Planet_Animation) { Validate_Save_Unit_Text(The_Planet, "Loop_Idle_Anim_00", "True"); }
                    else { Validate_Save_Unit_Text(The_Planet, "Loop_Idle_Anim_00", "False"); }


                    Validate_Save_Unit_Tag(The_Planet, "Galactic_Position", Text_Box_Galactic_Position);
                    Validate_Save_Unit_Tag(The_Planet, "Facing_Adjust", Text_Box_Facing_Adjust);

                    Verify_Tag_with_Extension(The_Planet, "Space_Tactical_Map", Text_Box_Space_Map, ".ted");
                    Verify_Tag_with_Extension(The_Planet, "Land_Tactical_Map", Text_Box_Land_Map, ".ted");
                    Verify_Tag_with_Extension(The_Planet, "Destroyed_Space_Tactical_Map", Text_Box_Destroyed_Map, ".ted");
       
                    Validate_Save_Unit_Text(The_Planet, "Zoomed_Terrain_Index", Track_Bar_Terrain.Value.ToString());
                    Validate_Save_Unit_Tag(The_Planet, "Max_Space_Base", Text_Box_Max_Star_Base);
                    Validate_Save_Unit_Tag(The_Planet, "Special_Structures_Space", Text_Box_Space_Structures);
                    Validate_Save_Unit_Tag(The_Planet, "Special_Structures_Land", Text_Box_Land_Structures);

                    Validate_Save_Unit_Tag(The_Planet, "Planet_Credit_Value", Text_Box_Credit_Income);
                    Validate_Save_Unit_Tag(The_Planet, "Planet_Destroyed_Credit_Value", Text_Box_Destroyed_Income);
                    Validate_Save_Unit_Tag(The_Planet, "Planet_Capture_Bonus_Reward", Text_Box_Bar_Capture_Bonus);
                    Validate_Save_Unit_Tag(The_Planet, "Additional_Population_Capacity", Text_Box_Planetary_Population);


                    // Changing Planet Name, needs to be at the very end
                    Temporal_A = Text_Box_Planet_Name.Text;
                    Temporal_B = "";

                    try 
                    {   if (Temporal_A != "" & Temporal_A != The_Planet.First().Attribute("Name").Value)
                        { The_Planet.First().Attribute("Name").Value = Temporal_A; }

                        foreach (string Entry in Conquest_Planet_List)
                        {   
                            // if (Entry == Planet_Name) { Entry = Temporal_A; } // Updating Name
                            if (Entry != "") { Temporal_B += Entry + ", "; }
                        }

                        Save_Setting(Setting, "Conquest_Planet_List", Temporal_B);
                        
                    } catch {}


                    //========= Saving All Changes we just applied to the .xml =========
                    Planet_Xml_File.Save(Planet_Xml);


                    if (Check_Box_Planet_Game_Objects.Checked)
                    {   // Adding the tag "File" to the specified Xml File above with the Name of the new file.
                        Save_Tag_Into_Xml(Xml_Directory + "GameObjectFiles.xml", "Root", "File", Path.GetFileName(Planet_Xml), false);
                    }

                } catch {}                              
            }  


            User_Input = true;
        }



        //===========================//

        // Inject_Into_Xml("Planet", Text_Box_Planet_Name, Label_Planet_File);
        void Inject_Into_Xml(string Xml_Path, string Instance_Type, Control Instance_Textbox, Control Xml_Info_Label)
        {
            string Unit_Name = Instance_Textbox.Text;
            string Current_Xml = "";


            if (Xml_Path == null)
            {
                Save_File_Dialog_1.InitialDirectory = Xml_Directory;
                Save_File_Dialog_1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
                Save_File_Dialog_1.DefaultExt = "xml";
                Save_File_Dialog_1.CheckFileExists = true;

                // Choosing a Xml File
                if (Save_File_Dialog_1.ShowDialog() == DialogResult.OK)
                {
                    Current_Xml = Save_File_Dialog_1.FileName;
                    Xml_Info_Label.Text = Path.GetFileName(Current_Xml);
                }
            }
            else if (Xml_Path != null)
            {   Current_Xml = Xml_Path;
                Xml_Info_Label.Text = Path.GetFileName(Current_Xml);
            }
            
           
            try
            {
                // ====================== Creating Instance ======================
                XDocument Xml_File = XDocument.Load(Current_Xml, LoadOptions.PreserveWhitespace);


                var Instances =
                   from All_Tags in Xml_File.Root.Descendants()
                   // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
                   where (string)All_Tags.Attribute("Name") == Unit_Name
                   select All_Tags;


                // =================== Checking Xml Instance ===================
                foreach (XElement Instance in Instances)
                {
                    if (Instance != null) 
                    {
                        Imperial_Dialogue(560, 160, "Overwrite", "Cancel", "false", "    " + Xml_Info_Label.Text + " already contains "
                                                            + Add_Line + "    " + Unit_Name + ". Do you wish to overwrite it?");


                        Save_File_Dialog_1.CheckFileExists = false;

                        if (Caution_Window.Passed_Value_A.Text_Data == "true")
                        {   
                            // Dependant on Unit Type we choose a saving method
                            if (Instance_Type == "Planet")
                            {   // if (Planet_Name == null | Planet_Name == "") { Planet_Name = Text_Box_Planet_Name.Text; }
                                Save_Current_Planet(); 
                            }
                            else if (Instance_Type == "Faction") { Mini_Button_Save_Faction_Click(null, null); }

                            Caution_Window.Passed_Value_A.Text_Data = "false";
                            return; // As code was already applied we're done 
                        }
                        else if (Caution_Window.Passed_Value_A.Text_Data == "false")
                        {
                            return;
                        }  
                    }
                }


                if (Unit_Name != "") 
                {
                    // Asking the User where he wants to inject the generated code
                    Imperial_Dialogue(540, 160, "Top", "Bottom", "false", "    At which position do you like to insert "
                                                             + Add_Line + "    " + Unit_Name + " ?");

                   
                    if (Caution_Window.Passed_Value_A.Text_Data == "true")
                    {   
                        // If "Top" we add it as first element
                        Xml_File.Descendants().First().AddFirst("\n\n\t",
                           new XElement(Instance_Type, // Root Tag Name
                                           new XAttribute("Name", Unit_Name), "\n"
                        ));

                        Caution_Window.Passed_Value_A.Text_Data = "false"; // Resetting for next usage
                    }

                    
                    else if (Caution_Window.Passed_Value_A.Text_Data == "false")
                    {   
                        // Otherwise appending to the bottom
                        Xml_File.Descendants().First().Add("\n\t",
                            new XElement(Instance_Type, 
                                            new XAttribute("Name", Unit_Name), "\n"
                        ), "\n");
                    }           
                }

                else if (Unit_Name == "") { Imperial_Console(600, 100, Add_Line + "    There is no Unit Name in the UI Textbox."); } 



                //=============================== Saving ===============================
                Xml_File.Save(Current_Xml); // Injecting the new Instance into the selected File

                // Dependant on Unit Type we choose a saving method
                if (Instance_Type == "Planet")
                {   // if (Planet_Name == null | Planet_Name == "") { Planet_Name = Text_Box_Planet_Name.Text; }
                    Save_Current_Planet();
                }
                else if (Instance_Type == "Faction") { Mini_Button_Save_Faction_Click(null, null); }
                


                Save_File_Dialog_1.CheckFileExists = false;

            } catch { Imperial_Console(680, 100, Add_Line + "    Failed to inject code into" + Add_Line + "    " + Xml_Info_Label.Text); } 
        }


        //===========================//
        void Load_Planet_To_UI(string Planet_Name)
        {           
            User_Input = false;
            Planet_Data_Files = Load_Setting(Setting, "Planet_Data_Files").Split(',');

            Planet_Instance = null;
            string Planet_Xml_File = ""; // There is also a Xdocument type of this???


            foreach (string The_File in Planet_Data_Files)
            {   
                Planet_Instance = Get_Instance(Xml_Directory + The_File, "Planet", Planet_Name);
                
                // Stop searching once the Instance was found
                if (Planet_Instance != null) 
                {   Label_Planet_File.Text = The_File;
                    Planet_Xml_File = Xml_Directory + The_File;
                    break; 
                }
            }
                  

            //======================== Loading Planet =========================
          
            Text_Box_Planet_Name.Text = Planet_Name;


            Temporal_A = Load_Instance_Text(Planet_Instance, "Variant_Of_Existing_Type");

            //======== Setting Variant_Of_Existing_Type ========     
            if (Temporal_A == "" | Temporal_A == null)
            {
                if (Check_Box_Planet_Is_Variant.Checked == true)
                {
                    Check_Box_Planet_Is_Variant.Checked = false;
                    Button_Open_Planet_Variant.Visible = false;
                }

                Text_Box_Planet_Is_Variant.Text = "";
            }
            else if (Temporal_A != "" & Temporal_A != null)
            {
                if (Check_Box_Planet_Is_Variant.Checked == false)
                {
                    Check_Box_Planet_Is_Variant.Checked = true;
                    Button_Open_Planet_Variant.Visible = true;
                }

                Text_Box_Planet_Is_Variant.Text = Temporal_A;      
            }


            Load_Instance_Value(Planet_Instance, Text_Box_Planet_Text_ID, "Text_ID");
            Load_Instance_Value(Planet_Instance, Text_Box_Planet_Encyclopedia, "Encyclopedia_Text");


            Load_Instance_Value(Planet_Instance, Text_Box_Planet_Scale, "Scale_Factor"); // This needs run before "Galactic_Model_Name" 
            Track_Bar_Planet_Scale_Scroll(null, null); // Because this sets "Planet_Image_Size" before Planet image is loaded 


            try // This needs run before "Galactic_Model_Name" or it will lit the Planet image
            {
                Temporal_A = Load_Instance_Text(Planet_Instance, "Pre_Lit");

                if (Regex.IsMatch(Temporal_A, "(?i).*?" + "Yes")) { Toggle_Is_Pre_Lit = false; Switch_Button_Is_Pre_Lit_Click(null, null); }
                else if (Regex.IsMatch(Temporal_A, "(?i).*?" + "No")) { Toggle_Is_Pre_Lit = true; Switch_Button_Is_Pre_Lit_Click(null, null); }
                else if (Toggle_Is_Pre_Lit == true) { Switch_Button_Is_Pre_Lit_Click(null, null); }
               
            } catch {}   
        

            // This triggers Combo_Box_Planet_Model_TextChanged() to set the 
            // Panet Image if its letters match any entry in the list "Planet_Images"
            try
            {
                Temporal_A = Load_Instance_Text(Planet_Instance, "Galactic_Model_Name"); // Removing .alo Extension
                Combo_Box_Planet_Model.Text = Regex.Replace(Temporal_A, "(?i)" + ".alo", "");
            } catch {}

            try
            {
                Temporal_A = Load_Instance_Text(Planet_Instance, "Destroyed_Galactic_Model_Name");
                Text_Box_Destroyed_Planet_Model.Text = Regex.Replace(Temporal_A, "(?i)" + ".alo", "");
            } catch {}



            try
            {
                Temporal_A = Load_Instance_Text(Planet_Instance, "Planet_Surface_Accessible");

                if (Regex.IsMatch(Temporal_A, "(?i).*?" + "True"))
                {   Toggle_Surface_Accessible = false;
                    Switch_Button_Surface_Accessible_Click(null, null); // Inverting Values using the Button
                }
                else if (Regex.IsMatch(Temporal_A, "(?i).*?" + "False")) { Toggle_Surface_Accessible = true; Switch_Button_Surface_Accessible_Click(null, null); }
                // Otherwise it has no value specified, which means false
                else if (Toggle_Surface_Accessible == true) { Switch_Button_Surface_Accessible_Click(null, null); }
                
            } catch {}


            try
            {
                Temporal_A = Load_Instance_Text(Planet_Instance, "Loop_Idle_Anim_00");

                if (Regex.IsMatch(Temporal_A, "(?i).*?" + "True")) { Toggle_Planet_Animation = false; Switch_Button_Planet_Animation_Click(null, null); }
                else if (Regex.IsMatch(Temporal_A, "(?i).*?" + "False")) { Toggle_Planet_Animation = true; Switch_Button_Planet_Animation_Click(null, null); }
                else if (Toggle_Planet_Animation == true) { Switch_Button_Planet_Animation_Click(null, null); }
             
            } catch {}



            Load_Instance_Value(Planet_Instance, Text_Box_Galactic_Position, "Galactic_Position");
            Load_Instance_Value(Planet_Instance, Text_Box_Facing_Adjust, "Facing_Adjust");


            try
            {
                Temporal_A = Load_Instance_Text(Planet_Instance, "Space_Tactical_Map");
                if (Regex.IsMatch(Temporal_A, "(?i).*?" + ".ted")) { Text_Box_Space_Map.Text = Regex.Replace(Temporal_A, "(?i).ted", ""); }
                else { Text_Box_Space_Map.Text = Temporal_A; }          
            } catch {}

            try
            {
                Temporal_A = Load_Instance_Text(Planet_Instance, "Land_Tactical_Map");
                if (Regex.IsMatch(Temporal_A, "(?i).*?" + ".ted")) { Text_Box_Land_Map.Text = Regex.Replace(Temporal_A, "(?i).ted", ""); }
                else { Text_Box_Land_Map.Text = Temporal_A; }
            } catch {}

            try
            {
                Temporal_A = Load_Instance_Text(Planet_Instance, "Destroyed_Space_Tactical_Map");
                if (Regex.IsMatch(Temporal_A, "(?i).*?" + ".ted")) { Text_Box_Destroyed_Map.Text = Regex.Replace(Temporal_A, "(?i).ted", ""); }
                else { Text_Box_Destroyed_Map.Text = Temporal_A; }
            } catch {}



            Temporal_A = "";
            try { Temporal_A = Planet_Instance.Descendants("Zoomed_Terrain_Index").First().Value; } catch {}
            if (Temporal_A != "") 
            { 
                Int32.TryParse(Temporal_A, out Temporal_C);
                Track_Bar_Terrain.Value = Temporal_C;
                try { Track_Bar_Terrain_Scroll(null, null); }
                catch { if (Debug_Mode == "true") { Imperial_Console(500, 100, Add_Line + "    Failed to scale Planet Image."); } }
            }

            Load_Instance_Value(Planet_Instance, Text_Box_Max_Star_Base, "Max_Space_Base");
            Load_Instance_Value(Planet_Instance, Text_Box_Space_Structures, "Special_Structures_Space");
            Load_Instance_Value(Planet_Instance, Text_Box_Land_Structures, "Special_Structures_Land");

            Load_Instance_Value(Planet_Instance, Text_Box_Credit_Income, "Planet_Credit_Value");
            Load_Instance_Value(Planet_Instance, Text_Box_Destroyed_Income, "Planet_Destroyed_Credit_Value");
            Load_Instance_Value(Planet_Instance, Text_Box_Bar_Capture_Bonus, "Planet_Capture_Bonus_Reward");
            Load_Instance_Value(Planet_Instance, Text_Box_Planetary_Population, "Additional_Population_Capacity");
            

            // Imperial_Console(600, 100, Add_Line + Planet_Instance.Descendants().First().ToString());

            User_Input = true;
        }


        //===========================//
        void Load_Projectile_To_UI(string File_Path, string Projectile_Name)
        {
            if (Projectile_Name == "") { return; }
            // Imperial_Console(500, 100, File_Path + ", " + Hard_Point_Name);


            User_Input = false; // Resetting from last Entity
            Projectile_Instance = null;

            Mini_Button_Clear_Projectile_Click(null, null); // Clearing UI 

            // Making sure the UI expands
            if (Size_Expander_P == "false") { Button_Expand_P_Click(null, null); }


            if (File_Path != null)
            {
                Label_Projectile_Xml_Name.Text = Path.GetFileName(File_Path);
                Selected_Projectile_Xml = File_Path;
                Projectile_Instance = Get_Instance(File_Path, "Projectile", Projectile_Name);
            }
            else
            {
                foreach (string The_File in Projectile_Data_Files)
                {   // Getting the first selected entry of the List View Box
                    Projectile_Instance = Get_Instance(Xml_Directory + The_File, "Projectile", Projectile_Name);


                    // Stop searching once the Instance was found, this matches a file to the instance we're looking for.
                    if (Projectile_Instance != null)
                    {
                        Label_Projectile_Xml_Name.Text = The_File;
                        Selected_Projectile_Xml = Xml_Directory + The_File;

                        // Imperial_Console(500, 100, Xml_Directory + The_File + ", " + Projectile_Name);
                        break;
                    }
                }
            }


            if (Projectile_Instance == null) // If nothing was found after the loop above 
            {
                Imperial_Console(500, 100, @"    Selected Projectile Instance not found.");
                return;
            }


            Selected_Projectile_Instance = Projectile_Name;
            Save_Setting(Setting, "Selected_Projectile_Instance", Projectile_Name);
            Save_Setting("0", "Selected_Projectile_Xml", Selected_Projectile_Xml); // "0" is with quotemarks


            //======================== Loading Projectile =========================

            Text_Box_Projectile_Name.Text = Projectile_Name;
            Load_Instance_Value(Projectile_Instance, Combo_Box_Projectile_Category, "Projectile_Category");

            //======================= Variant of Existing Type =======================

            Temporal_A = Load_Instance_Text(Projectile_Instance, "Variant_Of_Existing_Type");

            if (Temporal_A == "" | Temporal_A == null)
            {
                if (Check_Box_Proj_Is_Variant.Checked)
                {
                    Check_Box_Proj_Is_Variant.Checked = false;
                    Mini_Button_Open_Proj_Variant.Visible = false;
                }

                Text_Box_Proj_Is_Variant.Text = "";
            }
            else // if (Temporal_A != "" & Temporal_A != null)
            {   if (Check_Box_Proj_Is_Variant.Checked == false)
                {
                    Check_Box_Proj_Is_Variant.Checked = true;
                    Mini_Button_Open_Proj_Variant.Visible = true;
                }

                Text_Box_Proj_Is_Variant.Text = Temporal_A;
            }


            //======================== Loading Projectile =========================

            if (Matches_True(Load_Instance_Text(Projectile_Instance, "Projectile_Does_Shield_Damage")))
            {   // Changing state of the UI toggle by its own function
                if (Toggle_Shield_Damage == false) { Switch_Shield_Damage_Click(null, null); }
            }
            else { if (Toggle_Shield_Damage == true) { Switch_Shield_Damage_Click(null, null); } }

            if (Matches_True(Load_Instance_Text(Projectile_Instance, "Projectile_Does_Energy_Damage")))
            { if (Toggle_Energy_Damage == false) { Switch_Energy_Damage_Click(null, null); } }
            else { if (Toggle_Energy_Damage == true) { Switch_Energy_Damage_Click(null, null); } }

            if (Matches_True(Load_Instance_Text(Projectile_Instance, "Projectile_Does_Hitpoint_Damage")))
            { if (Toggle_Hitpoint_Damage == false) { Switch_Hitpoint_Damage_Click(null, null); } }
            else { if (Toggle_Hitpoint_Damage == true) { Switch_Hitpoint_Damage_Click(null, null); } }
           
            if (Matches_True(Load_Instance_Text(Projectile_Instance, "Projectile_Damages_Random_Hard_Points")))
            { Check_Box_Random_Hitpoint.Checked = true; }

            Load_Instance_Value(Projectile_Instance, Combo_Box_Projectile_Category, "Projectile_Category");

            Load_Instance_Value(Projectile_Instance, Combo_Box_Projectile_Damage, "Damage_Type");

            Load_Instance_Value(Projectile_Instance, Text_Box_HP_Health, "Projectile_Laser_Color");

            //======================== Main Data =========================

            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Damage, "Projectile_Damage");
            Load_Instance_Value(Projectile_Instance, Text_Box_Proj_AI_Combat_Power, "AI_Combat_Power");
            Load_Instance_Value(Projectile_Instance, Text_Box_Energy_Per_Shot, "Projectile_Energy_Per_Shot");


            // ========= Models =========
            Temporal_A = ""; Temporal_B = "";
            Temporal_A = Load_Instance_Text(Projectile_Instance, "Space_Model_Name");
            Temporal_B = Load_Instance_Text(Projectile_Instance, "Land_Model_Name");
     
            if (Temporal_A == "" & Temporal_B == "" | Temporal_A != "" & Temporal_B == "") { Button_Space_Click(null, null); } // Defaults to Space Mode          
            else if (Temporal_A != "" & Temporal_B != "") { Button_Use_Both_Click(null, null); }
            else if (Temporal_A == "" & Temporal_B != "") { Button_Ground_Click(null, null); }

            // Just updated switch by triggering these buttons above
            if (Projectile_Mode_Switch == "Ground") { Combo_Box_Projectile_Model.Text = Regex.Replace(Temporal_B, "(?i)" + ".alo", ""); }
            else { Combo_Box_Projectile_Model.Text = Regex.Replace(Temporal_A, "(?i)" + ".alo", ""); } // Space Model Dominates if there are 2


            
          

            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Color, "Projectile_Laser_Color");
            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Scale_Factor, "Scale_Factor");
            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Width, "Projectile_Width");
            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Length, "Projectile_Length");
            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Max_Speed, "Max_Speed");


            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Rate_Of_Turn, "Max_Rate_Of_Turn");
            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Max_Lifetime, "Projectile_Max_Lifetime");
            Load_Instance_Value(Projectile_Instance, Text_Box_Proj_Max_Flight_Distance, "Projectile_Max_Flight_Distance");

            //======================== Explosives =========================

            if (Matches_True(Load_Instance_Text(Projectile_Instance, "Explode_When_Reached_Target_Radius")))
            { Button_Explode_Click(null, null); }
            else if (Matches_True(Load_Instance_Text(Projectile_Instance, "Projectile_Grenade_Sticks_On_Collision")))
            { Button_Stick_Click(null, null); }
            else { Button_Ignore_Click(null, null); }



            // ===== Decide between Ground or/and Space =====           
            if (Matches_True(Load_Instance_Text(Projectile_Instance, "Projectile_Target_Point_On_Terrain")))
            {   // Projectile_Mode_Switch != "Both" because then it was already set correctly above by the "Land_Model_Name" tag.
                if (Projectile_Mode_Switch != "Both" & Projectile_Mode_Switch != "Ground") { Button_Ground_Click(null, null); }
                if (!Toggle_Target_On_Terrain) { Switch_Target_On_Terrain_Click(null, null); }
            }
            else if (Load_Instance_Text(Projectile_Instance, "Projectile_Blast_Area_Immune_Faction") != "")
            {   // if (Projectile_Mode_Switch != "Space") { Button_Space_Click(null, null); } // Space is default anyways
                Load_Instance_Value(Projectile_Instance, Text_Box_Blast_Area_Immune_Faction, "Projectile_Blast_Area_Immune_Faction");
            }

            string Temporal = "";

            // ========= Land Grenades =========
            Temporal = "";
            Temporal = Load_Instance_Text(Projectile_Instance, "Projectile_Grenade_Gravity");

            if (Temporal != "" & Temporal != "0")
            {
                if (Projectile_Mode_Switch != "Both" & Projectile_Mode_Switch != "Ground") { Button_Ground_Click(null, null); }
                Text_Box_Projectile_Grenade_Gravity.Text = Temporal;
            }

            Temporal = "";
            Temporal = Load_Instance_Text(Projectile_Instance, "Projectile_Rocket_Curve_Offset");
            if (Temporal != "" & Temporal != "0")
            {
                if (Projectile_Mode_Switch != "Both" & Projectile_Mode_Switch != "Ground") { Button_Ground_Click(null, null); }
                Text_Box_Grenade_Gravity_Lob.Text = Temporal;
            }

            
            // ========= Rockets =========
            Temporal = "";
            Temporal = Load_Instance_Text(Projectile_Instance, "Projectile_Rocket_Curve_Distance");

            if (Temporal != "" & Temporal != "0")
            {   // This trieggers to rename this UI tag and the next 2 under it
                if (Combo_Box_Projectile_Preset.Text != "Rocket") { Combo_Box_Projectile_Preset.Text = "Rocket"; }
                Text_Box_Curve_Distance_Or_Max_Scan_Range.Text = Temporal;
            }

            Temporal = "";
            Temporal = Load_Instance_Text(Projectile_Instance, "Projectile_Rocket_Curve_Offset");
            if (Temporal != "" & Temporal != "0")
            {
                if (Combo_Box_Projectile_Preset.Text != "Rocket") { Combo_Box_Projectile_Preset.Text = "Rocket"; }
                Text_Box_Curve_Offset_Or_Blast_Dropoff.Text = Temporal;
            }

            Temporal = "";
            Temporal = Load_Instance_Text(Projectile_Instance, "Projectile_Rocket_Straight_Distance");
            if (Temporal != "" & Temporal != "0")
            {
                if (Combo_Box_Projectile_Preset.Text != "Rocket") { Combo_Box_Projectile_Preset.Text = "Rocket"; }
                Text_Box_Straight_Distance_Or_Blast_Range.Text = Temporal;
            }

            // ========= Missiles =========
            Temporal = "";
            Temporal = Load_Instance_Text(Projectile_Instance, "Projectile_Max_Scan_Range");

            if (Temporal != "" & Temporal != "0")
            {   // This trieggers to rename this UI tag and the next 2 under it
                if (Combo_Box_Projectile_Preset.Text != "Missile") { Combo_Box_Projectile_Preset.Text = "Missile"; }
                Text_Box_Curve_Distance_Or_Max_Scan_Range.Text = Temporal;
            }

            Temporal = "";
            Temporal = Load_Instance_Text(Projectile_Instance, "Projectile_Blast_Area_Dropoff_Tiers");
            if (Temporal != "" & Temporal != "0")
            {
                if (Combo_Box_Projectile_Preset.Text != "Missile") { Combo_Box_Projectile_Preset.Text = "Missile"; }
                Text_Box_Curve_Offset_Or_Blast_Dropoff.Text = Temporal;
            }

            // This overwrites the Projectile Damage so it has no own UI Box
            Temporal = "";
            Temporal = Load_Instance_Text(Projectile_Instance, "Projectile_Blast_Area_Damage");
            if (Temporal != "" & Temporal != "0") { Text_Box_Projectile_Damage.Text = Temporal; }

            Temporal = "";
            Temporal = Load_Instance_Text(Projectile_Instance, "Projectile_Blast_Area_Range");
            if (Temporal != "" & Temporal != "0")
            {
                if (Combo_Box_Projectile_Preset.Text != "Missile") { Combo_Box_Projectile_Preset.Text = "Missile"; }
                Text_Box_Straight_Distance_Or_Blast_Range.Text = Temporal;
            }

            // ======= Grenades and Bombs =======
            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Death_Explosion, "Death_Explosions");
            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Death_SFX_Event, "Death_SFXEvent_Start_Die");


            //===================== Stun Effects ======================
            if (!Toggle_Stun_On_Detonation & Matches_True(Load_Instance_Text(Projectile_Instance, "Projectile_Ion_Stun_On_Detonation")))
            {   // This will also expand the full stunn cathegory. 
                Switch_Stun_On_Detonation_Click(null, null);
            }

            if (!Toggle_Disable_Engines & Matches_True(Load_Instance_Text(Projectile_Instance, "Projectile_Disables_Engines_When_Power_Drained")))
            { Switch_Projectile_Disable_Engines_Click(null, null); }
      
            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Stun_Duration, "Projectile_Ion_Stun_Duration");
            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Stun_Duration_Frames, "Projectile_Stun_Duration_Frames");

            // Decimal Values
            Temporal_A = ""; Temporal_A = Load_Instance_Text(Projectile_Instance, "Projectile_Ion_Stun_Speed_Reduction_Percent");
            if (Temporal_A.Contains("f")) { Temporal_A = Temporal_A.Replace("f", ""); }
            Text_Box_Speed_Reduction_Percent.Text = Temporal_A;

            Temporal_A = ""; Temporal_A = Load_Instance_Text(Projectile_Instance, "Projectile_Ion_Stun_Shot_Rate_Reduction_Percent");
            if (Temporal_A.Contains("f")) { Temporal_A = Temporal_A.Replace("f", ""); }
            Text_Box_Shot_Rate_Reduction.Text = Temporal_A;


            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Stun_Effect, "Projectile_Stun_Spawn_Effect");

            //============== Particle and Sound Effects ================
            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_Detonation_Particle, "Projectile_Object_Detonation_Particle");
            Load_Instance_Value(Projectile_Instance, Text_Box_Armor_Reduced_Particle, "Projectile_Object_Armor_Reduced_Detonation_Particle");

            Load_Instance_Value(Projectile_Instance, Text_Box_Absorbed_By_Shields_Particle, "Projectile_Absorbed_By_Shields_Particle");
            Load_Instance_Value(Projectile_Instance, Text_Box_Lifetime_Detonation_Particle, "Projectile_Lifetime_Detonation_Particle");

            Load_Instance_Value(Projectile_Instance, Text_Box_Projectile_SFX_Detonate, "Projectile_SFXEvent_Detonate");
            Load_Instance_Value(Projectile_Instance, Text_Box_Detonate_Reduced_By_Armor, "Projectile_SFXEvent_Detonate_Reduced_By_Armor");

            // For Ground Mode (which is hopefully already detected above.)
            Load_Instance_Value(Projectile_Instance, Text_Box_Ground_Detonation_Particle, "Projectile_Ground_Detonation_Particle");
            Load_Instance_Value(Projectile_Instance, Text_Box_Ground_Detonation_Surface, "Projectile_Ground_Detonation_SurfaceFX");


            User_Input = true;
        }


        //===========================//
        void Load_Hard_Point_To_UI(string File_Path, string Hard_Point_Name) // Insert Select_List_View_First(List_View_Hard_Points, false)
        {
            if (Hard_Point_Name == "") { return; }
            // Imperial_Console(500, 100, File_Path + ", " + Hard_Point_Name);
            

            User_Input = false;
            // Making sure the UI expands
            if (Size_Expander_L == "false") { Button_Expand_L_Click(null, null); }

            Hard_Point_Instance = null; // Resetting from last Entity
            Hard_Point_Data_Files = Load_Setting(Setting, "Hard_Point_Data_Files").Split(',');
        
            
            if (File_Path != null)  
            {
                Label_HP_Xml_Name.Text = Path.GetFileName(File_Path);
                Selected_HP_Xml = File_Path;
                Hard_Point_Instance = Get_Instance(File_Path, "HardPoint", Hard_Point_Name);
            }
            else
            {   foreach (string The_File in Hard_Point_Data_Files)
                {   // Getting the first selected entry of the List View Box
                    Hard_Point_Instance = Get_Instance(Xml_Directory + The_File, "HardPoint", Hard_Point_Name);
                

                    // Stop searching once the Instance was found, this matches a file to the instance we're looking for.
                    if (Hard_Point_Instance != null)
                    {   Label_HP_Xml_Name.Text = The_File;
                        Selected_HP_Xml = Xml_Directory + The_File;

                        // Imperial_Console(500, 100, Xml_Directory + The_File + ", " + Hard_Point_Name);
                        break;
                    }
                }             
            }


            if (Hard_Point_Instance == null) // If nothing was found after the loop above 
            {   Imperial_Console(500, 100, @"    Selected Instance not found, please check "
                              + Add_Line + @"    if its .xml is listed in HardPointDataFiles.xml.");
                return;
            }


            Selected_HP_Instance = Hard_Point_Name;         
            Save_Setting(Setting, "Selected_HP_Instance", Hard_Point_Name);
            Save_Setting("0", "Selected_HP_Xml", Selected_HP_Xml); // "0" is with quotemarks

            //======================== Loading Hard Point =========================

            Text_Box_Hard_Point_Name.Text = Hard_Point_Name;
            Load_Instance_Value(Hard_Point_Instance, Combo_Box_Hart_Point_Type, "Type"); // Need this Below



            //======== Setting Variant_Of_Existing_Type ========  
            Temporal_A = "";
            Temporal_A = Load_Instance_Text(Hard_Point_Instance, "Variant_Of_Existing_Type");

            if (Temporal_A == "" | Temporal_A == null)
            {
                if (Check_Box_HP_Is_Variant.Checked == true)
                {
                    Check_Box_HP_Is_Variant.Checked = false;
                    Mini_Button_Open_HP_Variant.Visible = false;
                }
                Text_Box_HP_Is_Variant.Text = "";
            }
            else // if (Temporal_A != "" & Temporal_A != null)
            {
                // Because Combo_Box_Hart_Point_Type_TextChanged() won't load
                Set_HP_Variant_Template();
                
               
                if (Check_Box_HP_Is_Variant.Checked == false)
                {   Check_Box_HP_Is_Variant.Checked = true;
                    Mini_Button_Open_HP_Variant.Visible = true;
                }

                // HP_Weapon_Template, HP_Shield_Template, HP_Engine_Template, HP_Special_Template etc..
                if (Temporal_A.Contains("_Template"))
                {   HP_Variant_Template = Temporal_A;  // Deliberately overwriting this with code entry
                    Radio_Button_HP_Template.Select();
                }           
                else // if (Radio_Button_Custom_HP.Checked == false) 
                {   HP_Variant_Normal = Temporal_A; 
                    Radio_Button_Custom_HP.Select();
                }       

                Text_Box_HP_Is_Variant.Text = Temporal_A;
            }


            //======================== Main Data =========================

            Load_Instance_Value(Hard_Point_Instance, Text_Box_Tooltip_Text, "Tooltip_Text");

            if (Matches_True(Load_Instance_Text(Hard_Point_Instance, "Is_Targetable"))) 
            {   // Changing state of the UI toggle by its own function
                if (Toggle_Is_Targatable == false) { Switch_Is_Targatable_Click(null, null); }
            }
            else { if (Toggle_Is_Targatable == true) { Switch_Is_Targatable_Click(null, null); } }


            if (Matches_True(Load_Instance_Text(Hard_Point_Instance, "Is_Destroyable"))) 
            { if (Toggle_Is_Destroyable == false) { Switch_Is_Destroyable_Click(null, null); } }
            else { if (Toggle_Is_Destroyable == true) { Switch_Is_Destroyable_Click(null, null); } }


            Load_Instance_Value(Hard_Point_Instance, Text_Box_HP_Health, "Health");

            Temporal_A = "";
            Temporal_A = Load_Instance_Text(Hard_Point_Instance, "Repair_Amount_Per_Frame");
            if (Temporal_A != "")
            {   // Getting rid of the Point 
                if (Temporal_A.StartsWith(".")) { Temporal_A = Temporal_A.Substring(1, Temporal_A.Length - 1); }
                Text_Box_Repair_Value.Text = Temporal_A;
                
                if (Toggle_Is_Repairable == false) {  Switch_Is_Repairable_Click(null, null);  }
            } else { if (Toggle_Is_Repairable == true) { Switch_Is_Repairable_Click(null, null); } }

      

            Load_Instance_Value(Hard_Point_Instance, Combo_Box_Fire_Projectile_Type, "Fire_Projectile_Type");
            Load_Instance_Value(Hard_Point_Instance, Combo_Box_Damage_Type, "Damage_Type");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Fire_Bone_A, "Fire_Bone_A");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Fire_Bone_B, "Fire_Bone_B");

            Load_Instance_Value(Hard_Point_Instance, Text_Box_Fire_Cone_Width, "Fire_Cone_Width");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Fire_Cone_Height, "Fire_Cone_Height");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Min_Recharge_Sec, "Fire_Min_Recharge_Seconds");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Max_Recharge_Sec, "Fire_Max_Recharge_Seconds");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Fire_Distance, "Fire_Range_Distance");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Fire_Pulses, "Fire_Pulse_Count");           
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Delay_Seconds, "Fire_Pulse_Delay_Seconds");

            if (Matches_True(Load_Instance_Text(Hard_Point_Instance, "Fire_When_Deployed")) && Matches_True(Load_Instance_Text(Hard_Point_Instance, "Fire_When_Undeployed")))
            { Button_Fire_Both_Click(null, null); } // Both True       
            else if (Matches_True(Load_Instance_Text(Hard_Point_Instance, "Fire_When_Deployed"))) { Button_Fire_Deployed_Click(null, null); }
            else if (Matches_True(Load_Instance_Text(Hard_Point_Instance, "Fire_When_Undeployed"))) { Button_Fire_Undeployed_Click(null, null); }
            else { Button_Fire_Both_Click(null, null); } // Both False
             
          
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Attachment_Bone, "Attachment_Bone");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Collision_Mesh, "Collision_Mesh");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Model_To_Attach, "Model_To_Attach");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Breakoff_Prop, "Death_Breakoff_Prop");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Damage_Decal, "Damage_Decal");

            Load_Instance_Value(Hard_Point_Instance, Text_Box_Damage_Particles, "Damage_Particles");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Death_Particles, "Death_Explosion_Particles");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Death_Explosion_SFX, "Death_Explosion_SFXEvent");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Fire_SFX, "Fire_SFXEvent");

          
      
            if (Matches_True(Load_Instance_Text(Hard_Point_Instance, "Is_Turret")))
            {   if (Toggle_Is_Turret == false) 
                {   // Need to open this Tab before the other one
                    if (Size_Expander_M == "false") { Button_Expand_M_Click(null, null); }
                    Switch_Is_Turret_Click(null, null);
                }
            }            
            else if (Toggle_Is_Turret == true)
            {   if (Size_Expander_M == "false") { Button_Expand_M_Click(null, null); }
                Switch_Is_Turret_Click(null, null);
            } 
   
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Rotation_Speed, "Turret_Rotate_Speed");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Turret_Rotation, "Turret_Rotate_Extent_Degrees");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Turret_Elevation, "Turret_Elevate_Extent_Degrees");

            // Getting the 3 Values of this tag
            Temporal_E = Load_Instance_Text(Hard_Point_Instance, "Turret_Rest_Angle").Replace(" ", "").Split(',');
            if (Temporal_E.Count() > 0) { Text_Box_Turret_X.Text = Remove_Zero(Temporal_E[0]); }
            if (Temporal_E.Count() > 1) { Text_Box_Turret_Y.Text = Remove_Zero(Temporal_E[1]); }
            if (Temporal_E.Count() > 2) { Text_Box_Turret_Z.Text = Remove_Zero(Temporal_E[2]); }

            Load_Instance_Value(Hard_Point_Instance, Text_Box_Turret_Bone, "Turret_Bone_Name");
            Load_Instance_Value(Hard_Point_Instance, Text_Box_Barrel_Bone, "Barrel_Bone_Name");




            // Getting all Values of this tag
            Temporal_C = 0;
            Temporal_D = List_View_Fire_Restrictions_2.Items.Count;
            Temporal_E = Load_Instance_Text(Hard_Point_Instance, "Fire_Category_Restrictions").Replace(" ", "").Split(',');

            if (Temporal_D > 0) // If has any entries
            {
                foreach (ListViewItem Item in List_View_Fire_Restrictions_2.Items)
                {
                    if (Item.Text != "" & !Table_Matches(Temporal_E, Item.Text) & !List_View_Matches(List_View_Fire_Restrictions_1, Item.Text))
                    { List_View_Fire_Restrictions_1.Items.Add(Item.Text); Temporal_C++; }
                }

                if (Temporal_C > 0) { List_View_Fire_Restrictions_2.Items.Clear(); }
            }


            
            foreach (string Entry in Temporal_E)
            {
                if (Entry != "")
                {
                    if (List_View_Matches(List_View_Fire_Restrictions_1, Entry)) // Removing from here
                    { Remove_From_List_View(List_View_Fire_Restrictions_1, Entry); }

                    if (!List_View_Matches(List_View_Fire_Restrictions_2, Entry)) // Only if not already inside here
                    { List_View_Fire_Restrictions_2.Items.Add(Entry); }                
                }         
            }

        



            Temporal_A = "";
            Temporal_B = "";
            string Is_Done = "";
            IEnumerable <string> Query = null;

            List_View_Fire_Distance.Items.Clear();
            
            try // As non weapon HPs have no such tags
            {   // Importing all entires of a repeating tag
                for (int i = 6; i >= 0; --i) // There are 7 Cathegories, thus 6.
                {
                    Query = from Entry in Hard_Point_Instance.Descendants("Fire_Inaccuracy_Distance")
                            where !Is_Done.Contains(Entry.Value) 
                            select Entry.Value;

                    Temporal_B = Remove_Zero(Query.First().ToString());            
                    Temporal_A += Temporal_B;
                    Is_Done += Query.First().ToString();
                   
                    List_View_Fire_Distance.Items.Add(Temporal_B);
                    
                }

                // Imperial_Console(400, 100, Temporal_A);
                Set_Checkmate_Color(List_View_Fire_Distance);
            } catch {}

          
            User_Input = true;
        }
        //=====================//

      
        void Set_HP_Variant_Template()
        {
            if (Combo_Box_Hart_Point_Type.Text == "") { return; } // Got nothing to compare

            HP_Is_Weapon = false;
            // HP_Variant_Normal = "";
            HP_Variant_Template = "";
            Text_Box_HP_Is_Variant.Text = ""; // Resetting Here

            // string[] Hard_Point_Types = { "Hard_Point_Weapon_Laser", "Hard_Point_Weapon_Ion_Cannon", 
            Temporal_E = new string[] { "Hard_Point_Weapon_Laser", "Hard_Point_Weapon_Ion_Cannon", 
                    "Hard_Point_Weapon_Missile", "Hard_Point_Weapon_Torpedo", "Hard_Point_Weapon_Mass_Driver"};

            foreach (string Entry in Temporal_E) // Matching Weapon Types
            {
                if (Regex.IsMatch(Combo_Box_Hart_Point_Type.Text, "(?i).*?" + Entry))
                {
                    HP_Variant_Template = "HP_Weapon_Template";
                    HP_Is_Weapon = true;
                    break;
                }
            }


            if (HP_Is_Weapon == false)
            {
                if (Regex.IsMatch(Combo_Box_Hart_Point_Type.Text, "(?i).*?" + "Hard_Point_Fighter_Bay"))
                { HP_Variant_Template = "HP_Bay_Template"; }

                else if (Regex.IsMatch(Combo_Box_Hart_Point_Type.Text, "(?i).*?" + "Hard_Point_Shield_Generator"))
                { HP_Variant_Template = "HP_Shield_Template"; }

                else if (Regex.IsMatch(Combo_Box_Hart_Point_Type.Text, "(?i).*?" + "Hard_Point_Engine"))
                { HP_Variant_Template = "HP_Engine_Template"; }

                else
                {
                    Temporal_E = new string[] {"Hard_Point_Weapon_Special", "Hard_Point_Enable_Special_Ability", "Hard_Point_Dummy_Art",
                        "Hard_Point_Tractor_Beam", "Hard_Point_Gravity_Well"};
                    foreach (string Entry in Temporal_E) // Matching Weapon Types
                    {
                        if (Regex.IsMatch(Combo_Box_Hart_Point_Type.Text, "(?i).*?" + Entry))
                        {
                            HP_Variant_Template = "HP_Special_Template";
                            break;
                        }
                    }
                }
            }               
        }

        //=====================//

        void Load_Instance_Value(XElement The_Instance, Control Text_Box, string Tag_Name)
        {   try {  Text_Box.Text = Remove_Zero(The_Instance.Descendants(Tag_Name).First().Value); } catch {}
        }

        string Load_Instance_Text(XElement The_Instance, string Tag_Name)
        {
            try { return The_Instance.Descendants(Tag_Name).First().Value; } catch {}
            
            return "";
        }


        bool Matches_True (string The_Text)
        {
            if (Regex.IsMatch(The_Text, "(?i).*?" + "True") 
             || Regex.IsMatch(The_Text, "(?i).*?" + "Yes"))
            {
                return true;
            } // Otherwise
            return false;
        }


        bool Matches_False(string The_Text)
        {
            if (Regex.IsMatch(The_Text, "(?i).*?" + "False")
             || Regex.IsMatch(The_Text, "(?i).*?" + "No"))
            {
                return true;
            } // Otherwise
            return false;
        }

        string Remove_Zero(string The_Text)
        {   // Getting rid of Decimal format
            if (The_Text.EndsWith(".0")) { The_Text = The_Text.Remove(The_Text.Length - 2); }
            else if (The_Text.EndsWith(".0 ")) { The_Text = The_Text.Remove(The_Text.Length - 3); }
            return The_Text;
        }
        
   

        string Load_Desired_Value(XElement The_Instance, string Tag_Name, string Required_Value)
        {
            try
            {   var Content =
                   from All_Tags in The_Instance.Descendants(Tag_Name)
                   where All_Tags.Value.Contains(Required_Value)
                   select All_Tags;


                foreach (XElement Tag in Content) // Grabbing the first one
                {
                    Temporal_E = Tag.Value.Replace(" ", "").Split(',');
                    return Temporal_E[1];                
                }

            } catch {}

            return null;
        }
        //===========================//

        // The random number provider.
        private System.Security.Cryptography.RNGCryptoServiceProvider Rand =
            new System.Security.Cryptography.RNGCryptoServiceProvider();

        // Return a random integer between a min and max value.
        private int Random_Integer(int min, int max)
        {
            uint scale = uint.MaxValue;
            while (scale == uint.MaxValue)
            {
                // Get four random bytes.
                byte[] four_bytes = new byte[4];
                Rand.GetBytes(four_bytes);

                // Convert that into an uint.
                scale = BitConverter.ToUInt32(four_bytes, 0);
            }

            // Add min to the scaled difference between max and min.
            return (int)(min + ((max + 1) - min) *
                (scale / (double)uint.MaxValue));
        }

        //===========================//
        void Load_Galaxy_To_UI(string Xml_File, string Galaxy_Name)
        {
            User_Input = false;

            XElement Galaxy_Instance = Get_Instance(Xml_File, "Campaign", Galaxy_Name);


            Load_Instance_Value(Galaxy_Instance, Text_Box_Sort_Order, "Sort_Order");
            Load_Instance_Value(Galaxy_Instance, Text_Box_Conquest_Text_ID, "Text_ID");
            Load_Instance_Value(Galaxy_Instance, Text_Box_Conquest_Description, "Description_Text");
            Load_Instance_Value(Galaxy_Instance, Text_Box_Campaign_Set, "Campaign_Set");



            Temporal_List.Clear();


            Temporal_A = "";

            try
            {
                // Active Faction, very important as this is the first one that will be loaded at the Red (A) slot
                Temporal_A = Galaxy_Instance.Descendants("Starting_Active_Player").First().Value.Replace(" ", "");
                Combo_Box_Conquest_Faction.Text = Temporal_A;
                Temporal_List.Add(Temporal_A);



                string Two_Letters = Temporal_A.Remove((Temporal_A.Length + 2) - Temporal_A.Length);

                Faction_Table_A = new List<string>();

                // Value 0 is Name of the Faction itself, 
                // Value 1 = Home Location
                // Value 2 = Story Name
                // Value 3 = AI Player Control


                Faction_Table_A.Add(Temporal_A);
                // Adding empty values so we can call their Index!
                Faction_Table_A.Add("");
                Faction_Table_A.Add("");
                Faction_Table_A.Add("");
                Faction_Table_A.Add("");

                // Removing all except for the first 2 letters in the Faction Name
                Label_Faction_A.Text = Two_Letters;
                Selected_Faction_Table = "A";
            } catch {}
         

            try 
            {   var Content =
                   from All_Tags in Galaxy_Instance.Descendants("AI_Player_Control")
                   where All_Tags.Value != null
                   select All_Tags;


                foreach (XElement Tag in Content) 
                {   // Getting all involved Factions of this GC listed
                    Temporal_E = Tag.Value.Replace(" ", "").Split(',');
                    if (Temporal_E[0] != Combo_Box_Conquest_Faction.Text) { Temporal_List.Add(Temporal_E[0]); }
                    // TODO Temporal_E[1] = Player Control Text
                }
            } catch {}



            Temporal_C = 0;

            try
            {   // Temporal_C < 4  because we only need the first 3 (including the "Starting_Active_Player")
                if (Temporal_C < 4 & Temporal_List[1] != Temporal_List[0]) 
               // { Combo_Box_Conquest_Faction.Text = Temporal_List[1]; Temporal_C++; }
                {
                    Faction_Table_B = new List<string>();
                    Faction_Table_B.Add(Temporal_List[1]);                  
                    Faction_Table_B.Add("");
                    Faction_Table_B.Add("");
                    Faction_Table_B.Add("");
                    Faction_Table_B.Add("");
                    Temporal_C++;

                    Label_Faction_B.Text = Temporal_List[1].Remove((Temporal_List[1].Length + 2) - Temporal_List[1].Length); 
                }
                if (Temporal_C < 4 & Temporal_List[2] != Temporal_List[0])
                {
                    Faction_Table_C = new List<string>();
                    Faction_Table_C.Add(Temporal_List[2]);
                    Faction_Table_C.Add("");
                    Faction_Table_C.Add("");
                    Faction_Table_C.Add("");
                    Faction_Table_C.Add(""); 
                    Temporal_C++;

                    Label_Faction_C.Text = Temporal_List[2].Remove((Temporal_List[2].Length + 2) - Temporal_List[2].Length); 
                }


                string Faction_A = Temporal_List[0];
                string Faction_B = Temporal_List[1];
                string Faction_C = Temporal_List[2];



                Temporal_A = "";
                Temporal_A = Load_Desired_Value(Galaxy_Instance, "Starting_Credits", Faction_A);
                if (Temporal_A != null) { Text_Box_Start_Credits_A.Text = Temporal_A; }

                Temporal_A = Load_Desired_Value(Galaxy_Instance, "Starting_Credits", Faction_B);
                if (Temporal_A != null) { Text_Box_Start_Credits_B.Text = Temporal_A; }

                Temporal_A = Load_Desired_Value(Galaxy_Instance, "Starting_Credits", Faction_C);
                if (Temporal_A != null) { Text_Box_Start_Credits_C.Text = Temporal_A; }


                Temporal_A = "";
                Temporal_A = Load_Desired_Value(Galaxy_Instance, "Starting_Tech_Level", Faction_A);
                if (Temporal_A != null) { Text_Box_Starting_Tech_LV_A.Text = Temporal_A; }

                Temporal_A = Load_Desired_Value(Galaxy_Instance, "Starting_Tech_Level", Faction_B);
                if (Temporal_A != null) { Text_Box_Starting_Tech_LV_B.Text = Temporal_A; }

                Temporal_A = Load_Desired_Value(Galaxy_Instance, "Starting_Tech_Level", Faction_C);
                if (Temporal_A != null) { Text_Box_Starting_Tech_LV_C.Text = Temporal_A; }


                Temporal_A = "";
                Temporal_A = Load_Desired_Value(Galaxy_Instance, "Max_Tech_Level", Faction_A);
                if (Temporal_A != null & Temporal_A != "") { Text_Box_Max_Tech_LV_A.Text = Temporal_A; }

                Temporal_A = Load_Desired_Value(Galaxy_Instance, "Max_Tech_Level", Faction_B);
                if (Temporal_A != null & Temporal_A != "") { Text_Box_Max_Tech_LV_B.Text = Temporal_A; }

                Temporal_A = Load_Desired_Value(Galaxy_Instance, "Max_Tech_Level", Faction_C);
                if (Temporal_A != null & Temporal_A != "") { Text_Box_Max_Tech_LV_C.Text = Temporal_A; }


                // Tables contain this: Value 0 is Name of the Faction itself, 
                // Value 1 = Home Location
                // Value 2 = Story Name
                // Value 3 = AI Player Control



                // Inverted Order to load Faction C, then B then A because the last one overwrites the text over previous. But the others stay if A tag is empty.
                Temporal_A = "";
                Temporal_A = Load_Desired_Value(Galaxy_Instance, "AI_Player_Control", Faction_C);
                if (Temporal_A != null & Temporal_A != "")
                {
                    Faction_Table_C[3] = Temporal_A;

                    Button_Indicator_Player_C.Visible = true;
                }

                Temporal_A = "";
                Temporal_A = Load_Desired_Value(Galaxy_Instance, "AI_Player_Control", Faction_B);
                if (Temporal_A != null & Temporal_A != "")
                {
                    Faction_Table_B[3] = Temporal_A;

                    Button_Indicator_Player_B.Visible = true;
                }

                // Faction_A is the active player for each part of the GC, but it still will be loaded from GCs where it is an AI (below in he same GC.xml).
                Temporal_A = "";
                Temporal_A = Load_Desired_Value(Galaxy_Instance, "AI_Player_Control", Faction_A);
                if (Temporal_A != null & Temporal_A != "")
                {
                    Faction_Table_A[3] = Temporal_A;
                    Combo_Box_AI_Player_Control.Text = Temporal_A;
                    Button_Indicator_Player_A.Visible = true;
                }



                Temporal_A = "";
                Temporal_A = Load_Desired_Value(Galaxy_Instance, "Home_Location", Faction_C);
                if (Temporal_A != null & Temporal_A != "")
                {
                    Faction_Table_C[1] = Temporal_A;
                    Text_Box_Home_Location.Text = Temporal_A;
                    Button_Indicator_Home_C.Visible = true;
                }


                Temporal_A = "";
                Temporal_A = Load_Desired_Value(Galaxy_Instance, "Home_Location", Faction_B);
                if (Temporal_A != null & Temporal_A != "")
                {
                    Faction_Table_B[1] = Temporal_A;
                    Text_Box_Home_Location.Text = Temporal_A;
                    Button_Indicator_Home_B.Visible = true;
                }


                Temporal_A = "";
                Temporal_A = Load_Desired_Value(Galaxy_Instance, "Home_Location", Faction_A);
                if (Temporal_A != null & Temporal_A != "")
                {
                    Faction_Table_A[1] = Temporal_A;
                    Text_Box_Home_Location.Text = Temporal_A;
                    Button_Indicator_Home_A.Visible = true;
                }



                // Only if the Faction is one of these 3 Valid factions:
                Temporal_A = "Rebel, Empire, Underworld";

                if (Temporal_A.Contains(Faction_A))
                {
                    Temporal_B = Load_Instance_Text(Galaxy_Instance, Faction_A + "_Story_Name");
                    if (Temporal_B != null & Temporal_B != "")
                    {
                        Faction_Table_A[2] = Text_Box_Story_Name.Text = Temporal_B;
                        Button_Indicator_Story_A.Visible = true;
                    }
                }

                if (Temporal_A.Contains(Faction_B))
                {
                    Temporal_B = Load_Instance_Text(Galaxy_Instance, Faction_B + "_Story_Name");
                    if (Temporal_B != null & Temporal_B != "")
                    {
                        Faction_Table_B[2] = Temporal_B;
                        Button_Indicator_Story_B.Visible = true;
                    }

                }

                if (Temporal_A.Contains(Faction_C))
                {
                    Temporal_B = Load_Instance_Text(Galaxy_Instance, Faction_C + "_Story_Name");
                    if (Temporal_B != null & Temporal_B != "")
                    {
                        Faction_Table_C[2] = Temporal_B;
                        Button_Indicator_Story_C.Visible = true;
                    }
                }



                //  Imperial_Console(600, 100, Add_Line + Galaxy_Instance.Descendants().First().ToString());
          
            } catch { }

            User_Input = true;
        }

        //===========================//
        void Build_Galaxy(int Amount_of_Planets, int Planet_Distance, int Maximal_Planet_Distance)
        {
            int Probability = Track_Bar_Planet_Density.Value;
          
            if (Maximal_Planet_Distance < 261) // We don't want to exceed the remaining space
            { if (Probability < 6) { Maximal_Planet_Distance = Maximal_Planet_Distance + Planet_Distance; } }


            int Planets_Per_Column = (Maximal_Planet_Distance * 2) / Planet_Distance;


            // Starting at the maximum in minus area
            int x = -Maximal_Planet_Distance; 
            int y = Maximal_Planet_Distance;
            int z = 10; // This stays at default value of 10

            int Skipped_Raws = 0;

      

            string Result = "";
          
            // Looping through all Planet Slots

            for (int i = 0; i < 200 + 1; i++)
            {
                if (Planet_Count == Amount_of_Planets + 1) { break; } // Escaping if enough planets were created

                // Building Columns
                for (int e = 0; e < 200 + 1; e++)
                {
                    // if (Planet_Count != 1 & Planet_Count != 10 & Planet_Count != 91 & Planet_Count != 100) // This is the original pattern to prevent edge planets
                    // Skipping this planet position for the 4 edge planets
                    if (Planet_Count == 1 | Planet_Count == Planets_Per_Column)
                        // | Planet_Count == (Maximal_Planet_Distance - Planets_Per_Column) | Planet_Count == Maximal_Planet_Distance)
                    { x = x + Planet_Distance; }

                    // The Randomize effect here makes sure some values are not applied so we have leaks in the planetary grid!
                    else if (Probability < Random_Integer(1, 10)) { x = x + Planet_Distance; } // Zero means we use the full grid without missing planets
                    if (Probability < Random_Integer(1, 10)) { x = x + Planet_Distance; } // Maybe skipping the second planet in a row                                                           

                    if (Planet_Count == Amount_of_Planets + 1) { break; } // First we escape this loop to finally escape the one above 

                    // Cycling from left - area to the + right and stopping when maximal Distance was reached, then starting back at - Max
                    if (x >= Maximal_Planet_Distance) { x = -Maximal_Planet_Distance; break; }
                   

                    Result += "Planet_" + Planet_Count + ", ";
                    Create_Planet("Planet_" + Planet_Count, x , y);
                    Conquest_Planet_List.Add("Planet_" + Planet_Count);
                    Planet_Count++;

                    // Incrementing by distance value
                    x = x + Planet_Distance;                                               
                }

                
                // Building Raws       
                if (y >= -Maximal_Planet_Distance)
                {   // Incrementing by distance value, skipping the whole column
                    if (Probability < 8 & Skipped_Raws < 2 & Probability < Random_Integer(1, 10)) { y = y - Planet_Distance; Skipped_Raws++; }
                    y = y - Planet_Distance;
                } else { break; }

            }

        } 

        //===========================//
        void Create_Planet_File()
        {           

            try
            {                   
                Planet_Xml_File = new XDocument
                ( 
                    new XElement("Planets", new XComment(" ================= Generated using Imperialware ================ "))
                );
             

                Save_File_Dialog_1.InitialDirectory = Xml_Directory;
                Save_File_Dialog_1.Filter = "xml files (*.xml)|*.xml|All files (*.*)|*.*";
                Save_File_Dialog_1.DefaultExt = "xml";


                string Planet_Xml = "";

                if (Save_File_Dialog_1.ShowDialog() == DialogResult.OK)
                {
                    Planet_Xml = Save_File_Dialog_1.FileName;
                    // Using Label as Global variable ^^
                    Label_Planet_File.Text = Path.GetFileName(Planet_Xml);
                }

           
                Planet_Xml_File.Save(Planet_Xml); // Creating new File


                if (Check_Box_Planet_Game_Objects.Checked)
                {   // Adding the tag "File" to the specified Xml File above with the Name of the new file.
                    Save_Tag_Into_Xml(Xml_Directory + "GameObjectFiles.xml", "Root", "File", Label_Planet_File.Text, false);
                }


                Temporal_A = Load_Setting(Setting, "Planet_Data_Files");
                if (!Temporal_A.Contains(Label_Planet_File.Text))
                {   // Appending the new generated File to Planet_Data_Files
                    Save_Setting("2", "Planet_Data_Files", Load_Setting(Setting, "Planet_Data_Files") + Label_Planet_File.Text + ",");
                }
                
                
            } catch {}  
        }


        //===========================//
        void Create_Planet(string Planet_Name, int Position_X, int Position_Y)
        {
            // Getting Random Planet
            Temporal_C = Random_Integer(1, Planet_Images.Count());
            
            Random Random_A = new Random();

            // Preparing Tag values to insert them into the Planet Instance
            try { Temporal_A = Planet_Images[Temporal_C]; } catch { Temporal_A = Planet_Images[0]; }
            // Trying again if we got the Deathstar, I want to exclude that
            if (Temporal_A.Contains("Deathstar")) { Temporal_C = Random_Integer(1, Planet_Images.Count()); }
            if (Temporal_A.Contains("Deathstar")) { Temporal_C = Random_Integer(1, Planet_Images.Count()); }

            string Is_Variant_Of = "Planet_Template";
            if (Check_Box_Planet_Is_Variant.Checked & Text_Box_Planet_Is_Variant.Text != "")
            { Is_Variant_Of = Text_Box_Planet_Is_Variant.Text; }
         

           
            try
            {   //================ Appending new Instance to the Bottom ================           
                
                // Otherwise appending to the bottom
                Planet_Xml_File.Root.Add("\n\t",
                   Planet_Instance = new XElement("Planet",
                                 new XAttribute("Name", Planet_Name), "\n\t\t",
                                 new XElement("Variant_Of_Existing_Type", Is_Variant_Of), "\n\t\t"                                                                                           
                          ),                              
                "\n");

                //======================================================================
          
            } catch {}
       
        
       
            string Planet_Image = "";
            try { Planet_Image = Planet_Images[Temporal_C]; } catch { Planet_Image = Planet_Images[0]; }
            Add_Tag_Text(Planet_Xml_File, Planet_Name, "Galactic_Model_Name", Planet_Image + ".alo", "\n\t\t");
            Add_Extension_To_Instance(Planet_Instance, "Destroyed_Galactic_Model_Name", Text_Box_Destroyed_Planet_Model, ".alo", "\n\t\t");


            Add_Tag_Text(Planet_Xml_File, Planet_Name, "Text_ID", Get_Terrain(Planet_Image, 2), "\n\t\t"); // 2 = Planet Name Text
            Add_Tag_Text(Planet_Xml_File, Planet_Name, "Encyclopedia_Text", Get_Terrain(Planet_Image, 3), "\n\t\t"); // 3 = Planet Description Text


            int Size_Scale = Random_Integer(1, 9); // Making sure the planets are not too tiny

            // Black hole needs tiny scaling of 0.01 or it would brake the GC appearance!
            if (Planet_Image == "W_Planet_Deathstar" | Planet_Image == "W_Planet_Deathstar02" | Planet_Image == "W_Planet_Deathstar02_II") { Add_Tag_Text(Planet_Xml_File, Planet_Name, "Scale_Factor", "0.01", "\n\t\t"); }
            else
            {   Temporal_D = Random_Integer(0, 1);
                

                // Another filter to reduce chance for really huge planets
                if (Size_Scale > 6) { Size_Scale = Random_Integer(5, 9); } 

                if (Temporal_D == 0)
                {  //Gas Giants are supposed to be greater
                    if (Planet_Image == "W_Planet_Gas_Silver" | Planet_Image == "W_Planet_Gas01" | Planet_Image == "W_Black_Hole_Small") { Temporal_D = 1; }
                    else if (Size_Scale < 7) { Size_Scale = Random_Integer(8, 9); }
                }
         
                Add_Tag_Text(Planet_Xml_File, Planet_Name, "Scale_Factor", Temporal_D + "." + Size_Scale, "\n\t\t");                             
            } 
          
      

            string Black_List = "W_Black_Hole, W_Black_Hole_Small, W_Planet_Asteroids, W_Planet_Deathstar, W_Planet_Deathstar_II_Destroyed, W_Planet_Deathstar02_II, W_Planet_Deathstar02";
            if (!Black_List.Contains(Planet_Image))
            {   // The blacklisted ones have no Ground map, so aviously are not accessible.
                if (Toggle_Surface_Accessible) { Add_Tag_Text(Planet_Xml_File, Planet_Name, "Planet_Surface_Accessible", "True", "\n\t\t"); }          
            }

            if (!Toggle_Is_Pre_Lit) { Add_Tag_Text(Planet_Xml_File, Planet_Name, "Pre_Lit", "No", "\n\t\t"); }
            
            Add_Tag_Text(Planet_Xml_File, Planet_Name, "Galactic_Position", Position_X + ", " + Position_Y + ", " + 10, "\n\t\t");


            Temporal_B = Mod_Name;
            // This line just makes sure we use the same info directory for different version of the Stargate TPC Mod, we need this in the Get_Map() Function! 
            if (Temporal_B == "StargateAdmin" | Temporal_B == "StargateBeta" | Temporal_B == "StargateOpenBeta") { Temporal_B = "Stargate"; }

            string The_Map = null;
            The_Map = Get_Map(Planet_Image, "Space");
            if (The_Map != null) { Add_Tag_Text(Planet_Xml_File, Planet_Name, "Space_Tactical_Map", The_Map + ".ted", "\n\t\t"); }
            else { Add_Tag_Text(Planet_Xml_File, Planet_Name, "Space_Tactical_Map", "", "\n\t\t"); }

            The_Map = null;
            The_Map = Get_Map(Planet_Image, "Land");
            if (The_Map != null) { Add_Tag_Text(Planet_Xml_File, Planet_Name, "Land_Tactical_Map", The_Map + ".ted", "\n\t\t"); }
            else { Add_Tag_Text(Planet_Xml_File, Planet_Name, "Land_Tactical_Map", "", "\n\t\t"); }


            // Blacklisted Planets are not supposed to get A destroyed Map
            if (!Black_List.Contains(Planet_Image))
            {   The_Map = Get_Map("W_Planet_Asteroids", "Space");
                if (The_Map != null) { Add_Tag_Text(Planet_Xml_File, Planet_Name, "Destroyed_Space_Tactical_Map", The_Map + ".ted", "\n\t\t"); }
            }

            // 1 inside of parameter 2 means the Index number
            // string The_Terrain = Get_Terrain(Planet_Image, 1);
            Add_Tag_Text(Planet_Xml_File, Planet_Name, "Zoomed_Terrain_Index", Get_Terrain(Planet_Image, 1), "\n\t\t");

            /*
            if (The_Terrain == "1") { The_Terrain = "Desert"; }
            else if (The_Terrain == "2") { The_Terrain = "Swamp"; }
            else if (The_Terrain == "3") { The_Terrain = "Volcanic"; }
            else if (The_Terrain == "4") { The_Terrain = "Arctic"; }
            else if (The_Terrain == "5") { The_Terrain = "Forest"; }
            else if (The_Terrain == "6") { The_Terrain = "Urban"; }
            else { The_Terrain = "false"; } // Temperate already in the Teplate Planet so we just leave it like that

            if (The_Terrain != "false") { Add_Tag_Text(Planet_Xml_File, Planet_Name, "Terrain", The_Terrain, "\n\t\t"); }
            */


            Add_Tag_Text(Planet_Xml_File, Planet_Name, "Max_Space_Base", Text_Box_Max_Star_Base.Text, "\n\t\t");
            Add_Tag_Text(Planet_Xml_File, Planet_Name, "Special_Structures_Space", Text_Box_Space_Structures.Text, "\n\t\t");
            Add_Tag_Text(Planet_Xml_File, Planet_Name, "Special_Structures_Land", Random_Integer(5, 9).ToString(), "\n\t\t");

            Temporal_C = Random_Integer(1, 15);
            Add_Tag_Text(Planet_Xml_File, Planet_Name, "Planet_Credit_Value", Temporal_C.ToString() + "0", "\n\t\t");
            Add_Tag_Text(Planet_Xml_File, Planet_Name, "Planet_Destroyed_Credit_Value", Temporal_C.ToString(), "\n\t\t");

            Add_Tag_Text(Planet_Xml_File, Planet_Name, "Planet_Capture_Bonus_Reward", (Temporal_C * 2) + "0", "\n\t\t");
            Add_Tag_Text(Planet_Xml_File, Planet_Name, "Additional_Population_Capacity", Random_Integer(5, 9).ToString(), "\n\t");

           
            // TODO Scale Factor ist noch auf 0
            Add_Planet_to_UI(Planet_Name, Planet_Image, 0, Position_X, Position_Y, Size_Scale);
      
        }



        //============== Creating an Planet Avatar in the Imperialware UI =============// 
        void Add_Planet_to_UI(string Planet_Name, string Planet_Image, int Scale_Factor, int Position_X, int Position_Y, int Size_Scale)       
        {         
            // TODO = Tausche das gegen den gleichen Code oben aus und führe es von dieser Funktion hier aus.
            int Image_Size = 50;
            decimal Planet_Size = 0;
            
            decimal.TryParse((Temporal_D + "," + Size_Scale), out Planet_Size);
            Size_Scale = (Convert.ToInt32(Planet_Size)) * (Image_Size / 20); // * 2 because maximum image scale is 40, And then Image_Size / 20 (because 2.0 is planet maximum) is 2

            int Size_Buffer = 0; // This buffer makes sure the image is centered at the exact planet position instead of being in the top left corner!
            try { Size_Buffer = (Image_Size - Size_Scale) / 2; } catch {}
         
                     

            PictureBox Planet = new PictureBox
            {
                Name = Planet_Name,
                Text = Planet_Image + "," + Size_Scale.ToString() + "," + Position_X + "," + Position_Y, // + "," + Position_Z, // Storing Name, Size and Location in Text value as smartass variables for later!
                Size = new Size(Size_Scale, Size_Scale),

                // + 400 because we want to move the Point 0 to the middle of the Image which is max 800x800 pixels       
                Location = new Point(Position_X + 400 + Image_Size + Size_Buffer, Position_Y + 400 + Image_Size + Size_Buffer),          
                BackColor = Color.Transparent,
                Image = Resize_Image(Program_Directory + @"Mods\Vanilla_Game\Planets\", Planet_Image + ".png", Size_Scale, Size_Scale),
          
            };

            Picture_Galactic_Background.Controls.Add(Planet);
            Planet.Click += new EventHandler(Open_Planet_Click);             
          
        }


        //===========================//     
        // Get_Map(Planet_Image_Name, "Space")    Map Type can whether be "Space" or "Land"
        string Get_Map(string Planet_Image_Name, string Map_Type)
        {        
            string Source_File = Program_Directory + Mods_Directory + Temporal_B + @"\Planets\" + Map_Type + "_Maps.txt";
            Temporal_A = "";

            if (File.Exists(Source_File)) { Temporal_A = Load_Setting(Source_File, Planet_Image_Name); }

            if (Temporal_A == "No_Map" | Temporal_A == null | Temporal_A == "") 
            {
                // If no entry was found in the corresponding dir of the Mod, we try planets of the Vanilla game
                Source_File = Program_Directory + @"Mods\Vanilla_Game\Planets\" + Map_Type + "_Maps.txt"; 
                
                Temporal_A = Load_Setting(Source_File, Planet_Image_Name);

                if (Temporal_A == "No_Map" | Temporal_A == null | Temporal_A == "")
                { Error_List.Add("<" + Map_Type + "_Tactical_Map> to " + Planet_Image_Name); return null; } // Nothing found             

            }
           
            if (Temporal_A.Contains(",")) // Then we know it is a table
            {
                // Splitting all Maps in the Setting 
                Temporal_E = Temporal_A.Split(',');

                // Choosing random map from the Setting array, for land we use the same value from the last time (to get space and land maps synchronous from their position in the table)
                if (Map_Type != "Land") { Temporal_C = Random_Integer(1, Temporal_E.Count() -1); }

                try { return Temporal_E[Temporal_C]; } catch { return null; }              
            }
            else // Otherwise we return the full value
            {
                return Temporal_A;
            }
        }


        //===========================//     

        string Get_Terrain(string Planet_Image_Name, int Parameter_Number)
        {
            string Source_File = Program_Directory + Mods_Directory + Temporal_B + @"\Planets\Terrain\Planet_Indexes.txt";
            Temporal_A = "";

            if (File.Exists(Source_File)) { Temporal_A = Load_Setting(Source_File, Planet_Image_Name); }

            if (Temporal_A == null | Temporal_A == "")
            {
                // If no entry was found in the corresponding dir of the Mod, we try planet index file of the Vanilla game
                Source_File = Program_Directory + @"Mods\Vanilla_Game\Planets\Terrain\Planet_Indexes.txt";

                Temporal_A = Load_Setting(Source_File, Planet_Image_Name);

                if (Temporal_A == null | Temporal_A == "")
                { return null; } // Nothing found             
            }

      


            if (Temporal_A.Contains(",")) // Then it has multiple values
            {  
                Temporal_E = Temporal_A.Split(',');               
            } 
            else
            {
                // If it has no specified parameter we just take the default ones specified in the generic Planets!
                if (Temporal_A == "0") { Planet_Image_Name = "W_Planet_Temperate01"; }
                else if (Temporal_A == "1") { Planet_Image_Name = "W_planet_Desert01"; }
                else if (Temporal_A == "2") { Planet_Image_Name = "W_Planet_Swamp01"; }
                else if (Temporal_A == "3") { Planet_Image_Name = "W_Planet_Volcanic01"; }
                else if (Temporal_A == "4") { Planet_Image_Name = "W_Planet_Ice01"; }
                else if (Temporal_A == "5") { Planet_Image_Name = "W_Planet_Forest01"; }
                else if (Temporal_A == "6") { Planet_Image_Name = "W_Planet_Urban01"; }
                else if (Temporal_A == "7") { Planet_Image_Name = "W_Planet_Ocean01"; }
                else if (Temporal_A == "12") { Planet_Image_Name = "W_Planet_Utapau_Low"; }

                Temporal_E = Load_Setting(Source_File, Planet_Image_Name).Split(',');           
            }
        

            // Parameter 3 will choose between parameter slot 3, 4 or 5 for different text strings
            if (Parameter_Number == 3) 
            {   
                if (Temporal_E.Count() > 4) { Parameter_Number = Random_Integer(3, 5); } // For 5 Slots
                else if (Temporal_E.Count() > 3) { Parameter_Number = Random_Integer(3, 4); } // For 5 Slots
                // Otherwise Parameter_Number stays at 3                
            }

            try { return Temporal_E[Parameter_Number - 1]; } catch { return null; }
        }     


        /*
        string Get_Terrain_Index(string Planet_Image_Name)
        {
            string Source_File = Program_Directory + Mods_Directory + Temporal_B + @"\Planets\Terrain\Planet_Indexes.txt";
            Temporal_A = "";

            if (File.Exists(Source_File)) { Temporal_A = Load_Setting(Source_File, Planet_Image_Name); }

            if (Temporal_A == null | Temporal_A == "")
            {
                // If no entry was found in the corresponding dir of the Mod, we try planet index file of the Vanilla game
                Source_File = Program_Directory + @"Mods\Vanilla_Game\Planets\Terrain\Planet_Indexes.txt";

                Temporal_A = Load_Setting(Source_File, Planet_Image_Name);

                if (Temporal_A == null | Temporal_A == "")
                { return null; } // Nothing found             
            }

            return Temporal_A;
        }      
        */
        //=====================//

        void Load_Unit_To_UI(string Selected_Xml, string Unit)
        {            
            // Making sure this only fires if any rescent Unit was edited
            if (Selected_Xml != "" & Unit != "")
            {
                if (Application.UseWaitCursor == false)
                {
                    Application.UseWaitCursor = true;
                    Application.DoEvents();
                }

                // Making sure the Textbox knows it was not the user who clicked the checkbox, so it doesent have to add the Variant tag
                User_Input = false;
                Loading_To_UI = true;

                // Needs to run under "User_Input = false" because otherwise it causes the "Save" function to have no Selected_XML for saving and finally fail. 
                if (Loading_Completed) { Mini_Button_Clear_Unit_Click(null, null); }



                // Passing the Unit Name and Type values from the instance above into their UI Boxes:
                Text_Box_Name.Text = Unit;
                // Loading Type according to the Search Settings, used to be disabled because that prevents Combo_Box_Type from acting on its own
                Combo_Box_Type.Text = Unit_Type;

                // This loading part needs to be over the other tags, or its code will overwrite loaded tags like Layer_Z_Adjust and other class relevant things
                Combo_Box_Class.Text = Load_Instance_Tag(Selected_Xml, Unit, "Ship_Class");

                Temporal_A = Combo_Box_Class.Text;

                if (Temporal_A == "") // If doesen't have that tag to load we try alternatives
                {
                    Combo_Box_Class.Text = Load_Instance_Tag(Selected_Xml, Unit, "Space_Layer");
                    Temporal_A = Combo_Box_Class.Text;
                }


                //======================== Loading the Squadron/Team =========================


                if (Temporal_A.Contains("Fighter") | Temporal_A.Contains("Bomber") | Unit_Mode == "Ground")
                {
                    // Assigning the type of Team or Squadron we are going to search for
                    if (Temporal_A.Contains("Fighter") | Temporal_A.Contains("Bomber")) { Team_Type = "Squadron"; Team_Units = "Squadron_Units"; }
                    else if (Temporal_A.Contains("HeroUnit") | Temporal_A.Contains("SpaceHero") | Temporal_A.Contains("LandHero")) { Team_Type = "HeroCompany"; Team_Units = "Company_Units"; }
                    else if (Unit_Mode == "Ground") { Team_Type = "GroundCompany"; Team_Units = "Company_Units"; }

                    // Clearing List
                    Error_List.Clear();


                    int Highest_Rated = 0;
                    string Team_Xml_File = "";
                    bool List_Contains_This = false;


                    // Getting the currently selected Item
                    if (List_View_Teams.SelectedItems.Count > 0)
                    { Temporal_A = List_View_Teams.SelectedItems[0].Text; }


                    foreach (var Xml in Get_Xmls())
                    {
                        try // Preventing the program from crashing because of xml errors
                        {
                            // ===================== Opening Xml File =====================
                            XDocument Xml_File = XDocument.Load(Xml, LoadOptions.PreserveWhitespace);

                            var Team_Objects =
                                from All_Tags in Xml_File.Descendants(Team_Type)
                                // Selecting all Instances that have any Team_Units inside

                                //  where All_Tags.Descendants(Team_Units).Any() 
                                // where All_Tags.Element(Team_Units).Value.Contains(Unit)                               
                                select All_Tags;


                            // =================== Checking Xml Instance ===================
                            foreach (XElement Team_Instance in Team_Objects)
                            {   // If our Unit is part of any Team/Squadron Team
                                if (Team_Instance.Elements(Team_Units).Any())
                                {
                                    if (Team_Instance.Element(Team_Units).Value.Contains(Unit))
                                    {
                                        var Item = List_View_Teams.Items.Add((string)Team_Instance.Attribute("Name"));


                                        // Adding all Teams of which this team is a Variant
                                        if (Team_Instance.Elements("Variant_Of_Existing_Type").Any())
                                        {
                                            Temporal_B = Team_Instance.Element("Variant_Of_Existing_Type").Value;

                                            List_Contains_This = false;

                                            foreach (string Entry in List_View_Teams.Items)
                                            { if (Entry == Temporal_B) { List_Contains_This = true; break; }; }


                                            //  If the value is not empty and if it is not already in that list
                                            if (Temporal_B != null & Temporal_B != "" & !List_Contains_This) // !List_View_Teams.Text.Contains(Temporal_B))
                                            {
                                                List_View_Teams.Items.Add(Team_Instance.Element("Variant_Of_Existing_Type").Value);
                                            }
                                        }

                                        Item.ForeColor = Color.Black;

                                        // Every second item should have this value in order to create a checkmate pattern with good contrast
                                        if (Checkmate_Color == "true" & Item.Index % 2 == 0 & Temporal_A != (string)Team_Instance.Attribute("Name"))
                                        {
                                            Item.ForeColor = Color_03;
                                            Item.BackColor = Color_07;
                                        }


                                        // And if this one has the most Descendant tags, that means it is probably the one with full code and no variant_of_existing_type.                         
                                        if (Team_Instance.Descendants().Count() > Highest_Rated)
                                        {
                                            Highest_Rated = Team_Instance.Descendants().Count();
                                            Team_Xml_File = Xml;
                                            Text_Box_Team_Name.Text = (string)Team_Instance.Attribute("Name");
                                        }



                                        // ============================================================
                                        // Adding all Teams of which this team is a Variant
                                        if (Team_Instance.Elements("Create_Team_Type").Any())
                                        {
                                            Temporal_A = Team_Instance.Element("Create_Team_Type").Value;                                         
                                        
                                            if (Temporal_A != "")
                                            {   Toggle_Button(Switch_Button_Use_Container, "Button_On", "Button_Off", 0, true); Use_Container = true;
                                                Text_Box_Container_Name.Text = Temporal_A;
                                            }
                                            else { Toggle_Button(Switch_Button_Use_Container, "Button_On", "Button_Off", 0, false); Use_Container = false; }
                                        }



                                    }
                                }

                                else if (List_View_Teams.Items.Count > 0)
                                {
                                    // For each object we already found above we do another query, and if it is a variant of them it will be added to our squadron list
                                    for (int i = List_View_Teams.Items.Count - 1; i >= 0; --i)
                                    {
                                        Temporal_A = List_View_Teams.Items[i].Text;
                                        List_Contains_This = false;

                                        foreach (string Entry in List_View_Teams.Items)
                                        { if (Entry == Temporal_A) { List_Contains_This = true; break; }; }


                                        // & !List_View_Teams.Text.Contains(Temporal_A))
                                        if (Team_Instance.Element("Variant_Of_Existing_Type").Value == Temporal_A & !List_Contains_This)
                                        { List_View_Teams.Items.Add((string)Team_Instance.Attribute("Name")); Imperial_Console(600, 100, Add_Line + Temporal_B); }
                                    }
                                }

                            }
                        }
                        catch { Error_List.Add(Path.GetFileName(Xml)); }
                    }


                    // Telling the user about crashed .XMLs that may contain syntax errors
                    if (Loading_Completed & Show_Load_Issues == "true" & Error_List.Count() > 0)
                    {
                        Temporal_A = "    Failed to load the following Xml files:" + Add_Line;
                        foreach (string Entry in Error_List)
                        {
                            Temporal_A += "    " + Entry + Add_Line;
                        }
                        Imperial_Console(700, 340, Temporal_A);
                    }

                    // We load just the team with most Descendnant tag entries, if there is only one thats the one we use.
                    Load_Team_To_UI(Team_Xml_File, Text_Box_Team_Name.Text);

                    // At the end of Load_Team_To_UI() it is set to true, but we need it to be false in order to accept ALL values
                    User_Input = false;

                    // Loading must have completed, or it would interfere with the correct UI startup!
                    if (Loading_Completed & Text_Box_Team_Name.Text != "" & !Is_Team) { if (Size_Expander_I == "false") { Button_Expand_I_Click(null, null); } Switch_Button_Is_Team_Click(null, null); } //Enabling Team Subtab
                    else if (!Loading_Completed & Text_Box_Team_Name.Text != "" & !Is_Team) { Launch_Team = true; } // Telling the MainWindow_Shown event to open team tab instead

                  
                    // Clearing List
                    Error_List.Clear();
                }



                //========================= Loading the actual Unit =========================     

                // TODO Use this to load faster from it
                XDocument Current_Xml_File = XDocument.Load(Selected_Xml, LoadOptions.PreserveWhitespace);

                IEnumerable<XElement> Selected_Unit =
                   from All_Tags in Current_Xml_File.Root.Descendants()
                   where (string)All_Tags.Attribute("Name") == Unit
                   select All_Tags;


                //============== Setting Variant_Of_Existing_Type ==============  

                string Variant_Value = Load_Instance_Tag(Selected_Xml, Unit, "Variant_Of_Existing_Type");


                if (Variant_Value == "Is_No_Variant")
                {
                    if (Check_Box_Is_Variant.Checked)
                    {
                        // Making sure the Checkbox is unchecked
                        Check_Box_Is_Variant.Checked = !Check_Box_Is_Variant.Checked;
                        // Clearing Textbox
                        Text_Box_Is_Variant.Text = "";
                        Button_Open_Variant.Visible = false;
                    }
                }
                else if (Variant_Value != "" && Variant_Value != "Is_No_Variant")
                {
                    if (!Check_Box_Is_Variant.Checked)
                    {    // Checking the "Is Variant" Checkbox in order to enable the Textfield
                        Check_Box_Is_Variant.Checked = !Check_Box_Is_Variant.Checked;
                        Button_Open_Variant.Visible = true;
                    }

                    // Inserting the value for Parent Xml Instance into the UI Field
                    Text_Box_Is_Variant.Text = Variant_Value;
                }


                //========================= Loading Art Settings =========================  
                Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Space_Model_Name");
                // Using Regex to remove the suffix, the (?i) enables case insensitive search
                Temporal_B = Regex.Replace(Temporal_A, "(?i).alo", "");
                Text_Box_Model_Name.Text = Temporal_B;


                if (Text_Box_Icon_Name.Text == "")
                {
                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Icon_Name");
                    Temporal_B = Regex.Replace(Temporal_A, "(?i).tga", "");
                    Text_Box_Icon_Name.Text = Temporal_B;
                }

                if (Text_Box_Radar_Icon.Text == "")
                {
                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Radar_Icon_Name");
                    Temporal_B = Regex.Replace(Temporal_A, "(?i).tga", "");
                    Text_Box_Radar_Icon.Text = Temporal_B;
                }

                if (Text_Box_Radar_Size.Text == "") { Text_Box_Radar_Size.Text = Load_Instance_Tag(Selected_Xml, Unit, "Radar_Icon_Size"); }
                // TODO: <Is_Visible_On_Radar>


                if (Text_Box_Text_Id.Text == "") { Text_Box_Text_Id.Text = Load_Instance_Tag(Selected_Xml, Unit, "Text_ID"); }
                if (Text_Box_Encyclopedia_Text.Text == "") { Text_Box_Encyclopedia_Text.Text = Load_Instance_Tag(Selected_Xml, Unit, "Encyclopedia_Text"); }
                if (Text_Box_Unit_Class.Text == "") { Text_Box_Unit_Class.Text = Load_Instance_Tag(Selected_Xml, Unit, "Encyclopedia_Unit_Class"); }


                // Scrollable numeral values
                Text_Box_Scale_Factor.Text = Load_Instance_Tag(Selected_Xml, Unit, "Scale_Factor");

                Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Layer_Z_Adjust");
                Text_Box_Model_Height.Text = Load_Operator_Tag(Temporal_A, Button_Operator_Model_Height_Click, Toggle_Operator_Model_Height);


                Temporal_B = Load_Instance_Tag(Selected_Xml, Unit, "GUI_Hide_Health_Bar");
                // If Health + Shield Bar is deactivated we uncheck that checkbox 
                if (Temporal_B == "Yes" | Temporal_B == "YES" | Temporal_B == "yes" | Temporal_B == "yeS")
                { if (Check_Box_Health_Bar_Size.Checked) { Check_Box_Health_Bar_Size.Checked = false; } }

                else
                {   // Otherwise we make sure this tag is activated
                    if (Check_Box_Health_Bar_Size.Checked == false) { Check_Box_Health_Bar_Size.Checked = true; }

                    // Then we can also proceed to import further details
                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "GUI_Bracket_Size");
                    if (Temporal_A == "0") { Combo_Box_Health_Bar_Size.Text = "Small"; }
                    else if (Temporal_A == "2") { Combo_Box_Health_Bar_Size.Text = "Large"; }
                    else { Combo_Box_Health_Bar_Size.Text = "Medium"; }
                }

                Text_Box_Health_Bar_Height.Text = Load_Instance_Tag(Selected_Xml, Unit, "GUI_Bounds_Scale");
                Text_Box_Select_Box_Scale.Text = Load_Instance_Tag(Selected_Xml, Unit, "Select_Box_Scale");

                Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Select_Box_Z_Adjust");
                Text_Box_Select_Box_Height.Text = Load_Operator_Tag(Temporal_A, Button_Operator_Select_Box_Height_Click, Toggle_Select_Box_Height);


                //========================= Land Unit Settings ========================= 

                if (Unit_Mode != "Space") // These are disable for Space Units
                {
                    Text_Box_Movement_Animation.Text = Load_Instance_Tag(Selected_Xml, Unit, "Movement_Animation_Speed");
                    Text_Box_Walk_Animation.Text = Load_Instance_Tag(Selected_Xml, Unit, "Walk_Animation_Speed");
                    Text_Box_Crouch_Animation.Text = Load_Instance_Tag(Selected_Xml, Unit, "Crouch_Animation_Speed");
                    Text_Box_Rotation_Animation.Text = Load_Instance_Tag(Selected_Xml, Unit, "Rotation_Animation_Speed");
                    Text_Box_Deploy_Animation.Text = Load_Instance_Tag(Selected_Xml, Unit, "Deployment_Anim_Rate");

                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Land_Damage_Alternates");
                    if (Temporal_A.Contains("3")) { Temporal_C = 4; }
                    else if (Temporal_A.Contains("2")) { Temporal_C = 3; }
                    else if (Temporal_A.Contains("1")) { Temporal_C = 2; }
                    else if (Temporal_A.Contains("0")) { Temporal_C = 1; }
                    Text_Box_Damage_Stages.Text = Temporal_C.ToString();

                    Text_Box_Collison_Box_X.Text = Load_Instance_Tag(Selected_Xml, Unit, "Custom_Hard_XExtent");
                    Text_Box_Collison_Box_Y.Text = Load_Instance_Tag(Selected_Xml, Unit, "Custom_Hard_YExtent");



                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Remove_Upon_Death");
                    if (Temporal_A.Contains("rue")) // Because either T-rue or t-rue
                    { Toggle_Button(Switch_Remove_Upon_Death, "Button_On", "Button_Off", 0, true); Toggle_Remove_Upon_Death = true; }
                    else { Toggle_Button(Switch_Remove_Upon_Death, "Button_On", "Button_Off", 0, false); Toggle_Remove_Upon_Death = false; }

                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Influences_Capture_Point");
                    if (Temporal_A.Contains("rue")) 
                    { Toggle_Button(Switch_Capture_Point, "Button_On", "Button_Off", 0, true); Toggle_Capture_Point = true; }
                    else { Toggle_Button(Switch_Capture_Point, "Button_On", "Button_Off", 0, false); Toggle_Capture_Point = false; }

                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "MovementClass");
                    if (Temporal_A.Contains("over")) // Because H-over or h-over       
                    { Toggle_Button(Switch_Hover, "Button_On", "Button_Off", 0, true); Toggle_Hover = true; }
                    else { Toggle_Button(Switch_Hover, "Button_On", "Button_Off", 0, false); Toggle_Hover = false; }

                    // Uses Container + FormationSpacing to do


                    Text_Box_Wind_Disturbances.Text = Load_Instance_Tag(Selected_Xml, Unit, "Wind_Disturbance_Strength");
                    Text_Box_Wind_Radius.Text = Load_Instance_Tag(Selected_Xml, Unit, "Wind_Disturbance_Radius");

                    Text_Box_Hover_Height.Text = Load_Instance_Tag(Selected_Xml, Unit, "Hover_Offset");
                    Text_Box_Fire_Height.Text = Load_Instance_Tag(Selected_Xml, Unit, "Ranged_Target_Z_Adjust");
                    Text_Box_Sensor_Range.Text = Load_Instance_Tag(Selected_Xml, Unit, "Sensor_Range");
                }           

                //========================= Loading Power Values =========================                   
                Text_Box_Hull.Text = Load_Instance_Tag(Selected_Xml, Unit, "Tactical_Health");

                Text_Box_Shield.Text = Load_Instance_Tag(Selected_Xml, Unit, "Shield_Points");

                // Checking whether God Mode is active
                Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Shield_Refresh_Rate");
                if (Temporal_A == "100000")
                {
                    Toggle_Button(Switch_Button_God_Mode, "Button_On", "Button_Off", 0, true); Toggle_God_Mode = true;
                    Text_Box_Shield_Rate.Text = "God";
                }
                else
                {
                    Toggle_Button(Switch_Button_God_Mode, "Button_On", "Button_Off", 0, false); Toggle_God_Mode = false;
                    Text_Box_Shield_Rate.Text = Temporal_A;
                }


                Text_Box_Energy.Text = Load_Instance_Tag(Selected_Xml, Unit, "Energy_Capacity");
                Text_Box_Energy_Rate.Text = Load_Instance_Tag(Selected_Xml, Unit, "Energy_Refresh_Rate");

                Text_Box_Speed.Text = Load_Instance_Tag(Selected_Xml, Unit, "Max_Speed");
                Text_Box_Rate_Of_Turn.Text = Load_Instance_Tag(Selected_Xml, Unit, "Max_Rate_Of_Turn");
                Text_Box_Bank_Turn_Angle.Text = Load_Instance_Tag(Selected_Xml, Unit, "Bank_Turn_Angle");

                if (Text_Box_AI_Combat.Text == "") { Text_Box_AI_Combat.Text = Load_Instance_Tag(Selected_Xml, Unit, "AI_Combat_Power"); }

                if (Text_Box_Population.Text == "")
                {   // Processing population value, according to these two tags:
                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Population_Value");
                    Temporal_B = Load_Instance_Tag(Selected_Xml, Unit, "Additional_Population_Capacity");

                    if (Temporal_B != "") // If existing we parse it to int 
                    {
                        Int32.TryParse(Temporal_A, out Temporal_C);
                        Int32.TryParse(Temporal_B, out Temporal_D);

                        // If Additional Population Capacity is bigger then Population Capacity we take Additional Population Capacity instead  
                        if (Temporal_C < Temporal_D)
                        {
                            Temporal_A = (Temporal_D - Temporal_C).ToString();
                            // Making sure Operator is set to +
                            if (Toggle_Operator_Population == false) { Button_Operator_Population_Click(null, null); }
                        }
                    }
                    else
                    {   // Otherwise there is no "Additional_Population_Capacity", making sure Operator is set to - 
                        if (Toggle_Operator_Population) { Button_Operator_Population_Click(null, null); }
                    }
                    Text_Box_Population.Text = Temporal_A;
                }


                if (Text_Box_Projectile.Text == "")
                {
                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Projectile_Damage");

                    if (Temporal_A != null & Temporal_A.Contains(".")) // Then it must be decimal
                    {
                        decimal Decimal_Text;
                        decimal.TryParse(Temporal_A, out Decimal_Text);
                        int Integer_Text = (int)Decimal_Text;
                        Text_Box_Projectile.Text = Integer_Text.ToString();
                    }
                    else if (Temporal_A != null) { Text_Box_Projectile.Text = Temporal_A; }
                }

                Process_Balancing_Percentage();


                //========================== Loading Abilities =========================== 

                // Getting a list of all abilities of this unit:
                Temporal_E = Ability_Utility(Selected_Xml, Unit, null, null).Split(',');


                try
                {   // Need to set these variables instead of the usual Textboxes, they get then loaded by the Ability buttons at the top
                    if (Temporal_E[0] != null)
                    {
                        Ability_1_Type = Temporal_E[0];
                        Ability_1_Activated_GUI = Ability_Utility(Selected_Xml, Unit, Ability_1_Type, "GUI_Activated_Ability_Name");

                        Temporal_A = Ability_Utility(Selected_Xml, Unit, Ability_1_Type, "Supports_Autofire");
                        // If this toggle is disabled and the Xml value is True we (indirectly) set that to true as well
                        if (Regex.IsMatch(Temporal_A, "(?i).*?" + "True")) { Ability_1_Toggle_Auto_Fire = true; }
                        else if (Regex.IsMatch(Temporal_A, "(?i).*?" + "False")) { Ability_1_Toggle_Auto_Fire = false; }


                        Temporal_A = Ability_Utility(Selected_Xml, Unit, Ability_1_Type, "Expiration_Seconds");
                        if (Temporal_A.Contains(".0")) { Temporal_A = Temporal_A.Replace(".0", ""); }
                        Ability_1_Expiration_Time = Temporal_A;

                        Temporal_B = Ability_Utility(Selected_Xml, Unit, Ability_1_Type, "Recharge_Seconds");
                        if (Temporal_B.Contains(".0")) { Temporal_B = Temporal_B.Replace(".0", ""); }
                        Ability_1_Recharge_Time = Temporal_B;


                        Ability_1_Name = Ability_Utility(Selected_Xml, Unit, Ability_1_Type, "Alternate_Name_Text");
                        Ability_1_Description = Ability_Utility(Selected_Xml, Unit, Ability_1_Type, "Alternate_Description_Text");

                        Temporal_A = Ability_Utility(Selected_Xml, Unit, Ability_1_Type, "Alternate_Icon_Name");
                        if (Regex.IsMatch(Temporal_A, "(?i).*?" + ".tga")) { Temporal_A = Temporal_A.Remove(Temporal_A.Length - 4); }
                        Ability_1_Icon = Temporal_A;

                        Ability_1_Activated_SFX = Ability_Utility(Selected_Xml, Unit, Ability_1_Type, "SFXEvent_GUI_Unit_Ability_Activated");
                        Ability_1_Deactivated_SFX = Ability_Utility(Selected_Xml, Unit, Ability_1_Type, "SFXEvent_GUI_Unit_Ability_Deactivated");

                        Ability_1_Mod_Multipliers = Ability_Utility(Selected_Xml, Unit, Ability_1_Type, "Mod_Multiplier");

                        Text_Box_Additional_Abilities.Text = Ability_Utility(Selected_Xml, Unit, Ability_1_Type, "Abilities");

                        // Pusing that button to display results of first ability
                        Button_Primary_Ability_Click(null, null);

                    }
                }
                catch { }


                try
                {
                    if (Temporal_E[1] != null)
                    {
                        Ability_2_Type = Temporal_E[1];
                        Ability_2_Activated_GUI = Ability_Utility(Selected_Xml, Unit, Ability_2_Type, "GUI_Activated_Ability_Name");

                        Temporal_A = Ability_Utility(Selected_Xml, Unit, Ability_2_Type, "Supports_Autofire");
                        if (Regex.IsMatch(Temporal_A, "(?i).*?" + "True")) { Ability_2_Toggle_Auto_Fire = true; }
                        else if (Regex.IsMatch(Temporal_A, "(?i).*?" + "False")) { Ability_2_Toggle_Auto_Fire = false; }

                        Temporal_A = Ability_Utility(Selected_Xml, Unit, Ability_2_Type, "Expiration_Seconds");
                        if (Temporal_A.Contains(".0")) { Temporal_A = Temporal_A.Replace(".0", ""); }
                        Ability_2_Expiration_Time = Temporal_A;

                        Temporal_B = Ability_Utility(Selected_Xml, Unit, Ability_2_Type, "Recharge_Seconds");
                        if (Temporal_B.Contains(".0")) { Temporal_B = Temporal_B.Replace(".0", ""); }
                        Ability_2_Recharge_Time = Temporal_B;

                        Ability_2_Name = Ability_Utility(Selected_Xml, Unit, Ability_2_Type, "Alternate_Name_Text");
                        Ability_2_Description = Ability_Utility(Selected_Xml, Unit, Ability_2_Type, "Alternate_Description_Text");

                        Temporal_A = Ability_Utility(Selected_Xml, Unit, Ability_2_Type, "Alternate_Icon_Name");
                        if (Regex.IsMatch(Temporal_A, "(?i).*?" + ".tga")) { Temporal_A = Temporal_A.Remove(Temporal_A.Length - 4); }
                        Ability_2_Icon = Temporal_A;

                        Ability_2_Activated_SFX = Ability_Utility(Selected_Xml, Unit, Ability_2_Type, "SFXEvent_GUI_Unit_Ability_Activated");
                        Ability_2_Deactivated_SFX = Ability_Utility(Selected_Xml, Unit, Ability_2_Type, "SFXEvent_GUI_Unit_Ability_Deactivated");

                        Ability_2_Mod_Multipliers = Ability_Utility(Selected_Xml, Unit, Ability_2_Type, "Mod_Multiplier");
                    }
                }
                catch { }


                // Disabling User Input for Hyperspace Ability
                if (Text_Box_Mod_Multiplier.Text.Contains("<Spawned_Object_Type>Hyperspace_Jump_Target</Spawned_Object_Type>"))
                {
                    Track_Bar_Expiration_Seconds.Enabled = false;
                    Text_Box_Expiration_Seconds.Enabled = false;
                    Track_Bar_Recharge_Seconds.Enabled = false;
                    Text_Box_Recharge_Seconds.Enabled = false;
                }

                //======================= Loading Unit Properties ======================== 

                // Team is loaded by the Load_Team_To_UI() function 

                if (!Is_Team) // If is a Team these tags will be dealed by the Team/Squadron instance, hopefully
                {
                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Is_Named_Hero");
                    if (Temporal_A == "Yes") { Toggle_Button(Switch_Button_Is_Hero, "Button_On", "Button_Off", 0, true); Toggle_Is_Hero = true; }
                    else if (Temporal_A == "No") { Toggle_Button(Switch_Button_Is_Hero, "Button_On", "Button_Off", 0, false); Toggle_Is_Hero = false; }

                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Show_Hero_Head");
                    if (Temporal_A == "Yes") { Toggle_Button(Switch_Button_Show_Head, "Button_On", "Button_Off", 0, true); Toggle_Show_Head = true; }
                    else if (Temporal_A == "No") { Toggle_Button(Switch_Button_Show_Head, "Button_On", "Button_Off", 0, false); Toggle_Show_Head = false; }

                    if (Load_Instance_Tag(Selected_Xml, Unit, "Hyperspace") == "Yes")
                    {
                        Check_Box_Has_Hyperspace.Checked = true;
                        Text_Box_Hyperspace_Speed.Text = Load_Instance_Tag(Selected_Xml, Unit, "Hyperspace_Speed");
                    }
                    else { Check_Box_Has_Hyperspace.Checked = false; }
                }


                Text_Box_Lua_Script.Text = Load_Instance_Tag(Selected_Xml, Unit, "Lua_Script");
                if (Text_Box_Lua_Script.Text == "ObjectScript_Hyper_Space") { Toggle_Button(Switch_Button_Use_Particle, "Button_On", "Button_Off", 0, true); Toggle_Use_Particle = true; }
                else { Toggle_Button(Switch_Button_Use_Particle, "Button_On", "Button_Off", 0, false); Toggle_Use_Particle = false; }





                //======================= Fighters ======================== 

                List<string> Entry_List = Get_All_Tags_Of_Type(Selected_Xml, Text_Box_Name.Text, "Starting_Spawned_Units_Tech_0");
            
                foreach (string Entry in Entry_List)
                {   if (Entry != "")
                    {   Text_Box_Starting_Unit_Name.Text = Entry.Split('#')[0];
                        Text_Box_Spawned_Unit.Text = Entry.Split('#')[1];
                        break; // After first for now
                    }
                }

                Entry_List = Get_All_Tags_Of_Type(Selected_Xml, Text_Box_Name.Text, "Reserve_Spawned_Units_Tech_0");

                foreach (string Entry in Entry_List)
                {
                    if (Entry != "")
                    {   Text_Box_Reserve_Unit_Name.Text = Entry.Split('#')[0];
                        Text_Box_Reserve_Unit.Text = Entry.Split('#')[1];
                        break;
                    }
                }


         



                Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Death_Clone");
                if (Temporal_A != "")
                {
                    Text_Box_Death_Clone.Text = Temporal_A.Replace("Damage_Normal, ", "");
                    Text_Box_Death_Clone.Text = Text_Box_Death_Clone.Text.Replace("Damage_Force_Whirlwind, ", "");

                    Temporal_B = Load_Instance_Tag(Selected_Xml, Text_Box_Death_Clone.Text, "Space_Model_Name");
                    // If Death Clone can't be found in the File we try to find it in any other file using the Utility
                    if (Temporal_B == null | Temporal_B == "") { Temporal_B = Xml_Utility(null, Text_Box_Death_Clone.Text, "Space_Model_Name", null); }

                    try { Text_Box_Death_Clone_Model.Text = Regex.Replace(Temporal_B, "(?i).alo", ""); }
                    catch { } // Getting rid of suffix
                }


                // ====================== Loading Unit Hardpoints ======================
                Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "HardPoints");
                if (Temporal_A != "")
                {
                    Selected_Hard_Points.Clear(); // Preventing HPs of this Unit from merging with prior selection.
                    List_View_Hard_Points.Items.Clear();

                    Temporal_A = Wash_String(Temporal_A);
                    Temporal_E = Temporal_A.Split(','); // Serializing

                    foreach (string Entry in Temporal_E)
                    {   if (Entry != "") 
                        {   Selected_Hard_Points.Add(Entry);
                            List_View_Hard_Points.Items.Add(Entry);
                        } 
                    }

                    Save_Setting("2", "Selected_Hard_Points", List_To_String(Selected_Hard_Points));
                }

                Set_Checkmate_Color(List_View_Hard_Points);     

                Render_Segment_Button(Button_Hardpoints_Mod, "Left", false, 85, 36, 15, 8, 12, Color_04, "In Mod");
                Render_Segment_Button(Button_Hardpoints_Selection, "Right", true, 85, 36, 5, 8, 12, Color_03, "Selection");

                Hard_Point_Switch = "Selection";
                Button_Hardpoint_Selection.Text = "Deselect";



                /*
                string The_File = null;
                if (Hard_Point_Switch == "Chosen_Xml") { The_File = Selected_HP_Xml; } // Otherwise stay null
                Process_Damage_Per_Minute(The_File, null, 0, 0, true, false); // Using the int Parameter 3 as bool
                */

                // Temporal_A = Temporal_A.Substring(5, Temporal_A.Length - 5); // Removing first entry, which is total Damage              
                //Label_Damage_Per_Minute_Details.Text = Temporal_A;
                //Label_Damage_Per_Minute.Text = Label_Damage_Per_Minute_Details.Text.Split(',')[0]; // Total DPM nly

                         

                // ====================== Loading Unit Behavoir ======================
                // Clearing both Listboxes and loading 
                List_Box_Inactive_Behavoir.Items.Clear();
                List_Box_Active_Behavoir.Items.Clear();


                string[] Unit_Behavoir = Load_Behavoir_Pool();

                // Adding to List Box (need to remove the matching ones later
                foreach (var Entry in Unit_Behavoir)
                { List_Box_Inactive_Behavoir.Items.Add(Entry); }




                //============ Getting the Space Bahavoir Tag and putting its value into a List Box Element.
                string Active_Behavoir = Load_Instance_Tag(Selected_Xml, Unit, Behavoir_Tag);


                string[] bits = Active_Behavoir.Split(',');

                foreach (string bit in bits)
                {
                    // Using Regex to get all Values into a List Box
                    string Remove_Emptyspace = Wash_String(bit);

                    // To make sure we won't add any empty string
                    if (Remove_Emptyspace != "") List_Box_Active_Behavoir.Items.Add(Remove_Emptyspace);
                }



                //=========== Removing matching entries in the Pool of Inactive Behavoir 
                foreach (string Entry in Unit_Behavoir)
                {
                    // i = Amount of Entries -1 because the List starts at -1; until I reached 0; count -1 at each iteration
                    for (int i = List_Box_Active_Behavoir.Items.Count - 1; i >= 0; --i)
                    {
                        string Behavoir = List_Box_Active_Behavoir.Items[i].ToString();

                        // Removing the currently selected Tag from list box
                        if (Behavoir == Entry)
                        {   // Removing matched Items                                                                     
                            List_Box_Inactive_Behavoir.Items.Remove(Entry);
                        }
                    }
                }



                //====================== Loading Build Requirements ======================                 

                //========= Getting the Affiliation Tag and putting its value into a List Box Element.  

                if (!Is_Team)
                {
                    List_View_Inactive_Affiliations.Items.Clear();
                    List_View_Active_Affiliations.Items.Clear();


                    Temporal_A = Xml_Utility(Selected_Xml, Unit, "Affiliation", null);

                    if (Temporal_A != null & Temporal_A != "")
                    {
                        Temporal_E = Temporal_A.Split(',');


                        foreach (string Entry in Temporal_E)
                        {
                            // Using Regex to get all Values into a List Box
                            Temporal_B = Wash_String(Entry); 

                            // To make sure we won't add any empty string
                            if (Temporal_B != "") List_View_Active_Affiliations.Items.Add(Temporal_B);
                        }
                    }



                    //=========== Removing matching entries in the Pool of Unused Affiliations

                    string[] The_Factions = All_Factions.Split(',');

                    foreach (string Entry in The_Factions)
                    {
                        // First we add all these entries, and later if matched they get removed
                        if (Entry != "") { List_View_Inactive_Affiliations.Items.Add(Entry); }


                        for (int i = List_View_Active_Affiliations.Items.Count - 1; i >= 0; --i)
                        {   if (List_View_Active_Affiliations.Items[i].Text == Entry) // Removing matched Items 
                            { Remove_List_View_Selection(List_View_Inactive_Affiliations, List_View_Active_Affiliations.Items[i].Text, false); }
                        }             
                    }
                    Set_Checkmate_Color(List_View_Inactive_Affiliations);


                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Build_Tab_Space_Units");

                    if (Temporal_A == "Yes") { Toggle_Build_Tab = true; Toggle_Button(Switch_Button_Build_Tab, "Button_On", "Button_Off", 0, true); }
                    else if (Temporal_A == "No") { Toggle_Build_Tab = false; Toggle_Button(Switch_Button_Build_Tab, "Button_On", "Button_Off", 0, false); }
                }


                bool Show_Build_Window = false;

                // If all of these boxes are empty, otherwise it is probably already loaded from the Team/Squadron instance
                if (Text_Box_Build_Cost.Text == "") { Text_Box_Build_Cost.Text = Load_Instance_Tag(Selected_Xml, Unit, "Build_Cost_Credits"); Show_Build_Window = true; }

                if (Text_Box_Skirmish_Cost.Text == "") { Text_Box_Skirmish_Cost.Text = Load_Instance_Tag(Selected_Xml, Unit, "Tactical_Build_Cost_Multiplayer"); Show_Build_Window = true; }

                if (Text_Box_Build_Time.Text == "") { Text_Box_Build_Time.Text = Load_Instance_Tag(Selected_Xml, Unit, "Build_Time_Seconds"); Show_Build_Window = true; }
                if (Text_Box_Tech_Level.Text == "") { Text_Box_Tech_Level.Text = Load_Instance_Tag(Selected_Xml, Unit, "Tech_Level"); Show_Build_Window = true; }

                if (Text_Box_Star_Base_LV.Text == "") { Text_Box_Star_Base_LV.Text = Load_Instance_Tag(Selected_Xml, Unit, "Required_Star_Base_Level"); Show_Build_Window = true; }
                if (Text_Box_Ground_Base_LV.Text == "") { Text_Box_Ground_Base_LV.Text = Load_Instance_Tag(Selected_Xml, Unit, "Required_Ground_Base_Level"); Show_Build_Window = true; }

                if (Text_Box_Required_Timeline.Text == "") { Text_Box_Required_Timeline.Text = Load_Instance_Tag(Selected_Xml, Unit, "Required_Timeline"); Show_Build_Window = true; }


                // Loading must have completed, or it would interfere with the correct UI startup!            
                if (Loading_Completed & Show_Build_Window & Size_Expander_G == "false") { Button_Expand_G_Click(null, null); }


                Show_Build_Window = false;

                if (!Is_Team)
                {
                    Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Build_Initially_Locked");
                    if (Temporal_A == "Yes") { Toggle_Innitially_Locked = true; Toggle_Button(Switch_Button_Innitially_Locked, "Button_On", "Button_Off", 0, true); }
                    else if (Temporal_A == "No") { Toggle_Innitially_Locked = false; Toggle_Button(Switch_Button_Innitially_Locked, "Button_On", "Button_Off", 0, false); }
                }

                if (Text_Box_Slice_Cost.Text == "") // If beginns empty no Team is probably loaded
                {
                    Text_Box_Slice_Cost.Text = Load_Instance_Tag(Selected_Xml, Unit, "Slice_Cost_Credits");
                    if (Text_Box_Slice_Cost.Text != "") // If textbox is meanwhile filled that means we can check it.
                    {   Check_Box_Slice_Cost.Checked = true;
                        Show_Build_Window = true; 
                    } 
                }

                if (Text_Box_Current_Limit.Text == "")
                {
                    Text_Box_Current_Limit.Text = Load_Instance_Tag(Selected_Xml, Unit, "Build_Limit_Current_Per_Player");
                    if (Text_Box_Current_Limit.Text != "") 
                    {   Check_Box_Current_Limit.Checked = true;
                        Show_Build_Window = true; 
                    }
                }

                if (Text_Box_Lifetime_Limit.Text == "")
                {
                    Text_Box_Lifetime_Limit.Text = Load_Instance_Tag(Selected_Xml, Unit, "Build_Limit_Lifetime_Per_Player");
                    if (Text_Box_Lifetime_Limit.Text != "") 
                    {   Check_Box_Lifetime_Limit.Checked = true;
                        Show_Build_Window = true; 
                    }
                }


                if (Building_List.Count() == 0)
                {   Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Required_Special_Structures");

                    if (Temporal_A != null & Temporal_A != "")
                    {
                        Temporal_A = Regex.Replace(Temporal_A, " ", "");
                        
                        foreach (string Item in Temporal_A.Split(','))
                        { if (!Building_List.Contains(Item)) { Building_List.Add(Item); } }
                        Button_Required_Structures_Click(null, null);
                        Show_Build_Window = true; 
                    }
                }

                if (Planet_List.Count() == 0) 
                {   Temporal_A = Load_Instance_Tag(Selected_Xml, Unit, "Required_Planets");
                    
                    if (Temporal_A != null & Temporal_A !="") 
                    {
                        Temporal_A = Regex.Replace(Temporal_A, " ", "");
                        
                        foreach (string Item in Temporal_A.Split(',')) 
                        { if (!Planet_List.Contains(Item)) { Planet_List.Add(Item); } }
                        Button_Required_Planets_Click(null, null);
                        Show_Build_Window = true;                        
                    }
                }


                if (Loading_Completed & Show_Build_Window & Size_Expander_H == "false") { Button_Expand_H_Click(null, null); } 

                //======================================================================= 


                User_Input = true;
                // Need this so the Button_Primary_Ability_Click() and Button_Secondary_Ability_Click() know when they are to leave User Input to false
                Loading_To_UI = false;

                if (Application.UseWaitCursor == true)
                {
                    Application.UseWaitCursor = false;
                    Application.DoEvents();
                }

                // Showing the Name of the Current Xml over the Editor Settings
                try { Label_Xml_Name.Text = Path.GetFileName(Selected_Xml); }
                catch { }

                // Moving the scrollbar down to focus on the editor UI
                Label_Text_Id.Focus();



                //================== Preparing "Out of Range" Error Messages for the User ==================
                string Error_Text = "";

                if (Error_List.Count > 0 & Show_Tag_Issues == "true")
                {
                    foreach (string Error in Error_List)
                    {
                        if (Error == "Text_Box_Name") { Error_Text += "   <Unit_Name>" + "\n"; }
                        else if (Error == "Combo_Box_Type") { Error_Text += "   " + Combo_Box_Type.Text + "  (Unit Type) \n"; }
                        else if (Error == "Text_Box_Is_Variant") { Error_Text += "   <Variant_Of_Existing_Type>" + "\n"; }

                        // Art Settings
                        else if (Error == "Text_Box_Model_Name") { Error_Text += "   <Space_Model_Name>" + "\n"; }
                        else if (Error == "Text_Box_Icon_Name") { Error_Text += "   <Icon_Name>" + "\n"; }
                        else if (Error == "Text_Box_Radar_Icon") { Error_Text += "   <Radar_Icon_Name>" + "\n"; }
                        else if (Error == "Text_Box_Radar_Size") { Error_Text += "   <Radar_Icon_Size>" + "\n"; }
                        else if (Error == "Text_Box_Text_Id") { Error_Text += "   <Text_ID>" + "\n"; }
                        else if (Error == "Text_Box_Unit_Class") { Error_Text += "   <Encyclopedia_Unit_Class>" + "\n"; }
                        else if (Error == "Text_Box_Encyclopedia_Text") { Error_Text += "   <Encyclopedia_Text>" + "\n"; }

                        // Power Values
                        else if (Error == "Combo_Box_Class") { Error_Text += "   <Ship_Class>, " + "<Space_Layer>, " + "<CategoryMask>" + "  (Class) \n"; }
                        else if (Error == "Text_Box_Hull") { Error_Text += "   <Tactical_Health>" + "  (Hull) \n"; }
                        else if (Error == "Text_Box_Shield") { Error_Text += "   <Shield_Points>" + "  (Shield) \n"; }
                        else if (Error == "Text_Box_Shield_Rate") { Error_Text += "   <Shield_Refresh_Rate>" + "\n"; }
                        else if (Error == "Text_Box_Energy") { Error_Text += "   <Energy_Capacity>" + "  (Energy) \n"; }
                        else if (Error == "Text_Box_Energy_Rate") { Error_Text += "   <Energy_Refresh_Rate>" + "\n"; }
                        else if (Error == "Text_Box_AI_Combat") { Error_Text += "   <AI_Combat_Power>" + "  (AI Combat) \n"; }
                        else if (Error == "Text_Box_Projectile") { Error_Text += "   <Projectile_Damage>" + "\n "; }


                        // Costum Tags                Text_Box_Tag_Name.Text because this is how the Costum Tags are named
                        else if (Error == "Text_Box_Costum_Tag_1") { Error_Text += "   " + Text_Box_Tag_1_Name.Text + "\n"; }
                        else if (Error == "Text_Box_Costum_Tag_2") { Error_Text += "   " + Text_Box_Tag_1_Name.Text + "\n"; }
                        else if (Error == "Text_Box_Costum_Tag_3") { Error_Text += "   " + Text_Box_Tag_1_Name.Text + "\n"; }
                        else if (Error == "Text_Box_Costum_Tag_4") { Error_Text += "   " + Text_Box_Tag_1_Name.Text + "\n"; }
                        else if (Error == "Text_Box_Costum_Tag_5") { Error_Text += "   " + Text_Box_Tag_1_Name.Text + "\n"; }
                        else if (Error == "Text_Box_Costum_Tag_6") { Error_Text += "   " + Text_Box_Tag_1_Name.Text + "\n"; }

                        else { Error_Text += Error + ", "; }
                    }
                }

                //================== Preparing "Tag not Found" Error Messages for the User ==================

                string Misload_Text = "";

                if (Misloaded_Tags.Count > 0)
                {
                    foreach (string Failed_Tag in Misloaded_Tags)
                    {
                        Misload_Text += "   <" + Failed_Tag + "> \n";
                    }
                }


                //================== Displaying Error Message
                if (Error_List.Count > 0 & Show_Tag_Issues == "true" & Loading_Completed | Misloaded_Tags.Count > 0 & Show_Tag_Issues == "true" & Loading_Completed)
                {
                    // Innitiating new Form
                    Caution_Window Display = new Caution_Window();
                    Display.Opacity = Transparency;
                    Display.Size = new Size(700, 400);

                    // Using Theme colors for Text and Background
                    Display.Text_Box_Caution_Window.BackColor = Color_05;
                    Display.Text_Box_Caution_Window.ForeColor = Color_03;

                    // If Error_List contains anything we show the first Message
                    if (Error_List.Count > 0)
                    { Display.Text_Box_Caution_Window.Text = "   (You can disable this message using \"Show Tag Issues\" in Settings.)\n   The Following Values are out of range, you can still lower them or increase \n   their Max settings and reload:" + "\n \n" + Error_Text; }

                    // Then we add the second Message to the Message Box
                    if (Misloaded_Tags.Count > 0) { Display.Text_Box_Caution_Window.Text += "\n\n   Failed to load the Following Values (probably not found or is Variant): \n\n" + Misload_Text; }


                    Display.Show();
                }


                // Clearing Lists
                Error_List.Clear();
                Misloaded_Tags.Clear();
            }   
        }


        //=====================//
        // Search_These_Xmls is a list of possible files, where it looks for them. If null, it searches the full Xml directory
        string Search_Xml_Of(string Unit_Name, List<string> Search_These_Xmls = null)
        {
            string Found_Xml = "";
            XDocument The_Xml = null;
            IEnumerable<XElement> Found_Entity = null;

            if (Search_These_Xmls == null) { Search_These_Xmls = Get_Xmls().ToList(); } // Get_Xmls().CopyTo(Search_These_Xmls); }


            foreach (var Xml in Search_These_Xmls)
            {   try
                {
                    string The_Path = Xml;
                    if (!Xml.StartsWith(Xml_Directory)) { The_Path = Xml_Directory + Xml; }

                    The_Xml = XDocument.Load(The_Path, LoadOptions.PreserveWhitespace);

                    Found_Entity =
                     from All_Tags in The_Xml.Root.Descendants()
                        where (string)All_Tags.Attribute("Name") == Unit_Name
                            select All_Tags;

                    if (Found_Entity.Any()) { Found_Xml = The_Path; break; } // Imperial_Console(500, 200, Xml_Selection); break; }
                } catch { }
            }

            return Found_Xml; 
        }


        //=====================//

        IEnumerable<XElement> Search_Entity_Of(string Xml_Name, string Unit_Name)
        {
            XDocument The_Xml = null;
            IEnumerable<XElement> Found_Entity = null;


            if (Xml_Name != "" & Xml_Name != null) try
            {   The_Xml = XDocument.Load(Xml_Name, LoadOptions.PreserveWhitespace);

                Found_Entity =
                 from All_Tags in The_Xml.Root.Descendants()
                 where (string)All_Tags.Attribute("Name") == Unit_Name
                 select All_Tags;

                if (Found_Entity.Any()) { return Found_Entity; } // Imperial_Console(500, 200, Xml_Selection); break; }
            }  catch {}


            foreach (var Xml in Get_Xmls())
            {
                try
                {
                    The_Xml = XDocument.Load(Xml, LoadOptions.PreserveWhitespace);

                    Found_Entity =
                     from All_Tags in The_Xml.Root.Descendants()
                     where (string)All_Tags.Attribute("Name") == Unit_Name
                     select All_Tags;

                    if (Found_Entity.Any()) { return Found_Entity; } // Imperial_Console(500, 200, Xml_Selection); break; }
                } catch {}
            }

            return Found_Entity;
        }

        //=====================//

        List<string> Get_All_Tags_Of_Type(string Selected_Xml, string Unit, string Tag_Name)
        {
            List<string> Entry_List = new List<string>();
            List<string> Release_List = new List<string>();

            // TODO Use this to load faster from it
            XDocument Current_Xml_File = XDocument.Load(Selected_Xml, LoadOptions.PreserveWhitespace);

            IEnumerable<XElement> Selected_Unit =
               from All_Tags in Current_Xml_File.Root.Descendants()
               where (string)All_Tags.Attribute("Name") == Unit
               select All_Tags;


            try // Merging ALL found tags into a single String and removing formatting!
            {   foreach (XElement Tag in Selected_Unit.Descendants(Tag_Name))
                {   int Amount_1 = 0;
                    int Amount_2 = 0;
                    string Tag_Value = Wash_String(Tag.Value);
                    string Entry_Name = Tag_Value.Split(',')[0];

                    // Feeding empty list
                    if (Entry_List.Count < 1) { Entry_List.Add(Tag_Value.Replace(",", "#")); } // Temporal_A += Entry + "& "; }
                    else 
                    {   for (int i = 0; i < Entry_List.Count(); i++)
                        {
                            string Item_Name = Entry_List[i].Split(',')[0];

                            if (Entry_Name == Item_Name) // If matching the same name we increment the amount
                            {
                                // Imperial_Console(500, 100, Entry_Name + " matched " + Item_Name);
                                Int32.TryParse(Tag_Value.Split(',')[1], out Amount_1);
                                Int32.TryParse(Entry_List[i].Split(',')[1], out Amount_2);

                                // We count them together, preventing duplicity from calling the expensive xml searcher function more often then necessary.
                                if (Amount_1 > 0 & Amount_2 > 0)
                                {   Entry_List[i] = Item_Name + "#" + (Amount_1 += Amount_2);
                                    // Temporal_A += Item_Name + "," + (Amount_1 += Amount_2) + "& ";
                                }
                            }
                        }

                        if (Amount_1 == 0 & Amount_2 == 0) // No match, we insert it
                        {   Entry_List.Add(Tag_Value.Replace(",", "#"));
                            // Temporal_A += Entry + "& ";
                        }
                    }
                }
            }  catch {}
            // Imperial_Console(500, 400, Temporal_A);


            return Entry_List;
        }

        //=====================//

        // This provides balancing information for the user in the UI
        void Process_Balancing_Percentage()
        {
            try
            {
                int Hull_Value = 100;
                if (Progress_Bar_Hull.Value != Progress_Bar_Hull.Maximum & Progress_Bar_Hull.Value != 0) { Hull_Value = (Progress_Bar_Hull.Value / (Maximum_Hull / 100)); }

                int Shield_Value = 100;
                if (Progress_Bar_Shield.Value != Progress_Bar_Shield.Maximum & Progress_Bar_Shield.Value != 0) { Shield_Value = (Progress_Bar_Shield.Value / (Maximum_Shield / 100)); }

                int Speed_Value = 100; // * 10 and * 100 to get away from decimal values after division, because result needs to be int, then / 100 to get the percentage
                if (Progress_Bar_Speed.Value != Progress_Bar_Speed.Maximum & Progress_Bar_Speed.Value != 0) { Speed_Value = ((Progress_Bar_Speed.Value * 10) / ((Maximum_Speed * 100) / 100)); }

                int AI_Combat_Value = 100;
                if (Progress_Bar_AI_Combat.Value != Progress_Bar_AI_Combat.Maximum & Progress_Bar_AI_Combat.Value != 0) { AI_Combat_Value = (Progress_Bar_AI_Combat.Value / (Maximum_AI_Combat / 100)); }

                int Population_Value = 100; // * 100 to get rid of decimal value and / 20 to get the percentage
                if (Progress_Bar_Population.Value != Progress_Bar_Population.Maximum & Progress_Bar_Population.Value != 0) { Population_Value = ((Progress_Bar_Population.Value * 100) / 20); }
                // If in minus mode we count that amount of balancing strengh away, otherwise its normal addition
                if (Toggle_Operator_Population == false) { Population_Value = -Population_Value; }

                int Projectile_Value = 200; // Counts * 2 because Projectile has a huge impact on balancing!
                if (Progress_Bar_Projectile.Value != Progress_Bar_Projectile.Maximum & Progress_Bar_Projectile.Value != 0) { Projectile_Value = ((Progress_Bar_Projectile.Value * 100) / Maximum_Projectile) * 2; }


                // /7 because they are 6 percentage fields (600%) that needs to be scaled down to 100% maximum, and Projectile_Value counts 200% (thus 700) because it is very important
                Label_Current_Balancing.Text = ((Hull_Value + Shield_Value + Speed_Value + AI_Combat_Value + Population_Value + Projectile_Value) / 7).ToString() + "%";
            }
            catch { }
        }


        // ================== Processing Damage per Minute! ==================
        string Process_Damage_Per_Minute(string Hard_Point_File, string[] List_Of_Hardpoints, int Unit_Count, int Squadron_Count, bool New_Score, bool Is_Reserve)
        {

            double Projectile_Damage = 0;
            double Damage_Points_Per_Minute = 0;
            // int Fighter_Damage_Points_Per_Minute = Fighter_Damage; // Incrementing over the Value we got from above


            if (New_Score) // Cause Fighter Stats overwrite these
            {   Damage_Score = ""; 
                Damage_Score_Window_Height = 60; // Resetting
                Total_Damage_Points_Per_Minute = 0;
            }
            //else if (!New_Score & Squads > 0) // Appending this to the last line          
            //{   if (Damage_Score_Window_Height < 800) { Damage_Score_Window_Height += 144; }
            //    Total_Damage_Points_Per_Minute += Fighter_Damage;
            //    Damage_Score += "    //======== " + (Unit_Count * Squadron_Count) + " Fighters inside of " + Squads + " Squadrons " + @"========\\" + Add_Line + Add_Line +
            //    "    " + Fighter_Damage_Points_Per_Minute + " = Damage by HP Mounted to active Fighters" + Add_Line + Add_Line;
            
            //}
       


            string Result = "";
            if (Temporal_List != null) { Temporal_List.Clear(); }
            Xml_Exception_Files.Clear();


        
            if (List_Of_Hardpoints == null) // If nothing specified we grab from Selected_Hard_Points
            {   // foreach (string Entry in Selected_Hard_Points) { Temporal_List.Add(Entry); }
                List_Of_Hardpoints = Selected_Hard_Points.ToArray();
            }


            //string Feedback = "";
            //foreach (string Entry in List_Of_Hardpoints) { Feedback += Entry + Add_Line; }
            //Imperial_Console(500, 400, Feedback);


            foreach (string Hard_Point in List_Of_Hardpoints) // From the loading Unit
            {   // Skipping HP types we already got their Prefix processed in the last cycles!

                // string Chopped_HP = Hard_Point;
                //if (Regex.IsMatch(Hard_Point, "([^_]+)$")) { Chopped_HP = Hard_Point.Remove(Hard_Point.Length - 2); }   
                string Chopped_HP = Regex.Replace(Hard_Point, "([^_]+)$", ""); // Removing Suffix after _ sign                    
                if (List_Matches(Temporal_List, Chopped_HP)) { continue; }

               

                int Hardpoint_Count = 0;
                foreach (string HP in List_Of_Hardpoints) // From the loading Unit
                {   string Prefix = Regex.Replace(HP, "([^_]+)$", ""); // Chopping Last digits away


                    if (Prefix == Chopped_HP) // Matched Prefix
                    {   Hardpoint_Count++;

                        // Marking all HP with this Prefix as done
                        if (!List_Matches(Temporal_List, Prefix)) { Temporal_List.Add(Prefix); }
                    }
                }




                double Salvos = 0;
                double Fire_Delay = 0;
                double Recharge_Time = 1;
                bool   Info_Is_Missing = false;

                string Projectile_Type = "";
                string Projectile_File = null;
                XDocument The_Xml = null;
                XDocument Projectile_Xml = null;
                IEnumerable<XElement> Found_Entity = null;


                try {

                    if (Hard_Point_File != null) try // Would crash if null
                    {   The_Xml = XDocument.Load(Hard_Point_File, LoadOptions.PreserveWhitespace);

                        Found_Entity =
                          from All_Tags in The_Xml.Root.Descendants()
                          where (string)All_Tags.Attribute("Name") == Hard_Point
                          select All_Tags;
                    } catch {}




                    // Will fail to match first time, when Proj files are distributed between different files and this keeps searching the old matched file!
                    if (Hard_Point_File == null | Found_Entity == null)
                    {   foreach (string The_File in Hard_Point_Data_Files)
                        {   try
                            {   The_Xml = XDocument.Load(Xml_Directory + The_File, LoadOptions.PreserveWhitespace);
                              Found_Entity =
                                from All_Tags in The_Xml.Root.Descendants()
                                where (string)All_Tags.Attribute("Name") == Hard_Point
                                select All_Tags;

                                // Then we browse for a new Xml file, if it matches we memorize it in Hard_Point_File
                                if (Found_Entity.Any()) { Hard_Point_File = Xml_Directory + The_File; break; }
                                // break because we're looking for the first one
                            } catch {}
                        }
                    }


                    if (!Found_Entity.Any()) // Maybe because we're looking for it in the wrong XML, lets search for another one:
                    {   foreach (string The_File in Hard_Point_Data_Files)
                        {   try
                            {   The_Xml = XDocument.Load(Xml_Directory + The_File, LoadOptions.PreserveWhitespace);
                                Found_Entity =
                                  from All_Tags in The_Xml.Root.Descendants()
                                  where (string)All_Tags.Attribute("Name") == Hard_Point
                                  select All_Tags;

                                // Then we browse for a new Xml file, if it matches we memorize it in Hard_Point_File
                                if (Found_Entity.Any()) { Hard_Point_File = Xml_Directory + The_File; break; }
                                // break because we're looking for the first one
                            } catch {}
                        }                       
                    } 
                            
                    if (!Found_Entity.Any()) { Xml_Exception_Files.Add(Hard_Point); continue; } // Security Passage
                    // Imperial_Console(500, 200, "Found " + Hard_Point + " in " + Hard_Point_File);


                    try
                    {   Projectile_Type = Found_Entity.Descendants("Fire_Projectile_Type").First().Value;
                        if (Projectile_Type == "" | Found_Entity == null) { Xml_Exception_Files.Add(Hard_Point + "   Fires no Projectile."); continue; } // Because has no Projectile


                        try { Double.TryParse(Found_Entity.Descendants("Fire_Pulse_Count").First().Value, out Salvos);  } catch { Info_Is_Missing = true; }
                        try { Double.TryParse(Found_Entity.Descendants("Fire_Pulse_Delay_Seconds").First().Value, out Fire_Delay);  } catch { Info_Is_Missing = true; }
                        try { Double.TryParse(Found_Entity.Descendants("Fire_Min_Recharge_Seconds").First().Value, out Recharge_Time);  } catch { Info_Is_Missing = true; }
                       
                        try 
                        { if (Recharge_Time == 1) { Double.TryParse(Found_Entity.Descendants("Fire_Max_Recharge_Seconds").First().Value, out Recharge_Time); } 
                        } catch { Info_Is_Missing = true; }
                    } catch {}


                    // Imperial_Console(400, 100, Hard_Point + " = " + Projectile_Type);

                    // =========== Getting Projectile Damage ============                  
                    if (Projectile_File != null) try // Would crash if null
                    {   Projectile_Xml = XDocument.Load(Projectile_File, LoadOptions.PreserveWhitespace);

                      Found_Entity =
                        from All_Tags in Projectile_Xml.Root.Descendants()
                        where (string)All_Tags.Attribute("Name") == Projectile_Type
                        select All_Tags;
                    } catch {}


                    if (Projectile_File == null | Found_Entity == null)
                    {   foreach (string The_File in Projectile_Data_Files)
                        {   try
                            {   Projectile_Xml = XDocument.Load(Xml_Directory + The_File, LoadOptions.PreserveWhitespace);

                              Found_Entity =
                                from All_Tags in Projectile_Xml.Root.Descendants()
                                where (string)All_Tags.Attribute("Name") == Projectile_Type
                                select All_Tags;

                                if (Found_Entity.Any()) { Projectile_File = Xml_Directory + The_File; break; }
                            } catch {}
                        }
                    }


                    string[] Projectile_Stats = null;

                    // =========== Dealing with Projectile variants ============  
                    if (Found_Entity.Descendants("Variant_Of_Existing_Type").Any())
                    {
                        Projectile_Stats = Get_Entity_Stats(Projectile_Type, "Projectile", null, Projectile_Data_Files.ToList()); // todo doesnt seem to work

                        if (Projectile_Stats[0] != null) try // Would crash if null
                        {
                                Projectile_Xml = XDocument.Load(Projectile_Stats[0], LoadOptions.PreserveWhitespace);

                                Found_Entity =
                                  from All_Tags in Projectile_Xml.Root.Descendants()
                                  where (string)All_Tags.Attribute("Name") == Projectile_Stats[1]
                                  select All_Tags;

                                // Projectile_Type = Projectile_Stats[1];
                                // Imperial_Console(400, 100, "Found Variant Ancestor \"" + Projectile_Stats[1] + "\" of \"" + Projectile_Type + "\" in " + Path.GetFileName(Projectile_Stats[0]));
                        } catch {}




                        // Stats for Projectiles:
                        // Projectile_Stats[3] = Projectile_Damage
                        // Projectile_Stats[4] = Fire_Pulse_Count
                        // Projectile_Stats[5] = Fire_Pulse_Delay_Seconds
                        // Projectile_Stats[6] = Fire_Min_Recharge_Seconds

                        try { if (Projectile_Stats[3] != null) { Double.TryParse(Projectile_Stats[3], out Projectile_Damage); } } catch { }                      
                        try { if (Projectile_Stats[4] != null) { Double.TryParse(Projectile_Stats[4], out Salvos); } } catch {}
                        try { if (Projectile_Stats[5] != null) { Double.TryParse(Projectile_Stats[5], out Fire_Delay); } } catch {}
                        try { if (Projectile_Stats[6] != null) { Double.TryParse(Projectile_Stats[6], out Recharge_Time); } } catch {}
                        // Imperial_Console(400, 100, Projectile_Damage.ToString());                 
                    }

                    if (Projectile_Damage == 0) // If still 0
                    {   // If no Proj Damage fond we're continuing, without mentioning this HP in the statistics
                        try {  Double.TryParse(Found_Entity.Descendants("Projectile_Damage").First().Value, out Projectile_Damage); } catch { }
                    }
                                   

                    if (Info_Is_Missing & Projectile_Damage > 0) // Then we got a Projectile but not all Information tags.
                    {
                        if (Salvos == 0 | Fire_Delay == 0) // | Recharge_Time == 0) // If one of them is missing
                        {
                            Temporal_A = Add_Line + "      " + Chopped_HP + "    is missing ";
                            if (Damage_Score_Window_Height < 800) { Damage_Score_Window_Height += 36; }

                            // These are all missing together since the Projectile that contains them was not found.
                            // if (Projectile_Damage == -1) { Temporal_A += "<Projectile_Damage>, "; }
                            if (Salvos == 0) { Temporal_A += "<Fire_Pulse_Count>, "; }
                            if (Fire_Delay == 0) { Temporal_A += "<Fire_Pulse_Delay_Seconds>, "; }
                            if (Recharge_Time == 1) { Temporal_A += "<Fire_Min_Recharge_Seconds>"; }
                            Damage_Score += Temporal_A + Add_Line; // We append this info to our Balancing Journal
                        }
                    }
                    // else { Imperial_Console(400, 100, "Damage of \"" + Projectile_Type + "\" is " + Projectile_Damage); } // Debug


                } 
                catch
                {   Hard_Point_File = null; // Resetting Because xml is Empty    
                    Xml_Exception_Files.Add(Hard_Point + "   Xml File not found.");
                    continue;
                }


                if (Projectile_Damage == 0) { continue; }


                // ================== Finally we got all wee need to process DPM ==================
                // Concidering amount of Fighters in the Squad
                if (Unit_Count * Squadron_Count > 0) { Hardpoint_Count = Hardpoint_Count * (Unit_Count * Squadron_Count); }

                double Damage = Projectile_Damage * Salvos * Hardpoint_Count;
                double Salvos_Per_Minute = 60 / ((Fire_Delay * Salvos) + Recharge_Time); // 60 Seconds a Minute
                Damage_Points_Per_Minute = Damage * Salvos_Per_Minute;
                Result +=  (int)Damage_Points_Per_Minute + ", ";
             
                // This counts the Damage Points of all selected HP together, ignoring it for Reserver Fighters though!
                if (!Is_Reserve) { Total_Damage_Points_Per_Minute += Damage_Points_Per_Minute; }




                //Imperial_Console(600, 400, "    Projectile_Damage = " + Projectile_Damage + Add_Line
                //                             + "    Salvos = " + Salvos + Add_Line + "    Fire_Delay = " + Fire_Delay + Add_Line
                //                             + "    Recharge_Time = " + Recharge_Time);
                

                if (Damage_Score_Window_Height < 800) { Damage_Score_Window_Height += 36; } // For each listed HP Slot

                Temporal_B = ""; // Formatting one more slot if less then 4 digits
                if (((int)Damage_Points_Per_Minute).ToString().Length < 4) { Temporal_B = "  "; }
                else if (((int)Damage_Points_Per_Minute).ToString().Length < 3) { Temporal_B = "   "; }
                else if (((int)Damage_Points_Per_Minute).ToString().Length < 2) { Temporal_B = "    "; }

                Damage_Score += "    " + Temporal_B + (int)Damage_Points_Per_Minute + " = " + Hardpoint_Count + " *   "
                                       + Chopped_HP + Add_Line;       
            
            }

            // Pre-pending Stats
            if (New_Score) { Damage_Score = @" Damage Points Per Minute =====\\" + Add_Line + Add_Line + Damage_Score + Add_Line; }
                  


            if (Show_Load_Issues == "true" & User_Input)
            {   Size Window_Size_01 = new Size(600, 400);
                // Using the Costum Massage Box
                Show_Message_Box_Xml_Fail(Window_Size_01);
            }
            Xml_Exception_Files.Clear();
            
            //// Check_Fighters prevents a recursive loop, as it calls back this function here.
            //if (New_Score & Text_Box_Name.Text != "")
            //{
                
            //    List<string> Entry_List = Get_All_Tags_Of_Type(Selected_Xml, Text_Box_Name.Text, "Starting_Spawned_Units_Tech_0");
            
            //    foreach (string Entry in Entry_List)
            //    {   if (Entry != "")
            //        {   //Text_Box_Starting_Unit_Name.Text = Entry.Split('#')[0];
            //            //Text_Box_Spawned_Unit.Text = Entry.Split('#')[1];
            //            Process_Squadron_Damage_Per_Minute(Entry.Split('#')[0], Entry.Split('#')[1], false);
            //        }
            //    }

            //    Entry_List = Get_All_Tags_Of_Type(Selected_Xml, Text_Box_Name.Text, "Reserve_Spawned_Units_Tech_0");

            //    foreach (string Entry in Entry_List)
            //    {
            //        if (Entry != "")
            //        { Process_Squadron_Damage_Per_Minute(Entry.Split('#')[0], Entry.Split('#')[1], true);  }
            //    }
             
                
            //    // Process_Squadron_Damage_Per_Minute(Text_Box_Reserve_Unit_Name.Text, Text_Box_Reserve_Unit.Text);
            //}


            return ((int)Total_Damage_Points_Per_Minute).ToString();
            // return ((int)Total_Damage_Points_Per_Minute).ToString() + ", " + Result;
        }



        //=====================// 
        private void Process_Squadron_Units(string The_Xml, string Unit_Name)
        {
            List<string> Entry_List = Get_All_Tags_Of_Type(The_Xml, Unit_Name, "Starting_Spawned_Units_Tech_0");
            foreach (string Entry in Entry_List)
            {   if (Entry != "") { Process_Squadron_Damage_Per_Minute(Entry.Split('#')[0], Entry.Split('#')[1], false); }
                // Imperial_Console(500, 200, Entry.Split('#')[0] + " and " + Entry.Split('#')[1]);
            }
            Imperial_Console(500, 200, List_To_String(Entry_List));

            Entry_List = Get_All_Tags_Of_Type(The_Xml, Unit_Name, "Reserve_Spawned_Units_Tech_0");
            foreach (string Entry in Entry_List)
            { if (Entry != "") { Process_Squadron_Damage_Per_Minute(Entry.Split('#')[0], Entry.Split('#')[1], true); } }
            Imperial_Console(500, 200, List_To_String(Entry_List));
        }


        //=====================//  

        // Use Entity_XML as   Fighter_Balancing_Xml
        // Set Entity_XML to "Unit_Data_Files" if the object is Space_Unit or Fitghter, that boost the speed to find the right file.
        public string[] Get_Entity_Stats(string Entity_Name, string Object_Mode = "Entity", string Entity_XML = null, List<string> Xml_Source = null)
        {           
            XDocument The_Xml = null;
            IEnumerable<XElement> Found_Entity = null;
          
            string Xml_Selection = "";
          
            if (Entity_XML == "Unit_Data_Files")
            {               
                foreach (string Line in File.ReadLines(Program_Directory + "Unit_Data_Files__" + Environment.UserName + ".txt"))
                {   try
                    {   string[] Part = Line.Split('=');

                        if (Part[1].Contains("=" + Entity_Name + ",") | Part[1].Contains(Entity_Name + ",")) // "," because it is a break at the end of the name and prevents "Contains()" to trigger for units with similar names.
                        {
                           Found_Entity = Search_Entity_Of(Part[0], Entity_Name);                    
                           // if (Found_Entity.Any()) { Xml_Selection = Part[0]; Imperial_Console(800, 200, "Found " + Entity_Name + " in " + Part[0]); break; }     
                           Xml_Selection = Part[0]; // Imperial_Console(800, 200, "Found " + Entity_Name + " in " + Part[0]); 
                           break;
                        } 
                    } catch {}           
                } 
            }
            else if (Entity_XML != null)
            {   Xml_Selection = Entity_XML; 
                The_Xml = XDocument.Load(Entity_XML, LoadOptions.PreserveWhitespace); 
            }
            else { Xml_Selection = Search_Xml_Of(Entity_Name, Xml_Source); } // this runs below anyways, in all cycles


           
            // Stats[0] = Corresponding Xml
            // Stats[1] = Entity_Name (= Variant_Hard_Point_Owner)
            // Stats[2] = Variant_Tree

            // Stats[3] = Hard_Point_Selection      
            // Stats[4] = Tactical_Health
            // Stats[5] = Shield_Points
            // Stats[6] = Shield_Refresh_Rate
            // Stats[7] = Max_Speed

            string[] Stats = new string[16]; // We feed all stats in here                     
            string Content = "";       
            string Variant_Tree = Add_Line + Add_Line + "    Variants:   " + Entity_Name; // First entry being the original parent.


            // Imperial_Console(800, 200, "Searching " + Entity_Name + " in  " + Xml_Selection); 

            // ========================== Get the right Variant ========================== 
            for (int i = 0; i < 8; i++) // Starting multiple Attempts to get values from "Variant_Of_Existing_Type" parent(s)
            {
                try
                {   // Xml_Source is a list of possible files, where it looks for them. If null, it searches the full Xml directory
                    Xml_Selection = Search_Xml_Of(Entity_Name, Xml_Source);
                    Found_Entity = Search_Entity_Of(Xml_Selection, Entity_Name);

                    //if (!Found_Entity.Any()) // If failed we're looking for new xml
                    //{   Xml_Selection = Search_Xml_Of(Entity_Name, Xml_Source);
                    //    Found_Entity = Search_Entity_Of(Xml_Selection, Entity_Name);
                    //}

                   
                    if (Found_Entity.Any())
                    {
                        if (Object_Mode == "Entity" | Object_Mode == "Fighter")
                        {   // Only the (first) entries in the youngest children matter for Shield_Points, Tactical_Health and HardPoints                      
                            // Variant_Hard_Point_Selection overrides Parent entities, we use the first one this loop finds: 
                            if (Stats[3] == null & Found_Entity.Descendants("HardPoints").Any())
                            {
                                Content = Wash_String(Found_Entity.Descendants("HardPoints").First().Value);
                                if (Content != "") { Stats[3] = Content; }
                            }
                            if (Stats[4] == null & Found_Entity.Descendants("Tactical_Health").Any())
                            {
                                Content = Wash_String(Found_Entity.Descendants("Tactical_Health").First().Value);
                                if (Content != "") { Stats[4] = Content; }
                            }
                            if (Stats[5] == null & Found_Entity.Descendants("Shield_Points").Any())
                            {
                                Content = Wash_String(Found_Entity.Descendants("Shield_Points").First().Value);
                                if (Content != "") { Stats[5] = Content; }
                            }
                            if (Stats[6] == null & Found_Entity.Descendants("Shield_Refresh_Rate").Any())
                            {
                                Content = Wash_String(Found_Entity.Descendants("Shield_Refresh_Rate").First().Value);
                                if (Content != "") { Stats[6] = Content; }
                            }
                            if (Stats[7] == null & Found_Entity.Descendants("Max_Speed").Any())
                            {
                                Content = Wash_String(Found_Entity.Descendants("Max_Speed").First().Value);
                                if (Content != "") { Stats[7] = Content; } // Int32.TryParse(Content, out Max_Speed); }
                            }
                       



                            if (Object_Mode != "Fighter")
                            {                                
                                // Dealing with all Starting_Spawned_Units_Tech_0 and Reserve_Spawned_Units_Tech_0 
                                // Occurances inside of all Variants we can find on the way to the 1st ancestor!
                                if (Found_Entity.Descendants("Starting_Spawned_Units_Tech_0").Any() | Found_Entity.Descendants("Reserve_Spawned_Units_Tech_0").Any())
                                {   
                                    
                                    Process_Squadron_Units(Xml_Selection, Entity_Name);
                                    // Imperial_Console(800, 200, "Found Squads are " + Found_Entity.Descendants("Starting_Spawned_Units_Tech_0").First().Value + " in " + Path.GetFileName(Xml_Selection)); 
                                }
                            }
                            else if (Object_Mode == "Fighter") 
                            {                                
                                if (Stats[9] == null)
                                {
                                    Content = Wash_String(Found_Entity.Descendants("Projectile_Types").First().Value);
                                    if (Content != "") { Stats[9] = Content; }
                                }
                                if (Stats[10] == null & Found_Entity.Descendants("Projectile_Damage").Any())
                                {
                                    Content = Wash_String(Found_Entity.Descendants("Projectile_Damage").First().Value);
                                    if (Content != "") { Stats[10] = Content; }
                                }                             
                                if (Stats[11] == null & Found_Entity.Descendants("Projectile_Fire_Pulse_Count").Any())
                                {
                                    Content = Wash_String(Found_Entity.Descendants("Projectile_Fire_Pulse_Count").First().Value);
                                    if (Content != "") { Stats[11] = Content; }
                                }
                                if (Stats[12] == null & Found_Entity.Descendants("Projectile_Fire_Pulse_Delay_Seconds").Any())
                                {
                                    Content = Wash_String(Found_Entity.Descendants("Projectile_Fire_Pulse_Delay_Seconds").First().Value);
                                    if (Content != "") { Stats[12] = Content; }
                                }
                                if (Stats[13] == null & Found_Entity.Descendants("Projectile_Fire_Recharge_Seconds").Any())
                                {
                                    Content = Wash_String(Found_Entity.Descendants("Projectile_Fire_Recharge_Seconds").First().Value);
                                    if (Content != "") { Stats[13] = Content; }
                                }
                            }



                            string Defense = ""; // Joining all of them into a edgy oneliner.
                            if (Stats[4] != null) { Defense += "    Health: " + Stats[4]; }

                            if (Stats[4] != null & Stats[5] != null) { Defense += "  /  Shield: " + Stats[5]; }
                            else if (Stats[5] != null) { Defense += "    Shield: " + Stats[5]; }

                            if (Stats[5] != null & Stats[6] != null) { Defense += "  /  Refresh Rate: " + Stats[6]; }
                            else if (Stats[6] != null) { Defense += "    Refresh Rate: " + Stats[6]; }

                            if (Stats[6] != null & Stats[7] != null) { Defense += "  /  Max Speed: " + Stats[7]; }
                            else if (Stats[7] != null) { Defense += "    Max Speed: " + Stats[7]; }
                            
                            Defense += Add_Line;
                            try { Stats[8] = Defense; } catch {}
                        }

                        else if (Object_Mode == "Projectile")
                        {
                            if (Stats[3] == null & Found_Entity.Descendants("Projectile_Damage").Any())
                            {
                                Content = Wash_String(Found_Entity.Descendants("Projectile_Damage").First().Value);
                                if (Content != "") { Stats[3] = Content; } // Int32.TryParse(Content, out Max_Speed); }
                            }
                            if (Stats[4] == null & Found_Entity.Descendants("Fire_Pulse_Count").Any())
                            {
                                Content = Wash_String(Found_Entity.Descendants("Fire_Pulse_Count").First().Value);
                                if (Content != "") { Stats[4] = Content; }
                            }
                            if (Stats[5] == null & Found_Entity.Descendants("Fire_Pulse_Delay_Seconds").Any())
                            {
                                Content = Wash_String(Found_Entity.Descendants("Fire_Pulse_Delay_Seconds").First().Value);
                                if (Content != "") { Stats[5] = Content; }
                            }
                            if (Stats[6] == null & Found_Entity.Descendants("Fire_Min_Recharge_Seconds").Any())
                            {
                                Content = Wash_String(Found_Entity.Descendants("Fire_Min_Recharge_Seconds").First().Value);
                                if (Content != "") { Stats[6] = Content; }
                            }                                                 
                        }
                       
                        
                        // =============== The actual Variants ===============

                        if (Found_Entity.Descendants("Variant_Of_Existing_Type").Any()) // Variant Detection
                        {                          
                            Entity_Name = Wash_String(Found_Entity.Descendants("Variant_Of_Existing_Type").First().Value);
                            Variant_Tree += " > " + Entity_Name;

                            // Imperial_Console(800, 200, "Parent is " + Entity_Name);

                            if (Entity_Name == "Template_Spawner") // From Thrawn's Revenge
                            {
                                if (Found_Entity.Descendants("Destruction_Survivors").Any())
                                {   // First Parameter of this Tag is the Name of the next Level of variant, which is spawned ingame through the <Destruction_Survivors> tag.                                     
                                    Entity_Name = (Wash_String(Found_Entity.Descendants("Destruction_Survivors").First().Value)).Split(',')[0];

                                    Variant_Tree += " > " + Entity_Name;
                                    // Imperial_Console(800, 200, "\"" + Entity_Name + "\"");     
                                }
                            }
                        } 

                        else // No more Variants to uncapsule, we got a target at the end of the variant chain
                        {
                            // Imperial_Console(500, 200, Entity_Name + " at  " + Found_Entity.Descendants("Text_ID").First().Value);
                            break;
                        }
                    }
                } catch {}
            }

            if (!Variant_Tree.Contains(">")) { Variant_Tree = ""; } // Clearing if nothing found
            else { Variant_Tree += Add_Line; }


            try { Stats[0] = Xml_Selection; } catch {} 
            try { Stats[1] = Entity_Name; } catch {}
            try { Stats[2] = Variant_Tree; } catch {}

            // If Object_Mode == "Projectile"
                // Stats[3] = Projectile_Damage
                // Stats[4] = Fire_Pulse_Count
                // Stats[5] = Fire_Pulse_Delay_Seconds
                // Stats[6] = Fire_Min_Recharge_Seconds

            // If Object_Mode == "Entity" & "Fighter"
                // Stats[3] = HardPoints
                // Stats[4] = Tactical_Health
                // Stats[5] = Shield_Points
                // Stats[6] = Shield_Refresh_Rate
                // Stats[7] = Max_Speed
                // Stats[8] = Defense;

            // If Object_Mode == "Fighter" (only)
                // Stats[9] = Projectile_Damage
                // Stats[10] = Fire_Pulse_Count
                // Stats[11] = Fire_Pulse_Delay_Seconds
                // Stats[12] = Fire_Min_Recharge_Seconds
                // Stats[13] = Fire_Min_Recharge_Seconds
                                   

            // Imperial_Console(500, 500, string.Join("\n", Stats));
            return Stats;
        }



        //=====================//  

        public bool Process_Squadron_Damage_Per_Minute(string Squadron_Name, string Squadron_Number, bool Is_Reserve)
        {   // Imperial_Console(600, 100, "Received " + Squadron_Name + ", " + Squadron_Number); // Check whether fighters were received in this function

            int Squadron_Count = 0;
            Temporal_A = ""; Temporal_D = 0; Temporal_E = new string[] {};
            if (Squadron_Number != "") { Int32.TryParse(Squadron_Number, out Squadron_Count); }

            if (Squadron_Count == 0) { return false; } // Means we have both the name and quanity of our Fighter SquadronS


            // The Difference to "Fighter_Balancing_Xml = Search_Xml_Of(Squadron_Name)" is,
            // that Stats[0] already considered all Variant_Of_Existing_Type tags!
            string[] Stats; 
            if (Fighter_Balancing_Xml == null) { Stats = Get_Entity_Stats(Squadron_Name, "None"); Fighter_Balancing_Xml = Stats[0]; }
            else { Stats = Get_Entity_Stats(Squadron_Name, "None", Fighter_Balancing_Xml); }
          



            XDocument The_Xml = XDocument.Load(Stats[0], LoadOptions.PreserveWhitespace);                   
            IEnumerable<XElement> Found_Entity = null;
            List<string> Fighter_List = new List<string>();
            List<string> All_Fighters = new List<string>();


            // ================= Grabbing the SpaceUnit Single Fighter =================         
            try {
                   
              Found_Entity =
                from All_Tags in The_Xml.Root.Descendants()
                where (string)All_Tags.Attribute("Name") == Squadron_Name
                select All_Tags;

                if (!Found_Entity.Any()) { return false; }
                  
        
                string Unit_Type ="Team";
                Temporal_A = "";


                if (Found_Entity.Descendants("Squadron_Units").Any()) { Temporal_A = "Squadron_Units"; Unit_Type = "Squadron"; }
                    else if (Found_Entity.Descendants("Company_Units").Any()) { Temporal_A = "Company_Units"; }
                        else if (Found_Entity.Descendants("HeroCompany").Any()) { Temporal_A = "Company_Units"; }



                // Merging ALL found tags into a single String and removing formatting!
                foreach (XElement Tag in Found_Entity.Descendants(Temporal_A))
                {   foreach (string Entry in Wash_String(Tag.Value).Split(','))
                    { All_Fighters.Add(Entry); }
                }
                
                if (All_Fighters.Count() == 0 | All_Fighters == null) { return false; }
                // else { Imperial_Console(600, 100, "Found " + Squadron_Name); } // Checking whether Squadron was found
                // Imperial_Console(600, 100, string.Join(",", All_Fighters)); // We got all Fighters


                foreach (string Entry in All_Fighters)
                {   if (Entry != "")
                    {   int Amount = 0;

                        // Counting all Units of same type together
                        foreach (string Item in All_Fighters) { if (Item == Entry) { Amount++; } }

                        // We use this to store all different Fighter Types
                        if (!Regex.IsMatch(List_To_String(Fighter_List), Entry + ".*?")) { Fighter_List.Add(Entry + "#" + Amount); }
                    }
                }
                // Imperial_Console(600, 100, string.Join(",", Fighter_List)); // The Counter List (This does not show the count of squadrons)



                foreach (string Entry in Fighter_List)
                {   try
                    {   string[] The_Unit = Entry.Split('#'); // Hashtag seperates Unit_Name and Amount   .Replace(" ", "")                  
                        int Unit_Count = 0;
                        Int32.TryParse(The_Unit[1], out Unit_Count);
                                   
                        // Regularly exiting means we probably found a Squad 
                        if (Unit_Count > 0) 
                        {   if (Process_Fighter_Damage_Per_Minute(Fighter_Balancing_Xml, The_Unit[0], Unit_Count, Unit_Type, Squadron_Count, Is_Reserve)) 
                            { // Imperial_Console(600, 100, Unit_Count + "x " + The_Unit[0] + " " + Unit_Type + " in " + Path.GetFileName(Fighter_Balancing_Xml));
                            return true; } 
                        }
                        
                    } catch {}                     
                }              
            } catch {}


            return false; 
        }



        //=====================//
        public bool Process_Fighter_Damage_Per_Minute(string Fighter_Xml_File, string Unit, int Unit_Count, string Unit_Type, int Squadron_Count, bool Is_Reserve)
        {
            // Imperial_Console(600, 100, Unit_Count + "x " + Unit + " " + Unit_Type + " in " + Path.GetFileName(Fighter_Xml_File));


            string Projectile_Name = "";       
            double Fighter_Damage_Points_Per_Minute = 0;
            XDocument The_Xml = null;
            IEnumerable<XElement> Found_Entity = null;
            if (Unit == "") { return false; }


            // ================= Getting the Hardpoints =================
            Temporal_A = ""; // Clearing
            Temporal_E = new string[] { };

            try 
            {    
                // This time we're looking for the "SpaceUnit" entity instead of the "Squadron" or Team
                
                
                // But first we try searching in the same .xml file from above, because it might be in the same file.
                string[] Fighter_Stats = Get_Entity_Stats(Unit, "Fighter", "Unit_Data_Files");
               

                if (Fighter_Stats[0] == null) { return false; } // Would crash if null          
                else try 
                {   The_Xml = XDocument.Load(Fighter_Stats[0], LoadOptions.PreserveWhitespace);

                    Found_Entity =
                      from All_Tags in The_Xml.Root.Descendants()
                        where (string)All_Tags.Attribute("Name") == Fighter_Stats[1]
                          select All_Tags;
                } catch {}

                // Imperial_Console(600, 600, string.Join("\n", Fighter_Stats));



                if (Found_Entity.Any()) // DON'T turn this into "else if()" !!  It needs to run seperately (my common mistake)
                {
                    // Imperial_Console(600, 100, "Found " + Unit + " in " + Path.GetFileName(Fighter_Xml_File));

                    string Plural = "s ";
                    

                    try { Temporal_A = Found_Entity.Descendants("HardPoints").First().Value; }
                    // Attention-less Modders write it as "Hardpoints" 
                    catch { try { Temporal_A = Found_Entity.Descendants("Hardpoints").First().Value; } catch { } }

                    // Repeat the procedure to capture its Hardpoints
                    if (Temporal_A != "") { Temporal_E = Wash_String(Temporal_A).Split(','); }
                    // Imperial_Console(600, 600, string.Join("\n", Temporal_E));

                    // ===============================


                    try { Projectile_Name = Found_Entity.Descendants("Projectile_Types").First().Value; } catch {}
                    if (Projectile_Name != "") // Repead the procedure to capture its Hardpoints
                    {
                        double Salvos = 0;
                        double Fire_Delay = 0;
                        double Recharge_Time = 1;
                        bool Info_Is_Missing = false;
                        double Projectile_Damage = 1; // I think 1 is what Vanilla uses as Default value  


                        try { Double.TryParse(Found_Entity.Descendants("Projectile_Fire_Pulse_Count").First().Value, out Salvos); }
                        catch { Info_Is_Missing = true; }
                        try { Double.TryParse(Found_Entity.Descendants("Projectile_Fire_Pulse_Delay_Seconds").First().Value, out Fire_Delay); }
                        catch { Info_Is_Missing = true; }
                        try { Double.TryParse(Found_Entity.Descendants("Projectile_Fire_Recharge_Seconds").First().Value, out Recharge_Time); }
                        catch {}



                        // =========== Getting Projectile Damage ============
                        if (Fighter_Xml_File != null) try // Would crash if null
                        {
                                // Projectile_Xml = XDocument.Load(Fighter_Xml_File, LoadOptions.PreserveWhitespace);
                                // We recycle Found_Entity for Projectile Instancec instead of the Fighter one it was before.

                                Found_Entity =
                                    from All_Tags in The_Xml.Root.Descendants()
                                where (string)All_Tags.Attribute("Name") == Projectile_Name
                            select All_Tags;
                        } catch {}


                        if (Fighter_Xml_File == null | Found_Entity == null)
                        {   foreach (string The_File in Projectile_Data_Files)
                            {   try
                                {   The_Xml = XDocument.Load(Xml_Directory + The_File, LoadOptions.PreserveWhitespace);

                                    Found_Entity =
                                      from All_Tags in The_Xml.Root.Descendants()
                                      where (string)All_Tags.Attribute("Name") == Projectile_Name
                                      select All_Tags;

                                    if (Found_Entity.Any()) { Fighter_Xml_File = Xml_Directory + The_File; break; }
                                } catch {}
                            }
                        }

                        try { Double.TryParse(Found_Entity.Descendants("Projectile_Damage").First().Value, out Projectile_Damage); } catch {}
                        // Imperial_Console(400, 100, Projectile_Damage.ToString() + ", " + Salvos + ", " + Fire_Delay + ", " + Recharge_Time);                            
                        // Imperial_Console(400, 100, Fighter_Xml_File);
                        // Imperial_Console(400, 100, Projectile_Damage.ToString()); 



                        double Damage = Projectile_Damage * Salvos * (Unit_Count * Squadron_Count);
                        double Salvos_Per_Minute = 60 / ((Fire_Delay * Salvos) + Recharge_Time); // 60 Seconds a Minute
                        Fighter_Damage_Points_Per_Minute = Damage * Salvos_Per_Minute;

                      
                        if (Damage_Score_Window_Height < 800) { Damage_Score_Window_Height += 144; }
                        if (Squadron_Count < 2) { Plural = " "; } // Removing the "s"


                        if (!Is_Reserve)
                        {   Total_Damage_Points_Per_Minute += Fighter_Damage_Points_Per_Minute; // Should ignore this for Reserves
                            Damage_Score += Add_Line + "    //======== + " + (Unit_Count * Squadron_Count) + " " + Unit + " inside of " + Squadron_Count + " " + Unit_Type + Plural + @"========\\" + Add_Line + Add_Line;
                            if ((int)Fighter_Damage_Points_Per_Minute > 0)
                            { Damage_Score +=  "    " + (int)Fighter_Damage_Points_Per_Minute + " = " + (Unit_Count * Squadron_Count) + " * HP mounted to the Hull of " + Unit + " (active)" + Add_Line + Add_Line; }
                        }
                        else //if (Is_Reserve)
                        {
                            if (Squadron_Count > 1) { Plural = "s) "; }

                            Damage_Score += Add_Line + "    //======== + " + (Unit_Count * Squadron_Count) + " " + Unit + " as Reserves (" + Squadron_Count + " " + Unit_Type + Plural + @"========\\" + Add_Line + Add_Line;
                            if ((int)Fighter_Damage_Points_Per_Minute > 0)
                            { Damage_Score += "    " + (int)Fighter_Damage_Points_Per_Minute + " = " + (Unit_Count * Squadron_Count) + " * HP mounted to the Hull of " + Unit + " (reserve)" + Add_Line + Add_Line; }
                        }
                     

                        // { Damage_Score += "    " + (int)Fighter_Damage_Points_Per_Minute + " = Damage by " + (Unit_Count * Squadron_Count) + " HP mounted to the Hull of active Fighters" + Add_Line + Add_Line; }


                        if (Info_Is_Missing & Projectile_Damage > 0) // Then we got a Projectile but not all Information tags.
                        {
                            if (Salvos == 0 | Fire_Delay == 0) // | Recharge_Time == 0) // If one of them is missing
                            {
                                Temporal_A = "      " + Unit + "    is missing ";
                                if (Damage_Score_Window_Height < 800) { Damage_Score_Window_Height += 72; }

                                // if (Projectile_Damage == -1) { Temporal_A += "<Projectile_Damage>, "; }
                                if (Salvos == 0) { Temporal_A += "<Projectile_Fire_Pulse_Count>, "; }
                                if (Fire_Delay == 0) { Temporal_A += "<Projectile_Fire_Pulse_Delay_Seconds>, "; }
                                // if (Recharge_Time == 1) { Temporal_A += "<Projectile_Fire_Recharge_Seconds>"; }
                                Damage_Score += Temporal_A + Add_Line + Add_Line; // We append this info to our Balancing Journal
                            }
                        }


                        // Passing value of Is_Reserve on to the next function
                        if (Salvos == 0 & Fire_Delay == 0) { Process_Damage_Per_Minute(null, Temporal_E, Unit_Count, Squadron_Count, false, Is_Reserve); }
                        else { Process_Damage_Per_Minute(null, Temporal_E, Unit_Count, Squadron_Count, false, Is_Reserve); } // Temporal_E being list of found HPs

                    }
                    else if (Temporal_E.Count() > 0) // If has any <Hardpoints> entries 
                    {
                        if (Damage_Score_Window_Height < 800) { Damage_Score_Window_Height += 144; }
                        if (Squadron_Count < 2) { Plural = " "; } 

                        if (!Is_Reserve)
                        {
                            Damage_Score += Add_Line + "    //======== + " + (Unit_Count * Squadron_Count) + " " + Unit + " inside of " + Squadron_Count
                                         + " " + Unit_Type + Plural + @"========\\" + Add_Line + Add_Line;
                        }
                        else // if (Is_Reserve)
                        {
                            if (Squadron_Count > 1) { Plural = "s) "; } 

                            Damage_Score += Add_Line + "    //====== + " + (Unit_Count * Squadron_Count) + " " + Unit + " as Reserves (" + Squadron_Count
                                         + " " + Unit_Type + Plural + @"======\\" + Add_Line + Add_Line;
                        }

                        // Process_Damage_Per_Minute() Calls this Function, when we got the HP we call back to Process_Damage_Per_Minute()
                        Process_Damage_Per_Minute(null, Temporal_E, Unit_Count, Squadron_Count, false, Is_Reserve);
                      
                    }

                    // Imperial_Console(400, 100, ((int)Fighter_Damage_Points_Per_Minute).ToString());
                } else { return false; } // Otherwise no Unit was found
            } catch {}
            return true;                     
        }



        // ================== Team Subbox ==================//

        // Xml_File value can be null, then this function will search the Xml directory for the required Team/Squadron
        //Load_Team_To_UI(Team_Xml_File, Team_Name);
        void Load_Team_To_UI(string Team_Xml_File, string Team_Name)
        {
            User_Input = false;
            //Clear_Team_Values();

            Text_Box_Team_Name.Text = Team_Name;
            Combo_Box_Team_Type.Text = Team_Type;

            Selected_Team = Team_Name;


            string All_Team_Members = "";


            // If no source .xml was specified we try the selected Xml, which is a global variable that already updated above
            if (Team_Xml_File != null) { All_Team_Members = Xml_Utility(Team_Xml_File, Team_Name, Team_Units, null); }
            else
            {
                Team_Xml_File = Selected_Xml;
                All_Team_Members = Xml_Utility(Team_Xml_File, Team_Name, Team_Units, null);


                // If still not found we go search for it
                if (All_Team_Members == null | All_Team_Members == "")
                {
                    foreach (var Xml in Get_Xmls())
                    {
                        try
                        {   // ===================== Opening Xml File =====================
                            XDocument Xml_File = XDocument.Load(Xml, LoadOptions.PreserveWhitespace);

                            var Instances =
                                from All_Tags in Xml_File.Root.Descendants()
                                // Selecting all non empty tags that have the Attribute "Name", null because we need all selected.
                                where (string)All_Tags.Attribute("Name") == Team_Name
                                select All_Tags;


                            // =================== Checking Xml Instance ===================
                            foreach (XElement Instance in Instances)
                            {
                                // Getting the Xml of this Team
                                Team_Xml_File = Xml;

                                // Getting tag value of the Team members (just because we are already in that XElement)
                                if (Team_Units != null & Team_Units != "")
                                { try { All_Team_Members = Instance.Descendants(Team_Units).First().Value; } catch { } }

                                continue;
                            }
                        }
                        catch { }
                    }
                }
            }



            if (All_Team_Members != null)
            {
                Temporal_A = Regex.Replace(All_Team_Members, " ", "");
                Temporal_B = Regex.Replace(Temporal_A, @"\t", "");
                string[] Team_Members = Temporal_B.Split(',');


                Temporal_A = "";

                foreach (string Team_Member in Team_Members)
                { Temporal_A += Team_Member + ", " + Add_Line; }

                Text_Box_Team_Amount.Text = Team_Members.Count().ToString();
                Text_Box_Team_Members.Text = Temporal_A;
            }


            string Variant_Value = Xml_Utility(Team_Xml_File, Team_Name, "Variant_Of_Existing_Type", null);

            //============== Setting Variant_Of_Existing_Type ==============     
            if (Variant_Value == "" | Variant_Value == null)
            {
                if (Check_Box_Team_Is_Variant.Checked == true)
                {  // Making sure the Checkbox is unchecked
                    Check_Box_Team_Is_Variant.Checked = false;                                
                    Button_Open_Team_Variant.Visible = false;
                }
                // Clearing Textbox
                Text_Box_Team_Is_Variant.Text = "";
            }
            else if (Variant_Value != "" & Variant_Value != null)
            {
                if (Check_Box_Team_Is_Variant.Checked == false)
                {   // Checking the "Is Variant" Checkbox in order to enable the Textfield
                    Check_Box_Team_Is_Variant.Checked = true;             
                    Button_Open_Team_Variant.Visible = true;
                }
                // Inserting the value for Parent Xml Instance into the UI Field
                Text_Box_Team_Is_Variant.Text = Variant_Value;

                // And into the Squadron list if it doesen't already contain them.
                // if (!List_View_Teams.Text.Contains(Variant_Value)) { List_View_Teams.Items.Add(Variant_Value); }
            }



            Text_Box_Shuttle_Type.Text = Xml_Utility(Team_Xml_File, Team_Name, "Company_Transport_Unit", null);


            // For space Units we set a offset
            if (Combo_Box_Class.Text == "Fighter") { Text_Box_Team_Offsets.Text = "3"; }
            else if (Combo_Box_Class.Text == "Bomber") { Text_Box_Team_Offsets.Text = "4"; }



            // ======== Loading all tags (which have priority over the tag of the same name from the team member instance) ========
            // Needs to be Xml_Utility because Load_Instance_Tag() currently only loads from the teammember unit

            try {
       
            //======================= Loading some Art Settings ======================= 
            Text_Box_Text_Id.Text = Xml_Utility(Team_Xml_File, Team_Name, "Text_ID", null);
            Text_Box_Encyclopedia_Text.Text = Xml_Utility(Team_Xml_File, Team_Name, "Encyclopedia_Text", null);
            Text_Box_Unit_Class.Text = Xml_Utility(Team_Xml_File, Team_Name, "Encyclopedia_Unit_Class", null);

            // TODO: <Is_Visible_On_Radar>


            Text_Box_Icon_Name.Text = Xml_Utility(Team_Xml_File, Team_Name, "Icon_Name", null);
            Text_Box_Radar_Icon.Text = Xml_Utility(Team_Xml_File, Team_Name, "Radar_Icon_Name", null);
            Text_Box_Radar_Size.Text = Xml_Utility(Team_Xml_File, Team_Name, "Radar_Icon_Size", null);
     

            //========================= Loading Power Values =========================             
            Text_Box_AI_Combat.Text = Xml_Utility(Team_Xml_File, Team_Name, "AI_Combat_Power", null);

      
            // Processing population value, according to these two tags:
            Temporal_A = Xml_Utility(Team_Xml_File, Team_Name, "Population_Value", null);
            Temporal_B = Xml_Utility(Team_Xml_File, Team_Name, "Additional_Population_Capacity", null);

            if (Temporal_B != "") // If existing we parse it to int 
            {
                Int32.TryParse(Temporal_A, out Temporal_C);
                Int32.TryParse(Temporal_B, out Temporal_D);

                // If Additional Population Capacity is bigger then Population Capacity we take Additional Population Capacity instead  
                if (Temporal_C < Temporal_D)
                {
                    Temporal_A = (Temporal_D - Temporal_C).ToString();
                    // Making sure Operator is set to +
                    if (Toggle_Operator_Population == false) { Button_Operator_Population_Click(null, null); }
                }
            }
            else
            {   // Otherwise there is no "Additional_Population_Capacity", making sure Operator is set to - 
                if (Toggle_Operator_Population) { Button_Operator_Population_Click(null, null); }
            }
            Text_Box_Population.Text = Temporal_A;

            try 
            {
                Temporal_A = Xml_Utility(Team_Xml_File, Team_Name, "Projectile_Damage", null);

                if (Temporal_A != null & Temporal_A.Contains(".")) // Then it must be decimal
                {
                    decimal Decimal_Text;
                    decimal.TryParse(Temporal_A, out Decimal_Text);
                    int Integer_Text = (int)Decimal_Text;
                    Text_Box_Projectile.Text = Integer_Text.ToString();
                }
                else if (Temporal_A != null) { Text_Box_Projectile.Text = Temporal_A; }
            } catch {}


            //======================= Loading Unit Properties ======================== 

            Temporal_A = Xml_Utility(Team_Xml_File, Team_Name, "Is_Named_Hero", null);
            if (Temporal_A == "Yes") { Toggle_Button(Switch_Button_Is_Hero, "Button_On", "Button_Off", 0, true); Toggle_Is_Hero = true; }
            else if (Temporal_A == "No") { Toggle_Button(Switch_Button_Is_Hero, "Button_On", "Button_Off", 0, false); Toggle_Is_Hero = false; }

            Temporal_A = Xml_Utility(Team_Xml_File, Team_Name, "Show_Hero_Head", null);
            if (Temporal_A == "Yes") { Toggle_Button(Switch_Button_Show_Head, "Button_On", "Button_Off", 0, true); Toggle_Show_Head = true; }
            else if (Temporal_A == "No") { Toggle_Button(Switch_Button_Show_Head, "Button_On", "Button_Off", 0, false); Toggle_Show_Head = false; }

            /* // Disabled because it is supposed to load from the Unit itself
            Text_Box_Lua_Script.Text = Xml_Utility(Team_Xml_File, Team_Name, "Lua_Script");
            if (Text_Box_Lua_Script.Text == "ObjectScript_Hyper_Space") { Toggle_Button(Switch_Button_Use_Particle, "Button_On", "Button_Off", 0, true); Toggle_Use_Particle = true; }
            else { Toggle_Button(Switch_Button_Use_Particle, "Button_On", "Button_Off", 0, false); Toggle_Use_Particle = false; }
            */


            if (Xml_Utility(Team_Xml_File, Team_Name, "Hyperspace", null) == "Yes")
            {
                Check_Box_Has_Hyperspace.Checked = true;
                Text_Box_Hyperspace_Speed.Text = Xml_Utility(Team_Xml_File, Team_Name, "Hyperspace_Speed", null);
            }
            else { Check_Box_Has_Hyperspace.Checked = false; }



            Temporal_A = Xml_Utility(Team_Xml_File, Team_Name, "Starting_Spawned_Units_Tech_0", null);
            if (Temporal_A != null & Temporal_A != "")
            {   try
                {   Temporal_E = Temporal_A.Split(',');
                    Text_Box_Starting_Unit_Name.Text = Temporal_E[0];
                    Text_Box_Spawned_Unit.Text = Temporal_E[1];
                } catch { }             
            }

            Temporal_B = Xml_Utility(Team_Xml_File, Team_Name, "Reserve_Spawned_Units_Tech_0", null);
            if (Temporal_B != null & Temporal_B != "")
            {   try
                {   Temporal_E = Temporal_B.Split(',');
                    Text_Box_Reserve_Unit_Name.Text = Temporal_E[0];
                    Text_Box_Reserve_Unit.Text = Temporal_E[1];
                } catch { }
            }

     
          
            //====================== Loading Build Requirements ====================== 

            //========= Getting the Affiliation Tag and putting its value into a List Box Element.
            List_View_Inactive_Affiliations.Items.Clear();
            List_View_Active_Affiliations.Items.Clear();


            Temporal_A = Xml_Utility(Team_Xml_File, Team_Name, "Affiliation", null);

            if (Temporal_A != null & Temporal_A != "")
            {   
                Temporal_E = Temporal_A.Split(',');

                foreach (string Entry in Temporal_E)
                {
                    // Using Regex to get all Values into a List Box
                    Temporal_B = Wash_String(Entry);

                    // To make sure we won't add any empty string
                    if (Temporal_B != "") List_View_Active_Affiliations.Items.Add(Temporal_B);
                }
            }



            //=========== Removing matching entries in the Pool of Unused Affiliations

            string[] The_Factions = All_Factions.Split(',');

            foreach (string Entry in The_Factions)
            {
                // First we add all these entries, and later if matched they get removed
                if (Entry != "") { List_View_Inactive_Affiliations.Items.Add(Entry); }

                for (int i = List_View_Active_Affiliations.Items.Count - 1; i >= 0; --i)
                {   if (List_View_Active_Affiliations.Items[i].Text == Entry) // Removing matched Items 
                    { Remove_List_View_Selection(List_View_Inactive_Affiliations, List_View_Active_Affiliations.Items[i].Text, false); }
                }
                Set_Checkmate_Color(List_View_Inactive_Affiliations);
            }



            Temporal_A = Xml_Utility(Team_Xml_File, Team_Name, "Build_Tab_Space_Units", null);

            if (Temporal_A == "Yes") { Toggle_Build_Tab = true; Toggle_Button(Switch_Button_Build_Tab, "Button_On", "Button_Off", 0, true); }
            else if (Temporal_A == "No") { Toggle_Build_Tab = false; Toggle_Button(Switch_Button_Build_Tab, "Button_On", "Button_Off", 0, false); }


            Text_Box_Build_Cost.Text = Xml_Utility(Team_Xml_File, Team_Name, "Build_Cost_Credits", null);
            Text_Box_Skirmish_Cost.Text = Xml_Utility(Team_Xml_File, Team_Name, "Tactical_Build_Cost_Multiplayer", null);

            Text_Box_Build_Time.Text = Xml_Utility(Team_Xml_File, Team_Name, "Build_Time_Seconds", null);
            Text_Box_Tech_Level.Text = Xml_Utility(Team_Xml_File, Team_Name, "Tech_Level", null);

            Text_Box_Star_Base_LV.Text = Xml_Utility(Team_Xml_File, Team_Name, "Required_Star_Base_Level", null);
            Text_Box_Ground_Base_LV.Text = Xml_Utility(Team_Xml_File, Team_Name, "Required_Ground_Base_Level", null);

            Text_Box_Required_Timeline.Text = Xml_Utility(Team_Xml_File, Team_Name, "Required_Timeline", null);


            Temporal_A = Xml_Utility(Team_Xml_File, Team_Name, "Build_Initially_Locked", null);
            if (Temporal_A == "Yes") { Toggle_Innitially_Locked = true; Toggle_Button(Switch_Button_Innitially_Locked, "Button_On", "Button_Off", 0, true); }
            else if (Temporal_A == "No") { Toggle_Innitially_Locked = false; Toggle_Button(Switch_Button_Innitially_Locked, "Button_On", "Button_Off", 0, false); }


            bool Show_Build_Window = false;

            Text_Box_Slice_Cost.Text = Xml_Utility(Team_Xml_File, Team_Name, "Slice_Cost_Credits", null);
            if (Text_Box_Slice_Cost.Text != "") 
            {   Check_Box_Slice_Cost.Checked = true;
                Show_Build_Window = true;
            }

            Text_Box_Current_Limit.Text = Xml_Utility(Team_Xml_File, Team_Name, "Build_Limit_Current_Per_Player", null);
            if (Text_Box_Current_Limit.Text != "") 
            {   Check_Box_Current_Limit.Checked = true;
                Show_Build_Window = true;
            }

            Text_Box_Lifetime_Limit.Text = Xml_Utility(Team_Xml_File, Team_Name, "Build_Limit_Lifetime_Per_Player", null);
            if (Text_Box_Lifetime_Limit.Text != "") 
            {   Check_Box_Lifetime_Limit.Checked = true;
                Show_Build_Window = true;
            }

        

            Temporal_A = Xml_Utility(Team_Xml_File, Team_Name, "Required_Special_Structures", null);

            if (Temporal_A != null & Temporal_A != "")
            {
                Temporal_A = Regex.Replace(Temporal_A, " ", "");
                
                foreach (string Item in Temporal_A.Split(','))
                { if (!Building_List.Contains(Item)) { Building_List.Add(Item); } }
                Button_Required_Structures_Click(null, null);

                Show_Build_Window = true;
            }
           

            Temporal_A = Xml_Utility(Team_Xml_File, Team_Name, "Required_Planets", null);
          
            if (Temporal_A != null & Temporal_A != "")
            {  
                Temporal_A = Regex.Replace(Temporal_A, " ", "");

                foreach (string Item in Temporal_A.Split(','))
                { if (!Planet_List.Contains(Item)) { Planet_List.Add(Item); } }
                Button_Required_Planets_Click(null, null);
                Show_Build_Window = true;
                 
            }


            if (Loading_Completed & Show_Build_Window & Size_Expander_H == "false") { Button_Expand_H_Click(null, null); }
           


            } catch { Imperial_Console(600, 100, Add_Line + "    The Team/Squadron loading function crashed."
                                               + Add_Line + "    One or more tags caused the issue.");}
            //======================================================================== 

            Text_Box_Team_Members.Focus();

            Application.UseWaitCursor = false;
            Application.DoEvents();

            User_Input = true;
        }


        // =================================================//


        void Clear_Team_Values()
        {
            Control[] All_UI_Text_Boxes = 
            {   Combo_Box_Team_Type, Text_Box_Name, Text_Box_Team_Is_Variant, Text_Box_Shuttle_Type,
                Text_Box_Team_Amount, Text_Box_Team_Offsets, Text_Box_Team_Members,           
            };

            foreach (Control Text_Box in All_UI_Text_Boxes)
            {   // Setting all Textboxes empty if they arn't already
                if (Text_Box.Text != "") { Text_Box.Text = ""; }
            }

            List_View_Teams.Items.Clear();
        }


        //=====================/
        void Clear_Ability_Values()
        {
            Toggle_Button(Switch_Button_Auto_Fire, "Button_On", "Button_Off", 0, false); 
            Toggle_Auto_Fire = false;

            Control[] All_UI_Text_Boxes = 
            {   Combo_Box_Ability_Type, Combo_Box_Activated_GUI_Ability, 
                Text_Box_Expiration_Seconds, Text_Box_Recharge_Seconds, 
                Text_Box_Alternate_Name, Text_Box_Alternate_Description, Text_Box_Ability_Icon, 
                Text_Box_SFX_Ability_Activated, Text_Box_SFX_Ability_Deactivated, 
                Combo_Box_Mod_Multiplier, Text_Box_Mod_Multiplier,
                // They must not be used or they delete text when switching between primary and secondary ability:
                // Combo_Box_Additional_Abilities, Text_Box_Additional_Abilities          
            };

            foreach (Control Text_Box in All_UI_Text_Boxes)
            {   // Setting all Textboxes empty if they arn't already
                if (Text_Box.Text != "") { Text_Box.Text = ""; }
            }      
        }
  
        //=====================//

        void Show_Message_Box_Xml_Fail(Size Caution_Window_Size)
        {   //========== Displaying Error Messages to User
            string Error_Text = "";

            if (Xml_Exception_Files.Count > 0)
            {
                foreach (string Error in Xml_Exception_Files)
                {
                    Error_Text += "\n   " + Error + ", ";
                }
           


                // Innitiating new Form
                Caution_Window Display = new Caution_Window();

                Display.Opacity = Transparency;
                Display.Size = Caution_Window_Size;

                // Using Theme colors for Text and Background
                Display.Text_Box_Caution_Window.BackColor = Color_05;
                Display.Text_Box_Caution_Window.ForeColor = Color_03;

                Display.Text_Box_Caution_Window.Text = "   (You can deactivate this Message using \"Show Load Issues\" in Settings.)\n" + "   Failed to load the following Xmls:" + "\n   " + Error_Text;
                Display.Show();
            }
        }
        //=====================//

        void Show_Message_Box_One_Button(Size Caution_Window_Size, string The_Xml_File, string New_Text)
        {   
            //========== Displaying Error Messages to User   
            // Innitiating new Form
            Caution_Window Display = new Caution_Window();
            Display.Opacity = Transparency;
            Display.Size = Caution_Window_Size;

            // Using Theme colors for Text and Background
            Display.Text_Box_Caution_Window.BackColor = Color_05;
            Display.Text_Box_Caution_Window.ForeColor = Color_03;


            Display.Button_Caution_Box_1.Visible = true;
            Display.Button_Caution_Box_1.Text = "Ok";
            Display.Button_Caution_Box_1.Location = new Point(184, 86);

            if (New_Text == null)
            { Display.Text_Box_Caution_Window.Text = "   Failed to load" + "\n" + "   " + Path.GetFileName(The_Xml_File); }
            else { Display.Text_Box_Caution_Window.Text = "   " + New_Text;}


            Display.Show();
        }
     
        //=====================//
        void Store_Savegame_Cache()
        {
            string Save_Path = Savegame_Path + @"Empire At War\Save\";
            string Target_Path = Savegame_Path + @"Empire At War Backup\";
           
            int Current_Count = 0;
            Int32.TryParse(Load_Setting(Setting, "EAW_Savegame_Count"), out Current_Count);  
           
    
            if (Game_Path == Game_Path_FOC) 
            {   Save_Path = Savegame_Path + @"Empire At War - Forces of Corruption\Save\"; 
                Target_Path = Savegame_Path + @"Forces of Corruption Backup\";
                Int32.TryParse(Load_Setting(Setting, "FOC_Savegame_Count"), out Current_Count);
            }

            if (!Directory.Exists(Target_Path)) { Directory.CreateDirectory(Target_Path); }

            

            if (Directory.Exists(Save_Path))
            {
                // We put all found Files inside of our target folder into a string table
                string[] File_Paths = Directory.GetFiles(Save_Path);                            
                       
                if (Current_Count < 10 & Current_Count != 0) {Current_Count++;}
                else {Current_Count = 01;} // Otherwise we start at 01 and overwrite the old 01 entry

                

                string Last_Entry = Target_Path + @"Save_" + (Current_Count -1).ToString();
               
                // Making sure Target directory was cleared and is existing
                if (Directory.Exists(Last_Entry))
                {                                             
                    // Returning function because this is only supposed to store the first entry per day
                    if (File.GetCreationTime(Last_Entry).Date == DateTime.Today.Date) { return; }
                    // Imperial_Console(600, 100, Add_Line + File.GetCreationTime(Last_Entry).Date + ",  " + DateTime.Today.Date);
                }
                
                // Creating target directory
                Directory.CreateDirectory(Target_Path + @"Save_" + Current_Count.ToString());



                if (Game_Path == Game_Path_FOC) { Save_Setting(Setting, "FOC_Savegame_Count", Current_Count.ToString()); }
                else { Save_Setting(Setting, "EAW_Savegame_Count", Current_Count.ToString()); }


                foreach (string Savegame in File_Paths)
                {   // Copying all Savegame files into the Target Path with the name Save_ + value of Savegame_Counter 
                    File.Copy(Savegame, Target_Path + @"Save_" + Current_Count.ToString() + @"\" + Path.GetFileName(Savegame) , true);
                }

            }

            // This tells the user which is the newest savegame direcory in the Backup folder
            Label_EAW_Savegame.Text = "EAW: " + Load_Setting(Setting, "EAW_Savegame_Count");
            Label_FOC_Savegame.Text = "FOC: " + Load_Setting(Setting, "FOC_Savegame_Count");
           
        }

        //=====================//
        void Push_Mod_Savegame_Directory()
        {
            if (Savegame_Path != "" & Mod_Name != "" & Mod_Name != "Choose_Mod")
            {   string Target_Path = Savegame_Path + @"Mod_Savegames\" + Mod_Name;

                if (Directory.Exists(Target_Path))
                {
                    string Source_Path = Savegame_Path + @"Empire At War\Save";
                    if (Game_Path == Game_Path_FOC) { Source_Path = Savegame_Path + @"Empire At War - Forces of Corruption\Save"; }

                    Moving(Source_Path, Target_Path);
                }
            }
        }


        void Pull_Mod_Savegame_Directory()
        {
            if (Savegame_Path != "" & Mod_Name != "" & Mod_Name != "Choose_Mod")
            {
                string Source_Path = Savegame_Path + @"Mod_Savegames\" + Mod_Name + @"\Save";

                if (Directory.Exists(Source_Path))
                {   string Target_Path = Savegame_Path + "Empire At War";
                    if (Game_Path == Game_Path_FOC) { Target_Path = Savegame_Path + "Empire At War - Forces of Corruption"; }
                   
                    Moving(Source_Path, Target_Path);
                }
               
            }
        }
        //=====================//     
        // Huge thanks for the Steam Functions to Kozuke Kamizu and Locutus, my teammates from the Star Gate Modding Group for allowing me using these functions of the SGMG_Launcher!! 


        Int32 EAW_Game_ID = 32470;
        // Int32 EAW_Game_ID = 243730; // Test Game  243730 or 215
        bool Config_File_Found = false;


        // Checks for Steam.exe, Checks Gamepaths for "Steam" and sets "Is Steam Version" UI Toggle to true and patches all Steam User start entries.
        void Check_for_Steam_Version()
        {
            RegistryKey Steam_Reg_Key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Classes\steam\Shell\Open\Command");
            if (Steam_Reg_Key == null) { Imperial_Console(600, 100, Add_Line + "    Error: Registry Key for Steam was not found."); return; }

            // Setting Global Variable
            Steam_Exe_Path = Steam_Reg_Key.GetValue("") as string;
            if (Steam_Exe_Path == null | Steam_Exe_Path == "") { Imperial_Console(600, 100, Add_Line + "    Value of Registry Key for Steam.exe was not found."); return; }

            // "C:\Program Files (x86)\Steam\steam.exe" "%1"
            try { Steam_Exe_Path = Steam_Exe_Path.Substring(1, Steam_Exe_Path.IndexOf('"', 1)).Replace('"', ' '); } catch {}

            if (!File.Exists(Steam_Exe_Path)) { Imperial_Console(600, 100, Add_Line + "    Error: Could not find the Steam.exe"); return; }
            // Otherwise Steam.exe was found and we set Steam Version to true if the path matches!
            else if (Regex.IsMatch(Game_Path_EAW, "(?i).*?" + "Steam.*?") | Regex.IsMatch(Game_Path_FOC, "(?i).*?" + "Steam.*?"))
            {
                if (Is_Steam_Version != "true")
                {
                    Toggle_Button(Switch_Button_Is_Steam_Version, "Button_On", "Button_Off", 0, true);
                    Save_Setting(Setting, "Is_Steam_Version", "true");

                    // Making hidden UI Elements for Steam users visible
                    Check_Box_Disable_Workshop.Visible = true;
                    Check_Box_Debug_Mode_Game.Visible = true;
                    Label_Log_File_Location.Visible = true;
                    Text_Box_Debug_Path.Visible = true;
                }
            }
            else if (!Regex.IsMatch(Game_Path_EAW, "(?i).*?" + "Steam.*?") & !Regex.IsMatch(Game_Path_FOC, "(?i).*?" + "Steam.*?"))
            {
                if (Is_Steam_Version != "false")
                {
                    Toggle_Button(Switch_Button_Is_Steam_Version, "Button_On", "Button_Off", 0, false);
                    Save_Setting(Setting, "Is_Steam_Version", "false");

                    Check_Box_Disable_Workshop.Visible = false;
                    Check_Box_Debug_Mode_Game.Visible = false;
                    Label_Log_File_Location.Visible = false;
                    Text_Box_Debug_Path.Visible = false;
                }
            }

            // Saving Steam Exe Path in any Case
            Save_Setting("2", "Steam_Exe_Path", "\"" + Steam_Exe_Path + "\"");
            Save_Setting("0", "Steam_Workshop_Path", Steam_Exe_Path.Remove(Steam_Exe_Path.Length - 10) + @"steamapps\workshop\content\32470\");
        }



        /*
        // Checks for Steam.exe, Checks Gamepaths for "Steam" and sets "Is Steam Version" UI Toggle to true and patches all Steam User start entries.
        void Update_Mod_Link_To_Steam() // Is currently inactive
        {
            RegistryKey Steam_Reg_Key = Registry.LocalMachine.OpenSubKey(@"SOFTWARE\Classes\steam\Shell\Open\Command");
            if (Steam_Reg_Key == null) { Imperial_Console(600, 100, Add_Line + "    Error: Registry Key for Steam was not found."); return; }
     
            // Setting Global Variable
            Steam_Exe_Path = Steam_Reg_Key.GetValue("") as string;
            if (Steam_Exe_Path == null | Steam_Exe_Path == "") { Imperial_Console(600, 100, Add_Line + "    Value of Registry Key for Steam.exe was not found."); return; }

            // "C:\Program Files (x86)\Steam\steam.exe" "%1"
            try { Steam_Exe_Path = Steam_Exe_Path.Substring(1, Steam_Exe_Path.IndexOf('"', 1)).Replace('"', ' '); } catch {}

            if (!File.Exists(Steam_Exe_Path)) { Imperial_Console(600, 100, Add_Line + "    Error: Could not find the Steam.exe"); return; }          
            // Otherwise Steam.exe was found and we set Steam Version to true if the path matches!
            else if (Game_Path_EAW.Contains("Steam") | Game_Path_FOC.Contains("Steam"))
            {
                if (Is_Steam_Version == "false") 
                {   Toggle_Button(Switch_Button_Is_Steam_Version, "Button_On", "Button_Off", 0, true);
                    Save_Setting(Setting, "Is_Steam_Version", "true");
                }
            }
            else if (!Game_Path_EAW.Contains("Steam") & !Game_Path_FOC.Contains("Steam"))
            {   
                if (Is_Steam_Version == "true")
                {   Toggle_Button(Switch_Button_Is_Steam_Version, "Button_On", "Button_Off", 0, false);
                    Save_Setting(Setting, "Is_Steam_Version", "false");
                }
            }

            // Saving Steam Exe Path in any Case
            Save_Setting("2", "Steam_Exe_Path", "\"" + Steam_Exe_Path + "\"");

            string User_Directory = Directory.GetParent(Steam_Exe_Path) + @"\userdata";
            if (!Directory.Exists(User_Directory)) { Imperial_Console(600, 100, Add_Line + "    No Steam User directory exists. Please log into Steam."); return; }


            Config_File_Found = false;

            foreach (var User_Folder in Directory.GetDirectories(User_Directory))
            {
                Modify_Config_File(User_Folder);             
            }

            if (!Config_File_Found) { Imperial_Console(600, 100, Add_Line + "    No Steam Config file exists. Please log into Steam."); }
        }


        //=====================//        


        void Modify_Config_File(string User_Directory) // Test Version of the Function below, trying to get it working
        {
             string Config_Path = User_Directory + @"\config\localconfig.vdf";
                if (File.Exists(Config_Path)) { Config_File_Found = true; }
                else { return; } // Otherwise this User_Directory is one without config file

                string Config = File.ReadAllText(Config_Path).Trim().Trim('\t');


                string Launch_Options_Template = "\"LaunchOptions\"" + ".*?\".*?\""; // = "LaunchOptions" " "
                string Updated_Launch_Options = "\"LaunchOptions\"\t\t\"" + Get_Launch_Paremeters() + "\"";

                string Open = Regex.Escape("{");
                string Close = Regex.Escape("}");


                Config = Config.Replace("\"" + EAW_Game_ID + "\"" + @"*{*}", "\"" + "Daaaaaaaaa" + "\"");

                // "(?<={)" + ".*?" + "(?=})"

               //  Config = Regex.Replace(Config, "\"" + EAW_Game_ID + "\"" + ".*?" + Open + ".*?" + Close, "\"" + "Daaaaaaaaa" + "\"");

            

             


                try
                {   // If the Config File doesen't contain the text we specified as Updated_Launch_Options             
                    if (!Regex.IsMatch(Config, Updated_Launch_Options))
                    {
                        Config = Regex.Replace(Config, "\"" + EAW_Game_ID + "\".*?" + "{(.*?)}", "\"" + EAW_Game_ID + "\".*?" + "{" + Updated_Launch_Options + "}");
                        
                        //Config = Regex.Replace(Config, "\"" + EAW_Game_ID + "\".*?" + "{.*?" + Launch_Options_Template + ".*?}", Updated_Launch_Options);
                    }

                 


                // var Apps_Index = Config.IndexOf("\"apps\"", 0);
                // string Apps = Config.Substring(Apps_Index, Config.IndexOf("\"DownloadRates\"") - Apps_Index);



                File.WriteAllText(Config_Path, Config, Encoding.Unicode);

            }
            catch
            {
                if (Debug_Mode == "true")
                {
                    Imperial_Console(600, 100, Add_Line + "    Failed to modify Steam Shortcut in "
                                             + Add_Line + "    " + User_Directory);
                }
            }
        }
        */
       
        
        /* This is the Original, it works but is outdated due to changes in the .vdf file from valves side
        
        void Modify_Config_File(string User_Directory)
        {   try
            {   string Config_Path = User_Directory + @"\config\localconfig.vdf";
                if (File.Exists(Config_Path)) { Config_File_Found = true; }
                else { return; } // Otherwise this User_Directory is one without config file

                string Config = File.ReadAllText(Config_Path).Trim().Trim('\t');


                var Software_Mark_Index = Config.IndexOf("\"Software\"", 0);
                string Software_Mark = "";
                Software_Mark = Config.Substring(Software_Mark_Index, Config.IndexOf("\"DownloadRates\"") - Software_Mark_Index);

                var Apps_Mark_Index = Software_Mark.IndexOf("\"apps\"", 0) + "\"apps\"".Count();
                var Opeining_Apps = Software_Mark.Substring(Apps_Mark_Index);
                int Closing_Tag_Index = Calculate_Closing_Tag(Software_Mark.Substring(Apps_Mark_Index));

                var Apps_Mark = Opeining_Apps.Substring(0, Closing_Tag_Index);
                var Index_Of_ID = Apps_Mark.IndexOf("\"" + EAW_Game_ID + "\"");

                if (Index_Of_ID == -1) { Imperial_Console(600, 100, Add_Line + "    Error: No Empire at War Gold installation found."); return; }


                int buff = Apps_Mark.Count();
                var Sub_Id_Mark = Apps_Mark.IndexOf('}', Index_Of_ID);
                var ID_Mark = Apps_Mark.Substring(Index_Of_ID, Sub_Id_Mark - Index_Of_ID + 1);
                var Original_ID_Mark = ID_Mark;


                // Editing Launch Option Parameters
                if (!ID_Mark.Contains("\"LaunchOptions\""))
                {
                    ID_Mark = ID_Mark.Insert(ID_Mark.LastIndexOf('"') + 1, "\r\n\t\t\t\t\t\t\"LaunchOptions\"		\"" + Get_Launch_Paremeters() + "\"");
                }
                else
                {
                    try
                    {
                        ID_Mark = Regex.Replace(ID_Mark, "\"LaunchOptions\"" + ".*?\".*?\"", "\"LaunchOptions\"\t\t\"" + Get_Launch_Paremeters() + "\"");
                    }
                    catch { }
                }

                // Patching Config File
                Config = Config.Replace(Original_ID_Mark, ID_Mark);

                var principal = new WindowsPrincipal(WindowsIdentity.GetCurrent());
                if (!principal.IsInRole(WindowsBuiltInRole.Administrator))
                {
                    if (!principal.IsInRole(WindowsBuiltInRole.Administrator))
                    {
                        Imperial_Console(600, 100, Add_Line + "    Imperialware has no admin rights to overwrite Steam Config file."
                                                 + Add_Line + "    Please rightclick Imperialware and start it as Administrator.");
                        return;
                    }
                }

                File.WriteAllText(Config_Path, Config, Encoding.Unicode);

            }             
            catch 
            {   
                if (Debug_Mode == "true") {Imperial_Console(600, 100, Add_Line + "    Failed to modify Steam Shortcut in "
                                                                    + Add_Line + "    " + User_Directory); } 
            }          
        }
        */
        
        //=====================//   
        private static int Calculate_Closing_Tag(string text)
        {
            int counter = 0;
            for (int i = 0; i < text.Count(); i++)
            {
                if (text.ElementAt(i) == '\t' || text.ElementAt(i) == '\n' || text.ElementAt(i) == '\r')
                    continue;
                if (text.ElementAt(i) == '{')
                    counter++;
                else if (text.ElementAt(i) == '}')
                    counter--;
                if (counter == 0)
                    return i;
            }
            return -1;
        }




        //=====================// 
     


        //=====================// 


        /*

      using Microsoft.Win32;
using System;
using System.Windows;

      
      /// Sucht das Steam-Fenster.
      ///<param name="name">Fenstername</param>
      private static string FindSteamWindow()
      {
          try
          {
              //Alle vorhandenen Fenster bestimmen
              ProcessListHelper.Windows windows = new ProcessListHelper.Windows();
              string titel = null;
              //Alle Fenster unstersuchen
              foreach (ProcessListHelper.Window w in windows.lstWindows)
              {   // If that Title contains Steam
                  if (w.winTitle.IndexOf("steam", StringComparison.InvariantCultureIgnoreCase) > -1)
                  {
                      titel = w.winTitle;
                      break;
                  }
              }
              return titel;
          }
          catch (Exception ex)
          {
              Logger.LogException(ex);
          }
          return null;
      }

           
      // Anderer Registry Pfad?
      public static string SteamExePath
      {
          get
          {
              try
              {
                  RegistryKey regKey = Registry.CurrentUser.CreateSubKey(@"Software\Valve\Steam", RegistryKeyPermissionCheck.ReadSubTree);
                  return regKey.GetValue("SteamExe", null).ToString();
              }
              catch (Exception ex)
              {
                  Logger.LogException(ex);
              }
              return null;
          }
      }
  
       */

        //=====================//
        string Get_Launch_Paremeters()
        {
          
            // Defining Arguments
            string Argument_1 = @"MODPATH=Mods\" + Mod_Name;
            
            // If all characters are numbers, that means we target a Workshop mod thus Argument 1 targets now the Workshop dir
            if (Mod_Name.All(char.IsDigit)) { Argument_1 = @"MODPATH=..\..\..\workshop\content\32470\" + Mod_Name; }


            // If the User checked the checkbox for playing Vanilla game only we just disable the Argument for selected Mod
            if (Play_Vanilla_Game == "true") { Argument_1 = null; }


            string Argument_2 = null;
            // Please be aware that we need that empty space between first and second argument or the game won't find the mod or language! 
            // If you try to Launch the game in a Language that doesen't exist as .dat file, the original EAW can crash,
            // So we give the User some control over this variable we use the Checkbox Check_Box_Use_Language and its value Use_Language


            string Speech_Path = @"Data\Audio\Speech\";
            string Text_Path = @"Data\Text\";

            if (Language_Mode == "Mod")
            {   Speech_Path = Mods_Directory + Mod_Name + @"\Data\Audio\Speech\";
                Text_Path = Mods_Directory + Mod_Name + @"\Data\Text\";
            }



            if (Use_Language == "true")
            {
                if (Evade_Language == "true")
                {
                    if (Directory.Exists(Game_Path + Speech_Path + Game_Language)) { Argument_2 = " LANGUAGE=" + Game_Language; }
                    // Then we check the Game Speech folders and cycle through all other Languages and try to evade to the right one
                    else if (Game_Language != "English" & Directory.Exists(Game_Path + Speech_Path + "English")) { Argument_2 = " LANGUAGE=English"; }
                    else if (Game_Language != "German" & Directory.Exists(Game_Path + Speech_Path + "German")) { Argument_2 = " LANGUAGE=German"; }
                    else if (Game_Language != "French" & Directory.Exists(Game_Path + Speech_Path + "French")) { Argument_2 = " LANGUAGE=French"; }
                    else if (Game_Language != "Spanish" & Directory.Exists(Game_Path + Speech_Path + "Spanish")) { Argument_2 = " LANGUAGE=Spanish"; }
                    else if (Game_Language != "Italian" & Directory.Exists(Game_Path + Speech_Path + "Italian")) { Argument_2 = " LANGUAGE=Italian"; }
                    else if (Game_Language != "Russian" & Directory.Exists(Game_Path + Speech_Path + "Russian")) { Argument_2 = " LANGUAGE=Russian"; }
                   
                    else // If no language directory was identified we are going to check the text .dat files
                    {   
                        if (File.Exists(Game_Path + Text_Path + "MasterTextFile_" + Game_Language + ".dat")) { Argument_2 = " LANGUAGE=" + Game_Language; }
                        // Then we check the Game .text files and cycle through all other Languages and try to evade to the right one
                        else if (Game_Language != "English" & File.Exists(Game_Path + Text_Path + "MasterTextFile_English.dat")) { Argument_2 = " LANGUAGE=English"; }
                        else if (Game_Language != "German" & File.Exists(Game_Path + Text_Path + "MasterTextFile_German.dat")) { Argument_2 = " LANGUAGE=German"; }
                        else if (Game_Language != "French" & File.Exists(Game_Path + Text_Path + "MasterTextFile_French.dat")) { Argument_2 = " LANGUAGE=French"; }
                        else if (Game_Language != "Italian" & File.Exists(Game_Path + Text_Path + "MasterTextFile_Italian.dat")) { Argument_2 = " LANGUAGE=Italian"; }
                        else if (Game_Language != "Spanish" & File.Exists(Game_Path + Text_Path + "MasterTextFile_Spanish.dat")) { Argument_2 = " LANGUAGE=Spanish"; }
                        else if (Game_Language != "Russian" & File.Exists(Game_Path + Text_Path + "MasterTextFile_Russian.dat")) { Argument_2 = " LANGUAGE=Russian"; }
                        else
                        {
                            Imperial_Console(600, 100, Add_Line + Data_Directory + @"Text\MasterTextFile_" + Game_Language + ".dat"
                                                     + Add_Line + " doesn't exist, please try a other language in Game Settings.");
                            return null;
                        }
                    }
                }
                // Otherwise we just use the language defined by the user
                else { Argument_2 = " LANGUAGE=" + Game_Language; }
            }


            // Attaching the additional option for "Windowed_Mode" to the second parameter
            if (Windowed_Mode == "true") { Argument_2 = Argument_2 + "- Windowed"; }


            // Finally returning what ever comes out as processed Parameters
            return Argument_1 + Argument_2;
        }


      



        //=====================//


        // MessageBox.Show(Tag.First().ToString());

        // Imperial_Console(600, 100, Add_Line + "Da");
        // Save_Setting(Setting, "Name", Name);
        // Load_Setting(Setting, "Value");
        // Toggle_Checkbox(Check_Box, "1", "Variable_Name");
        //======================================================== End of File  ====================================================    
    }

}
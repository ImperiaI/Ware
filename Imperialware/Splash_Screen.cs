﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

using System.IO;
using Microsoft.Win32;
using System.Text.RegularExpressions;



namespace Imperialware
{
    public partial class Splash_Screen : Form
    {
      
        public Splash_Screen()
        {
            InitializeComponent();
           
        }

        string Splash_Program_Dir = "";
        string Old_Program_Dir = "";  
        string Setting = "";

    
        // if (MainWindow.Loadscreen_Speed != null){}
        public static int Loading_Bar_Speed = 2;


        private void Splash_Screen_Load(object sender, EventArgs e)
        {
            Imperialware.MainWindow Imp = new Imperialware.MainWindow();
  
            try
            {   // Fetching Program Directories setting from Registry
                Splash_Program_Dir = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Imperialware",
                                        "Program_Directory", "").ToString();
            } catch { }

            try
            {   // Fetching Program Directories setting from Registry
                Old_Program_Dir = Registry.GetValue(@"HKEY_LOCAL_MACHINE\SOFTWARE\Imperialware",
                                        "Old_Program_Directory", "").ToString();
            } catch { }


            try
            {
                // Disabling Splash screen for Upgrade or Uninstall process
                if (Old_Program_Dir != "Delete_It" & Old_Program_Dir != "Upgrade")                 
                {
                    // Loading and assigning Variables from Settings.txt  
                    Setting = Splash_Program_Dir + "Settings_" + Environment.UserName + ".txt";

                    string Splash_Image = Imp.Load_Setting(Setting, "Selected_Theme") + "Splash_Screen.jpg";

                    if (File.Exists(Splash_Image)) { this.BackgroundImage = new Bitmap(Splash_Image); }
                    else { this.BackgroundImage = new Bitmap(Splash_Program_Dir + @"Themes\Default\Splash_Screen.jpg"); }

                    this.Size = new Size(BackgroundImage.Size.Width, BackgroundImage.Size.Height);
                }

                // ====== For different Window Scalings ======
                int DPI_X_Size = 0;

                // Getting Font scale setting of user
                using (Graphics gfx = this.CreateGraphics())
                {
                    DPI_X_Size = (int)gfx.DpiX;
                }

                // Depending on Windows Font scale setting of user, we adjust the progress bar:                
                if (DPI_X_Size == 120) { Progress_Bar_Splash.Location = new Point(this.Size.Width / 8, (this.Size.Height / 8) * 7); }
                else { Progress_Bar_Splash.Location = new Point(this.Size.Width / 9 * 2, (this.Size.Height / 8) * 7); } 
            }
            catch 
            {
                // this.BackColor = Color.FromArgb(250, 64, 64, 64);
                Loading_Bar_Speed = 1;
            }


            this.Opacity = 0.89;

            // This timer is used to raise the progress bar of loading
            Timer_Splash_Screen.Start();
        }


        // =================================
        // Increasing loading process - progress bar
        private void Timer_Splash_Screen_Tick(object sender, EventArgs e)
        {
            Progress_Bar_Splash.Increment(Loading_Bar_Speed);
            if (Progress_Bar_Splash.Value == 100)
            {
                Timer_Splash_Screen.Stop();
                this.Dispose(); // Backup for the dispose command in Program
            }
        }

        // ================================= End of File =====================================


    }
}
